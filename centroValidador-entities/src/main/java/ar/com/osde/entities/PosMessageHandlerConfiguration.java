package ar.com.osde.entities;

import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.osde.framework.entities.FrameworkEntity;

/*
 * Clase base para la persistencia de las configuracion del los handlers
 * @author MT27789605
 *
 */
@XmlSeeAlso({ PosMessageHandlerInterceptorConfiguration.class, AbstractResponseMessageHandlerConfiguration.class,
        FilteredMessageHandlerConfiguration.class, MigracionMessageHandlerConfiguration.class })
public class PosMessageHandlerConfiguration extends AbstractCleanable{

    private static final long serialVersionUID = -6998591099813064167L;

    private String className;

    public PosMessageHandlerConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        PosMessageHandlerConfiguration messageHandlerConfiguration = new PosMessageHandlerConfiguration();
        this.copyValues(messageHandlerConfiguration);
        return messageHandlerConfiguration;
    }

    protected void copyValues(PosMessageHandlerConfiguration messageHandlerConfiguration) throws CloneNotSupportedException {
        messageHandlerConfiguration.setId(this.getId());
        messageHandlerConfiguration.className = className;
    }

   
    
    /**
     * Nombre de la clase a instanciar
     * @return
     */
    public String getClassName() {
        return className;
    }

    /**
     * Setter del nombre de la clase a instanciar
     * @param className
     */
    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o ) {
            return true;
        }
        if (!(o instanceof PosMessageHandlerConfiguration)) {
            return false;
        }

        PosMessageHandlerConfiguration that = (PosMessageHandlerConfiguration) o;

        if (getId() != that.getId()) {
            return false;
        }
        if (className != null ? !className.equals(that.className) : that.className != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId()>>> 32));
        result = 31 * result + (className != null ? className.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PosMessageHandlerConfiguration{" + "id=" + getId() + ", className='" + className + '\'' + '}';
    }
}
