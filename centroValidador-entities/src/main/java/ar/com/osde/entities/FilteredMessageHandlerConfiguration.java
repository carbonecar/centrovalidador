package ar.com.osde.entities;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;

/**
 * Entidad para guardar la configuracion de un filtro
 * 
 * @author MT27789605
 * 
 */
public class FilteredMessageHandlerConfiguration extends PosMessageHandlerInterceptorConfiguration {

    private static final long serialVersionUID = 7092659878750139346L;

    private List<FilterConfiguration> filters = new ArrayList<FilterConfiguration>();
    private List<FilterStrategyConfiguration> filterStrategyList = new ArrayList<FilterStrategyConfiguration>();

    public FilteredMessageHandlerConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        FilteredMessageHandlerConfiguration filteredMessageHandlerConfiguration = new FilteredMessageHandlerConfiguration();
        this.copyValues(filteredMessageHandlerConfiguration);
        return filteredMessageHandlerConfiguration;
    }

    @Override
    protected void copyValues(PosMessageHandlerConfiguration messageHandlerConfiguration) throws CloneNotSupportedException {
        super.copyValues(messageHandlerConfiguration);
        List<FilterConfiguration> listFilterConfigurations = new ArrayList<FilterConfiguration>();
        if (this.filters != null) {
            for (FilterConfiguration filter : this.filters) {
                listFilterConfigurations.add((FilterConfiguration) filter.clone());
            }
        }
        ((FilteredMessageHandlerConfiguration) messageHandlerConfiguration).setFilters(listFilterConfigurations);
        List<FilterStrategyConfiguration> listStrategy = new ArrayList<FilterStrategyConfiguration>();
        if (this.filterStrategyList != null) {
            for (FilterStrategyConfiguration filterStrategy : filterStrategyList) {
                listStrategy.add((FilterStrategyConfiguration) filterStrategy.clone());
            }
        }
        ((FilteredMessageHandlerConfiguration) messageHandlerConfiguration).setFilterStrategyList(listStrategy);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration#cleanToSave
     * ()
     */
    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.filters != null) {
            for (FilterConfiguration filter : this.filters) {
                filter.cleanToSave();
            }
        }
        if (this.filterStrategyList != null) {
            for (FilterStrategyConfiguration filterStrategy : filterStrategyList) {
                filterStrategy.cleanToSave();
            }
        }
    }

    /**
     * Lista de filtros internos (and, or, ==, not)
     */
    public List<FilterConfiguration> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterConfiguration> filters) {
        this.filters = filters;
    }

    public List<FilterStrategyConfiguration> getFilterStrategyList() {
        return filterStrategyList;
    }

    public void setFilterStrategyList(List<FilterStrategyConfiguration> filterStrategyList) {
        this.filterStrategyList = filterStrategyList;
    }

    @Override
    public String toString() {
        return "FilteredMessageHandlerConfiguration{" + "filters=" + filters + ", filterStrategyList=" + filterStrategyList
                + super.toString() + "} ";
    }
}
