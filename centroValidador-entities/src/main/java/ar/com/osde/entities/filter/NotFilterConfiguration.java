package ar.com.osde.entities.filter;

/**
 * Entity for NotFilter.
 */
public class NotFilterConfiguration extends FilterConfiguration {

    private static final long serialVersionUID = -4678903045636313392L;

    private FilterConfiguration filterConfiguration;

    public NotFilterConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.filter.FilterConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        NotFilterConfiguration not = new NotFilterConfiguration();
        this.copyValues(not);
        return not;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.FilterConfiguration#copyValues(ar.com.osde
     * .entities.filter.FilterConfiguration)
     */
    @Override
    protected void copyValues(FilterConfiguration filterConfiguration) throws CloneNotSupportedException {
        super.copyValues(filterConfiguration);
        if (this.filterConfiguration != null) {
            ((NotFilterConfiguration) filterConfiguration).filterConfiguration = (FilterConfiguration) this.filterConfiguration.clone();
        }
    }

    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.filterConfiguration != null) {
            this.filterConfiguration.cleanToSave();
        }
    }

    @Override
    public void agregarHijo(FilterConfiguration filterConfiguration) {
        this.filterConfiguration = filterConfiguration;
    }

    @Override
    public int cantidadHijosNecesarios() {
        return 1;
    }

    public FilterConfiguration getFilterConfiguration() {
        return filterConfiguration;
    }

    public void setFilterConfiguration(FilterConfiguration filterConfiguration) {
        this.filterConfiguration = filterConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotFilterConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        NotFilterConfiguration that = (NotFilterConfiguration) o;

        if (filterConfiguration != null ? !filterConfiguration.equals(that.filterConfiguration) : that.filterConfiguration != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (filterConfiguration != null ? filterConfiguration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NotFilterConfiguration{" + "filterConfiguration=" + filterConfiguration + super.toString() + "} ";
    }
}
