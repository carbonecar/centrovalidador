package ar.com.osde.entities;

import java.util.HashSet;
import java.util.Set;

/**
 * Configuracion del handler de migracion
 * 
 * @author MT27789605
 * 
 */
public class MigracionMessageHandlerConfiguration extends PosMessageHandlerConfiguration {

    private static final long serialVersionUID = -1L;

    private Set<CodigoExceptuable> codigosExceptuar;

    public MigracionMessageHandlerConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.PosMessageHandlerConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        MigracionMessageHandlerConfiguration migracion = new MigracionMessageHandlerConfiguration();
        this.copyValues(migracion);
        return migracion;
    }

    @Override
    protected void copyValues(PosMessageHandlerConfiguration messageHandlerConfiguration) throws CloneNotSupportedException {
        super.copyValues(messageHandlerConfiguration);
        Set<CodigoExceptuable> set = new HashSet<CodigoExceptuable>();
        if (this.codigosExceptuar != null) {
            for (CodigoExceptuable codigo : this.codigosExceptuar) {
                set.add((CodigoExceptuable) codigo.clone());
            }
        }
        ((MigracionMessageHandlerConfiguration) messageHandlerConfiguration).setCodigosExceptuar(set);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.PosMessageHandlerConfiguration#cleanToSave()
     */
    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.codigosExceptuar != null) {
            for (CodigoExceptuable codigo : this.codigosExceptuar) {
                codigo.cleanToSave();
            }
        }
    }

    /**
     * Lista de c�digos a exceptuar.
     * 
     * @return
     */
    public Set<CodigoExceptuable> getCodigosExceptuar() {
        return codigosExceptuar;
    }

    public void setCodigosExceptuar(Set<CodigoExceptuable> codigosExceptuar) {
        this.codigosExceptuar = codigosExceptuar;
    }

    @Override
    public String toString() {
        return "MigracionMessageHandlerConfiguration{" + "codigosExceptuar=" + codigosExceptuar + super.toString() + "} ";
    }
}
