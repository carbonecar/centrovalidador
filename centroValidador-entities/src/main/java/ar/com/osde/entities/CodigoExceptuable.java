package ar.com.osde.entities;

import ar.com.osde.framework.entities.FrameworkEntity;

/**
 * Entidad que represetna un codigo descriptcion
 * 
 * @author MT27789605
 *
 */
public class CodigoExceptuable implements FrameworkEntity, Cleanable, Cloneable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String descripcion;
	private int codigo;

	public CodigoExceptuable() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		CodigoExceptuable codigoExceptuable = new CodigoExceptuable();
		this.copyValues(codigoExceptuable);
		return codigoExceptuable;
	}

	protected void copyValues(CodigoExceptuable codigoExceptuable) {
		codigoExceptuable.id = this.id;
		codigoExceptuable.codigo = this.codigo;
		codigoExceptuable.descripcion = this.descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ar.com.osde.entities.Cleanable#cleanToSave()
	 */
	public void cleanToSave() {
		// No cambia nada porque no se crean nuevos sino solo se tienen
		// referencias a los existentes
	}

	/**
	 * Id De persistencia
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * Seter para el id de persistencia
	 */
	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Descripcion del codigo
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Valor del codigo
	 * 
	 * @return
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * Valor del codigo
	 * 
	 * @param codigo
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null) {
			return false;
		}

		if (o.getClass() != this.getClass()) {
			return false;
		}

		CodigoExceptuable that = (CodigoExceptuable) o;

		if (codigo != that.codigo) {
			return false;
		}
		if (id != that.id) {
			return false;
		}
		if (descripcion == null && that.descripcion != null || that.descripcion == null && this.descripcion != null) {
			return false;
		} else {
			if (descripcion == null) {
				return that.descripcion.equals(this.descripcion);
			}
			return this.descripcion.equals(that.descripcion);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
		result = 31 * result + codigo;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CodigoExceptuable{" + "id=" + id + ", descripcion='" + descripcion + '\'' + ", codigo=" + codigo + '}';
	}
}
