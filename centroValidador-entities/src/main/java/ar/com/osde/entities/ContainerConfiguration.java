package ar.com.osde.entities;

import org.apache.commons.lang.StringUtils;

import ar.com.osde.framework.entities.FrameworkEntity;

/**
 * Entidad para encapsular la configuracion de un container. Un container es la
 * clase de spring @see DefaultMessageListenerContainer que se encarga de
 * escuchar los mensajes del pos
 * 
 * @author MT27789605
 * 
 */
public class ContainerConfiguration implements FrameworkEntity, Cleanable, Cloneable {

    private static final long serialVersionUID = 5760762257826581552L;

    private long id;
    private String containerDescription;
    private int numberOfConcurrentConsumers;
    private String queueName;
    private PosMessageHandlerConfiguration posMessageHandler;
    private ErrorHandlerConfiguration errorHandler;
    private boolean startOnLoad;
    private boolean running;
    private boolean acceptMessageWhileStoping = true;
    private String connectionFactoryName;
    private String groupName;

    public ContainerConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        this.copyValues(containerConfiguration);
        return containerConfiguration;
    }

    protected void copyValues(ContainerConfiguration containerConfiguration) throws CloneNotSupportedException {
        containerConfiguration.id = this.id;
        containerConfiguration.containerDescription = this.containerDescription;
        containerConfiguration.numberOfConcurrentConsumers = this.numberOfConcurrentConsumers;
        containerConfiguration.queueName = this.queueName;
        containerConfiguration.startOnLoad = this.startOnLoad;
        containerConfiguration.running = this.running;
        containerConfiguration.acceptMessageWhileStoping = this.acceptMessageWhileStoping;
        containerConfiguration.connectionFactoryName = this.connectionFactoryName;
        containerConfiguration.groupName = this.groupName;
        if (this.posMessageHandler != null) {
            containerConfiguration.posMessageHandler = (PosMessageHandlerConfiguration) posMessageHandler.clone();

        }
        if (this.errorHandler != null) {
            containerConfiguration.errorHandler = (ErrorHandlerConfiguration) errorHandler.clone();
        }
    }

    public void cleanToSave() {
        this.id = 0;
        if (this.posMessageHandler != null) {
            this.posMessageHandler.cleanToSave();
        }
        if (this.errorHandler != null) {
            this.errorHandler.cleanToSave();
        }
    }

    public long getId() {
        return id;
    }

    public String getContainerDescription() {
        return containerDescription;
    }

    public void setContainerDescription(String containerDescription) {
        this.containerDescription = containerDescription;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public int getNumberOfConcurrentConsumers() {
        return numberOfConcurrentConsumers;
    }

    public boolean isAcceptMessageWhileStoping() {
        return acceptMessageWhileStoping;
    }

    public void setAcceptMessageWhileStoping(boolean acceptMessageWhileStoping) {
        this.acceptMessageWhileStoping = acceptMessageWhileStoping;
    }

    public void setNumberOfConcurrentConsumers(int numberOfConcurrentConsumers) {
        this.numberOfConcurrentConsumers = numberOfConcurrentConsumers;
    }

    public PosMessageHandlerConfiguration getPosMessageHandler() {
        return posMessageHandler;
    }

    public void setPosMessageHandler(PosMessageHandlerConfiguration posMessageHandler) {
        this.posMessageHandler = posMessageHandler;
    }

    public ErrorHandlerConfiguration getErrorHandler() {
        return errorHandler;
    }

    public void setErrorHandler(ErrorHandlerConfiguration errorHandler) {
        this.errorHandler = errorHandler;
    }

    public boolean isStartOnLoad() {
        return startOnLoad;
    }

    public void setStartOnLoad(boolean startOnLoad) {
        this.startOnLoad = startOnLoad;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContainerConfiguration)) {
            return false;
        }

        ContainerConfiguration that = (ContainerConfiguration) o;

        if (acceptMessageWhileStoping != that.acceptMessageWhileStoping) {
            return false;
        }
        if (id != that.id) {
            return false;
        }
        if (running != that.running) {
            return false;
        }
        if (numberOfConcurrentConsumers != that.numberOfConcurrentConsumers) {
            return false;
        }
        if (startOnLoad != that.startOnLoad) {
            return false;
        }

        if (!StringUtils.equals(connectionFactoryName, that.connectionFactoryName)) {
            return false;
        }
        if (!StringUtils.equals(containerDescription, that.containerDescription)) {
            return false;
        }
        
      
        if (!propertyEquals(errorHandler,that.errorHandler)) {
            return false;
        }
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) {
            return false;
        }
        if (posMessageHandler != null ? !posMessageHandler.equals(that.posMessageHandler) : that.posMessageHandler != null) {
            return false;
        }
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (containerDescription != null ? containerDescription.hashCode() : 0);
        result = 31 * result + numberOfConcurrentConsumers;
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (posMessageHandler != null ? posMessageHandler.hashCode() : 0);
        result = 31 * result + (errorHandler != null ? errorHandler.hashCode() : 0);
        result = 31 * result + (startOnLoad ? 1 : 0);
        result = 31 * result + (running ? 1 : 0);
        result = 31 * result + (acceptMessageWhileStoping ? 1 : 0);
        result = 31 * result + (connectionFactoryName != null ? connectionFactoryName.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ContainerConfiguration{" + "id=" + id + ", containerDescription='" + containerDescription + '\''
                + ", numberOfConcurrentConsumers=" + numberOfConcurrentConsumers + ", queueName='" + queueName + '\''
                + ", posMessageHandler=" + posMessageHandler + ", errorHandler=" + errorHandler + ", startOnLoad=" + startOnLoad
                + ", isRunning=" + running + ", acceptMessageWhileStoping=" + acceptMessageWhileStoping + ", connectionFactoryName='"
                + connectionFactoryName + '\'' + ", groupName='" + groupName + '\'' + '}';
    }
    
    private boolean propertyEquals(Object o1,Object o2){
    	 return  ((o1 == null) ? false : (o2 == null) ? true : o1.equals(o2));
    }
}
