package ar.com.osde.entities.filter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Entity for AndFilter and OrFilter.
 */
public class CompositeFilterConfiguration extends FilterConfiguration {

    private static final long serialVersionUID = 7155373095067924345L;

    private List<FilterConfiguration> filterConfigurationList = new LinkedList<FilterConfiguration>();

    public CompositeFilterConfiguration() {

    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.filter.FilterConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        CompositeFilterConfiguration compositeFilterConfiguration = new CompositeFilterConfiguration();
        this.copyValues(compositeFilterConfiguration);
        return compositeFilterConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.FilterConfiguration#copyValues(ar.com.osde
     * .entities.filter.FilterConfiguration)
     */
    @Override
    protected void copyValues(FilterConfiguration filterConfiguration) throws CloneNotSupportedException {
        super.copyValues(filterConfiguration);
        List<FilterConfiguration> list = new ArrayList<FilterConfiguration>();
        if (this.filterConfigurationList != null) {
            for (FilterConfiguration filter : filterConfigurationList) {
                list.add((FilterConfiguration) filter.clone());
            }
        }
        ((CompositeFilterConfiguration) filterConfiguration).setFilterConfigurationList(list);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.filter.FilterConfiguration#cleanToSave()
     */
    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.filterConfigurationList != null) {
            for (FilterConfiguration filter : this.filterConfigurationList) {
                filter.cleanToSave();
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.FilterConfiguration#agregarHijo(ar.com.osde
     * .entities.filter.FilterConfiguration)
     */
    @Override
    public void agregarHijo(FilterConfiguration filterConfiguration) {
        this.filterConfigurationList.add(filterConfiguration);
    }

    @Override
    public int cantidadHijosNecesarios() {
        return 2;
    }

    public List<FilterConfiguration> getFilterConfigurationList() {
        return filterConfigurationList;
    }

    public void setFilterConfigurationList(List<FilterConfiguration> filterConfigurationList) {
        this.filterConfigurationList = filterConfigurationList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompositeFilterConfiguration)) {
            return false;
        }

        CompositeFilterConfiguration that = (CompositeFilterConfiguration) o;

        if (filterConfigurationList != null ? !filterConfigurationList.containsAll(that.filterConfigurationList)
                : that.filterConfigurationList != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return filterConfigurationList != null ? filterConfigurationList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "CompositeFilterConfiguration{" + "filterConfigurationList=" + filterConfigurationList + super.toString() + "} ";
    }
}
