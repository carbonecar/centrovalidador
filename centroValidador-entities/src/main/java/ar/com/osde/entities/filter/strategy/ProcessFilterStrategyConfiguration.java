package ar.com.osde.entities.filter.strategy;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Entity for ProcessStrategy.
 */
public class ProcessFilterStrategyConfiguration extends FilterStrategyConfiguration {

    private static final long serialVersionUID = -1123587019732068626L;

    private PosMessageHandlerConfiguration posMessageHandler;

    @Override
    public Object clone() throws CloneNotSupportedException {
        ProcessFilterStrategyConfiguration processFilterStrategyConfiguration = new ProcessFilterStrategyConfiguration();
        this.copyValues(processFilterStrategyConfiguration);
        return processFilterStrategyConfiguration;
    }

    @Override
    protected void copyValues(FilterStrategyConfiguration filterStrategyConfiguration) throws CloneNotSupportedException {
        super.copyValues(filterStrategyConfiguration);
        if (this.posMessageHandler != null) {
            ((ProcessFilterStrategyConfiguration) filterStrategyConfiguration).posMessageHandler = (PosMessageHandlerConfiguration) this.posMessageHandler
                    .clone();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration#cleanToSave
     * ()
     */
    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.posMessageHandler != null) {
            this.posMessageHandler.cleanToSave();
        }
    }

    public PosMessageHandlerConfiguration getPosMessageHandler() {
        return posMessageHandler;
    }

    public void setPosMessageHandler(PosMessageHandlerConfiguration posMessageHandler) {
        this.posMessageHandler = posMessageHandler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProcessFilterStrategyConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        ProcessFilterStrategyConfiguration that = (ProcessFilterStrategyConfiguration) o;

        if (posMessageHandler != null ? !posMessageHandler.equals(that.posMessageHandler) : that.posMessageHandler != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (posMessageHandler != null ? posMessageHandler.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProcessFilterStrategyConfiguration{" + "posMessageHandler=" + posMessageHandler + super.toString() + "} ";
    }
}
