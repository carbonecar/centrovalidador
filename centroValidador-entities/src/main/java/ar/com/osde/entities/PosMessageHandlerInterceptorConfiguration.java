package ar.com.osde.entities;

/**
 * Configuarcion de un interceptor de un handler
 * 
 * @author MT27789605
 * 
 */
public class PosMessageHandlerInterceptorConfiguration extends PosMessageHandlerConfiguration {

    private static final long serialVersionUID = -1415256802642762139L;

    private PosMessageHandlerConfiguration posMessageHandler;

    public PosMessageHandlerInterceptorConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.PosMessageHandlerConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        PosMessageHandlerInterceptorConfiguration pos = new PosMessageHandlerInterceptorConfiguration();
        this.copyValues(pos);
        return pos;
    }

    @Override
    protected void copyValues(PosMessageHandlerConfiguration messageHandlerConfiguration) throws CloneNotSupportedException {
        super.copyValues(messageHandlerConfiguration);
        if (posMessageHandler != null) {
            ((PosMessageHandlerInterceptorConfiguration) messageHandlerConfiguration).posMessageHandler = (PosMessageHandlerConfiguration) this.posMessageHandler
                    .clone();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.PosMessageHandlerConfiguration#cleanToSave()
     */
    @Override
    public void cleanToSave() {
        super.cleanToSave();
        if (this.posMessageHandler != null) {
            this.posMessageHandler.cleanToSave();
        }
    }

    public PosMessageHandlerConfiguration getPosMessageHandler() {
        return posMessageHandler;
    }

    public void setPosMessageHandler(PosMessageHandlerConfiguration posMessageHandler) {
        this.posMessageHandler = posMessageHandler;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.PosMessageHandlerConfiguration#equals(java.lang.
     * Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PosMessageHandlerInterceptorConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PosMessageHandlerInterceptorConfiguration that = (PosMessageHandlerInterceptorConfiguration) o;

        if (posMessageHandler != null ? !posMessageHandler.equals(that.posMessageHandler) : that.posMessageHandler != null) {
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.PosMessageHandlerConfiguration#hashCode()
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (posMessageHandler != null ? posMessageHandler.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PosMessageHandlerInterceptorConfiguration{" + "posMessageHandler=" + posMessageHandler + super.toString() + "} ";
    }
}
