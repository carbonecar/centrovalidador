package ar.com.osde.entities.filter.strategy;

/**
 * Entity for RouteFilterStrategy.
 */
public class RouteFilterStrategyConfiguration extends FilterStrategyConfiguration {

    private static final long serialVersionUID = 8745567231968727152L;

    private String queueName;
    private String connectionFactoryName;

    public RouteFilterStrategyConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        RouteFilterStrategyConfiguration routeFilterStrategyConfiguration = new RouteFilterStrategyConfiguration();
        this.copyValues(routeFilterStrategyConfiguration);
        return routeFilterStrategyConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration#copyValues
     * (ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration)
     */
    @Override
    protected void copyValues(FilterStrategyConfiguration filterStrategyConfiguration) throws CloneNotSupportedException {
        super.copyValues(filterStrategyConfiguration);
        ((RouteFilterStrategyConfiguration) filterStrategyConfiguration).queueName = this.queueName;
        ((RouteFilterStrategyConfiguration) filterStrategyConfiguration).connectionFactoryName = this.connectionFactoryName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RouteFilterStrategyConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        RouteFilterStrategyConfiguration that = (RouteFilterStrategyConfiguration) o;

        if (connectionFactoryName != null ? !connectionFactoryName.equals(that.connectionFactoryName) : that.connectionFactoryName != null) {
            return false;
        }
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (connectionFactoryName != null ? connectionFactoryName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RouteFilterStrategyConfiguration{" + "queueName='" + queueName + '\'' + ", connectionFactoryName='" + connectionFactoryName
                + '\'' + super.toString() + "} ";
    }
}
