package ar.com.osde.entities;

import ar.com.osde.framework.entities.FrameworkEntity;

/**
 * Clase para persisitr la configuracion del handler de error
 * 
 * @author MT27789605
 * 
 */
public class ErrorHandlerConfiguration implements FrameworkEntity, Cleanable, Cloneable {

    private static final long serialVersionUID = 6901962374327365862L;

    private long id;
    private String queueName;
    private String timeOutQueueName;
    private String connectionFactoryName;

    public ErrorHandlerConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        ErrorHandlerConfiguration errorHandlerConfiguration = new ErrorHandlerConfiguration();
        this.copyValues(errorHandlerConfiguration);
        return errorHandlerConfiguration;
    }

    protected void copyValues(ErrorHandlerConfiguration errorHandlerConfiguration) {
        errorHandlerConfiguration.id = this.id;
        errorHandlerConfiguration.queueName = this.queueName;
        errorHandlerConfiguration.timeOutQueueName = this.timeOutQueueName;
        errorHandlerConfiguration.connectionFactoryName = this.connectionFactoryName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.Cleanable#cleanToSave()
     */
    public void cleanToSave() {
        this.id = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getTimeOutQueueName() {
        return timeOutQueueName;
    }

    /**
     * Setea la cola donde se envian los errores al recibirse un timeout
     * 
     * @param timeOutQueueName
     */
    public void setTimeOutQueueName(String timeOutQueueName) {
        this.timeOutQueueName = timeOutQueueName;
    }

    /**
     * Nombre JNDI de la connectionFactory
     * 
     * @return
     */
    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ErrorHandlerConfiguration)) {
            return false;
        }

        ErrorHandlerConfiguration that = (ErrorHandlerConfiguration) o;

        if (id != that.id) {
            return false;
        }
        if (connectionFactoryName != null ? !connectionFactoryName.equals(that.connectionFactoryName) : that.connectionFactoryName != null) {
            return false;
        }
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) {
            return false;
        }
        if (timeOutQueueName != null ? !timeOutQueueName.equals(that.timeOutQueueName) : that.timeOutQueueName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (timeOutQueueName != null ? timeOutQueueName.hashCode() : 0);
        result = 31 * result + (connectionFactoryName != null ? connectionFactoryName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ErrorHandlerConfiguration{" + "id=" + id + ", queueName='" + queueName + '\'' + ", timeOutQueueName='" + timeOutQueueName
                + '\'' + ", connectionFactoryName='" + connectionFactoryName + '\'' + '}';
    }
}
