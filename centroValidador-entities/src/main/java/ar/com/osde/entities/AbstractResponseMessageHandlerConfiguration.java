package ar.com.osde.entities;

/**
 * Entity for AlwaysRouteMessageHandler y StreamResponseMessageHandler.
 */
public class AbstractResponseMessageHandlerConfiguration extends PosMessageHandlerInterceptorConfiguration {

    private static final long serialVersionUID = 7478869354107376583L;
    private String queueName;
    private String connectionFactoryName;

    public AbstractResponseMessageHandlerConfiguration(){
        super();
    }
    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        AbstractResponseMessageHandlerConfiguration abstractResponseMessageHandlerConfiguration = new AbstractResponseMessageHandlerConfiguration();
        this.copyValues(abstractResponseMessageHandlerConfiguration);
        return abstractResponseMessageHandlerConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration#copyValues
     * (ar.com.osde.entities.PosMessageHandlerConfiguration)
     */
    @Override
    protected void copyValues(PosMessageHandlerConfiguration messageHandlerConfiguration) throws CloneNotSupportedException {
        super.copyValues(messageHandlerConfiguration);
        ((AbstractResponseMessageHandlerConfiguration) messageHandlerConfiguration).queueName = this.queueName;
        ((AbstractResponseMessageHandlerConfiguration) messageHandlerConfiguration).connectionFactoryName = this.connectionFactoryName;

    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractResponseMessageHandlerConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        AbstractResponseMessageHandlerConfiguration that = (AbstractResponseMessageHandlerConfiguration) o;

        if (connectionFactoryName != null ? !connectionFactoryName.equals(that.connectionFactoryName) : that.connectionFactoryName != null) {
            return false;
        }
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (connectionFactoryName != null ? connectionFactoryName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AbstractResponseMessageHandlerConfiguration{" + "queueName='" + queueName + '\'' + ", connectionFactoryName='"
                + connectionFactoryName + '\'' + super.toString() + "} ";
    }
}
