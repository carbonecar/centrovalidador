package ar.com.osde.entities;

import java.io.Serializable;

/**
 * 
 * @author MT27789605
 *
 */
public class QueueMessage implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String message;
    private String id;

    public QueueMessage() {
    }

    public QueueMessage(String id, String message) {
        this.setId(id);
        this.setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "QueueMessage{" +
                "message='" + message + '\'' +
                ", id=" + id +
                '}';
    }
}

