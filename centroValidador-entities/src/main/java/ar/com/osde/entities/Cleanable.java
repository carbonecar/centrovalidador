package ar.com.osde.entities;

/**
 * Interfaz para cuando un objeto se copia/clona que se borren los ids para que pueda ser guardado
 * como un nuevo objeto
 * */
public interface Cleanable {

    /**
     * Limpia el objeto de manera de dejarlo como "nuevo" para ser persitido.
     */
    void cleanToSave();

}
