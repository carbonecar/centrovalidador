package ar.com.osde.entities.filter;

import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.osde.entities.AbstractCleanable;

/**
 * Entity for NullFilter.
 */
@XmlSeeAlso({ CompositeFilterConfiguration.class, NotFilterConfiguration.class, PropertyEqualFilterConfiguration.class })
public class FilterConfiguration extends AbstractCleanable {

    private static final long serialVersionUID = -4442373373358060079L;

    // TODO ver si esta clase la podemos hacer abstracta y esta propiedad que la
    // setee la misma subclase.
    private String className;

    public FilterConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        FilterConfiguration filterConfiguration = new FilterConfiguration();
        this.copyValues(filterConfiguration);
        return filterConfiguration;
    }

    protected void copyValues(FilterConfiguration filterConfiguration) throws CloneNotSupportedException {
        filterConfiguration.setId(this.getId());
        filterConfiguration.className = this.className;
    }

    public void agregarHijo(FilterConfiguration filterConfiguration) {

    }

    public int cantidadHijosNecesarios() {
        return 0;
    }

  
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FilterConfiguration)) {
            return false;
        }

        FilterConfiguration that = (FilterConfiguration) o;

        if (getId() != that.getId()) {
            return false;
        }
        
        if (className != null ? !className.equals(that.className) : that.className != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (className != null ? className.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FilterConfiguration{" + "id=" + getId() + ", className='" + className + '\'' + '}';
    }
}
