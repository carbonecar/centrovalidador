package ar.com.osde.entities;

import ar.com.osde.framework.entities.FrameworkEntity;

/**
 * Implementacion abstracta comnun a todos los objetos clenable
 * @author MT27789605
 *
 */
public abstract class AbstractCleanable implements FrameworkEntity, Cleanable, Cloneable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private long id;
    
    public void cleanToSave() {
        this.id = 0;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
