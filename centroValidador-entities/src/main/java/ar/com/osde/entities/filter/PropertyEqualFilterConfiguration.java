package ar.com.osde.entities.filter;

/**
 * Entity for PropertyEqualFilter.
 */
public class PropertyEqualFilterConfiguration extends FilterConfiguration {

    private static final long serialVersionUID = -1410834500794896984L;

    private String propertyName;
    private String value;
    private String type;

    public PropertyEqualFilterConfiguration() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.entities.filter.FilterConfiguration#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        PropertyEqualFilterConfiguration propertyEqualFilterConfiguration = new PropertyEqualFilterConfiguration();
        this.copyValues(propertyEqualFilterConfiguration);
        return propertyEqualFilterConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.filter.FilterConfiguration#copyValues(ar.com.osde
     * .entities.filter.FilterConfiguration)
     */
    @Override
    protected void copyValues(FilterConfiguration filterConfiguration) throws CloneNotSupportedException {
        super.copyValues(filterConfiguration);
        ((PropertyEqualFilterConfiguration) filterConfiguration).type = this.type;
        ((PropertyEqualFilterConfiguration) filterConfiguration).propertyName = this.propertyName;
        ((PropertyEqualFilterConfiguration) filterConfiguration).value = this.value;
    }

    public String getValue() {
        return value;
    }

    public Object getRealValue() {

        if ("Integer".equals(getType())) {
            return Integer.parseInt(value);
        }

        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PropertyEqualFilterConfiguration)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PropertyEqualFilterConfiguration that = (PropertyEqualFilterConfiguration) o;

        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (propertyName != null ? propertyName.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PropertyEqualFilterConfiguration{" + "propertyName='" + propertyName + '\'' + ", value='" + value + '\'' + ", type='"
                + type + '\'' + super.toString() + "} ";
    }
}
