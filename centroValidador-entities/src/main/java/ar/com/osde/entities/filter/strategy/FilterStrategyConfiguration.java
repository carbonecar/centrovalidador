package ar.com.osde.entities.filter.strategy;

import javax.xml.bind.annotation.XmlSeeAlso;

import ar.com.osde.entities.AbstractCleanable;

/**
 * Entity for NullFilterStrategy.
 */
@XmlSeeAlso({ ProcessFilterStrategyConfiguration.class, RouteFilterStrategyConfiguration.class })
public class FilterStrategyConfiguration extends AbstractCleanable{

    private static final long serialVersionUID = 7125014297715225891L;


    /**
     * Clonado de la clase
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        FilterStrategyConfiguration filterStrategyConfiguration = new FilterStrategyConfiguration();
        this.copyValues(filterStrategyConfiguration);
        return filterStrategyConfiguration;
    }

    protected void copyValues(FilterStrategyConfiguration filterStrategyConfiguration) throws CloneNotSupportedException {
        filterStrategyConfiguration.setId(this.getId());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
       
        if (!(o instanceof FilterStrategyConfiguration )) {
            return false;
        }

        AbstractCleanable that = (AbstractCleanable) o;

        if (getId() != that.getId()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "FilterStrategyConfiguration{" + "id=" + getId() + '}';
    }
}
