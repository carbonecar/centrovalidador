drop table CENTRO_VALIDADOR.ABSTRACT_MESSAGE_HANDLER;
drop table CENTRO_VALIDADOR.COMPOSITE_FILTER_CONFIGURATION;
drop table CENTRO_VALIDADOR.CONTAINER;
drop table CENTRO_VALIDADOR.ERROR_HANDLER;
drop table CENTRO_VALIDADOR.FILTER_CONFIGURATION;
drop table CENTRO_VALIDADOR.FILTER_MESSAGE_HANDLER;
drop table CENTRO_VALIDADOR.FILTER_STRATEGY_CONFIGURATION;
drop table CENTRO_VALIDADOR.INTERCEPTOR_MESSAGE_HANDLER;
drop table CENTRO_VALIDADOR.MESSAGE_HANDLER;
drop table CENTRO_VALIDADOR.NOT_FILTER_CONFIGURATION;
drop table CENTRO_VALIDADOR.PROCESS_FILTER_STRATEGY;
drop table CENTRO_VALIDADOR.PROPERTY_EQUAL_FILTER_CONFIGURATION;
drop table CENTRO_VALIDADOR.ROUTE_FILTER_STRATEGY;
drop schema CENTRO_VALIDADOR RESTRICT;