-- Con el VRS-114152 se ejecuto erronamente en el ambiente de produccion
create table CENTRO_VALIDADOR.MQ_ROLLBACK (
        codigoOperadorTransaccion VARCHAR(10) NOT NULL,
	numeroTransaccion BIGINT NOT NULL,	
	fechaSolicitudServicio DATE NOT NULL,
        codigoFilial VARCHAR(10) NOT NULL,
        mensaje_mq_timeout varchar(2048)
);
ALTER TABLE CENTRO_VALIDADOR.MQ_ROLLBACK
	ADD CONSTRAINT PK_ROLLBACK PRIMARY KEY  (codigoOperadorTransaccion, numeroTransaccion, fechaSolicitudServicio);