CREATE TABLE CENTRO_VALIDADOR.MIGRACION_MESSAGE_HANDLER (
        MESSAGE_HANDLER_ID bigint NOT NULL
);

CREATE TABLE CENTRO_VALIDADOR.CODIGO_EXCEPTUABLE (
        CODIGO_ID bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
        DESCRIPCION VARCHAR(60) NOT NULL,
        CODIGO INTEGER NOT NULL
);

CREATE TABLE CENTRO_VALIDADOR.MIGRACION_HANDLER_CODIGO_EXCEPTUABLE (
        MESSAGE_HANDLER_ID bigint NOT NULL,
        CODIGO_ID bigint NOT NULL
);

ALTER TABLE CENTRO_VALIDADOR.MIGRACION_MESSAGE_HANDLER ADD CONSTRAINT PK_MIGRACION_HANDLER
	PRIMARY KEY (MESSAGE_HANDLER_ID)
;

ALTER TABLE CENTRO_VALIDADOR.MIGRACION_MESSAGE_HANDLER ADD CONSTRAINT FK_MESSAGE_HANDLER
	FOREIGN KEY (MESSAGE_HANDLER_ID) REFERENCES CENTRO_VALIDADOR.MESSAGE_HANDLER(MESSAGE_HANDLER_ID)
;

ALTER TABLE CENTRO_VALIDADOR.CODIGO_EXCEPTUABLE ADD CONSTRAINT PK_CODIGO_EXCEPTUABLE
	PRIMARY KEY (CODIGO_ID)
;

ALTER TABLE CENTRO_VALIDADOR.MIGRACION_HANDLER_CODIGO_EXCEPTUABLE ADD CONSTRAINT PK_MIGRACION_HANDLER_CODIGO_EXCEPTUABLE
	PRIMARY KEY (MESSAGE_HANDLER_ID , CODIGO_ID)
;

ALTER TABLE CENTRO_VALIDADOR.MIGRACION_HANDLER_CODIGO_EXCEPTUABLE ADD CONSTRAINT FK_MIGRACION_HANDLER
	FOREIGN KEY (MESSAGE_HANDLER_ID) REFERENCES CENTRO_VALIDADOR.MIGRACION_MESSAGE_HANDLER(MESSAGE_HANDLER_ID)
;

ALTER TABLE CENTRO_VALIDADOR.MIGRACION_HANDLER_CODIGO_EXCEPTUABLE ADD CONSTRAINT FK_CODIGO_EXCEPTUABLE
	FOREIGN KEY (CODIGO_ID) REFERENCES CENTRO_VALIDADOR.CODIGO_EXCEPTUABLE(CODIGO_ID)
;