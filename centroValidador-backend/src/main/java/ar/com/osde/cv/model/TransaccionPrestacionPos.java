package ar.com.osde.cv.model;

import java.io.Serializable;

/**
 * Wrapper para el modelo de pos y el de transacciones
 * 
 * @author MT27789605
 * 
 */
public class TransaccionPrestacionPos extends AbstractTransaccionPrestacionPos implements Serializable, OrdenReferenciable {
    private static final long serialVersionUID = 6676943545696549863L;

    @Deprecated
    // hay que usar autorizacionBid
    private Orden orden;

    public TransaccionPrestacionPos() {
        super();
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.SocioPosMessage#accept(ar.com.osde.
     * centroValidador.model.TransaccionPosVisitor)
     */
    @Override
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);
    }

}
