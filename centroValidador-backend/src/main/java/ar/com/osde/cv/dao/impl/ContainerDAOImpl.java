package ar.com.osde.cv.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import ar.com.osde.cv.dao.ContainerDAO;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.persistence.dao.crud.impl.hibernate.GenericCRUDDaoHIBImpl;

/**
 * Dao para acceder a la base de datos de los container. Los container se
 * representan en la base de datos mediante
 * 
 * @see ar.com.osde.entities.ContainerConfiguration
 * @author MT27789605
 *
 */
public class ContainerDAOImpl extends GenericCRUDDaoHIBImpl<ContainerConfiguration> implements ContainerDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ar.com.osde.cv.dao.impl.ContainerDAO#getAll()
	 */
	public List<ContainerConfiguration> getAll() {
		// super.getAll();
		@SuppressWarnings("unchecked")
		List<ContainerConfiguration> containers =

		(List<ContainerConfiguration>) this.getHibernateTemplate().execute(new HibernateCallback() {

			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(ContainerConfiguration.class).addOrder(Order.asc("id"))
		                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				return criteria.list();
			}
		});

		for (ContainerConfiguration containerConfiguration : containers) {
			containerConfiguration.setRunning(containerConfiguration.isStartOnLoad());
		}
		return containers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ar.com.osde.cv.dao.impl.ContainerDAO#getAllContainersWithGroupName(java.
	 * lang.String)
	 */
	public List<ContainerConfiguration> getAllContainersWithGroupName(final String groupName) throws BusinessException {
		return (List<ContainerConfiguration>) this.getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(ContainerConfiguration.class)
		                .add(Restrictions.eq("groupName", groupName));
				return criteria.list();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ar.com.osde.cv.dao.impl.ContainerDAO#getParentContainer(long)
	 */
	public ContainerConfiguration getParentContainer(final long idHandler) {
		return (ContainerConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(ContainerConfiguration.class)
		                .add(Restrictions.eq("posMessageHandler.id", idHandler));
				return criteria.uniqueResult();
			}
		});
	}
}
