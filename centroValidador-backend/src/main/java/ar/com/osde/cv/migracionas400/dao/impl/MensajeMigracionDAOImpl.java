package ar.com.osde.cv.migracionas400.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import ar.com.osde.cv.migracionas400.dao.MensajeMigracionDAO;
import ar.com.osde.cv.migracionas400.dao.TransaccionMigracionFilter;
import ar.com.osde.cv.migraciontoas400.persistence.MensajeMigracion;
import ar.com.osde.entities.transaccion.TransaccionBID;
import ar.com.osde.framework.persistence.dao.crud.impl.hibernate.GenericCRUDDaoHIBImpl;
import ar.com.osde.framework.persistence.exception.PersistenceException;

public class MensajeMigracionDAOImpl extends GenericCRUDDaoHIBImpl<MensajeMigracion> implements MensajeMigracionDAO {

    @SuppressWarnings("unchecked")
    public List<MensajeMigracion> findByFilter(final TransaccionMigracionFilter filter) {

        return (List<MensajeMigracion>) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws  SQLException {
                Criteria criteria = session.createCriteria(MensajeMigracion.class);
                criteria.add(Restrictions.eq("codigoFilial", filter.getCodigoFilial()));

                return criteria.list();
            }
        });
    }

    public Integer crearMensajesMQ(final TransaccionMigracionFilter filter) {
        return (Integer) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws  SQLException {
                int returnValue = session.getNamedQuery("creacion_mensaje_farmacia_mq").setProperties(filter).executeUpdate();

                return returnValue;

            }
        });

    }

    public Integer deleteAllMessage(final TransaccionMigracionFilter filter) {
        return (Integer) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws  SQLException {
                return session.getNamedQuery("eliminar_mensajes_migracion_farmacia").setProperties(filter).executeUpdate();

            }
        });

    }

    @Override
    public MensajeMigracion saveNew(MensajeMigracion instance) throws PersistenceException {
        instance.setTransaccionBid((TransaccionBID) this.getHibernateTemplate().save(instance));
        return instance;
    }

}
