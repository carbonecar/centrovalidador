package ar.com.osde.cv.dao.impl;

import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;
import ar.com.osde.framework.persistence.dao.crud.impl.hibernate.GenericCRUDDaoHIBImpl;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import java.sql.SQLException;

public class PosMessageHandlerDAOImpl extends GenericCRUDDaoHIBImpl<PosMessageHandlerConfiguration> implements PosMessageHandlerDAO {


    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.PosMessageHandlerDAO#getParentHandler(long)
     */
    public PosMessageHandlerConfiguration getParentHandler(final long idHandler) {
        return (PosMessageHandlerConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria criteria = session
                        .createCriteria(PosMessageHandlerInterceptorConfiguration.class)
                        .add(Restrictions.eq("posMessageHandler.id", idHandler));
                return criteria.uniqueResult();
            }
        });
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.PosMessageHandlerDAO#getParentHandlerForFilter(java.lang.Long)
     */
    public PosMessageHandlerConfiguration getParentHandlerForFilter(final Long idFilter) {
        return (PosMessageHandlerConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria criteria = session.
                        createCriteria(FilteredMessageHandlerConfiguration.class).
                        createCriteria("filters").
                        add(Restrictions.eq("id", idFilter));
                return criteria.uniqueResult();
            }
        });
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.PosMessageHandlerDAO#getParentHandlerForFilterStrategy(java.lang.Long)
     */
    public PosMessageHandlerConfiguration getParentHandlerForFilterStrategy(final Long idFilter) {
        return (PosMessageHandlerConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria criteria = session.
                        createCriteria(FilteredMessageHandlerConfiguration.class).
                        createCriteria("filterStrategyList").
                        add(Restrictions.eq("id", idFilter));
                return criteria.uniqueResult();
            }
        });
    }
}