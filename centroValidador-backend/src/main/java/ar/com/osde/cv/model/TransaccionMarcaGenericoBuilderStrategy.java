package ar.com.osde.cv.model;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.MarcaGenericoPosMessage;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.aptoServicio.Medicamento;

public class TransaccionMarcaGenericoBuilderStrategy extends
        AbstractTransaccion4XBuilderStrategy<MarcaGenericoTransaccionPos, MarcaGenericoPosMessage> {

    public TransaccionMarcaGenericoBuilderStrategy() {
        super();
    }

    @Override
    public MarcaGenericoTransaccionPos buildTransaccion(Mapper beanMapper, MarcaGenericoPosMessage posMessage) {
        MarcaGenericoTransaccionPos transaccion = super.buildTransaccion(beanMapper, posMessage);
        this.buildMedicamentos(transaccion, posMessage);
        return transaccion;
    }

    @Override
    public Class getClazz() {
        return MarcaGenericoTransaccionPos.class;
    }

    private void buildMedicamentos(MarcaGenericoTransaccionPos transaccion, MarcaGenericoPosMessage posMessage) {
        MedicamentoBuilder medicamentoBuilder = new MedicamentoBuilder();

        Medicamento medicamento1 = medicamentoBuilder.buildMedicamento(posMessage.getCodigoTroquelMedicamento1(),
                posMessage.getCodigoBarrasMedicamento1(), posMessage.getNumeroDeRegistroAlfabeta1(), posMessage.getMarcaGenerico1(), 1);
        Medicamento medicamento2 = medicamentoBuilder.buildMedicamento(posMessage.getCodigoTroquelMedicamento2(),
                posMessage.getCodigoBarrasMedicamento2(), posMessage.getNumeroDeRegistroAlfabeta2(), posMessage.getMarcaGenerico2(), 2);
        Medicamento medicamento3 = medicamentoBuilder.buildMedicamento(posMessage.getCodigoTroquelMedicamento3(),
                posMessage.getCodigoBarrasMedicamento3(), posMessage.getNumeroDeRegistroAlfabeta3(), posMessage.getMarcaGenerico2(), 3);

        List<Medicamento> medicamentos = new ArrayList<Medicamento>();
        if (medicamento1 != null) {
            medicamentos.add(medicamento1);
        }
        if (medicamento2 != null) {
            medicamentos.add(medicamento2);
        }
        if (medicamento3 != null) {
            medicamentos.add(medicamento3);
        }
        transaccion.getTransaccionMarcaGenerico().getSolicitudServicio().setMedicamentos(medicamentos);
    }

    public class MedicamentoBuilder {

        Medicamento buildMedicamento(int codigoTroquel, long codigoBarras, String numeroRegistroAlfabeta, String marcaGenerico, int nroItem) {

            if (((numeroRegistroAlfabeta != null && !"000000".equals(numeroRegistroAlfabeta) && !"".equals(numeroRegistroAlfabeta.trim()))
                    || codigoBarras != 0 || codigoTroquel != 0)) {
                Medicamento medicamento = new Medicamento();
                medicamento.setNroTroquel(codigoTroquel);
                medicamento.setCodMedicamento(Integer.parseInt(numeroRegistroAlfabeta));
                medicamento.setCodBarras(codigoBarras);
                medicamento.setTipoElemento(TipoElemento.FARMACIA);
                medicamento.setTipoDeOrigen(Medicamento.TIPO_ORIGEN_ALFABETA);

                if ("1".equals(marcaGenerico)) {
                    medicamento.setGenerico(true);
                }
                medicamento.setNroItem(nroItem);
                return medicamento;
            }
            return null;
        }
    }

}
