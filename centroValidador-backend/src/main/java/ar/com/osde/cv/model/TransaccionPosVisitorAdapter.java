package ar.com.osde.cv.model;

/**
 * Adapter para evitar la implementacion de todos los metodos vacios
 * 
 * @author MT27789605
 * 
 */
public class TransaccionPosVisitorAdapter implements TransaccionPosVisitor {

    public TransaccionPosVisitorAdapter() {

    }

    public void visit(TransaccionPrestacionPos transaccion) {
        // nothing to do

    }

    public void visit(RegistracionMedicamentoMessage transaccion) {
        // nothing to do

    }

    public void visit(AbstractTransaccionPos transaccionPos) {
        // nothing to do

    }

    public void visit(SocioPosMessage socioPosMessage) {
        // nothing to do

    }

    public void visit(AnulacionTransaccionPos anulacionTransaccionPos) {
        // nothing to do

    }

    public void visit(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoPosMessage) {
        // nothing to do

    }

    public void visit(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos) {
        // nothing to do

    }

    public void visit(TransaccionRescatePos transaccionRescatePos) {
        // nothing to do

    }

    public void visit(TransaccionIngresoEgresoPos transaccionIngresoEgresoPos) {
        // nothing to do

    }

    public void visit(TransaccionTimeOutPos transaccionTimeoutPos) {
        // nothing to do

    }

    public void visit(TransaccionMigracionPos transaccionMigracionPos) {
        // nothing to do

    }

    public void visit(ConsultaTransaccionPos consultaTransaccionPos) {
        // nothing to do

    }

    public void visit(MarcaGenericoTransaccionPos transaccionMarcaGenerico) {
        // nothing to do

    }

    public void visit(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion) {
        // nothing to do

    }

}
