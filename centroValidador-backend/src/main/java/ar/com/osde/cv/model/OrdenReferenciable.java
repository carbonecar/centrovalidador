package ar.com.osde.cv.model;

public interface OrdenReferenciable {

    public Orden getOrden();

    public void setOrden(Orden orden);

}
