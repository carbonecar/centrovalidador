package ar.com.osde.cv.model;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptoServicio.AutorizacionBidFactory;
import ar.com.osde.entities.aptoServicio.TransaccionReferencia;
import ar.com.osde.entities.transaccion.ItemPrestador;

public class Transaccion2LBuilderStrategy extends Transaccion2ABuilderStrategy {

    public Transaccion2LBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    @Override
    protected void buildElementosParticularesItem(PosMessage posMessage, TransaccionPrestacionPos transaccion, ItemPrestador p) {
        // Para las 2L el campo consignado como numeroAutorizacionReferencia es
        // el numero de transaccion anterior. Y la delegacion de la orden es el operador transacionador.
        // hay que hacer el mapeo con dozer pero el cambio es muy riesgoso por 2
        // campos no vale la pena.
        int numeroTransaccionAnterior = posMessage.getNumeroAutorizacionReferencia();
        String operadorTransaccionador=posMessage.getCodigoOperador();
        if(operadorTransaccionador!=null){
            operadorTransaccionador=operadorTransaccionador.trim();            
        }
        if (numeroTransaccionAnterior != 0) {
            
            getTransaccionReferencia(transaccion).setNumeroTransaccion(numeroTransaccionAnterior);
            getTransaccionReferencia(transaccion).setCodigoOperador(operadorTransaccionador);
            if (FixedFormatMessageHandler.WEB_GESTION.equalsIgnoreCase(transaccion.getTransaccion().getCodigoOperadorTransaccion())) {
                transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia().setCodigoOperador(posMessage.getDelegacionOrden());
            }
            if (transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia().getCodigoOperador() != null) {
                String codigoOperador = transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia()
                        .getCodigoOperador();
                if (codigoOperador.trim().equals("") || codigoOperador.trim().equals("0")) {
                    transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia().setCodigoOperador(null);
                }
            }
            LocalDate fechaPracticaOffline = posMessage.getFechaPracticaOffLine();
            if (fechaPracticaOffline != null) {
                Date localDate = fechaPracticaOffline.toDateMidnight().toDate();
                if (localDate != null) {
                    transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia()
                            .setFechaTransaccionOriginal(localDate);
                }
            }
        }
    }

    private TransaccionReferencia getTransaccionReferencia(TransaccionPrestacionPos transaccion) {
        TransaccionReferencia trRef = new TransaccionReferencia();
        if (transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia() == null) {
            transaccion.getTransaccionConsumo().getSolicitudServicio().setTransaccionReferencia(trRef);
        }
        return transaccion.getTransaccionConsumo().getSolicitudServicio().getTransaccionReferencia();
    }
}
