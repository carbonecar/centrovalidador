package ar.com.osde.cv.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessageFarmacia;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.aptoServicio.ItemConsumo;
import ar.com.osde.entities.aptoServicio.Medicamento;
import ar.com.osde.entities.aptoServicio.PedidoMedico;
import ar.com.osde.entities.aptoServicio.PrestadorOSDE;
import ar.com.osde.entities.transaccion.ItemPrestador;

public class Transaccion2FBuilderStrategy extends
        AbstractTransaccionConsumoPosBuilderStrategyImpl<RegistracionMedicamentoMessage, PosMessageFarmacia> {

    public static final String FORMA_VENTA_MOSTRADOR = "M";
    public static final String FORMA_VENTA_INTERNACION = "I";
    public static final String FORMA_VENTA_INTERNACION_DOMICILIARIA = "D";
    public static final String FORMA_VENTA_INTERNACION_CLINICA = "C";
    public static final String FORMA_VENTA_ANTICONCEPTIVOS = "P";
    // TODO sacar de aca incyectaro u obtenerlo de la base
    private final Map<String, Integer> formaVentaMedicamentoAmbito = new HashMap<String, Integer>();

    public Transaccion2FBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);

        formaVentaMedicamentoAmbito.put(FORMA_VENTA_MOSTRADOR, Integer.valueOf(1));
        formaVentaMedicamentoAmbito.put(FORMA_VENTA_INTERNACION, Integer.valueOf(17));
        formaVentaMedicamentoAmbito.put(FORMA_VENTA_INTERNACION_DOMICILIARIA, Integer.valueOf(40));
        formaVentaMedicamentoAmbito.put(FORMA_VENTA_INTERNACION_CLINICA, Integer.valueOf(7));
        formaVentaMedicamentoAmbito.put(FORMA_VENTA_ANTICONCEPTIVOS, Integer.valueOf(1));
        formaVentaMedicamentoAmbito.put("", Integer.valueOf(1));
    }

    public RegistracionMedicamentoMessage buildTransaccion(Mapper beanMapper, PosMessageFarmacia posMessage) {
        PosMessageFarmacia posMessageFarmacia = (PosMessageFarmacia) posMessage;
        RegistracionMedicamentoMessage transaccion = this.buildTransaccion(beanMapper, posMessage, RegistracionMedicamentoMessage.class);
        transaccion.setFechaReceta(posMessageFarmacia.getFechaReceta());

        return buildMedicamentos(transaccion, (PosMessageFarmacia) posMessage);
    }

    protected RegistracionMedicamentoMessage buildMedicamentos(RegistracionMedicamentoMessage transaccion, PosMessageFarmacia posMessage) {
        PedidoMedico pedidoMedico = new PedidoMedico();
        PosMessageFarmacia medicamentoPosMessage = (PosMessageFarmacia) transaccion.getPosMessage();
        String formaVenta = posMessage.getFormaDeVenta();
        Integer ambito = 0;
        if (formaVenta != null) {
            ambito = formaVentaMedicamentoAmbito.get(formaVenta.trim().toUpperCase());
        } else {
            ambito = formaVentaMedicamentoAmbito.get(FORMA_VENTA_MOSTRADOR);
        }
        if (ambito == null) {
            ambito = 1;
        }
        pedidoMedico.setMatriculaPrescriptor(this.buildMatriculaMatricula(medicamentoPosMessage));

        List<ItemConsumo> medicamentos = new ArrayList<ItemConsumo>();
        AutorizacionBid autorizacionBid = this.createAutorizacionBid(transaccion,transaccion.getTransaccion().getPrestadorTransaccionador().getTerminalID());
        if ((posMessage.getNumeroDeRegistroAlfabeta1() != null && !"000000".equals(posMessage.getNumeroDeRegistroAlfabeta1()) && !""
                .equals(posMessage.getNumeroDeRegistroAlfabeta1()))
                || posMessage.getCodigoBarrasMedicamento1() != 0
                || posMessage.getCodigoTroquelMedicamento1() != 0) {

            pedidoMedico.setFecha(posMessage.getFechaReceta());
            Medicamento medicamentoInformado = createMedicamento(transaccion, pedidoMedico, posMessage.getNumeroDeRegistroAlfabeta1(),
                    posMessage.getCodigoTroquelMedicamento1(), posMessage.getCodigoBarrasMedicamento1(),posMessage.getMarcaGenerico1(), autorizacionBid);

            medicamentoInformado.setNroItem(1);
            Date fechaConsumo = this.buildFechaConsumo(posMessage, transaccion);
            medicamentoInformado.setAmbito(ambito);
            medicamentoInformado.setFechaConsumo(fechaConsumo);
            medicamentoInformado.setTipoElemento(TipoElemento.FARMACIA);
            medicamentoInformado.setTipoDeOrigen(Medicamento.TIPO_ORIGEN_ALFABETA);
            medicamentoInformado.setCantidad(posMessage.getCantidadMedicamento1());
            medicamentos.add(medicamentoInformado);

        }

        if ((posMessage.getNumeroDeRegistroAlfabeta2() != null && !"000000".equals(posMessage.getNumeroDeRegistroAlfabeta2()) && !""
                .equals(posMessage.getNumeroDeRegistroAlfabeta2()))
                || posMessage.getCodigoBarrasMedicamento2() != 0
                || posMessage.getCodigoTroquelMedicamento2() != 0) {
            pedidoMedico.setFecha(posMessage.getFechaReceta());
            Medicamento medicamentoInformado = createMedicamento(transaccion, pedidoMedico, posMessage.getNumeroDeRegistroAlfabeta2(),
                    posMessage.getCodigoTroquelMedicamento2(), posMessage.getCodigoBarrasMedicamento2(),posMessage.getMarcaGenerico2(), autorizacionBid);
            medicamentoInformado.setNroItem(2);
            Date fechaConsumo = this.buildFechaConsumo(posMessage, transaccion);
            medicamentoInformado.setAmbito(ambito);
            medicamentoInformado.setFechaConsumo(fechaConsumo);
            medicamentoInformado.setTipoElemento(TipoElemento.FARMACIA);
            medicamentoInformado.setTipoDeOrigen(Medicamento.TIPO_ORIGEN_ALFABETA);
            medicamentoInformado.setCantidad(posMessage.getCantidadMedicamento2());
            medicamentos.add(medicamentoInformado);

        }

        if ((posMessage.getNumeroDeRegistroAlfabeta3() != null && !"000000".equals(posMessage.getNumeroDeRegistroAlfabeta3()) && !""
                .equals(posMessage.getNumeroDeRegistroAlfabeta3()))
                || posMessage.getCodigoBarrasMedicamento3() != 0
                || posMessage.getCodigoTroquelMedicamento3() != 0) {

            pedidoMedico.setFecha(posMessage.getFechaReceta());
            Medicamento medicamentoInformado = createMedicamento(transaccion, pedidoMedico, posMessage.getNumeroDeRegistroAlfabeta3(),
                    posMessage.getCodigoTroquelMedicamento3(), posMessage.getCodigoBarrasMedicamento3(),posMessage.getMarcaGenerico3(), autorizacionBid);

            medicamentoInformado.setNroItem(3);
            Date fechaConsumo = this.buildFechaConsumo(posMessage, transaccion);
            medicamentoInformado.setAmbito(ambito);
            medicamentoInformado.setFechaConsumo(fechaConsumo);
            medicamentoInformado.setTipoElemento(TipoElemento.FARMACIA);
            medicamentoInformado.setTipoDeOrigen(Medicamento.TIPO_ORIGEN_ALFABETA);
            medicamentoInformado.setCantidad(posMessage.getCantidadMedicamento3());
            medicamentos.add(medicamentoInformado);

        }
        transaccion.getTransaccionConsumo().getSolicitudServicio().setItemsConsumo(medicamentos);
        return transaccion;
    }

    private Medicamento createMedicamento(RegistracionMedicamentoMessage transaccion, PedidoMedico pedidoMedico,
            String numeroDeRegistroAlfabeta, int codigoTroquelMedicamento, long codigoBarrasMedicamento,String marcaGenerico, AutorizacionBid autorizacionBid) {
        PrestadorOSDE transaccionador = transaccion.getPrestador();
        // Se construye un prestadorOSDE porque hay un bug en el motor de reglas
        // y se esta evaluando al transaccionador como transaccionador en lugar
        // de su rol de efector. No deberia hacerse esto!!!.
        PrestadorOSDE prestadorOSDE = new PrestadorOSDE();

        prestadorOSDE.setCodigoFilial(transaccionador.getCodigoFilial());
        prestadorOSDE.setCuit(transaccionador.getCuit());
        prestadorOSDE.setEfector(transaccionador.getEfector());
        prestadorOSDE.setNumeroRelacion(transaccionador.getNumeroRelacion());

        Medicamento medicamentoInformado = new Medicamento();
        medicamentoInformado.setPrestador(prestadorOSDE);
        if (numeroDeRegistroAlfabeta != null) {
            try {
                medicamentoInformado.setCodMedicamento(Integer.valueOf((numeroDeRegistroAlfabeta)));
            } catch (NumberFormatException e) {
                LOG.error(e);
            }

        }
        try {
            medicamentoInformado.setNroTroquel(Integer.valueOf(codigoTroquelMedicamento));
        } catch (NumberFormatException e) {
            LOG.error(e);
        }
        //TODO: Si es ==1 es generico, caso contrario hay que rechazar la transaccion
        if("1".equals(marcaGenerico)){
            medicamentoInformado.setGenerico(true); 
        }else{
            medicamentoInformado.setGenerico(false);
        }
        
        
        medicamentoInformado.setCodBarras(codigoBarrasMedicamento);
        medicamentoInformado.setPedidoMedico(pedidoMedico);
        medicamentoInformado.setViaOtorgamiento(ItemPrestador.VIA_OTORGAMIENTO_DEFAULT);
        medicamentoInformado.setAutorizacionBidInformada(autorizacionBid);
        medicamentoInformado.setOrdenBeneficiario(transaccion.getBeneficiarioBid().getOrden());
        
        return medicamentoInformado;
    }
}
