package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.aptoServicio.Efector;
import ar.com.osde.entities.aptoServicio.Matricula;
import ar.com.osde.entities.transaccion.ItemPrestador;

/**
 * Builder para la tranascción 2A (Asi el sonar no me dice que no documento) con
 * una fecha de consumo que se le pasa por parametro al constructor.
 * 
 * @author MT27789605
 * 
 */
public class Transaccion2ABuilderStrategy extends AbstractPrestacionTransaccionPosBuilderStrategyImpl<TransaccionPrestacionPos, PosMessage> {

    /**
     * Construcion con el builder de la fecha de consumo.
     * 
     * @param fechaConsumoCalculador
     */
    public Transaccion2ABuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);

    }

    /**
     * Construye la transaccion.
     */
    public TransaccionPrestacionPos buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        TransaccionPrestacionPos trx = super.buildTransaccion(beanMapper, posMessage, TransaccionPrestacionPos.class);
        this.buildEfector(posMessage, trx);
        this.buildPrestaciones(posMessage, trx);
        return trx;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.cv.model.AbstractPrestacionTransaccionPosBuilderStrategyImpl
     * #buildElementosParticularesItem
     * (ar.com.osde.centroValidador.pos.PosMessage,
     * ar.com.osde.cv.model.AbstractTransaccionPrestacionPos,
     * ar.com.osde.entities.transaccion.ItemPrestador)
     */
    @Override
    protected void buildElementosParticularesItem(PosMessage posMessage, TransaccionPrestacionPos transaccion, ItemPrestador p) {
        p.setAutorizacionBid(createAutorizacionBid(transaccion, transaccion.getTransaccion().getPrestadorTransaccionador().getTerminalID()));

    }

    /**
     * Devuevle el timpo de elemtno de la clase que construye por defecto es
     * Medicina
     */
    protected TipoElemento getTipoElemento() {
        return TipoElemento.MEDICINA;
    }

    /**
     * Creacion del efector
     * 
     * @param posMessage
     * @param transaccionPos
     * @return
     */
    protected Efector buildEfector(PosMessage posMessage, TransaccionPrestacionPos transaccionPos) {
        Efector efector = null;
        Matricula matricula = this.buildMatriculaMatricula(posMessage);
        if (matricula != null) {
            efector = new Efector();
            efector.setMatricula(matricula);
            // efector.setFilial(filial);
            transaccionPos.getPrestador().setEfector(efector);
        }
        return efector;
    }

}
