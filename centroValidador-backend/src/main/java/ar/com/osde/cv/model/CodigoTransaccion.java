package ar.com.osde.cv.model;

/**
 * 
 * @author VA27789605
 * 
 */
public class CodigoTransaccion {

    private Integer codigo;
    private String atributo;

    public CodigoTransaccion(Integer codigo, String atributo) {
        this.codigo = codigo;
        this.atributo = atributo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo = atributo;
    }

    @Override
    public boolean equals(Object anotherMe) {
        if (!(anotherMe instanceof CodigoTransaccion))
            return false;

        CodigoTransaccion other = (CodigoTransaccion) anotherMe;

        return codigo.equals(other.codigo) && atributo.equalsIgnoreCase(other.atributo);

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + codigo;
        result = prime * result + atributo.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return codigo + atributo;
    }
}
