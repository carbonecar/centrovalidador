package ar.com.osde.cv.migracionas400.bo.impl;

import java.util.Iterator;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import ar.com.osde.centroValidador.factory.UtilFactory;
import ar.com.osde.cv.migracionas400.bo.TransaccionMigracionBO;
import ar.com.osde.cv.migracionas400.dao.MensajeMigracionDAO;
import ar.com.osde.cv.migracionas400.dao.TransaccionMigracionFilter;
import ar.com.osde.cv.migracionas400.dao.TransaccionMigracionFilterBuilder;
import ar.com.osde.cv.migraciontoas400.persistence.MensajeMigracion;
import ar.com.osde.cv.pos.utils.ExceptionFactory;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Implementación del BO para la migracion
 * 
 * @author MT27789605
 * 
 */
public class TransaccionMigracionBOImpl implements TransaccionMigracionBO {

    private MensajeMigracionDAO mensajeMigracionDAO;
    private String queueName;
    private String connectionFactoryName;

    public void migrarTransacciones(String codigoFilial) throws BusinessException {

        JmsTemplate jmsTemplate = UtilFactory.createJmsTemplate(queueName, connectionFactoryName);
        ExceptionFactory.createBusinessException(StringUtils.isEmpty(codigoFilial), "debe especificar un valor para la filial");
        TransaccionMigracionFilter filter = new TransaccionMigracionFilter();
        filter.setCodigoFilial(codigoFilial);

        List<MensajeMigracion> mensajesMigracion = mensajeMigracionDAO.findByFilter(filter);
        for (Iterator<MensajeMigracion> iterator = mensajesMigracion.iterator(); iterator.hasNext();) {
            final MensajeMigracion mensajeMigracion = iterator.next();
            jmsTemplate.send(new MessageCreator() {

                public Message createMessage(Session session) throws JMSException {
                    TextMessage textMessage = session.createTextMessage();
                    textMessage.setText(mensajeMigracion.getMensajeMQ());
                    return textMessage;
                }
            });
        }
    }

    public int crearMensajesMigracion(String codigoFilial) {
        return this.mensajeMigracionDAO.crearMensajesMQ(new TransaccionMigracionFilterBuilder().withCodigoFilial(codigoFilial).build());
    }

    public int eliminarMensajesMigracion(String codigoFilial) {
        return this.mensajeMigracionDAO.deleteAllMessage(new TransaccionMigracionFilterBuilder().withCodigoFilial(codigoFilial).build());
    }

    // *********************************************/
    // ********Accesores****************************/
    // *********************************************/

    public MensajeMigracionDAO getMensajeMigracionDAO() {
        return mensajeMigracionDAO;
    }

    public void setMensajeMigracionDAO(MensajeMigracionDAO mensajeMigracionDAO) {
        this.mensajeMigracionDAO = mensajeMigracionDAO;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

}
