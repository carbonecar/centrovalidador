package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.centroValidador.pos.PosMessage;

public interface FechaConsumoCalculador {

    public abstract Date buildFechaConsumo(PosMessage posMessage, TransaccionPos transaccion);

}