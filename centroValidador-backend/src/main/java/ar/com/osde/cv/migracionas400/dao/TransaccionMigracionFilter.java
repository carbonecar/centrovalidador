package ar.com.osde.cv.migracionas400.dao;

/**
 * Filtro para el recupero de transacciones a migrar
 * @author MT27789605
 *
 */
public class TransaccionMigracionFilter {

    private String codigoFilial;

    public void setCodigoFilial(String codigoFilial) {

        this.codigoFilial = codigoFilial;
    }

    public String getCodigoFilial() {
        return codigoFilial;
    }

}
