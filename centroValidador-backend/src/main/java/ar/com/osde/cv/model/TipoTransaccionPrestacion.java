package ar.com.osde.cv.model;

public enum TipoTransaccionPrestacion {

	//PRESCRIPCION DE UNA ORDEN PARA OTRO PRESTADOR
	PRESCIPCION,
	//REGISTRACION DE LA PRESTACION
	REGISTRACION,
	//solicitud DE AUTORIZACION para prestar una prestacion
	SOLICITUD_AUTORIZACION,
	//consulta habilitacion de la prestacion
	HABILITACION,
	//
	VALIDACION;
}
