package ar.com.osde.cv.model;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessageRescate;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.entities.aptoServicio.TransaccionReferencia;

/**
 * Estrategia de construccion de la transaccion de rescate (3A)
 * 
 * @author MT27789605
 * 
 */
public class TransaccionRescateBuilderStrategy extends AbstractTransaccionPosBuilderStrategyImpl<TransaccionRescatePos, PosMessageRescate> {

    private static final String MARCASOLICITUD_PROTESISOIMPLANTE = "OD";

    public TransaccionRescatePos buildTransaccion(Mapper beanMapper, PosMessageRescate posMessage) {

        TransaccionRescatePos transaccionRescate = super.buildTransaccion(beanMapper, posMessage, TransaccionRescatePos.class);

        // Solo si la delegacion esta informada se genera el bid de
        // autoriaciones.
        // Caso contrario el numero representa o un numero de transaccion o un
        // numero de solicitud de Protesis o implante.
        String delegacion = transaccionRescate.getOrden().getDelegacion();

        // Primero normalizo
        if (StringUtils.isEmpty(delegacion)) {
            delegacion = "0";
        } else {
            delegacion = delegacion.replace(' ', '0');
        }

        // Ahora vemos los diferentes casos
        if (!StringUtils.isNumeric(delegacion)) {

            if (MARCASOLICITUD_PROTESISOIMPLANTE.equals(delegacion)) {
                // Entonces es un valor no Numero, con lo cual puede ser un OD.
                // lo que implicaria que es odontologia y es
                // una solicitud de protesis o implante. Este caso no esta
                // soportado y por lo tanto arrojo una exception.
                // en el campo nref llega un numero. (nRef. El numero de
                // referencia de la solicitud de protesis o implante)
                throw new UnsupportedPosMessageException("Rescate OD no soportado. Operatoria modificada: "
                        + posMessage.getOriginalMessage());
            } else {
                setTransaccionReferencia(transaccionRescate);
            }

        } else {
            if (Integer.parseInt(delegacion) != 0) {
                transaccionRescate
                        .getTransaccion()
                        .getSolicitudServicio()
                        .setAutorizacionBid(
                                this.createAutorizacionBid(transaccionRescate, transaccionRescate.getTransaccion()
                                        .getPrestadorTransaccionador().getTerminalID()));

            } else {
                setTransaccionReferencia(transaccionRescate);
            }
        }

        // FIN de los if cochinos!
        return transaccionRescate;

    }

    private void setTransaccionReferencia(TransaccionRescatePos transaccionRescate) {
        // Es un numero de transaccion lo que esta informado.
        int numeroTransaccion = transaccionRescate.getOrden().getNumeroAutorizacion();
        if (numeroTransaccion != 0) {
            TransaccionReferencia transaccionReferencia = new TransaccionReferencia();
            transaccionReferencia.setCodigoOperador(transaccionRescate.getTransaccion().getCodigoOperadorTransaccion());
            transaccionReferencia.setNumeroTransaccion(numeroTransaccion);
            transaccionRescate.getTransaccion().getSolicitudRescate().setTransaccionReferencia(transaccionReferencia);
        }
    }
}
