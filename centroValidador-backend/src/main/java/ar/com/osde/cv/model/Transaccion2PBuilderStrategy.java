package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.ProtocoloPosMessage;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptoServicio.InformacionAutorizacion;
import ar.com.osde.entities.aptoServicio.InformacionTransaccion;
import ar.com.osde.entities.aptoServicio.TransaccionReferencia;

public class Transaccion2PBuilderStrategy extends
        AbstractTransaccionPosBuilderStrategyImpl<EnvioDocumentacionTransaccionPos, ProtocoloPosMessage> {

    public EnvioDocumentacionTransaccionPos buildTransaccion(Mapper beanMapper, ProtocoloPosMessage posMessage) {
        EnvioDocumentacionTransaccionPos envioDocumentacionTransacionPos = super.buildTransaccion(beanMapper, posMessage,
                EnvioDocumentacionTransaccionPos.class);
        buildBeneficiario(posMessage, envioDocumentacionTransacionPos);
        if (posMessage.getDelegacionNumero().representaAutorizacion()) {
            AutorizacionBid autorizacionBid = this.createAutorizacionBid(envioDocumentacionTransacionPos, envioDocumentacionTransacionPos
                    .getTransaccion().getPrestadorTransaccionador().getTerminalID());
            ((InformacionAutorizacion) envioDocumentacionTransacionPos.getTransaccion().getSolicitudServicio())
                    .setAutorizacionBid(autorizacionBid);
        } else {
            // agregamos el numero de transaccion.
            if (posMessage.getDelegacionNumero().representaTransaccion()) {
                TransaccionReferencia transaccionReferencia = new TransaccionReferencia();
                transaccionReferencia.setNumeroTransaccion(posMessage.getNumeroReferencia());
                ((InformacionTransaccion) envioDocumentacionTransacionPos.getTransaccion().getSolicitudServicio())
                        .setTransaccionReferencia(transaccionReferencia);

            }
        }

        return envioDocumentacionTransacionPos;
    }

}
