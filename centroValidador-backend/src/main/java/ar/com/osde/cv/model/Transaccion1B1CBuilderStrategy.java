package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;

public class Transaccion1B1CBuilderStrategy extends Transaccion2ABuilderStrategy {

    public Transaccion1B1CBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public TransaccionPrestacionPos buildTransaccion(Mapper beanMapper, PosMessage posMessage) {

        TransaccionPrestacionPos transaccionPos = buildTransaccion(beanMapper, posMessage, TransaccionPrestacionPos.class);

        buildFechas(posMessage, transaccionPos);
        return transaccionPos;
    }

}
