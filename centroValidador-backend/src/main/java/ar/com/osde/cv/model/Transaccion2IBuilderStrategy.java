package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.IngresoEgresoPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.transaccion.ItemPrestador;

public class Transaccion2IBuilderStrategy extends
        AbstractPrestacionTransaccionPosBuilderStrategyImpl<TransaccionIngresoEgresoPos, IngresoEgresoPosMessage> {

    public Transaccion2IBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public TransaccionIngresoEgresoPos buildTransaccion(Mapper beanMapper, IngresoEgresoPosMessage posMessage) {
        TransaccionIngresoEgresoPos transaccion = super.buildTransaccion(beanMapper, posMessage, TransaccionIngresoEgresoPos.class);
        this.buildFechas(posMessage, transaccion);
        transaccion.getTransaccion().getSolicitudServicio().setFechaHora(this.buildFechaConsumo(posMessage, transaccion));
        if (posMessage.getCodigoPrestacion1() != 0) {
            transaccion.getTransaccionIngresoEgreso().setItemPrestador(this.buildItemPrestador1(posMessage, transaccion));
        }

        return transaccion;
    }

    @Override
    protected void buildElementosParticularesItem(PosMessage posMessage, TransaccionIngresoEgresoPos transaccion, ItemPrestador p) {
        // nothing to do

    }

    @Override
    protected TipoElemento getTipoElemento() {
        return TipoElemento.MEDICINA;
    }

}
