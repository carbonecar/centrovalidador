package ar.com.osde.cv.model;

import java.io.Serializable;

public class Orden implements Serializable{

	/**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String delegacion;
	private int numeroAutorizacion;

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public int getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(int numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

}
