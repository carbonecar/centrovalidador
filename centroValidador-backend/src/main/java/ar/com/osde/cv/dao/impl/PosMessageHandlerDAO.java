package ar.com.osde.cv.dao.impl;

import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;

/**
 * Dao para los Handlers de los mensajes pos
 * @author MT27789605
 *
 */
public interface PosMessageHandlerDAO extends GenericCRUDDao<PosMessageHandlerConfiguration> {

    /**
     * Devuelve el handler padre
     * @param idHandler
     * @return
     */
    PosMessageHandlerConfiguration getParentHandler(final long idHandler);

    /**
     * Devuelve el handler padre para el fitro que se pasa por id
     * @param idFilter
     * @return
     */
    PosMessageHandlerConfiguration getParentHandlerForFilter(final Long idFilter);

    /**
     * Devuelve el handler padre para el id del fitroStrategy
     * @param idFilter
     * @return
     */
    PosMessageHandlerConfiguration getParentHandlerForFilterStrategy(final Long idFilter);

}