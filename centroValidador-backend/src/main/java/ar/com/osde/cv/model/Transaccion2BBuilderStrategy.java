package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptoServicio.Efector;
import ar.com.osde.entities.aptoServicio.Matricula;

public class Transaccion2BBuilderStrategy extends Transaccion2SBuilderStrategy {


    public Transaccion2BBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public TransaccionPrestacionPos buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        TransaccionPrestacionPos transaccionPos = super.buildTransaccion(beanMapper, posMessage);
        return transaccionPos;
    }

    /**
     * En la 2B no viene la matricula. Esos campos se usan para el efector
     */
    @Override
    protected Matricula buildMatriculaMatricula(PosMessage posMessage) {
        return null;
    }

    /**
     * El efector es obligatorio
     * @param posMessage
     * @param transaccionPos
     * @return
     */
    protected Efector buildEfector(PosMessage posMessage, TransaccionPrestacionPos transaccionPos) {
        Efector efector=super.buildEfector(posMessage, transaccionPos);
        transaccionPos.getTransaccion().getPrestadorTransaccionador().setEfector(efector);
        return efector;
    }

}
