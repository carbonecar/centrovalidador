package ar.com.osde.cv.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.centroValidador.pos.handlers.TransaccionPosMessageCreator;
import ar.com.osde.entities.aptoServicio.ItemConsumo;
import ar.com.osde.entities.transaccion.EstadoTransaccion;
import ar.com.osde.framework.entities.FrameworkEntityID;

/**
 * Mensaje que representa que es parte de un todo
 * 
 * @author VA27789605
 * 
 */
// @ProvidedId
// @Indexed(index = "SequentialMessage")
public class SequentialMessage extends TransaccionPosVisitorAdapter implements FrameworkEntityID, Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private List<AbstractPosMessage> posMessages = new Vector<AbstractPosMessage>();

    private int totalMensajes;

    // @Id
    // @Field(store = Store.NO, index = Index.UN_TOKENIZED)
    private long id;

    private Mapper beanMapper;

    private AbstractTransaccionPos abstractTransaccionPos;

    private EstadoTransaccion estadoTransaccion;

    public AbstractTransaccionPos getAbstractTransaccionPos() {
        return abstractTransaccionPos;
    }

    public boolean isFull() {
        return posMessages.size() == totalMensajes;
    }

    public void addPosMessages(Collection<AbstractPosMessage> messages) {
        for (AbstractPosMessage posMessage : messages) {
            this.addPosMessage(posMessage);
        }
    }

    public void addPosMessage(AbstractPosMessage message) {
        if (isFull()) {
            throw new RuntimeException("Cantidad de mensajes invalida segun la transaccion inicial se esperaba: 0"
                    + (message.getNumeroTotalMensajes() - 1) + " y se encontraron " + this.posMessages.size());
        }
        this.posMessages.add(message);
    }

    /**
     * Realizamos el mergeo de los mensajes.
     * 
     * @param beanMapper
     * @return
     */
    public void buildMessage(Mapper beanMapper, TransaccionPosMessageCreator transaccionPosMessageCreator) {
        // Esto es solamente agregar las transacciones que tienen los mensajes.
        boolean incompleta = false;
        AbstractPosMessage firstPosMessage = CollectionUtils.find(this.posMessages, new Predicate<AbstractPosMessage>() {
            public boolean evaluate(AbstractPosMessage posMessage) {

                return posMessage.getNumeroSecuenciaMensaje() == 1;
            }
        });

        // como no esta el primero, entonces es incompleta, tomo cualquiera dado
        // que todas las trx tiene cabecera.
        if (firstPosMessage == null) {
            firstPosMessage = this.posMessages.get(0);
            incompleta = true;
        }

        this.posMessages.remove(firstPosMessage);
        this.beanMapper = beanMapper;
        AbstractTransaccionPos firstTransaccion = transaccionPosMessageCreator.createTransaccion(firstPosMessage);

        firstTransaccion.accept(this);
        if (incompleta) {
            firstTransaccion.getTransaccion().setEstado(EstadoTransaccion.INCOMPLETA);
        }

        this.beanMapper = null;
        this.abstractTransaccionPos = firstTransaccion;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;

    }

    public int getTotalMensajes() {
        return totalMensajes;
    }

    public void setTotalMensajes(int totalMensajes) {
        this.totalMensajes = totalMensajes;
    }

    public int getCantidadMensajes() {
        return this.posMessages.size();
    }

    public void visit(TransaccionPrestacionPos transaccion) {
        for (IPosMessage posMessage : this.posMessages) {
            AbstractPosMessage abstractPosMessage = (AbstractPosMessage) posMessage;
            TransaccionPrestacionPos partTransaccionPrestacionPos = (TransaccionPrestacionPos) TransaccionFactory.getInstance()
                    .createTransaccion(abstractPosMessage, beanMapper);

            transaccion.getTransaccionConsumo().getItemsPrestador()
                    .addAll(partTransaccionPrestacionPos.getTransaccionConsumo().getItemsPrestador());

        }
    }

    public void visit(RegistracionMedicamentoMessage transaccion) {

        for (IPosMessage posMessage : this.posMessages) {
            RegistracionMedicamentoMessage partTransaccionPrestacionPos = (RegistracionMedicamentoMessage) TransaccionFactory.getInstance()
                    .createTransaccion((AbstractPosMessage) posMessage, beanMapper);
            List<ItemConsumo> itemsFromPart = partTransaccionPrestacionPos.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo();
            for (Iterator<ItemConsumo> iterator = itemsFromPart.iterator(); iterator.hasNext();) {
                ItemConsumo itemConsumo = (ItemConsumo) iterator.next();
                itemConsumo.setNroItem(transaccion.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().size() + 1);
                transaccion.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().add(itemConsumo);

            }
        }
    }

    public void visit(TransaccionMigracionPos transaccionMigracionPos) {
        transaccionMigracionPos.getTransaccionPosTimeOut().accept(this);
    }

    public void visit(TransaccionTimeOutPos transaccionTimeoutPos) {
        transaccionTimeoutPos.getTransaccionPosTimeOut().accept(this);
    }

}
