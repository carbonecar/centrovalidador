package ar.com.osde.cv.model;

import ar.com.osde.centroValidador.pos.TipoCuatroPosMessage;

public class TransaccionAnulacionBuilderStrategy extends AbstractTransaccion4XBuilderStrategy<AnulacionTransaccionPos,TipoCuatroPosMessage> {

    @Override
    public Class getClazz() {
        return AnulacionTransaccionPos.class;
    }

}
