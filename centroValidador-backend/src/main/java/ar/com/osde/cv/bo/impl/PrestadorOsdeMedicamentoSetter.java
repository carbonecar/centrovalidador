package ar.com.osde.cv.bo.impl;

import java.util.List;

import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionMigracionPos;
import ar.com.osde.cv.model.TransaccionPosVisitorAdapter;
import ar.com.osde.cv.model.TransaccionTimeOutPos;
import ar.com.osde.entities.aptoServicio.ItemConsumo;
import ar.com.osde.entities.aptoServicio.Medicamento;
import ar.com.osde.entities.aptoServicio.PrestadorOSDE;

/**
 * Visitor para setear el prestador osde a los medicamentos
 * 
 * @author MT27789605
 * 
 */
public class PrestadorOsdeMedicamentoSetter extends TransaccionPosVisitorAdapter {

    public PrestadorOsdeMedicamentoSetter() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.TransaccionPosVisitorAdapter#visit(
     * ar.com.osde.centroValidador.model.RegistracionMedicamentoMessage)
     */
    public void visit(RegistracionMedicamentoMessage transaccion) {
        List<ItemConsumo> itemsConsumo = transaccion.getTransaccionConsumo().getSolicitudConsumo().getItemsConsumo();
        PrestadorOSDE transaccionador = transaccion.getPrestador();
        for (ItemConsumo itemConsumo : itemsConsumo) {
            PrestadorOSDE prestadorOSDE = new PrestadorOSDE();
            prestadorOSDE.setCodigoFilial(transaccionador.getCodigoFilial());
            prestadorOSDE.setCuit(transaccionador.getCuit());
            prestadorOSDE.setEfector(transaccionador.getEfector());
            prestadorOSDE.setNumeroRelacion(transaccionador.getNumeroRelacion());
            if (itemConsumo instanceof Medicamento) {
                ((Medicamento) itemConsumo).setPrestador(prestadorOSDE);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.TransaccionPosVisitorAdapter#visit(
     * ar.com.osde.centroValidador.model.TransaccionMigracionPos)
     */
    public void visit(TransaccionMigracionPos transaccionMigracionPos) {
        transaccionMigracionPos.getTransaccionPosTimeOut().accept(this);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.TransaccionPosVisitorAdapter#visit(
     * ar.com.osde.centroValidador.model.TransaccionTimeOutPos)
     */
    public void visit(TransaccionTimeOutPos transaccionTimeoutPos) {
        transaccionTimeoutPos.getTransaccionPosTimeOut().accept(this);
    }
}
