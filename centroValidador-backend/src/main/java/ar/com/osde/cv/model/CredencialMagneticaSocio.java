package ar.com.osde.cv.model;

public class CredencialMagneticaSocio extends CredencialSocio {

	private int versionCredencial;

	private int numeroISOEmisorCredencial;
	private int digitoVerificadorAsociado;

	public int getVersionCredencial() {
		return versionCredencial;
	}

	public void setVersionCredencial(int versionCredencial) {
		this.versionCredencial = versionCredencial;
	}

	public int getNumeroISOEmisorCredencial() {
		return numeroISOEmisorCredencial;
	}

	public void setNumeroISOEmisorCredencial(int numeroISOEmisorCredencial) {
		this.numeroISOEmisorCredencial = numeroISOEmisorCredencial;
	}

	public int getDigitoVerificadorAsociado() {
		return digitoVerificadorAsociado;
	}

	public void setDigitoVerificadorAsociado(int digitoVerificadorAsociado) {
		this.digitoVerificadorAsociado = digitoVerificadorAsociado;
	}

}
