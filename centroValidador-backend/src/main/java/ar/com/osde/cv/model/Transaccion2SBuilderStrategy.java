package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptoServicio.Efector;
import ar.com.osde.entities.aptoServicio.EfectorOSDE;
import ar.com.osde.entities.aptoServicio.Matricula;
import ar.com.osde.entities.transaccion.ItemPrestador;

/**
 * Builder de la transacion 2S
 * 
 * @author MT27789605
 * 
 */
public class Transaccion2SBuilderStrategy extends Transaccion2ABuilderStrategy {
    public static final String PREFIJO_RELACION = "01";

    /*
     * Consturctor con el calculador de la fecha de consumo que se pasa por
     * parametro
     */
    public Transaccion2SBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.cv.model.Transaccion2ABuilderStrategy#buildTransaccion(org
     * .dozer.Mapper, ar.com.osde.centroValidador.pos.PosMessage)
     */
    public TransaccionPrestacionPos buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        TransaccionPrestacionPos transaccionPos = super.buildTransaccion(beanMapper, posMessage);

        return transaccionPos;
    }

    /**
     * En la 2B no viene la matricula. Esos campos se usan para el efector
     */
    @Override
    protected Matricula buildMatriculaMatricula(PosMessage posMessage) {
        return null;
    }

    /**
     * El efector es obligatorio
     * 
     * @param posMessage
     * @param transaccionPos
     * @return
     */
    protected Efector buildEfector(PosMessage posMessage, TransaccionPrestacionPos transaccionPos) {
        buildFechas(posMessage, transaccionPos);
        EfectorOSDE efector = new EfectorOSDE();
        // en este caso el tipo de matricula se utiliza como filial

        efector.setCodigoFilial(posMessage.getTipoMatriculaPrestador());
        efector.setNumeroRelacion(posMessage.getTipoMatriculaPrestador().concat(PREFIJO_RELACION)
                .concat(String.format("%06d", posMessage.getNumeroMatriculaPrestador())));
        return efector;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.cv.model.Transaccion2ABuilderStrategy#
     * buildElementosParticularesItem
     * (ar.com.osde.centroValidador.pos.PosMessage,
     * ar.com.osde.cv.model.TransaccionPrestacionPos,
     * ar.com.osde.entities.transaccion.ItemPrestador)
     */
    @Override
    protected void buildElementosParticularesItem(PosMessage posMessage, TransaccionPrestacionPos transaccion, ItemPrestador p) {
        super.buildElementosParticularesItem(posMessage, transaccion, p);

        p.setEfectorGastos((EfectorOSDE) this.buildEfector(posMessage, transaccion));

    }
}
