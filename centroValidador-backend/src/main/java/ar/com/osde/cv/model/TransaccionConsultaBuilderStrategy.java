package ar.com.osde.cv.model;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.ConsultaFarmaciaPosMessage;
import ar.com.osde.entities.aptitud.TipoConsultaTransaccion;

/**
 * Estrategia de construccion para la transaccion de consulta
 * 
 * @author MT27789605
 * 
 */
public class TransaccionConsultaBuilderStrategy extends
        AbstractTransaccionPosBuilderStrategyImpl<ConsultaTransaccionPos, ConsultaFarmaciaPosMessage> {

    public TransaccionConsultaBuilderStrategy() {
        super();
    }

    public ConsultaTransaccionPos buildTransaccion(Mapper beanMapper, ConsultaFarmaciaPosMessage posMessage) {
        ConsultaTransaccionPos trx = this.buildTransaccion(beanMapper, posMessage, ConsultaTransaccionPos.class);

        return trx;
    }

    @Override
    protected ConsultaTransaccionPos buildSolicitud(AbstractPosMessage posMessage, ConsultaTransaccionPos trxPos) {
        ConsultaFarmaciaPosMessage consultaFarmaciaPosMessage = (ConsultaFarmaciaPosMessage) trxPos.getPosMessage();

        trxPos.getTransaccion().getSolicitudServicio()
                .setTipoConsultaTransaccion(this.getTipoConsultaByValue(consultaFarmaciaPosMessage.getTipoConsulta()));
        ConsultaTransaccionPos trx = super.buildSolicitud(posMessage, trxPos);
        return trx;
    }

    @Override
    protected ConsultaTransaccionPos buildFechas(AbstractPosMessage posMessage, ConsultaTransaccionPos transaccionPos) {
        ConsultaTransaccionPos consultaTransaccion = super.buildFechas(posMessage, transaccionPos);
        ConsultaFarmaciaPosMessage consultaPosMessage = (ConsultaFarmaciaPosMessage) posMessage;
        // Agregamos 1 dia y le restamos 1 milisegundo para que entren todas las
        // transacciones del dia de hoy, inclusive.
        Date fechaHastaRecuperar = null;
        if (consultaPosMessage.getFechaHastaRecuperar()== null) {
            fechaHastaRecuperar = consultaTransaccion.getFechaSolicitudServicio();
        } else {
            fechaHastaRecuperar = consultaPosMessage.getFechaHastaRecuperar().toDateTimeAtStartOfDay().plusDays(1).minusMillis(1).toDate();
        }
        // Si la fecha es mayor a fechaSolicitudServicio-1 dia, entonces le
        // poneos como fecha
        // limite el dia fechaSolicitudServicio-1
        // construyo fechaSolicitudServicio -1

        TipoConsultaTransaccion tipoConsultaTransaccion = consultaTransaccion.getTransaccionConsulta().getSolicitudServicio()
                .getTipoConsultaTransaccion();
        if (TipoConsultaTransaccion.PENDIENTE_CIERRE.equals(tipoConsultaTransaccion)) {
            fechaHastaRecuperar = buildDateForPendientesAndCierre(fechaHastaRecuperar, consultaTransaccion.getTransaccionConsulta()
                    .getSolicitudServicio().getFechaSolicitudServicio());
        } else {
            // Si la fecha es mayor a la fecha de solicitud de servicio, se le
            // pone la fecha de la solicitud
            // de servicio
            Date fechaSolicitudServicio = DateUtils.truncate(transaccionPos.getTransaccion().getSolicitudServicio()
                    .getFechaSolicitudServicio(), Calendar.HOUR);
            if (DateUtils.truncate(fechaHastaRecuperar, Calendar.HOUR).after(fechaSolicitudServicio)) {
                fechaHastaRecuperar = consultaTransaccion.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio();
            }
        }

        consultaTransaccion.getTransaccionConsulta().getSolicitudServicio().setFechaTransaccionHasta(fechaHastaRecuperar);

        return consultaTransaccion;
    }

    /**
     * Para convertir del valor que se obtiene de las transacciones del pos
     * (transacciones viejas), al enumerado de las solicitudes
     * 
     * @param value
     * @return
     */
    private TipoConsultaTransaccion getTipoConsultaByValue(String value) {
        TipoConsultaTransaccion tipoConsultaTransaccion = TipoConsultaTransaccion.PENDIENTE_CIERRE;

        if (ConsultaFarmaciaPosMessage.ACEPTADAS.equalsIgnoreCase(value)) {
            tipoConsultaTransaccion = TipoConsultaTransaccion.A;
        }
        if (ConsultaFarmaciaPosMessage.ANULADAS.equalsIgnoreCase(value)) {
            tipoConsultaTransaccion = TipoConsultaTransaccion.X;
        }

        if (ConsultaFarmaciaPosMessage.BLOQUEADAS.equalsIgnoreCase(value)) {
            tipoConsultaTransaccion = TipoConsultaTransaccion.B;
        }

        if (ConsultaFarmaciaPosMessage.CIERRE_PARTICULAR.equalsIgnoreCase(value)) {
            tipoConsultaTransaccion = TipoConsultaTransaccion.CERRADAS;
        }

        if (ConsultaFarmaciaPosMessage.PENDIENTES.equalsIgnoreCase(value)) {
            tipoConsultaTransaccion = TipoConsultaTransaccion.PENDIENTE_CIERRE;
        }
        return tipoConsultaTransaccion;
    }
}
