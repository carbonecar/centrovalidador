package ar.com.osde.cv.model;

/**
 * Los que implementan esta interfaz son aquellos que implementan el algoritmo
 * diferente seg�n cada clase.
 * 
 * @author VA27789605
 * 
 */
public interface TransaccionPosVisitor extends TransaccionSecuencialPosVisitor {

    void visit(AbstractTransaccionPos transaccionPos);

    void visit(SocioPosMessage socioPosMessage);

    void visit(AnulacionTransaccionPos anulacionTransaccionPos);

    void visit(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoPosMessage);

    void visit(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos);

    void visit(TransaccionRescatePos transaccionRescatePos);

    void visit(TransaccionIngresoEgresoPos transaccionIngresoEgresoPos);

    void visit(TransaccionTimeOutPos transaccionTimeoutPos);

    void visit(TransaccionMigracionPos transaccionMigracionPos);

    void visit(ConsultaTransaccionPos consultaTransaccionPos);

    void visit(MarcaGenericoTransaccionPos transaccionMarcaGenerico);

    void visit(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion);
}