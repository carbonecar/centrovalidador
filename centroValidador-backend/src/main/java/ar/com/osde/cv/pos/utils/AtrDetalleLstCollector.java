package ar.com.osde.cv.pos.utils;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.entities.aptitud.visitor.AtributoVisitorAdapter;

/**
 * Colector de los detalles para el atributo AtrDetalleLst
 * 
 * @author MT27789605
 * 
 */
public class AtrDetalleLstCollector extends AtributoVisitorAdapter {

    private List<String> detalles = new ArrayList<String>();

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.entities.aptitud.visitor.AtributoVisitorAdapter#visit(ar.
     * com.osde.entities.inconsistencias.AtrDetallesLst)
     */
    public void visit(ar.com.osde.entities.inconsistencias.AtrDetallesLst atrDetalleLst) {

        this.detalles.addAll(atrDetalleLst.getDet());
    }

    public List<String> getDetalles() {
        return detalles;
    }

}
