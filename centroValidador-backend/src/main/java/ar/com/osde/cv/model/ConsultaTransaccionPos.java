package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.transaccion.TransaccionConsulta;

/**
 * 
 * @author MT27789605
 * 
 */
public class ConsultaTransaccionPos extends AbstractTransaccionPos {

    private static final long serialVersionUID = 1L;
    private TransaccionConsulta transaccionConsulta;

    public TransaccionConsulta getTransaccionConsulta() {
        return transaccionConsulta;
    }

    public void setTransaccionConsulta(TransaccionConsulta transaccionConsulta) {
        this.transaccionConsulta = transaccionConsulta;
    }

    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }

    @Override
    public TransaccionConsulta getTransaccion() {
        return getTransaccionConsulta();
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return transaccionConsulta.getSolicitudServicio().getFechaSolicitudServicio();
    }

}
