package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.aptoServicio.TransaccionReferencia;
import ar.com.osde.entities.transaccion.TransaccionAnulacion;

public class AnulacionTransaccionPos extends AbstractTransaccionReferencia  {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private TransaccionAnulacion transaccionAnulacion;

    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);

    }

    public TransaccionAnulacion getTransaccionAnulacion() {
        return transaccionAnulacion;
    }

    public void setTransaccionAnulacion(TransaccionAnulacion transaccionAnulacion) {
        this.transaccionAnulacion = transaccionAnulacion;
    }

    @Override
    public TransaccionAnulacion getTransaccion() {
        return transaccionAnulacion;
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return this.transaccionAnulacion.getFechaSolicitud();
    }

    public TransaccionReferencia getTransaccionReferencia() {
        return this.transaccionAnulacion.getSolicitudServicio().getTransaccionReferencia();
    }
}
