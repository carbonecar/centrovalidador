package ar.com.osde.cv.dao;

import java.util.List;

import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;

/**
 * DAO para el manejo de los containers
 * 
 * @author MT27789605
 * 
 */
public interface ContainerDAO extends GenericCRUDDao<ContainerConfiguration> {

    /**
     * Retorna todos las configuraciones de los containers
     */
    List<ContainerConfiguration> getAll();

    /**
     * Retorna todos los containers con el nombre de grupo especificado
     * @param groupName
     * @return
     * @throws BusinessException
     */
    List<ContainerConfiguration> getAllContainersWithGroupName(final String groupName) throws BusinessException;

    /**
     * Retorna el container padre
     * @param idHandler
     * @return
     */
    ContainerConfiguration getParentContainer(final long idHandler);

}