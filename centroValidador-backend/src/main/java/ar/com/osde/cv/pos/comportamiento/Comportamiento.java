package ar.com.osde.cv.pos.comportamiento;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;

/**
 * Clase para bjar los logs y analizarlos. En funcion de los mismos se mofifican
 * las estad�sticas.
 * 
 * @author MT27789605
 * 
 */
public class Comportamiento implements Runnable {
    private static final Log LOG = LogFactory.getLog(Comportamiento.class);
    private TransaccionServiceStatistics statistics;
    private Date lastDayComputed24;
    private Date lastDayComputed26;
    private SimpleDateFormat formmatter = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public TransaccionServiceStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(TransaccionServiceStatistics statistics) {
        this.statistics = statistics;
    }

    public Comportamiento(TransaccionServiceStatistics statistics) {
        this.statistics = statistics;
    }

    public Comportamiento() {

    }

    public void setLastDayComputed(Date lastDayComputed) {

        lastDayComputed24 = lastDayComputed;
        lastDayComputed26 = lastDayComputed;
    }

    /**
     * Implementacion de runnable: paba los logs y realiza el analisis
     */
    public void run() {
        while (true) {
            String requestRespuestasp24 = "http://plnxwas24.intranet.osde:8180/logs/servlet/Checklogs?filename=log/respuestas.log&cantrow=0&rows=false&format=txt";
            String requestRespuestasp26 = "http://plnxwas26.intranet.osde:8180/logs/servlet/Checklogs?filename=/Appweb/jboss-4.3/server/OSASPOS1B/log/respuestas.log&cantrow=0&rows=false&format=txt";

            try {
                lastDayComputed24 = downloadResponses(requestRespuestasp24, lastDayComputed24);
                lastDayComputed26 = downloadResponses(requestRespuestasp26, lastDayComputed26);
            } catch (Exception e) {
                LOG.error(e);
            } finally {
                try {
                    Thread.sleep((1000 * 60 * 10));
                } catch (InterruptedException e) {
                    LOG.error(e);
                }
            }
        }

    }

    Date downloadResponses(String requestRespuestasp24, Date lastDayComputed) {
        HttpClient client = new HttpClient();

        PostMethod post = new PostMethod(requestRespuestasp24);
        String[] responseLines = new String[0];
        try {
            int statusCode = client.executeMethod(post);

            if (statusCode != HttpStatus.SC_OK) {
                LOG.debug("Error al invocar el servicio. Status code: " + statusCode + ". Status text: " + post.getStatusText());
            }
            String response = post.getResponseBodyAsString();
            responseLines = response.split("\n");

        } catch (HttpException e) {
            LOG.error("Error de violaci�n de protocolo al invocar el servicio.");

        } catch (java.io.IOException e) {
            LOG.error("Error de transporte al invocar el servicio.");
        } finally {
            post.releaseConnection();
        }

        // InputStream resource =
        // Comportamiento.class.getResourceAsStream("/respuestas.log");
        //
        // BufferedReader bf = new BufferedReader(new
        // InputStreamReader(resource));

        for (int i = 0; i < responseLines.length; i++) {
            String line = responseLines[i];
            if (line != null) {
                String[] lineSplited = line.split(" ");
                String dia = lineSplited[1];
                String hora = lineSplited[2];

                try {
                    Date dayComputed = formmatter.parse(dia + " " + hora);

                    if (lastDayComputed.before(dayComputed)) {
                        lastDayComputed = dayComputed;
                        for (int i1 = 3; i1 < lineSplited.length; i1++) {
                            if (lineSplited[i1].contains("ERROR.APTITUD")) {
                                System.out.println(line.substring(215, 295));
                                statistics.addErrorMDR(true);
                            }
                        }
                    }
                } catch (ParseException e) {
                    LOG.error(e);
                }
            }
        }
        return lastDayComputed;
    }
}
