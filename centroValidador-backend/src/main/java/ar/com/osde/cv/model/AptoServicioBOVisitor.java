package ar.com.osde.cv.model;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.pos.exception.MigracionOutRulesException;
import ar.com.osde.centroValidador.pos.exception.TimeOutRulesException;
import ar.com.osde.centroValidador.pos.exception.UnsoportedOperationException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedTransactionException;
import ar.com.osde.centroValidador.pos.handlers.HandlerTime;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

/**
 * TODO redise�ar. 
 * Es posible sacar el visitor si agregamos estrategias por tipo de transaccion. Evaluar la ventaja de este visitor
 * dado que no tiene comportamiento delegado en la clase sino un simple doble dispacher
 * @author MT27789605
 * 
 */
public class AptoServicioBOVisitor implements TransaccionPosVisitor {

    private AptoServicioBO bo;
    private RespuestaTransaccion respuestaTransaccion;
    private List<HandlerTime> handlersTime = new ArrayList<HandlerTime>();

    public AptoServicioBOVisitor(AptoServicioBO bo) {
        this.bo = bo;
    }

    public List<HandlerTime> getHandlersTime() {
        return handlersTime;
    }

    public RespuestaTransaccion getRespuestaAptitud() {
        return this.respuestaTransaccion;
    }

    public void visit(AbstractTransaccionPos transaccionPos) {
        this.handlersTime.add(bo.completarCodigoRelacion(transaccionPos));
        transaccionPos.accept(this);
    }

    public void visit(TransaccionPrestacionPos transaccion) {
        long init = System.currentTimeMillis();
        if (TransaccionFactory.isAptoServicioRegistracion(transaccion.getTransaccion().getTipoTransaccion())) {
            this.respuestaTransaccion = bo.isAptoServicioRegistracion(transaccion);
            this.handlersTime.add(new HandlerTime(AptoServicioBO.APTITUD_SERIVCE_APTO_SERVICIO, System.currentTimeMillis() - init));
        } else {
            if (TipoTransaccion.O1B.equals(transaccion.getTransaccion().getTipoTransaccion())) {
                this.respuestaTransaccion = bo.isAptoServicioValidacionSinPrestador(transaccion);
                this.handlersTime.add(new HandlerTime(AptoServicioBO.APTITUD_SERIVCE_APTO_SERVICIO, System.currentTimeMillis() - init));
            }else{
                //TODO redise�ar la clase
                if(TipoTransaccion.O1C.equals(transaccion.getTransaccion().getTipoTransaccion())){
                    this.respuestaTransaccion = bo.isAptoServicioValidacion(transaccion);
                    this.handlersTime.add(new HandlerTime(AptoServicioBO.APTITUD_SERIVCE_APTO_SERVICIO, System.currentTimeMillis() - init));
                }
            }
        }
    }

    public void visit(RegistracionMedicamentoMessage transaccion) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.isAptoRegistracionMedicamento(transaccion);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.APTITUD_SERIVCE_APTO_SERVICIO, System.currentTimeMillis() - init));

    }

    public void visit(SocioPosMessage socioPosMessage) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.isAptoSocio(socioPosMessage);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.APTITUD_SERIVCE_APTO_SOCIO, System.currentTimeMillis() - init));

    }

    public void visit(AnulacionTransaccionPos anulacionTransaccionPos) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.registrarAnulacion(anulacionTransaccionPos);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_ANULACION, System.currentTimeMillis() - init));

    }

    public void visit(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.registrarCierre(cierreFarmaciaTransaccionPos);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_CIERRE, System.currentTimeMillis() - init));
    }

    public void visit(ConsultaTransaccionPos consultaTransaccionPos) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.registrarConsulta(consultaTransaccionPos);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_CONSULTA, System.currentTimeMillis() - init));
    }

    public void visit(TransaccionRescatePos transaccionRescatePos) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.registrarRescate(transaccionRescatePos);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_REGISTRAR_RESCATE, System.currentTimeMillis() - init));
    }

    public void visit(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoPosMessage) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.registrarBloqueoDesbloqueo(bloqueoDesbloqueoPosMessage);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_REGISTRARBLOQUEODESBLOQUEO, System.currentTimeMillis()
                - init));

    }

    public void visit(TransaccionIngresoEgresoPos transaccionIngresoEgresoPos) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.informarIngresoEgreso(transaccionIngresoEgresoPos.getTransaccionIngresoEgreso());
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_INFORMARINGRESOEGRESO, System.currentTimeMillis() - init));

    }

    public void visit(TransaccionTimeOutPos transaccionTimeoutPos) {
        long init = System.currentTimeMillis();
        try {
            this.respuestaTransaccion = bo.registrarTimeOut(transaccionTimeoutPos.getTransaccionTimeOut());
            this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_REGISTRARTIMEOUT, System.currentTimeMillis() - init));
        }catch(UnsoportedOperationException uex){
            throw new UnsupportedTransactionException(uex);
        } catch (BusinessException e) {
            throw new TimeOutRulesException(transaccionTimeoutPos.getPosMessage().getOriginalMessage());
        }
    }

    public void visit(TransaccionMigracionPos transaccionMigracionPos) {
        long init = System.currentTimeMillis();
        try {
            this.respuestaTransaccion = bo.registrarMigracion(transaccionMigracionPos.getTransaccioMigracion());
            this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERIVCE_REGISTRARTIMEOUT, System.currentTimeMillis() - init));
        } catch (BusinessException e) {
            throw new MigracionOutRulesException(transaccionMigracionPos.getPosMessage().getOriginalMessage());
        }

    }

    public void visit(MarcaGenericoTransaccionPos transaccionMarcaGenerico) {
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.informarMarcaGenerico(transaccionMarcaGenerico);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_SERVICE_INFORMARMARCAGENERICO, System.currentTimeMillis() - init));

    }
    
    public void visit(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion){
        long init = System.currentTimeMillis();
        this.respuestaTransaccion = bo.envioDocumentacion(transaccionEnvioDocumentacion);
        this.handlersTime.add(new HandlerTime(AptoServicioBO.TRANSACCION_ENVIO_DOCUMENTACION, System.currentTimeMillis() - init));
    }

}
