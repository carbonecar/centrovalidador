package ar.com.osde.cv.model;

import java.util.List;
import java.util.Vector;

import ar.com.osde.entities.transaccion.ItemPrestador;

public abstract class AbstractTransaccionPrestacionPos extends SocioPosMessage {

    private static final long serialVersionUID = 1L;


    private List<ItemPrestador> prestaciones;


    public List<ItemPrestador> getPrestaciones() {
        if (prestaciones == null)
            this.buildPrestaciones();
        return prestaciones;
    }

    public void setPrestaciones(List<ItemPrestador> prestaciones) {
        this.prestaciones = prestaciones;
    }

    // TODO hacer esto bien y no asi. Tiene que ser una coleccion y no valores
    // atomicos.
    public int getTipoPrestacion1() {
        return tipoPrestacion1;
    }

    public void setTipoPrestacion1(int tipoPrestacion1) {
        this.tipoPrestacion1 = tipoPrestacion1;
    }

    public int getArancelPrestacion1() {
        return arancel1;
    }

    public void setArancelPrestacion1(int arancel1) {
        this.arancel1 = arancel1;
    }

    public int getCodigoPrestacion1() {
        return codigoPrestacion1;
    }

    public void setCodigoPrestacion1(int codigoPrestacion1) {
        this.codigoPrestacion1 = codigoPrestacion1;
    }

    public int getCantidad1() {
        return cantidad1;
    }

    public void setCantidad1(int cantidad1) {
        this.cantidad1 = cantidad1;
    }

    public int getTipoPrestacion2() {
        return tipoPrestacion2;
    }

    public void setTipoPrestacion2(int tipoPrestacion2) {
        this.tipoPrestacion2 = tipoPrestacion2;
    }

    public int getArancelPrestacion2() {
        return arancel2;
    }

    public void setArancelPrestacion2(int arancel2) {
        this.arancel2 = arancel2;
    }

    public int getCodigoPrestacion2() {
        return codigoPrestacion2;
    }

    public void setCodigoPrestacion2(int codigoPrestacion2) {
        this.codigoPrestacion2 = codigoPrestacion2;
    }

    public int getCantidad2() {
        return cantidad2;
    }

    public void setCantidad2(int cantidad2) {
        this.cantidad2 = cantidad2;
    }

    public int getTipoPrestacion3() {
        return tipoPrestacion3;
    }

    public void setTipoPrestacion3(int tipoPrestacion3) {
        this.tipoPrestacion3 = tipoPrestacion3;
    }

    public int getArancelPrestacion3() {
        return arancel3;
    }

    public void setArancelPrestacion3(int arancel3) {
        this.arancel3 = arancel3;
    }

    public int getCodigoPrestacion3() {
        return codigoPrestacion3;
    }

    public void setCodigoPrestacion3(int codigoPrestacion3) {
        this.codigoPrestacion3 = codigoPrestacion3;
    }

    public int getCantidad3() {
        return cantidad3;
    }

    public void setCantidad3(int cantidad3) {
        this.cantidad3 = cantidad3;
    }

    public int getTipoPrestacion4() {
        return tipoPrestacion4;
    }

    public void setTipoPrestacion4(int tipoPrestacion4) {
        this.tipoPrestacion4 = tipoPrestacion4;
    }

    public int getArancelPrestacion4() {
        return arancel4;
    }

    public void setArancelPrestacion4(int arancel4) {
        this.arancel4 = arancel4;
    }

    public int getCodigoPrestacion4() {
        return codigoPrestacion4;
    }

    public void setCodigoPrestacion4(int codigoPrestacion4) {
        this.codigoPrestacion4 = codigoPrestacion4;
    }

    public int getCantidad4() {
        return cantidad4;
    }

    public void setCantidad4(int cantidad4) {
        this.cantidad4 = cantidad4;
    }

    public int getFrecuenciaOCantidadPrestacion1() {
        return frecuenciaOCantidadPrestacion1;
    }

    public void setFrecuenciaOCantidadPrestacion1(int frecuenciaOCantidadPrestacion1) {
        this.frecuenciaOCantidadPrestacion1 = frecuenciaOCantidadPrestacion1;
    }

    public int getFrecuenciaOCantidadPrestacion2() {
        return frecuenciaOCantidadPrestacion2;
    }

    public void setFrecuenciaOCantidadPrestacion2(int frecuenciaOCantidadPrestacion2) {
        this.frecuenciaOCantidadPrestacion2 = frecuenciaOCantidadPrestacion2;
    }

    public int getFrecuenciaOCantidadPrestacion3() {
        return frecuenciaOCantidadPrestacion3;
    }

    public void setFrecuenciaOCantidadPrestacion3(int frecuenciaOCantidadPrestacion3) {
        this.frecuenciaOCantidadPrestacion3 = frecuenciaOCantidadPrestacion3;
    }

    public int getFrecuenciaOCantidadPrestacion4() {
        return frecuenciaOCantidadPrestacion4;
    }

    public void setFrecuenciaOCantidadPrestacion4(int frecuenciaOCantidadPrestacion4) {
        this.frecuenciaOCantidadPrestacion4 = frecuenciaOCantidadPrestacion4;
    }

    private int tipoPrestacion1;
    private int arancel1;
    private int codigoPrestacion1;
    private int cantidad1;
    private String piezaOSectorDental1;
    private String caraDental1;

    private int tipoPrestacion2;
    private int arancel2;
    private int codigoPrestacion2;
    private int cantidad2;
    private String piezaOSectorDental2;
    private String caraDental2;

    private int tipoPrestacion3;
    private int arancel3;
    private int codigoPrestacion3;
    private int cantidad3;
    private String piezaOSectorDental3;
    private String caraDental3;

    private int tipoPrestacion4;
    private int arancel4;
    private int codigoPrestacion4;
    private int cantidad4;
    private String piezaOSectorDental4;
    private String caraDental4;

    private int frecuenciaOCantidadPrestacion1;
    private int frecuenciaOCantidadPrestacion2;
    private int frecuenciaOCantidadPrestacion3;
    private int frecuenciaOCantidadPrestacion4;

    private int digitoVerficador;

    @Deprecated
    // TODO Esto es cualquiera! hay que sacaralo!
    public int getFrecuenciaOCantidadPrestacion(int i) {

        switch (i) {
        case 1:
            return frecuenciaOCantidadPrestacion1;
        case 2:
            return frecuenciaOCantidadPrestacion2;
        case 3:
            return frecuenciaOCantidadPrestacion3;

        case 4:
            return frecuenciaOCantidadPrestacion4;
        default:
            break;
        }
        return 0;
    }

    public String getPiezaOSectorDental1() {
        return piezaOSectorDental1;
    }

    public void setPiezaOSectorDental1(String piezaOSectorDental1) {
        this.piezaOSectorDental1 = piezaOSectorDental1;
    }

    public String getCaraDental1() {
        return caraDental1;
    }

    public void setCaraDental1(String caraDental1) {
        this.caraDental1 = caraDental1;
    }

    public String getPiezaOSectorDental2() {
        return piezaOSectorDental2;
    }

    public void setPiezaOSectorDental2(String piezaOSectorDental2) {
        this.piezaOSectorDental2 = piezaOSectorDental2;
    }

    public String getCaraDental2() {
        return caraDental2;
    }

    public void setCaraDental2(String caraDental2) {
        this.caraDental2 = caraDental2;
    }

    public String getPiezaOSectorDental3() {
        return piezaOSectorDental3;
    }

    public void setPiezaOSectorDental3(String piezaOSectorDental3) {
        this.piezaOSectorDental3 = piezaOSectorDental3;
    }

    public String getCaraDental3() {
        return caraDental3;
    }

    public void setCaraDental3(String caraDental3) {
        this.caraDental3 = caraDental3;
    }

    public String getPiezaOSectorDental4() {
        return piezaOSectorDental4;
    }

    public void setPiezaOSectorDental4(String piezaOSectorDental4) {
        this.piezaOSectorDental4 = piezaOSectorDental4;
    }

    public String getCaraDental4() {
        return caraDental4;
    }

    public void setCaraDental4(String caraDental4) {
        this.caraDental4 = caraDental4;
    }

    @Deprecated
    // usar la strategy
    private void buildPrestaciones() {

        this.prestaciones = new Vector<ItemPrestador>();
        if (this.codigoPrestacion1 != 0) {
            // TODO
            // VER SI LOS CAMPOS NO SON NULL Y SI ES ASI CARGAR UNA PRESTACION
            // ODONTOLOGICA, NOT NULL OR EMPTY
            // NO ESTA DEFINIDO QUE DETERMINA CUANDO UNA PRESTACION ES
            // ODONTOLOGICA O NO.
            ItemPrestador p = new ItemPrestador();
            p.setCodigoPrestacionPrestador(String.valueOf(codigoPrestacion1));
            p.setArancel(arancel1);
            p.setFrecuencia(frecuenciaOCantidadPrestacion1);
            p.setAmbito(TipoPrestacion.getTipoByValue(tipoPrestacion1).getValue());
            this.prestaciones.add(p);
        }

        if (this.codigoPrestacion2 != 0) {
            ItemPrestador p = new ItemPrestador();
            p.setCodigoPrestacionPrestador(String.valueOf(codigoPrestacion2));
            p.setArancel(arancel2);
            p.setFrecuencia(frecuenciaOCantidadPrestacion2);
            p.setAmbito(TipoPrestacion.getTipoByValue(tipoPrestacion2).getValue());
            this.prestaciones.add(p);
        }

        if (this.codigoPrestacion3 != 0) {
            ItemPrestador p = new ItemPrestador();
            p.setCodigoPrestacionPrestador(String.valueOf(codigoPrestacion3));
            p.setArancel(arancel3);
            p.setFrecuencia(frecuenciaOCantidadPrestacion3);
            p.setAmbito(TipoPrestacion.getTipoByValue(tipoPrestacion3).getValue());
            this.prestaciones.add(p);
        }

        if (this.codigoPrestacion4 != 0) {
            ItemPrestador p = new ItemPrestador();
            p.setCodigoPrestacionPrestador(String.valueOf(codigoPrestacion4));
            p.setArancel(arancel4);
            p.setFrecuencia(frecuenciaOCantidadPrestacion4);
            p.setAmbito(TipoPrestacion.getTipoByValue(tipoPrestacion4).getValue());
            this.prestaciones.add(p);
        }
    }

}
