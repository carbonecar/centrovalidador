package ar.com.osde.cv.model;

import ar.com.osde.entities.aptitud.TipoElemento;

public class Transaccion2QBuilderStrategy extends Transaccion2ABuilderStrategy {

    public Transaccion2QBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    protected TipoElemento getTipoElemento() {
        return TipoElemento.CIRUGIA;
 }
}
