package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.IPosMessage;

/**
 * Strategy de construccion de las transacciones. Las imlementaciones de esta
 * interfaz tiene la l�gica para convertir un mensaje del formato viejo (Stream)
 * al formato nuevo.
 * 
 * @author VA27789605
 * 
 */
public interface TransaccionPosBuilderStrategy<T extends AbstractTransaccionPos,E extends IPosMessage> {

    public T buildTransaccion(Mapper beanMapper, E posMessage);

}
