package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.transaccion.TransaccionEnvioDocumentacion;

public class EnvioDocumentacionTransaccionPos extends AbstractTransaccionPos implements OrdenReferenciable {

    private static final long serialVersionUID = 1L;

    private TransaccionEnvioDocumentacion transaccion;

    private Orden orden;

    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }

    public void setTransaccion(TransaccionEnvioDocumentacion transaccion) {
        this.transaccion = transaccion;
    }

    @Override
    public TransaccionEnvioDocumentacion getTransaccion() {
        return this.transaccion;
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return this.transaccion.getTransaccionBid().getFechaSolicitud();
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

}
