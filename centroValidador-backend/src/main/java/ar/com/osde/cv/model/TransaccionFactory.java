package ar.com.osde.cv.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.MigracionPosMessage;
import ar.com.osde.centroValidador.pos.TimeOutResponsePosMessage;
import ar.com.osde.centroValidador.pos.exception.UnsupportedTransactionException;
import ar.com.osde.centroValidador.pos.mq.PosMDP;
import ar.com.osde.cv.pos.utils.ExceptionFactory;
import ar.com.osde.entities.transaccion.EstadoTransaccion;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionMigracion;
import ar.com.osde.entities.transaccion.TransaccionTimeOut;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * 
 <TABLE WIDTH=535 BORDER=1 BORDERCOLOR="#000000" CELLPADDING=5 CELLSPACING=0>
 * <COL WIDTH=78> <COL WIDTH=254> <COL WIDTH=171> <THEAD>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78 BGCOLOR="#c0c0c0">
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER>
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt"><B>Transacci&oacute;n</B></FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=254 BGCOLOR="#c0c0c0">
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER>
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt"><B>Objetivo</B></FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171 BGCOLOR="#c0c0c0">
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER>
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt"><B>Validaciones</B></FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * 
 * </THEAD> <TBODY>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">01A</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificar si el asociado se encuentra habilitado a
 * recibir prestaciones en general.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * 
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">01B</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificar si el asociado se encuentra habilitado para
 * recibir una prestaci&oacute;n por parte de alg&uacute;n
 * prestador.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * 
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">01C</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificar si el asociado se encuentra habilitado para
 * recibir la prestaci&oacute;n por parte del prestador.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">01F</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Realizar
 * la validaci&oacute;n de la cobertura de recetas de farmacia.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * 
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02A</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE que el prestador efectu&oacute; la prestaci&oacute;n al
 * asociado.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * 
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02B</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE que el prestador prescribi&oacute; la prestaci&oacute;n al asociado
 * para ser ejecutada por el prestador x.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * 
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02C</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE que el prestador solicita autorizaci&oacute;n para realizar la
 * prestaci&oacute;n al asociado.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02D</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE en forma diferida que el prestador efectu&oacute; la prestaci&oacute;n
 * al asociado el una fecha determinada.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02E</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE el egreso de internaci&oacute;n.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * 
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02F</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Realizar
 * la registraci&oacute;n de los medicamentos </FONT></FONT>
 * </P>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">provistos al asociado.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * 
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02I</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE el ingreso de internaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * 
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02J</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE en forma diferida la registraci&oacute;n de los medicamentos provistos
 * al asociado.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02L</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE que el prestador efectu&oacute; una prestaci&oacute;n de
 * laboratorio.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * 
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02P</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE el protocolo quir&uacute;rgico / informe m&eacute;dico.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * autorizaci&oacute;n.</FONT></FONT>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">02Q</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE la registraci&oacute;n de cirug&iacute;as.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Verificaci&oacute;n de asociado, prestador,
 * prestaci&oacute;n.</FONT></FONT>
 * </P>
 * 
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">03A</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Consultar a OSDE la resoluci&oacute;n tomada para la
 * solicitud realizada por el prestador para efectuar la prestaci&oacute;n al
 * asociado.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * 
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">03F</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Consultar a OSDE las transacciones de farmacia para un
 * rango de fechas.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">04
 * A</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE la anulaci&oacute;n de una transacci&oacute;n 02A, 02B, 02C, 02D, 02F,
 * 02J</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * 
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">04B</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE el bloqueo de una transacci&oacute;n de farmacia para evitar que la
 * misma se incluya en un cierre.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * 
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">04D</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Informar
 * a OSDE el desbloqueo de una transacci&oacute;n bloqueada de farmacia para que
 * la misma se incluya en el pr&oacute;ximo cierre.</FONT></FONT>
 * </P>
 * </TD>
 * 
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">05A Esta
 * transaccion no esta siquiera en el as400</FONT></FONT>
 * </P>
 * 
 * </TD>
 * <TD WIDTH=254>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">Consultar a OSDE el env&iacute;o de informaci&oacute;n
 * disponible</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <BR>
 * </P>
 * 
 * </TD>
 * </TR>
 * <TR VALIGN=TOP>
 * <TD WIDTH=78>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1
 * STYLE="font-size: 8pt">05F</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=254>
 * 
 * <P LANG="es-ES" CLASS="western" STYLE="font-weight: normal">
 * <FONT FACE="Verdana, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">Realizar
 * el cierre de tr&aacute;mites de farmacia.</FONT></FONT>
 * </P>
 * </TD>
 * <TD WIDTH=171>
 * <P LANG="es-ES" CLASS="western" ALIGN=CENTER STYLE="font-weight: normal">
 * <BR>
 * </P>
 * </TD>
 * </TR>
 * 
 * </TBODY>
 * </TABLE>
 * 
 * @author VA27789605
 * 
 */
public final class TransaccionFactory {

    public static final String FARMACIA = "F";
    public static final String FARMACIA_DIFERIDO = "J";
    private static final String LABORATORIO = "L";
    public static final String TIPO_CUATRO = "4";
    // TODO ver si se puede usar tipoTransaccion
    public static final CodigoTransaccion O2F = new CodigoTransaccion(2, FARMACIA);
    public static final CodigoTransaccion O2L = new CodigoTransaccion(2, LABORATORIO);
    public static final CodigoTransaccion O1F = new CodigoTransaccion(1, FARMACIA);
    public static final CodigoTransaccion O2J = new CodigoTransaccion(2, FARMACIA_DIFERIDO);
    public static final CodigoTransaccion O1A = new CodigoTransaccion(1, "A");
    public static final CodigoTransaccion O1B = new CodigoTransaccion(1, "B");
    public static final CodigoTransaccion O1C = new CodigoTransaccion(1, "C");
    public static final CodigoTransaccion O2A = new CodigoTransaccion(2, "A");
    public static final CodigoTransaccion O2D = new CodigoTransaccion(2, "D");
    public static final CodigoTransaccion O4A = new CodigoTransaccion(4, "A");
    public static final CodigoTransaccion O5F = new CodigoTransaccion(5, "F");
    public static final CodigoTransaccion O4G = new CodigoTransaccion(4, "G");
    // Transacciones de autorizacion
    public static final CodigoTransaccion O2B = new CodigoTransaccion(2, "B");
    public static final CodigoTransaccion O2C = new CodigoTransaccion(2, "C");

    // Transacciones de consulta
    public static final CodigoTransaccion O3A = new CodigoTransaccion(3, "A");
    public static final CodigoTransaccion O3F = new CodigoTransaccion(3, "F");

    // Transacciones de bloqueo desbloqueo
    public static final CodigoTransaccion O4B = new CodigoTransaccion(4, "B");
    public static final CodigoTransaccion O4D = new CodigoTransaccion(4, "D");

    // Transaccion de ingreso/egreso
    public static final CodigoTransaccion O2I = new CodigoTransaccion(2, "I");
    public static final CodigoTransaccion O2E = new CodigoTransaccion(2, "E");

    // Transacciones de cirugia
    public static final CodigoTransaccion O2Q = new CodigoTransaccion(2, "Q");
    /**
     * La transaccion 2P es el envio de documentacion adjunta
     */
    public static final CodigoTransaccion O2P = new CodigoTransaccion(2, "P");
    public static final CodigoTransaccion O2S = new CodigoTransaccion(2, "S");
    @SuppressWarnings("rawtypes")
    private final Map<CodigoTransaccion, TransaccionPosBuilderStrategy> builderStrategies;
    private static TransaccionFactory INSTANCE = new TransaccionFactory();

    @SuppressWarnings("rawtypes")
    private TransaccionFactory() {
        builderStrategies = new HashMap<CodigoTransaccion, TransaccionPosBuilderStrategy>();
        builderStrategies.put(O1A, new Transaccion1ABuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O1B, new Transaccion2ABuilderStrategy(new FechaConsumoCalculadorValidacionImpl()));
        builderStrategies.put(O1C, new Transaccion2ABuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O1F, new Transaccion2FBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));

        // Transacciones de farmacia
        builderStrategies.put(O2F, new Transaccion2FBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2J, new Transaccion2JBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O5F, new TransaccionCierreBuilderStrategy());
        builderStrategies.put(O3F, new TransaccionConsultaBuilderStrategy());

        builderStrategies.put(O4G, new TransaccionMarcaGenericoBuilderStrategy());

        builderStrategies.put(O2A, new Transaccion2ABuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2L, new Transaccion2LBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2B, new Transaccion2BBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2D, new Transaccion2DBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O4A, new TransaccionAnulacionBuilderStrategy());

        builderStrategies.put(O3A, new TransaccionRescateBuilderStrategy());

        // Autorizaciones
        builderStrategies.put(O2C, new Transaccion2ABuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2B, new Transaccion2BBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));
        builderStrategies.put(O2S, new Transaccion2SBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));

        // builders para transaccones de bloqueo y desbloqueo
        builderStrategies.put(O4B, new TransaccionBloqueoBuilderStrategy());
        builderStrategies.put(O4D, new TransaccionBloqueoBuilderStrategy());

        // builder para la transaccion de ingreso/egreso
        builderStrategies.put(O2I, new Transaccion2IBuilderStrategy(new FechaConsumoCalculadorIngresoEgresoImpl()));
        builderStrategies.put(O2E, new Transaccion2IBuilderStrategy(new FechaConsumoCalculadorIngresoEgresoImpl()));

        // builder para la transaccion de cirugia
        builderStrategies.put(O2Q, new Transaccion2QBuilderStrategy(new FechaConsumoCalculadorRegistracionImpl()));

        // Builder para la transaccion de protocolo quirurgico
        builderStrategies.put(O2P, new Transaccion2PBuilderStrategy());

    }

    public static final TransaccionFactory getInstance() {
        return INSTANCE;
    }

    public static boolean isAptoServicioRegistracion(TipoTransaccion tipoTransaccion) {
        return (TipoTransaccion.O2A.equals(tipoTransaccion) || TipoTransaccion.O2Q.equals(tipoTransaccion)
                || TipoTransaccion.O2C.equals(tipoTransaccion) || TipoTransaccion.O2B.equals(tipoTransaccion)
                || TipoTransaccion.O2D.equals(tipoTransaccion) || TipoTransaccion.O2S.equals(tipoTransaccion) || TipoTransaccion.O2L
                .equals(tipoTransaccion));
    }

    @Deprecated
    // usar TipoTransaccion.generaAutorizacion()
    public boolean generaAutorizacion(Transaccion transaccion) {
        return transaccion.getTipoTransaccion().generaAutorizacion();
    }

    public AbstractTransaccionPos createTransaccion(AbstractPosMessage posMessage, Mapper beanMapper) {
        int codigo = posMessage.getCodigoTransaccion();
        String atributo = posMessage.getAtributoCodigoTransaccion().trim();

        atributo = atributo.toUpperCase();

        CodigoTransaccion codigoTransaccion = new CodigoTransaccion(codigo, atributo.toUpperCase());
        @SuppressWarnings("unchecked")
        TransaccionPosBuilderStrategy builderStrategy = builderStrategies.get(codigoTransaccion);
        ExceptionFactory.createTipoAtributoInvalido(builderStrategy == null, "Transaccion:  " + codigoTransaccion + " no implementada");
        return builderStrategy.buildTransaccion(beanMapper, posMessage);
        // 2c y 2f hay que implementarlas
        // if (codigoTransaccion.getCodigo() == 2 &&
        // "C".equalsIgnoreCase(codigoTransaccion.getAtributo())) {
        // // TODO parsear la transaccion al modelo nuevo
        // }

    }

    public TransaccionMigracionPos createTransaccionMigracion(AbstractPosMessage posMessage, Mapper beanMapper,
            MigracionPosMessage timeOutResponsePosMessage) {
        return (TransaccionMigracionPos) this.createTransaccion(posMessage, beanMapper, timeOutResponsePosMessage,
                new TransaccionMigracionPosFactory());
    }

    public TransaccionTimeOutPos createTransaccion(AbstractPosMessage posMessage, Mapper beanMapper,
            TimeOutResponsePosMessage timeOutResponsePosMessage) {
        return this.createTransaccion(posMessage, beanMapper, timeOutResponsePosMessage, new TransaccionTimeOutPosFactory());
    }

    private TransaccionTimeOutPos createTransaccion(AbstractPosMessage posMessage, Mapper beanMapper,
            TimeOutResponsePosMessage timeOutResponsePosMessage, TransaccionTimeOutPosFactory transaccionTimeoutPosFactory) {
        AbstractTransaccionPos transaccionPos = TransaccionFactory.getInstance().createTransaccion(posMessage, beanMapper);

        TransaccionTimeOut transaccionTimeOut = transaccionTimeoutPosFactory.createTransaccion();
        try {
            transaccionTimeOut.setEstadoTransaccionRespondidaTimeOut(EstadoTransaccion.OK.getByCode(timeOutResponsePosMessage
                    .getRespuestaTransaccion()));
        } catch (BusinessException e) {
            transaccionTimeOut.setEstadoTransaccionRespondidaTimeOut(EstadoTransaccion.RECHAZADA);
        }

        try {
            transaccionTimeOut.setGeneradorRespuesta(Integer.parseInt(timeOutResponsePosMessage.getGeneradorRespuesta()));
        } catch (NumberFormatException e) {
            transaccionTimeOut.setGeneradorRespuesta(0);
            this.getLog().info("El atributo generador de respuesta no es numerico. Se define a 0 por default");
        }
        transaccionTimeOut.setTrasaccionRespondidaTimeOut(transaccionPos.getTransaccion());
        TransaccionTimeOutPos transaccionTimeOutPos = transaccionTimeoutPosFactory.create(transaccionPos, transaccionTimeOut);

        return transaccionTimeOutPos;
    }

    public class TransaccionTimeOutPosFactory {
        private TransaccionTimeOutPosFactory() {

        }

        public TransaccionTimeOutPos create(AbstractTransaccionPos transaccionPos, TransaccionTimeOut transaccionTimeOut) {

            return new TransaccionTimeOutPos(transaccionPos, transaccionTimeOut);
        }

        public TransaccionTimeOut createTransaccion() {
            return new TransaccionTimeOut();
        }

    }

    private class TransaccionMigracionPosFactory extends TransaccionTimeOutPosFactory {
        @Override
        public TransaccionMigracionPos create(AbstractTransaccionPos transaccionPos, TransaccionTimeOut transaccionTimeOut) {

            if (transaccionTimeOut instanceof TransaccionMigracion) {
                return new TransaccionMigracionPos(transaccionPos, (TransaccionMigracion) transaccionTimeOut);
            } else {
                throw new UnsupportedTransactionException();
            }
        }

        public TransaccionMigracion createTransaccion() {
            return new TransaccionMigracion();
        }
    }

    private Log getLog() {
        return LogFactory.getLog(PosMDP.class);
    }
}
