package ar.com.osde.cv.model;

import java.io.Serializable;

//TODO RENAME A AMBITO
public enum TipoPrestacion implements Serializable {

	AMBULATORIO(1), INTERNACION_CLINICA(2), INTERNACION_QUIRURGICA(3), DOMICILIO(
			4);

	private int value;

	private TipoPrestacion(int value) {
		this.value = value;
	}

	public static final TipoPrestacion getTipoByValue(int value){
		switch (value) {
		case 1:
			return AMBULATORIO;
		case 2:
			return INTERNACION_CLINICA;
		case 3:
			return INTERNACION_QUIRURGICA;
		case 4:
			return DOMICILIO;
		default:
			return null;
		}
	}

	public int getValue() {
		return this.value;
	}
}
