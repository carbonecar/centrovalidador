package ar.com.osde.cv.model;

@Deprecated
public enum CodigoRespuestaTransaccion {

	OK("00"),
	FAIL("01"),	
	RESPUESTA_PARCIAL("99"),
	GENERADOR_RESPUESTA_PROPIO("98");
	
	private String codigo;
	 CodigoRespuestaTransaccion(String codigo){
		 this.codigo=codigo;
	 }
	 
	public String getCodigo(){
		return this.codigo;
	}
	 
}
