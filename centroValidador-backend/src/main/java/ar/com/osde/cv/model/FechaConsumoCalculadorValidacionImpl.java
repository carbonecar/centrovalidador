package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.centroValidador.pos.PosMessage;

public class FechaConsumoCalculadorValidacionImpl implements FechaConsumoCalculador {

    public Date buildFechaConsumo(PosMessage posMessage, TransaccionPos transaccion) {
        return transaccion.getTransaccionConsumo().getFechaSolicitud();
    }

}
