package ar.com.osde.cv.model;

import java.util.Date;

import org.joda.time.LocalDate;

import ar.com.osde.centroValidador.pos.IngresoEgresoPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;

/**
 * Calculador de la fecha de consumo para las transacciones de Ingreso y egreso
 * @author MT27789605
 *
 */
public class FechaConsumoCalculadorIngresoEgresoImpl implements FechaConsumoCalculador {

    public FechaConsumoCalculadorIngresoEgresoImpl() {

    }

    public Date buildFechaConsumo(PosMessage posMessage, TransaccionPos transaccion) {

        Date fechaConsumo = null;
        if (posMessage instanceof IngresoEgresoPosMessage) {
            IngresoEgresoPosMessage ingresoEgresoPossMessage = (IngresoEgresoPosMessage) posMessage;
            LocalDate fechaOfflineLocalDate = ingresoEgresoPossMessage.getFechaPracticaOffLine();
            if (fechaOfflineLocalDate != null) {
                if (ingresoEgresoPossMessage.getHoraPractica() != null) {
                    fechaConsumo = fechaOfflineLocalDate.toDateTime(ingresoEgresoPossMessage.getHoraPractica()).toDate();
                } else {
                    if (ingresoEgresoPossMessage.getHoraTransaccion() == null) {
                        fechaConsumo = fechaOfflineLocalDate.toDateMidnight().toDate();
                    } else {
                        fechaConsumo = fechaOfflineLocalDate.toDateTime(ingresoEgresoPossMessage.getHoraTransaccion()).toDate();
                    }
                }

            } else {
                fechaOfflineLocalDate = ingresoEgresoPossMessage.getFechaTransaccion();
                if (ingresoEgresoPossMessage.getHoraPractica() == null) {
                    fechaConsumo = (transaccion.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());
                } else {
                    fechaConsumo = fechaOfflineLocalDate.toDateTime(ingresoEgresoPossMessage.getHoraPractica()).toDate();
                }
            }
        }
        return fechaConsumo;
    }

}
