package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionTimeOut;

/**
 * 
 * Transccion de timeout correspondiente al modelo del pos. Decora la transaccion de timeout del modelo de 
 * transacciones y la transaccionPos
 * @author MT27789605
 *
 */
public class TransaccionTimeOutPos extends AbstractTransaccionPos {

    private static final long serialVersionUID = 1L;
    /**
     * Transaccion de timeOut correspondiente al modelo  de transacciones
     */
    private TransaccionTimeOut transaccionTimeOut;

    /**
     * TransaccionPos. Decora la transaccion que se quiere impactar como real.
     */
    private AbstractTransaccionPos transaccionPosTimeOut;
    
    public TransaccionTimeOutPos(){
        
    }
    
    
    public AbstractTransaccionPos getTransaccionPosTimeOut() {
        return transaccionPosTimeOut;
    }


    public TransaccionTimeOutPos(AbstractTransaccionPos transaccionPosTimeOut,TransaccionTimeOut transaccionTimeOut) {
        this.transaccionPosTimeOut=transaccionPosTimeOut;
        this.transaccionTimeOut=transaccionTimeOut;
    }

    
    @Override
    public long getId() {
       return this.transaccionPosTimeOut.getId();
    }
    
    @Override
    public Secuencia getSecuencia() {
        return this.transaccionPosTimeOut.getSecuencia();
    }

    @Override
    public IPosMessage getPosMessage() {
        return this.transaccionPosTimeOut.getPosMessage();
    }
    
    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }

    public TransaccionTimeOut getTransaccionTimeOut() {
        return transaccionTimeOut;
    }

    public void setTransaccionTimeOut(TransaccionTimeOut transaccionTimeOut) {
        this.transaccionTimeOut = transaccionTimeOut;
    }

    @Override
    public Transaccion getTransaccion() {
        return this.transaccionTimeOut.getTrasaccionRespondidaTimeOut();
    }

    @Deprecated
    @Override
    public Date getFechaSolicitudServicio() {
        return this.getTransaccion().getFechaSolicitud();
    }

}
