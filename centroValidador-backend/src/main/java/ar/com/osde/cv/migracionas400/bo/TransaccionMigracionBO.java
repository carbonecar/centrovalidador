package ar.com.osde.cv.migracionas400.bo;

import ar.com.osde.framework.business.exception.BusinessException;

/**
 * BO para las transacciones de migración
 * 
 * @author MT27789605
 * 
 */
public interface TransaccionMigracionBO {

    void migrarTransacciones(String codigoFilial) throws BusinessException;

    int crearMensajesMigracion(String codigoFilial);

    int eliminarMensajesMigracion(String codigoFilial);
    
}
