package ar.com.osde.cv.model;

import java.io.Serializable;
import java.util.Date;

import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.entities.aptoServicio.PrestadorTransaccionador;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.transaccionService.services.GeneradorRespuesta;

public abstract class AbstractTransaccionPos implements Serializable, TransaccionVisitable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Referencial al mensaje orignalParseado. Esto se hace a efectos de debugin
     */
    private IPosMessage posMessage;

    private Secuencia secuencia;

    private String generadorRespuesta =String.valueOf(GeneradorRespuesta.OSDE.getCodigo());

    public String getGeneradorRespuesta() {
        return generadorRespuesta;
    }

    public void setGeneradorRespuesta(String generadorRespuesta) {
        this.generadorRespuesta = generadorRespuesta;
    }

    public IPosMessage getPosMessage() {
        return posMessage;
    }

    public void setPosMessage(IPosMessage posMessage) {
        this.posMessage = posMessage;
    }

    public Secuencia getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Secuencia secuencia) {
        this.secuencia = secuencia;
    }

    public abstract Transaccion getTransaccion();

    public abstract Date getFechaSolicitudServicio();

    @Deprecated
    // usar el getTransaccion.getPrestadorTransaccionador
    public PrestadorTransaccionador getPrestador() {
        return this.getTransaccion().getPrestadorTransaccionador();
    }

    /**
     * Genera un indentificador un�voco en base a la clave de la transaccion.
     * 
     * @return
     */
    public long getId() {
        return Long.valueOf(getTransaccion().getTransaccionBid().hashCode());

    }
}
