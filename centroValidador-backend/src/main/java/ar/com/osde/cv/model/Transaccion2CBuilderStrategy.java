package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.transaccion.ItemPrestador;

public class Transaccion2CBuilderStrategy extends
        AbstractPrestacionTransaccionPosBuilderStrategyImpl<TransaccionPrestacionPosOdonto, PosMessage> {

    public Transaccion2CBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public TransaccionPrestacionPosOdonto buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        TransaccionPrestacionPosOdonto trx = super.buildTransaccion(beanMapper, posMessage, TransaccionPrestacionPosOdonto.class);
        this.buildPrestaciones((PosMessage) posMessage, trx);
        // en este caso el tipo de matricula se utiliza como filial
        // efector.setCodigoPrestador(posMessage.getNumeroMatriculaPrestador());
        // efector.setFilial(filial);
        // TODO revisar de unificar con una 2A. Que
        // TransaccionPrestacionPosOdonto herede de TransaccionPrestacionPos

        return trx;
    }

    /**
     * Para las transacciones 2C no hay autorizacion sino que el tipo de
     * tratamiento odontologico.
     */
    @Override
    protected void buildElementosParticularesItem(PosMessage posMessage,TransaccionPrestacionPosOdonto transaccion, ItemPrestador p) {
        // Este dato no se utiliza actualmente
        transaccion.getTipoTratamientoOdontologico();
        p.setTipoElemento(this.getTipoElemento());
        
    }

    @Override
    protected TipoElemento getTipoElemento() {
        return TipoElemento.ODONTOLOGIA;
    }
}
