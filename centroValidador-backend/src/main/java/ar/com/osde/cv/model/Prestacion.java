package ar.com.osde.cv.model;

/**
 * 
 * @deprecated //cambia por ItemPrestador
 * @author VA27789605
 * 
 */
public class Prestacion {
	// el arancel se refiere a los honorarios.
	private int arancel;
	private int codigoPrestacion;
	private int frecuencia;
	
	
	
	private TipoPrestacion tipoPrestacion;

	public int getArancel() {
		return arancel;
	}

	public void setArancel(int arancel) {
		this.arancel = arancel;
	}

	public int getCodigoPrestacion() {
		return codigoPrestacion;
	}

	public void setCodigoPrestacion(int codigoPrestacion) {
		this.codigoPrestacion = codigoPrestacion;
	}

	public int getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}

	public TipoPrestacion getTipoPrestacion() {
		return tipoPrestacion;
	}

	public void setTipoPrestacion(TipoPrestacion tipoPrestacion) {
		this.tipoPrestacion = tipoPrestacion;
	}

}
