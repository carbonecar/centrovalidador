package ar.com.osde.cv.model;

/**
 * Representacion de los programas especiales.
 * 
 * @author MT27789605
 * 
 */
public enum ProgramaEspecial {

    CRONICO("C", "CX"), MATERNO_INFANTIL("M", "MI"), MATERNO_INFANTIL_CRONICO("X", "MX");
    private String codigo;
    private String codigoTraducido;

    private ProgramaEspecial(String codigo, String codigoTraducido) {
        this.codigo = codigo;
        this.codigoTraducido = codigoTraducido;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getCodigoTraducido() {
        return this.codigoTraducido;
    }
}
