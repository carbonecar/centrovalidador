package ar.com.osde.cv.dao;

import java.util.List;

import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.framework.business.crud.GenericCRUDBOImpl;
import ar.com.osde.framework.business.exception.BusinessException;

public interface CodigoExceptuableDAO {

    /**
     * @see GenericCRUDBOImpl.saveNew
     */
    public abstract void saveNew(CodigoExceptuable codigoExceptuable) throws BusinessException;

    /**
     * @see GenericCRUDBOImpl.update
     * @param codigoExceptuable
     * @throws BusinessException
     */
    public abstract void update(CodigoExceptuable codigoExceptuable) throws BusinessException;

    /**
     * @see GenericCRUDBOImpl.getAll
     * @return
     * @throws BusinessException
     */
    public abstract List<CodigoExceptuable> getAllCodigos() throws BusinessException;

    /**
     * @see GenericCRUDBOImpl.delete
     * @param codigoExceptuable
     * @throws BusinessException
     */
    public abstract void deleteContainer(CodigoExceptuable codigoExceptuable) throws BusinessException;

    public abstract GenericCRUDBOImpl<CodigoExceptuable> getGenericCrud();

    public abstract void setGenericCrud(GenericCRUDBOImpl<CodigoExceptuable> genericCrud);

    public abstract CodigoExceptuable getById(Long id) throws BusinessException;

}