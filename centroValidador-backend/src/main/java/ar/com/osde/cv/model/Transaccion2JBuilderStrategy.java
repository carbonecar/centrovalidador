package ar.com.osde.cv.model;

import java.util.HashMap;
import java.util.Map;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessageFarmacia;

public class Transaccion2JBuilderStrategy extends Transaccion2FBuilderStrategy {

    private Map<String, ProgramaEspecial> codigoCronicoConverter = new HashMap<String, ProgramaEspecial>();

    public Transaccion2JBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
        codigoCronicoConverter.put(ProgramaEspecial.MATERNO_INFANTIL.getCodigo(), ProgramaEspecial.MATERNO_INFANTIL);
        codigoCronicoConverter
                .put(ProgramaEspecial.MATERNO_INFANTIL_CRONICO.getCodigo(), ProgramaEspecial.MATERNO_INFANTIL_CRONICO);
        codigoCronicoConverter.put(ProgramaEspecial.CRONICO.getCodigo(), ProgramaEspecial.CRONICO);
    }

    public RegistracionMedicamentoMessage buildTransaccion(Mapper beanMapper, PosMessageFarmacia posMessage) {
        RegistracionMedicamentoMessage trx = super.buildTransaccion(beanMapper, posMessage);
        ProgramaEspecial programaEspecial=null;
        if(posMessage.getCoberturaEspecialOCondicionDelSocio()!=null){
            programaEspecial = this.codigoCronicoConverter.get(posMessage.getCoberturaEspecialOCondicionDelSocio().trim());
        }
        if (programaEspecial != null) {
            trx.getTransaccionConsumo().getSolicitudServicio().setCoberturaEspecial(programaEspecial.getCodigoTraducido());
        }
        return trx;
    }
}
