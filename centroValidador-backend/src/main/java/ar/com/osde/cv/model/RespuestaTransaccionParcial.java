package ar.com.osde.cv.model;

import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.MensajePos;
import ar.com.osde.entities.transaccion.RespuestaTransaccionVisitor;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

/**
 * Modela la respuesta parcial que se da en transacciones que son partidas
 */
public class RespuestaTransaccionParcial extends RespuestaTransaccion {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public RespuestaTransaccionParcial(RespuestaAptitud respuestaAptitud, MensajePos mensajePos) {
        this.setRespuestaAptitud(respuestaAptitud);
        this.setMensajePos(mensajePos);
        this.setGeneradorRespuesta(98);
    }

    public void accept(RespuestaTransaccionVisitor respuestaTransaccionVisitor) {

        
    }

}
