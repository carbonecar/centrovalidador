package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.aptoServicio.BeneficiarioBid;
import ar.com.osde.entities.aptoServicio.Diagnostico;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionConsumo;

/**
 * Objecto que representa una transaccion recibida desde el pos. Canoniza!!???
 * El modelo de transacciones.
 * 
 * 
 * // datos de identificacion univoca. (TransaccionConsumo.TerminalID+ //
 * nrotrx+fechaSolicitudServicio
 * 
 * @author VA27789605
 * 
 */
public class TransaccionPos extends AbstractTransaccionPos {
    private static final long serialVersionUID = 20100501L;

    private TransaccionConsumo transaccionConsumo;

    // Dato de la credencial magnetica.
    private int nroIsoCredencial;

    private Diagnostico diagnosticoPrincipal;
    private Diagnostico diagnosticoSecundario;
    // datos de la horden
    private int nroOrden;

    public TransaccionPos() {

    }

    public TransaccionConsumo getTransaccionConsumo() {
        return transaccionConsumo;
    }

    public void setTransaccionConsumo(TransaccionConsumo transaccionConsumo) {
        this.transaccionConsumo = transaccionConsumo;
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return this.getTransaccionConsumo().getSolicitudServicio().getFechaSolicitudServicio();
    }

    @Override
    public Transaccion getTransaccion() {
        return this.transaccionConsumo;
    }

    public BeneficiarioBid getBeneficiarioBid() {
        BeneficiarioBid beneficiarioBid = this.transaccionConsumo.getSolicitudServicio().getBeneficiarioBid();
        if (beneficiarioBid == null) {
            beneficiarioBid = this.transaccionConsumo.getSolicitudServicio().getBeneficiarioProvisorio();
        }
        return beneficiarioBid;
    }

    public Diagnostico getDiagnosticoPrincipal() {
        return diagnosticoPrincipal;
    }

    public void setDiagnosticoPrincipal(Diagnostico diagnosticoPrincipal) {
        this.diagnosticoPrincipal = diagnosticoPrincipal;
    }

    public Diagnostico getDiagnosticoSecundario() {
        return diagnosticoSecundario;
    }

    public void setDiagnosticoSecundario(Diagnostico diagnosticoSecundario) {
        this.diagnosticoSecundario = diagnosticoSecundario;
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(int nroOrden) {
        this.nroOrden = nroOrden;
    }

    @Deprecated
    public int getNroIsoCredencial() {
        return this.nroIsoCredencial;
    }

    public void setNroIsoCredencial(int nroIsoCredencial) {
        this.nroIsoCredencial = nroIsoCredencial;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.TransaccionVisitable#accept(ar.com.
     * osde.centroValidador.model.TransaccionPosVisitor)
     */
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        StringBuilder msn = new StringBuilder();
        TerminalID terminalId = getPrestador().getTerminalID();
        msn.append("Codigo Operador: " + terminalId.getCodigoOperador()).append("Filial Pos: " + terminalId.getCodigoFilial())
                .append("Agrupador: " + terminalId.getAgrupador()).append("Nro Pos: " + terminalId.getNumero())
                .append("Nro Trx: " + transaccionConsumo.getNumeroTransaccion())
                .append("Fecha Solicitud" + transaccionConsumo.getSolicitudServicio().getFechaSolicitudServicio()).append("secuencia: ")
                .append(getSecuencia().toString());
        return msn.toString();
    }

}
