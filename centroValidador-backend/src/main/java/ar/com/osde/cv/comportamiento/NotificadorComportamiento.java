package ar.com.osde.cv.comportamiento;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.common.webService.DespachadorMensajes;
import ar.com.osde.common.webService.Mensaje;

/**
 * Se encargada de notificar el comportamiento diario y anomalias que se
 * detectan.
 * 
 * @author MT27789605
 * 
 */
public class NotificadorComportamiento {

    private DespachadorMensajes despachadorMensajesService;

    private Map<String, Hechos> eventoHechos = new HashMap<String, Hechos>();

    private String mails;
    private String environment;
    
    
    /**
     * Mails para reportar un incidente
     */
    private String mailsSoporte;

    
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public NotificadorComportamiento() {
        eventoHechos.put(Hechos.TIME_OUT_FORZADO, new Hechos(Hechos.TIME_OUT_FORZADO));
        eventoHechos.put(Hechos.ERROR_MDR, new Hechos(Hechos.ERROR_MDR));
        eventoHechos.put(Hechos.SOAP_FAULT, new Hechos(Hechos.SOAP_FAULT));
        eventoHechos.put(Hechos.TIMEOUT_RECIBIDO,new Hechos(Hechos.TIMEOUT_RECIBIDO));
    }

    // agregar un mapa para poder evaluar segun el hecho ocurrido
    public DespachadorMensajes getDespachadorMensajesService() {
        return despachadorMensajesService;
    }

    public void setDespachadorMensajesService(DespachadorMensajes despachadorMensajesService) {
        this.despachadorMensajesService = despachadorMensajesService;
    }

    public String getMails() {
        return mails;
    }

    public void setMails(String mails) {
        this.mails = mails;
    }

    public String getMailsSoporte() {
        return mailsSoporte;
    }

    public void setMailsSoporte(String mailsSoporte) {
        this.mailsSoporte = mailsSoporte;
    }

    public Map<String, Hechos> getEventoHechos() {
        return eventoHechos;
    }

    public synchronized void notificar(TransaccionServiceStatistics statistics) {
        this.notificar(statistics,"");
    }

    public  void notificar(TransaccionServiceStatistics transaccionServiceStatistics, List<String> serviciosTimeout) {
        StringBuffer strBuffer=new StringBuffer(100);
        for (String servicioFallido : serviciosTimeout) {
            strBuffer.append(servicioFallido).append(", ");
        }
        this.notificar(transaccionServiceStatistics,strBuffer.toString());
    }
    
    
    private synchronized void notificar(TransaccionServiceStatistics statistics,String serviciosFallidos) {

        long cant = statistics.getHCantidadTransaccionesTimeOutForzado();
        Hechos hecho = this.eventoHechos.get(Hechos.TIME_OUT_FORZADO);
        evaluarHecho(cant, hecho, "timeout forzado (codigo 9999) "+serviciosFallidos, false);

        cant = statistics.getCantErrorMDR();
        hecho = this.eventoHechos.get(Hechos.ERROR_MDR);
        evaluarHecho(cant, hecho, "ERROR MDR ", false);  

        cant = statistics.getSoapFault();
        hecho = this.eventoHechos.get(Hechos.SOAP_FAULT);
        evaluarHecho(cant, hecho, "SOAP_FAULT ", false);

        cant=statistics.getCantTotalTransaccionesAtendidasTimeOut();
        long cantTotalTransacciones=statistics.getACantTotalTransaccionesAtendidas();
        hecho=this.eventoHechos.get(Hechos.TIMEOUT_RECIBIDO);
        //ver como se elimina el dia anterior o si reseteamos el contador o acumulamos por dia.
        
        evaluarHechoTimout(cant,hecho,"TIMEOUT RECIBIDO",false);
        
        
    }
    private void evaluarHechoTimout(long cantidadTimeout, Hechos hecho, String string, boolean b) {
       
        
    }

    private void evaluarHecho(long cant, Hechos hecho, String mensaje, boolean notificaSoporte) {
        if (cant != hecho.getCant()) {
            if (cant == hecho.getCant()) {
                // ya se hizo la notificacion.
                return;
            }

            if (cant < hecho.getCant()) {
                // se hizo un reinicio. Asi que actualizo el contador. Esta
                // forma de conteo hay que mejorarla
                hecho.setCant(cant);
            }

            if (cant > hecho.getCant()) {
                // ocurrio un nuevo time out, me fijo que el intervalo sea mayor
                // a 10' o bien sea el primero
                Date fechaActual = new Date();
                long milisegundos = fechaActual.getTime() - hecho.getFechaUltimaNotification().getTime();
                long minutos = ((milisegundos / 1000) / 60);

                if (minutos >= 10) {
                    // ahora notificamos.
                    hecho.setFechaUltimaNotification(new Date());
                    String msg = "Ocurrio un " + mensaje + " y en los " + minutos + " minutos desde la ultima notificacion : "
                            + hecho.getCantInLastInterval() + " errores ";
                    hecho.setCantInLastInterval(0);
                    hecho.setCant(cant);
                    this.enviarMail(this.buildMessage(msg, notificaSoporte));

                } else {
                    // si fue menor a 10 minutos pero la cantidad es "grande"
                    hecho.addCantInLastInterval();
                    // enviamos un mensaje de criticidad;
                    if (hecho.getCantInLastInterval() > 20) {
                        // enviamos un mensaje de criticidad;
                        String msg = "Durante los ultimos " + minutos + "' 20 transacciones dieron " + mensaje
                                + ". Verifique que todos los servicios funcionen correctamente";
                        this.enviarMail(this.buildMessage(msg, notificaSoporte));
                        hecho.setCant(cant);
                        hecho.setCantInLastInterval(0);
                    }
                }
            }
        }
    }

    private void enviarMail(final Mensaje message) {
        ExecutorService executor= Executors.newFixedThreadPool(1);
        executor.execute(new Runnable() {

            public void run() {
                try {
                    despachadorMensajesService.mandarMail(message);
                } catch (Exception e) {
                    // Nothing to do
                }

            }
        });
        executor.shutdown();

    }

    private Mensaje buildMessage(String msg, boolean notificaSoporte) {
        Mensaje mensaje = new Mensaje();
        mensaje.setBody("Fecha envio: " + new Date().toString().concat(" ").concat(msg));
        mensaje.setDestinatario(this.mails);
        if (notificaSoporte && this.mailsSoporte != null) {
            mensaje.setDestinatario(mensaje.getDestinatario() + "," + this.mailsSoporte);
        }
        mensaje.setRemitente("jose.carbone@osde.com.ar");
        if(environment!=null){
            environment=environment.toUpperCase();
        }
        mensaje.setTema("Error Comunicacion Servicio POS: "+environment);
        mensaje.setUsuario("MT27789605");
        return mensaje;
    }

    
}
