package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;

public class Transaccion1ABuilderStrategy extends AbstractTransaccionConsumoPosBuilderStrategyImpl<SocioPosMessage,PosMessage> {

    public Transaccion1ABuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public SocioPosMessage buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        SocioPosMessage message = this.buildTransaccion(beanMapper, posMessage, SocioPosMessage.class);
        return message;
    }

  

}
