package ar.com.osde.cv.model;

import ar.com.osde.entities.aptoServicio.TransaccionReferencia;

public abstract class AbstractTransaccionReferencia extends AbstractTransaccionPos {

    private static final long serialVersionUID = 1L;

    public abstract TransaccionReferencia getTransaccionReferencia();

}
