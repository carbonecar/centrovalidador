package ar.com.osde.cv.model;

import org.apache.commons.lang.StringUtils;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptoServicio.CasoUrgencia;
import ar.com.osde.entities.aptoServicio.SolicitudConsumo;

public class Transaccion2DBuilderStrategy extends Transaccion2ABuilderStrategy {

    public static final String OPERADOR_URGENCIAS = "OS";

    public Transaccion2DBuilderStrategy(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    public TransaccionPrestacionPos buildTransaccion(Mapper beanMapper, PosMessage posMessage) {
        TransaccionPrestacionPos trx = super.buildTransaccion(beanMapper, posMessage);

        SolicitudConsumo solicitudServicio = trx.getTransaccionConsumo().getSolicitudServicio();
        
        if ((OPERADOR_URGENCIAS.equals(posMessage.getCodigoOperador()))) {
            if (StringUtils.isNumeric(posMessage.getNumeroCaso()) && !StringUtils.isWhitespace(posMessage.getNumeroCaso())) {
                Long numeroCasoAsociado = Long.parseLong(posMessage.getNumeroCaso());
                CasoUrgencia casoUrgencia = new CasoUrgencia();
                casoUrgencia.setNumeroCaso(numeroCasoAsociado);
                casoUrgencia.setFecha(solicitudServicio.getFechaSolicitudServicio());
                casoUrgencia.setCodigoFilial(trx.getPrestador().getTerminalID().getCodigoFilial());
                solicitudServicio.setCasoUrgencia(casoUrgencia);
            }
        }

        return trx;
    }
}
