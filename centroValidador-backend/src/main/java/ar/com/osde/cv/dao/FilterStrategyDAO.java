package ar.com.osde.cv.dao;

import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;

/**
 * Market interface para el dao de las estrategias del filtro
 * @author MT27789605
 *
 */
public interface FilterStrategyDAO extends GenericCRUDDao<FilterStrategyConfiguration>{

}