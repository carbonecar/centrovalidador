package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.aptoServicio.TransaccionReferencia;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionMarcaGenerico;

public class MarcaGenericoTransaccionPos extends AbstractTransaccionReferencia {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private TransaccionMarcaGenerico transaccionMarcaGenerico;

    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);
    }

    public TransaccionMarcaGenerico getTransaccionMarcaGenerico() {
        return transaccionMarcaGenerico;
    }

    public void setTransaccionMarcaGenerico(TransaccionMarcaGenerico transaccionMarcaGenerico) {
        this.transaccionMarcaGenerico = transaccionMarcaGenerico;
    }

    @Override
    public Transaccion getTransaccion() {
        return transaccionMarcaGenerico;
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return transaccionMarcaGenerico.getSolicitudServicio().getFechaSolicitudServicio();
    }

    @Override
    public TransaccionReferencia getTransaccionReferencia() {
        return transaccionMarcaGenerico.getSolicitudServicio().getTransaccionReferencia();
    }

}
