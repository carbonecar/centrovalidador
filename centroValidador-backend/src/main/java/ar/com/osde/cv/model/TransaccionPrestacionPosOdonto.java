package ar.com.osde.cv.model;

public class TransaccionPrestacionPosOdonto extends AbstractTransaccionPrestacionPos {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String tipoTratamientoOdontologico;

    public TransaccionPrestacionPosOdonto() {
        super();
    }

    public String getTipoTratamientoOdontologico() {
        return tipoTratamientoOdontologico;
    }

    public void setTipoTratamientoOdontologico(String tipoTratamientoOdontologico) {
        this.tipoTratamientoOdontologico = tipoTratamientoOdontologico;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.SocioPosMessage#accept(ar.com.osde.
     * centroValidador.model.TransaccionPosVisitor)
     */
    @Override
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);
    }

}
