package ar.com.osde.cv.model;


/**
 * Interfaz para implementar visitors.
 * 
 * @author VA27789605
 * 
 */
public interface TransaccionVisitable {
	public void accept(TransaccionPosVisitor bo);
}
