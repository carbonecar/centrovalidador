package ar.com.osde.cv.model;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.BloqueoDesbloqueoPosMessage;
import ar.com.osde.entities.transaccion.TipoTransaccion;

public class TransaccionBloqueoBuilderStrategy extends AbstractTransaccion4XBuilderStrategy<BloqueoDesbloqueoTransaccionPos,BloqueoDesbloqueoPosMessage> {

    @Override
    public BloqueoDesbloqueoTransaccionPos buildTransaccion(Mapper beanMapper, BloqueoDesbloqueoPosMessage posMessage) {
    
        BloqueoDesbloqueoTransaccionPos transaccion= super.buildTransaccion(beanMapper, posMessage);
        
        if(TipoTransaccion.O4B.equals(transaccion.getTransaccionBloqueoDesbloqueo().getTipoTransaccion())){
            transaccion.getTransaccion().setBloqueo(true);
        }else{
            transaccion.getTransaccion().setBloqueo(false);
        }
        
        return transaccion;
                
    }

    @Override
    public Class getClazz() {
        return BloqueoDesbloqueoTransaccionPos.class;
    }

}
