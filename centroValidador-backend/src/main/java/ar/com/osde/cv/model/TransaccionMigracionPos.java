package ar.com.osde.cv.model;

import ar.com.osde.entities.transaccion.TransaccionMigracion;

/**
 * 
 * @author MT27789605
 *
 */
public class TransaccionMigracionPos extends TransaccionTimeOutPos {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private TransaccionMigracion transaccioMigracion;

    public TransaccionMigracionPos() {

    }

    public TransaccionMigracionPos(AbstractTransaccionPos transaccionPosTimeOut, TransaccionMigracion transaccionMigracion) {
        super(transaccionPosTimeOut, transaccionMigracion);
        this.transaccioMigracion = transaccionMigracion;
    }

    public TransaccionMigracion getTransaccioMigracion() {
        return transaccioMigracion;
    }

    public void setTransaccioMigracion(TransaccionMigracion transaccioMigracion) {
        this.transaccioMigracion = transaccioMigracion;
    }

    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }

}
