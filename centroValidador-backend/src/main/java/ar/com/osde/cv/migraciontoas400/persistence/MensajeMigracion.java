package ar.com.osde.cv.migraciontoas400.persistence;

import java.io.Serializable;

import ar.com.osde.entities.transaccion.TransaccionBID;

/**
 * Mensaje de migracion hacia el as400
 * 
 * @author MT27789605
 * 
 */
public class MensajeMigracion implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private TransaccionBID transaccionBid;
    private String mensajeMQ;
    private String codigoFilial;

    public TransaccionBID getTransaccionBid() {
        return transaccionBid;
    }

    public void setTransaccionBid(TransaccionBID transaccionBid) {
        this.transaccionBid = transaccionBid;
    }

    public String getCodigoFilial() {
        return codigoFilial;
    }

    public void setCodigoFilial(String codigoFilial) {
        this.codigoFilial = codigoFilial;
    }

    public String getMensajeMQ() {
        return mensajeMQ;
    }

    public void setMensajeMQ(String mensajeMQ) {
        this.mensajeMQ = mensajeMQ;
    }

}
