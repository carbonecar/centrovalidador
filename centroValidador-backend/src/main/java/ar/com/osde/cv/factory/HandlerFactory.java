package ar.com.osde.cv.factory;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;

import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.jms.core.JmsTemplate;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.factory.FilterFactory;
import ar.com.osde.centroValidador.factory.PosMessageHandlerStrategyBuilder;
import ar.com.osde.centroValidador.factory.UtilFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.filter.estrategy.FilterStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.NullFilterStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.ProcessStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.RouteFilterStrategy;
import ar.com.osde.centroValidador.pos.handlers.AlwaysRouteMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.ErrorHandler;
import ar.com.osde.centroValidador.pos.handlers.FilteredMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.FixedPosFormatManager;
import ar.com.osde.centroValidador.pos.handlers.LoggerMessageRecordHandler;
import ar.com.osde.centroValidador.pos.handlers.MatchingRequestHandler;
import ar.com.osde.centroValidador.pos.handlers.MatchingResponseHandler;
import ar.com.osde.centroValidador.pos.handlers.MigracionMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.NullMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandlerInterceptor;
import ar.com.osde.centroValidador.pos.handlers.SimpleNewSystemResponseHandler;
import ar.com.osde.centroValidador.pos.handlers.SimpleTextResponseHandler;
import ar.com.osde.centroValidador.pos.handlers.StreamResponseMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.TimeOutFilteredMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.TimeOutMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.TimeOutPosFormatManager;
import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.entities.AbstractResponseMessageHandlerConfiguration;
import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.entities.ErrorHandlerConfiguration;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.MigracionMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;
import ar.com.osde.rules.paralelo.ResponseMessageMatcher;

/**
 * Clase Factory que permite crear PosMessagehandlers.
 */
public class HandlerFactory {

	private FixedFormatManager formatManager;
	private TimeOutPosFormatManager timeOutFormatManager;
	private Mapper beanMapper;
	private AptoServicioBO aptoServicioBO;
	private GenericCRUDDao<AbstractPosMessage> transaccionPrestacionCrud;
	private ResponseMessageMatcher matcher;

	private Map<String, PosMessageHandlerStrategyBuilder> handlerNameStrategy = new HashMap<String, PosMessageHandlerStrategyBuilder>();
	private TransaccionServiceStatistics statistics;
	private Map<String, String> operadores = new HashMap<String, String>();
	private MessageCreatorFactory messageCreatorFactory;

	private static final Logger LOG = Logger.getLogger(HandlerFactory.class);

	public Map<String, String> getOperadores() {
		return operadores;
	}

	public void setOperadores(Map<String, String> operadores) {
		this.operadores = operadores;
	}

	public TimeOutPosFormatManager getTimeOutFormatManager() {
		return timeOutFormatManager;
	}

	public void setTimeOutFormatManager(TimeOutPosFormatManager timeOutFormatManager) {
		this.timeOutFormatManager = timeOutFormatManager;
	}

	public HandlerFactory() {
		handlerNameStrategy.put(NullMessageHandler.class.getName(), null);
		handlerNameStrategy.put(AlwaysRouteMessageHandler.class.getName(), null);
		handlerNameStrategy.put(StreamResponseMessageHandler.class.getName(), null);
		handlerNameStrategy.put(FilteredMessageHandler.class.getName(), null);
		handlerNameStrategy.put(FixedFormatMessageHandler.class.getName(), null);
		handlerNameStrategy.put(LoggerMessageRecordHandler.class.getName(), null);
		handlerNameStrategy.put(MatchingResponseHandler.class.getName(), null);
		handlerNameStrategy.put(SimpleTextResponseHandler.class.getName(), null);
		handlerNameStrategy.put(SimpleNewSystemResponseHandler.class.getName(), null);
		handlerNameStrategy.put(MatchingRequestHandler.class.getName(), null);
		handlerNameStrategy.put(TimeOutMessageHandler.class.getName(), null);
		handlerNameStrategy.put(MigracionMessageHandler.class.getName(), null);
		handlerNameStrategy.put(TimeOutFilteredMessageHandler.class.getName(), null);
	}

	public TransaccionServiceStatistics getStadistics() {
		return statistics;
	}

	public void setStadistics(TransaccionServiceStatistics stadistics) {
		this.statistics = stadistics;
	}

	public Collection<String> getAllHandlersNames() {
		return handlerNameStrategy.keySet();
	}

	public Map<String, PosMessageHandlerStrategyBuilder> getHandlerNameStrategy() {
		return handlerNameStrategy;
	}

	public void setHandlerNameStrategy(Map<String, PosMessageHandlerStrategyBuilder> handlerNameStrategy) {
		this.handlerNameStrategy = handlerNameStrategy;
	}

	public FixedFormatManager getFormatManager() {
		return formatManager;
	}

	public void setFormatManager(FixedFormatManager formatManager) {
		this.formatManager = formatManager;
	}

	public Mapper getBeanMapper() {
		return beanMapper;
	}

	public void setBeanMapper(Mapper beanMapper) {
		this.beanMapper = beanMapper;
	}

	public AptoServicioBO getAptoServicioBO() {
		return aptoServicioBO;
	}

	public void setAptoServicioBO(AptoServicioBO aptoServicioBO) {
		this.aptoServicioBO = aptoServicioBO;
	}

	public GenericCRUDDao<AbstractPosMessage> getTransaccionPrestacionCrud() {
		return transaccionPrestacionCrud;
	}

	public void setTransaccionPrestacionCrud(GenericCRUDDao<AbstractPosMessage> transaccionPrestacionCrud) {
		this.transaccionPrestacionCrud = transaccionPrestacionCrud;
	}

	public void setMessageCreatorFactory(MessageCreatorFactory messageCreatorFactory) {
		this.messageCreatorFactory = messageCreatorFactory;
	}

	public PosMessageHandler createHandler(PosMessageHandlerConfiguration configuration) throws BusinessException {

		String handlerToCreateClassName = configuration.getClassName();

		// TODO: refactorizar esto que da asco!
		if (handlerToCreateClassName.equals(LoggerMessageRecordHandler.class.getName())
		        || handlerToCreateClassName.equals(MatchingResponseHandler.class.getName())) {
			PosMessageHandler handler = this
			        .createInterceptedMessageHandler((PosMessageHandlerInterceptorConfiguration) configuration);
			
			if (handlerToCreateClassName.equals(MatchingResponseHandler.class.getName())) {
				MatchingResponseHandler handlerReal = (MatchingResponseHandler) handler;				
				handlerReal.setMatcher(this.matcher);
				return handler;
			}
			return handler;
		}
		if (handlerToCreateClassName.equals(NullMessageHandler.class.getName())) {
			return createNullMessageHandler();
		}

		if (handlerToCreateClassName.equals(AlwaysRouteMessageHandler.class.getName())
		        || handlerToCreateClassName.equals(StreamResponseMessageHandler.class.getName())) {

			return createAbstractResponseHandler((AbstractResponseMessageHandlerConfiguration) configuration);

		}

		if (handlerToCreateClassName.equals(FilteredMessageHandler.class.getName())
		        || handlerToCreateClassName.equals(TimeOutFilteredMessageHandler.class.getName())) {
			return createFilterHandler((FilteredMessageHandlerConfiguration) configuration);
		}

		if (FixedFormatMessageHandler.class.getName().equals(handlerToCreateClassName)
		        || handlerToCreateClassName.equals(TimeOutMessageHandler.class.getName())
		        || handlerToCreateClassName.equals(MigracionMessageHandler.class.getName())) {
			FixedFormatMessageHandler fxmHandler;
			try {
				fxmHandler = (FixedFormatMessageHandler) Class.forName(configuration.getClassName()).newInstance();
				fxmHandler.setBeanMapper(this.beanMapper);
				if (handlerToCreateClassName.equals(TimeOutMessageHandler.class.getName())
				        || handlerToCreateClassName.equals(MigracionMessageHandler.class.getName())
				        || handlerToCreateClassName.equals(TimeOutFilteredMessageHandler.class.getName())) {
					fxmHandler.setFormatManager(this.timeOutFormatManager);
				} else {
					fxmHandler.setFormatManager(this.formatManager);
				}
				fxmHandler.setTransaccionPrestacionCrud(this.transaccionPrestacionCrud);
				fxmHandler.setAptoServicioBO(this.aptoServicioBO);
				if (handlerToCreateClassName.equals(MigracionMessageHandler.class.getName()))
					this.addInformation((MigracionMessageHandler) fxmHandler,
					        (MigracionMessageHandlerConfiguration) configuration);
				return fxmHandler;
			} catch (IllegalAccessException e) {
				LOG.error(e);
				throw new BusinessException(e);
			} catch (InstantiationException e) {
				LOG.error(e);
				throw new BusinessException(e);
			} catch (ClassNotFoundException e) {
				LOG.error(e);
				throw new BusinessException(e);
			}
		}

		if (handlerToCreateClassName.equals(SimpleTextResponseHandler.class.getName())) {
			SimpleTextResponseHandler handler = new SimpleTextResponseHandler();
			return handler;
		}
		if (handlerToCreateClassName.equals(SimpleNewSystemResponseHandler.class.getName())) {
			SimpleNewSystemResponseHandler handler = new SimpleNewSystemResponseHandler();
			return handler;
		}
		
		if (handlerToCreateClassName.equals(MatchingRequestHandler.class.getName())) {
			MatchingRequestHandler handler = new MatchingRequestHandler();
			handler.setMatcher(this.matcher);
			handler.setStadistics(this.statistics);
			return handler;
		}
		throw new BusinessException("No se puede crear el handler.: " + handlerToCreateClassName);
	}

	public PosMessageHandler createNullMessageHandler() {
		NullMessageHandler handler= new NullMessageHandler();
		handler.setStatistics(this.statistics);
		return handler;
	}

	private void addInformation(MigracionMessageHandler fxmHandler,
	        MigracionMessageHandlerConfiguration configuration) {
		if (configuration.getCodigosExceptuar() != null) {
			for (CodigoExceptuable codigoExceptuable : configuration.getCodigosExceptuar())
				fxmHandler.addCodigoExceptuable(codigoExceptuable.getCodigo());
		}
	}

	public ErrorHandler createRealErrorHandler(ErrorHandlerConfiguration errorHandlerConfiguration)
	        throws BusinessException {
		// TODO: INYECTAR
		ErrorHandler errorHandler = new ErrorHandler();

		JmsTemplate templateResponse = new JmsTemplate();
		ConnectionFactory connectionFactory = UtilFactory
		        .obtainConnectionFactory(errorHandlerConfiguration.getConnectionFactoryName());
		if (connectionFactory == null) {
			// TODO: ARROJAR UNA EXCEPTION
		}
		templateResponse.setConnectionFactory(connectionFactory);

		errorHandler.setTemplateResponse(UtilFactory.createJmsTemplate(errorHandlerConfiguration.getQueueName(), connectionFactory));
		errorHandler.setQueue(UtilFactory.obtainQueue(errorHandlerConfiguration.getQueueName()));
		errorHandler.setQueueTimeOut(UtilFactory.obtainQueue(errorHandlerConfiguration.getTimeOutQueueName()));
		errorHandler.setMessageCreatorFactory(this.messageCreatorFactory);
		return errorHandler;
		
	
	}

	public ResponseMessageMatcher getMatcher() {
		return matcher;
	}

	public void setMatcher(ResponseMessageMatcher matcher) {
		this.matcher = matcher;
	}

	// *****************************************************//
	// *******************Impl. Interna*********************//
	// *****************************************************//
	private PosMessageHandler createInterceptedMessageHandler(PosMessageHandlerInterceptorConfiguration configuration)
	        throws BusinessException {
		PosMessageHandlerInterceptor handler = null;
		try {
			handler = (PosMessageHandlerInterceptor) Class.forName(configuration.getClassName()).newInstance();
			if (handler instanceof MatchingResponseHandler) {
				((MatchingResponseHandler) handler).setStatistics(statistics);
			}
			if (configuration.getPosMessageHandler() != null) {
				handler.setIntercepted(this.createHandler(configuration.getPosMessageHandler()));
			}
			// REFACTORME!
		} catch (IllegalAccessException e) {
			LOG.error(e);
		} catch (InstantiationException e) {
			LOG.error(e);
		} catch (ClassNotFoundException e) {
			LOG.error(e);
		}

		return handler;
	}

	private PosMessageHandler createAbstractResponseHandler(AbstractResponseMessageHandlerConfiguration configuration)
	        throws BusinessException {

		String className = configuration.getClassName();

		PosMessageHandlerConfiguration innerHandlerConfiguration = configuration.getPosMessageHandler();
		String queueName = configuration.getQueueName();

		if (className.equals(AlwaysRouteMessageHandler.class.getName())) {
			return createAlwaysRouteMessageHandler(innerHandlerConfiguration, queueName,
			        configuration.getConnectionFactoryName());
		}

		return createStreamResponseMessageHandler(innerHandlerConfiguration, queueName,
		        configuration.getConnectionFactoryName());
	}

	private PosMessageHandler createFilterHandler(FilteredMessageHandlerConfiguration filterConfiguration)
	        throws BusinessException {

		FilteredMessageHandler filterMessageHandler;
		try {
			filterMessageHandler = (FilteredMessageHandler) Class.forName(filterConfiguration.getClassName())
			        .newInstance();
			List<PosMessageFilter> filters = new LinkedList<PosMessageFilter>();

			for (FilterConfiguration filter : filterConfiguration.getFilters()) {
				filters.add(FilterFactory.createFilter(filter));
			}

			filterMessageHandler.setFilters(filters);

			List<FilterStrategy> filterStrategyList = new LinkedList<FilterStrategy>();

			for (FilterStrategyConfiguration filterStrategy : filterConfiguration.getFilterStrategyList()) {
				filterStrategyList.add(this.createFilterStrategy(filterStrategy));
			}

			filterMessageHandler.setFilterStrategyList(filterStrategyList);
			filterMessageHandler.setFormatManager(new FixedPosFormatManager());

			if (filterConfiguration.getPosMessageHandler() != null) {
				filterMessageHandler.setIntercepted(createHandler(filterConfiguration.getPosMessageHandler()));
			}
			filterMessageHandler.setStatistics(this.statistics);
			return filterMessageHandler;
		} catch (IllegalAccessException e) {
			LOG.error(e);
			throw new BusinessException(e);
		} catch (InstantiationException e) {
			LOG.error(e);
			throw new BusinessException(e);
		} catch (ClassNotFoundException e) {
			LOG.error(e);
			throw new BusinessException(e);
		}

	}

	private PosMessageHandler createStreamResponseMessageHandler(
	        PosMessageHandlerConfiguration innerHandlerConfiguration, String queueName, String connectionFactoryName)
	                throws BusinessException {

		StreamResponseMessageHandler messageHandler = new StreamResponseMessageHandler();
		messageHandler.setFormatManager(new FixedPosFormatManager());

		if (innerHandlerConfiguration != null) {
			messageHandler.setIntercepted(createHandler(innerHandlerConfiguration));
		}

		messageHandler.setQueue(UtilFactory.obtainQueue(queueName));
		ConnectionFactory connectionFactory = UtilFactory.obtainConnectionFactory(connectionFactoryName);
		messageHandler.setTemplateResponse(UtilFactory.createJmsTemplate(queueName, connectionFactory));

		messageHandler.setOperadores(this.getOperadores());
		messageHandler.setMessageCreatorFactory(this.messageCreatorFactory);
		return messageHandler;
	}

	private AlwaysRouteMessageHandler createAlwaysRouteMessageHandler(
	        PosMessageHandlerConfiguration innerHandlerConfiguration, String queueName, String connectionFactoryName)
	                throws BusinessException {

		AlwaysRouteMessageHandler messageHandler = new AlwaysRouteMessageHandler();
		messageHandler.setFormatManager(new FixedPosFormatManager());

		messageHandler.setQueue(UtilFactory.obtainQueue(queueName));

		if (innerHandlerConfiguration != null) {
			messageHandler.setIntercepted(createHandler(innerHandlerConfiguration));
		}
		ConnectionFactory connectionFactory = UtilFactory.obtainConnectionFactory(connectionFactoryName);
		messageHandler.setTemplateResponse(UtilFactory.createJmsTemplate(queueName, connectionFactory));

		return messageHandler;
	}

	public FilterStrategy createFilterStrategy(FilterStrategyConfiguration filterStrategyConfiguration)
	        throws BusinessException {

		String className = filterStrategyConfiguration.getClass().getName();

		if (FilterStrategyConfiguration.class.getName().equals(className)) {
			return new NullFilterStrategy();
		} else if (RouteFilterStrategyConfiguration.class.getName().equals(className)) {
			return createRouteStrategy((RouteFilterStrategyConfiguration) filterStrategyConfiguration);
		} else if (ProcessFilterStrategyConfiguration.class.getName().equals(className)) {
			return createProcessStrategy((ProcessFilterStrategyConfiguration) filterStrategyConfiguration);
		}

		throw new BusinessException("No se puede crear el filter strategy.");
	}

	private FilterStrategy createProcessStrategy(ProcessFilterStrategyConfiguration configuration)
	        throws BusinessException {

		ProcessStrategy processStrategy = new ProcessStrategy();
		processStrategy.setPosMessageHandler(this.createHandler(configuration.getPosMessageHandler()));

		return processStrategy;
	}

	private FilterStrategy createRouteStrategy(RouteFilterStrategyConfiguration filterStrategyConfiguration)
	        throws BusinessException {
		RouteFilterStrategy routeStrategy = new RouteFilterStrategy();
		routeStrategy.setMessageCreatorFactory(this.messageCreatorFactory);
		ConnectionFactory connectionFactory = UtilFactory
		        .obtainConnectionFactory(filterStrategyConfiguration.getConnectionFactoryName());
		if (connectionFactory == null) {
			// TODO: arrojar una exception
		}
		routeStrategy.setTemplateResponse(
		        UtilFactory.createJmsTemplate(filterStrategyConfiguration.getQueueName(), connectionFactory));

		return routeStrategy;
	}

}
