package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.centroValidador.pos.PosMessageRescate;
import ar.com.osde.entities.transaccion.TransaccionRescate;

public class TransaccionRescatePos extends SocioPosMessage implements OrdenReferenciable{

    private static final long serialVersionUID = 1L;

    
    private TransaccionRescate transaccionRescate;


    private Orden orden;

    @Override
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public TransaccionRescate getTransaccion() {
        return this.getTransaccionRescate();
    }
    
    public TransaccionRescate getTransaccionRescate() {
        return transaccionRescate;
    }


    public void setTransaccionRescate(TransaccionRescate transaccionRescate) {
        this.transaccionRescate = transaccionRescate;
    }

    @Override
    public Date getFechaSolicitudServicio() {     
        return this.getTransaccionRescate().getTransaccionBid().getFechaSolicitud();
    }
    @Override
    public PosMessageRescate getPosMessage() {
        return (PosMessageRescate) super.getPosMessage();
    }

    public Orden getOrden() {
        return this.orden;
    }

    public void setOrden(Orden orden) {
        this.orden=orden;
    }

    
}
