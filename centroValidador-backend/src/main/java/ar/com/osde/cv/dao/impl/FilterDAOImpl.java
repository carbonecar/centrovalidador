package ar.com.osde.cv.dao.impl;

import java.sql.SQLException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.framework.persistence.dao.crud.impl.hibernate.GenericCRUDDaoHIBImpl;

public class FilterDAOImpl extends GenericCRUDDaoHIBImpl<FilterConfiguration> {


    public FilterConfiguration getParentFilter(final Long filterId) {
        FilterConfiguration filterConfiguration = (FilterConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Criteria criteria = session.
                        createCriteria(CompositeFilterConfiguration.class).
                        createCriteria("filterConfigurationList").
                        add(Restrictions.eq("id", filterId));
                return criteria.uniqueResult();
            }
        });

        if (filterConfiguration == null)
            filterConfiguration = (FilterConfiguration) this.getHibernateTemplate().execute(new HibernateCallback() {
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    Criteria criteria = session.
                            createCriteria(NotFilterConfiguration.class).
                            add(Restrictions.eq("filterConfiguration.id", filterId));
                    return criteria.uniqueResult();
                }
            });
        return filterConfiguration;
    }
}
