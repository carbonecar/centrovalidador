package ar.com.osde.cv.model;

import java.util.Date;

import org.apache.log4j.Logger;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptoServicio.Matricula;

public abstract class AbstractTransaccionConsumoPosBuilderStrategyImpl<T extends TransaccionPos, E extends PosMessage> extends
        AbstractTransaccionPosBuilderStrategyImpl<T, E> {
    protected static final Logger LOG = Logger.getLogger(AbstractTransaccionConsumoPosBuilderStrategyImpl.class);
    private FechaConsumoCalculador fechaConsumoCalculador;

    
    
    public AbstractTransaccionConsumoPosBuilderStrategyImpl(FechaConsumoCalculador fechaConsumoCalculador) {
        this.fechaConsumoCalculador = fechaConsumoCalculador;
    }

    /**
     * En general si la fecha de consumo no esta informada se toma la fecha de
     * solicitud de servicio
     * 
     * @param posMessage
     * @param transaccion
     * @param fechaSolicitudServicio
     * @return
     */
    protected Date buildFechaConsumo(PosMessage posMessage, TransaccionPos transaccion) {
        return this.fechaConsumoCalculador.buildFechaConsumo(posMessage, transaccion);
    }
    
    
    protected T buildTransaccion(Mapper beanMapper, AbstractPosMessage posMessage, Class<T> clazz) {
        T trxPos=super.buildTransaccion(beanMapper, posMessage, clazz);
        this.buildBeneficiario(posMessage, trxPos);
        return trxPos;
    }
    
    /**
     * Crea la matricula si es que es posible
     * 
     * @param posMessage
     * @return
     */
    protected Matricula buildMatriculaMatricula(PosMessage posMessage) {
        Matricula matricula = null;
        String tipoMatriculaPrestador = posMessage.getTipoMatriculaPrestador();
        if (tipoMatriculaPrestador != null && !"".equals(tipoMatriculaPrestador) && !"0".equals(tipoMatriculaPrestador)
                && tipoMatriculaPrestador.length() == 2) {
            matricula = new Matricula();
            String tipoMatricula = posMessage.getTipoMatriculaPrestador();
            int numeroMatricula = posMessage.getNumeroMatriculaPrestador();

            matricula.setEspecialidad(tipoMatricula.substring(0, 1));
            matricula.setProvincia(tipoMatricula.substring(1, 2));
            matricula.setNroMatricula(numeroMatricula);
        }
        return matricula;
    }

   

}
