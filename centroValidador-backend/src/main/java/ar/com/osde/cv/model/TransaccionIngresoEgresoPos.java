package ar.com.osde.cv.model;

import ar.com.osde.entities.transaccion.TransaccionIngresoEgreso;

public class TransaccionIngresoEgresoPos extends TransaccionPrestacionPos {

    private static final long serialVersionUID = 1L;

    private TransaccionIngresoEgreso transaccionIngresoEgreso;

    public TransaccionIngresoEgreso getTransaccionIngresoEgreso() {
        return this.transaccionIngresoEgreso;
    }

    public void setTransaccionIngresoEgreso(TransaccionIngresoEgreso transaccionIngresoEgreso) {
        this.transaccionIngresoEgreso = transaccionIngresoEgreso;
    }

    @Override
    public TransaccionIngresoEgreso getTransaccion() {
        return this.transaccionIngresoEgreso;
    }
    
//    @Override
//    public void setTransaccionConsumo(TransaccionConsumo transaccionConsumo) {
//        throw new UnsupportedOperationException("setTransaccionConsumo no es valido en una transaccion ingreso egreso");
//    }
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);
    }
}
