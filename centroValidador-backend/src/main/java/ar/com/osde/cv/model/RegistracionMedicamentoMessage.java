package ar.com.osde.cv.model;

import java.io.Serializable;
import java.util.Date;

import ar.com.osde.centroValidador.pos.mq.PosMessageResponseCreator;

public class RegistracionMedicamentoMessage extends SocioPosMessage implements Serializable, TransaccionVisitable,OrdenReferenciable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    private Date fechaReceta;
    private Orden orden;

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public void accept(PosMessageResponseCreator visitor) {
        visitor.visit(this);
        visitor.visit((SocioPosMessage) this);
    }

    @Override
    public void accept(TransaccionPosVisitor visitor) {
        visitor.visit(this);

    }

    public Date getFechaReceta() {
        return fechaReceta;
    }

    public void setFechaReceta(Date fechaReceta) {
        this.fechaReceta = fechaReceta;
    }
}
