package ar.com.osde.cv.model;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.joda.time.DateTime;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.CredencialPosMessage;
import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptitud.ContextoEvaluacion;
import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.aptoServicio.AutorizacionBidFactory;
import ar.com.osde.entities.aptoServicio.BeneficiarioBid;
import ar.com.osde.entities.aptoServicio.Credencial;
import ar.com.osde.entities.aptoServicio.ModoIngreso;
import ar.com.osde.entities.aptoServicio.NroContrato;
import ar.com.osde.entities.aptoServicio.SolicitudServicio;
import ar.com.osde.entities.aptoServicio.TipoServicio;
import ar.com.osde.entities.transaccion.Transaccion;

public abstract class AbstractTransaccionPosBuilderStrategyImpl<T extends AbstractTransaccionPos, E extends IPosMessage> implements
        TransaccionPosBuilderStrategy<T, E> {

    protected Logger LOG = Logger.getLogger(AbstractTransaccionPosBuilderStrategyImpl.class);

    protected T buildTransaccion(Mapper beanMapper, AbstractPosMessage posMessage, Class<T> clazz) {
        T message = beanMapper.map(posMessage, clazz);
        message.getTransaccion().setVersionMensajeria(StringUtils.trim(posMessage.getVersionMensajeria()));
        message.setPosMessage(posMessage);
        String codigoRelacion = String.format("%2s",
                message.getTransaccion().getPrestadorTransaccionador().getTerminalID().getCodigoFilial()).replace(' ', '0');
        int tipoPrestadorInt = posMessage.getTipoPrestador();
        if (tipoPrestadorInt == 0) {
            tipoPrestadorInt = 1;
        }
        String tipoPrestador = new String("0" + tipoPrestadorInt);
        codigoRelacion = codigoRelacion.concat(tipoPrestador);
        String codigoPrestadorFormateado = null;
        if (posMessage.getCodigoPrestador() != null) {
            codigoPrestadorFormateado = String.format("%6s", posMessage.getCodigoPrestador()).replace(' ', '0');
            codigoRelacion = codigoRelacion.concat(codigoPrestadorFormateado);
        }
        if (codigoPrestadorFormateado != null && !"000000".equals(codigoPrestadorFormateado)) {
            message.getTransaccion().getPrestadorTransaccionador().setNumeroRelacion(codigoRelacion);
        }
        return this.buildSolicitud(posMessage, message);
    }

    protected T buildFechas(AbstractPosMessage posMessage, T transaccionPos) {

        Transaccion transaccion = transaccionPos.getTransaccion();

        // Construccion de la fecha de solicitud del servicio
        // FIX para el bug de fechas. Se construye la fecha a mano porque en el
        // servidor remoto toma el horario de verano y no pude encontrar.
        // un timezone que sea valido
        DateTime dateTime = posMessage.getFechaTransaccion().toDateTime(posMessage.getHoraTransaccion());
        SolicitudServicio ss = transaccion.getSolicitudServicio();
        Calendar cal = Calendar.getInstance();
        cal.set(dateTime.getYear(), dateTime.getMonthOfYear() - 1, dateTime.getDayOfMonth(), 0, 0, dateTime.getSecondOfDay());
        ss.setFechaSolicitudServicio(cal.getTime());

        // Construccion de la fecha de consumo

        // la fecha de consumo se fue para cada uno de los items
        // .setFechaConsumo(this.buildFechaConsumo(posMessage,
        // transaccion,fechaHoraSolicitudServicio));
        return transaccionPos;
    }

    protected T buildSolicitud(AbstractPosMessage posMessage, T trxPos) {
        // SolicitudServicio solicitud = trxPos.getSolicitudServicio();
        //
        // TODO crear una subclase de esta
        if (posMessage instanceof SocioPosMessageBase) {
            String formaIngreso = ((SocioPosMessageBase) posMessage).getFormaIngresoDelAsociado().toUpperCase();
            try {
                trxPos.getTransaccion().getSolicitudServicio().setModoIngresoCredencial(ModoIngreso.valueOf(formaIngreso));
            } catch (java.lang.IllegalArgumentException e) {
                // no se hace nada. En caso de venir algo invalido se deja como
                // valor vacio
            }
        }

        // solicitud.setTransaccionador(trxPos.getPrestador());
        this.buildFechas(posMessage, trxPos);
        trxPos.getTransaccion().getSolicitudServicio()
                .setUsuarioGenerador(trxPos.getTransaccion().getPrestadorTransaccionador().getNumeroRelacion());

        // TODO SACAR ESTO A CONFIGURACION cuando definan bien como son los
        // TIPOS de Elemento
        if (trxPos.getTransaccion().getTipoTransaccion().getTipo() == 1) {
            trxPos.getTransaccion().getSolicitudServicio().setTipoServicio(TipoServicio.VALIDACION);
        }
        if (trxPos.getTransaccion().getTipoTransaccion().getTipo() == 2) {
            trxPos.getTransaccion().getSolicitudServicio().setTipoServicio(TipoServicio.REGISTRACION);
        }
        if ((trxPos.getTransaccion().getTipoTransaccion().getTipo() == 4) || (trxPos.getTransaccion().getTipoTransaccion().getTipo() == 3)
                || trxPos.getTransaccion().getTipoTransaccion().getTipo() == 5) {
            trxPos.getTransaccion().getSolicitudServicio().setTipoServicio(TipoServicio.SOLICITUD);
        }
        // Por defecto es ONLINE
        if (trxPos.getTransaccion().getTipoTransaccion().isOffLne()) {
            trxPos.getTransaccion().getSolicitudServicio().setContextoEvaluacion(ContextoEvaluacion.OFFLINE);
        } else {
            trxPos.getTransaccion().getSolicitudServicio().setContextoEvaluacion(ContextoEvaluacion.ONLINE);
        }

        return trxPos;

    }

    /**
     * Para las transacciones de cierre solo se permite hasta un dia antes de la
     * fecha de la solicitud de servicio.
     * 
     * @param fechaHastaRecuperar
     * @return
     */
    protected Date buildDateForPendientesAndCierre(Date fechaHastaRecuperar, Date fechaSolicitudServicio) {
        // TODO: revisar esto que esta mal, funciona pero es incorrecto el
        // algoritmo.
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaSolicitudServicio);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date ayer = DateUtils.truncate(cal.getTime(), Calendar.HOUR);
        if (DateUtils.truncate(fechaHastaRecuperar, Calendar.HOUR).after(ayer)) {
            Date hoy = DateUtils.truncate(DateUtils.add(ayer, Calendar.DAY_OF_YEAR, 1), Calendar.DAY_OF_MONTH);
            Date fechaCierreCalculada = DateUtils.add(hoy, Calendar.MILLISECOND, -1);
            fechaHastaRecuperar = fechaCierreCalculada;
        }
        return fechaHastaRecuperar;
    }

    /**
     * Devuelve la autoriazacion si pudo crearla. Caso contrario devuevle null.
     * 
     * @param ordenReferenciable
     * @param terminalID
     * @return
     */
    protected AutorizacionBid createAutorizacionBid(OrdenReferenciable ordenReferenciable, TerminalID terminalID) {
        String delegacion = ordenReferenciable.getOrden().getDelegacion();
        AutorizacionBid autorizacionBid = null;
        if ((StringUtils.isNotEmpty(delegacion) && StringUtils.isNumeric(delegacion))
                && ordenReferenciable.getOrden().getNumeroAutorizacion() != 0) {
            autorizacionBid = AutorizacionBidFactory.getInstance().getByNumeroEmisor(Integer.parseInt(delegacion));

            autorizacionBid.setNumeroAutorizacion(ordenReferenciable.getOrden().getNumeroAutorizacion());
            autorizacionBid.setNumeroEmisor(Integer.parseInt(delegacion));
            autorizacionBid.setCodigoFilial(terminalID.getCodigoFilial());
        }
        return autorizacionBid;
    }

    protected T buildBeneficiario(IPosMessage posMessage, T trxPos) {
        // TODO: mapear esto con dozzer
        if (posMessage instanceof CredencialPosMessage) {
            CredencialPosMessage socioPosMessage = (CredencialPosMessage) posMessage;
            if (socioPosMessage.getVersionCredencial() > 0) {
                Credencial credencial = new Credencial();
                credencial.setVersion(socioPosMessage.getVersionCredencial());
                credencial.setCodigoSeguridadExterno(socioPosMessage.getCodigoSeguridadExterno());
                trxPos.getTransaccion().getSolicitudServicio().setCredencial(credencial);
            }
        }
        BeneficiarioBid beneficiarioBid = trxPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid();
        if (NroContrato.PREFIJO_BENEFICIARIO_PROVISORIO.equals(beneficiarioBid.getNroContrato().getPrefijo())) {
            trxPos.getTransaccion().getSolicitudServicio().setBeneficiarioProvisorio(beneficiarioBid);
            trxPos.getTransaccion().getSolicitudServicio().setBeneficiarioBid(null);
        }
        return trxPos;

    }

}
