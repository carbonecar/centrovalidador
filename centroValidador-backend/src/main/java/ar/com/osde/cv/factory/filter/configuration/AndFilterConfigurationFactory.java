package ar.com.osde.cv.factory.filter.configuration;

import ar.com.osde.centroValidador.factory.filter.configuration.IFilterConfigurationFactory;
import ar.com.osde.centroValidador.pos.filter.AndFilter;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

import java.util.LinkedList;

/**
 * Factory para los filtros and
 */
public class AndFilterConfigurationFactory implements IFilterConfigurationFactory {
    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * IFilterConfigurationFactory#createFilter(java.util.LinkedList)
     */
    public FilterConfiguration createFilter(LinkedList<String> expresion) throws ServiceException {
        CompositeFilterConfiguration filter = new CompositeFilterConfiguration();
        filter.setClassName(AndFilter.class.getName());
        return filter;
    }
}
