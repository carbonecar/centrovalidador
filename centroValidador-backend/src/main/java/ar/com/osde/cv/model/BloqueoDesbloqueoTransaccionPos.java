package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.aptoServicio.TransaccionReferencia;
import ar.com.osde.entities.transaccion.TransaccionBloqueoDesbloqueo;

/**
 * 
 * @author MT27789605
 * 
 */
public class BloqueoDesbloqueoTransaccionPos extends AbstractTransaccionReferencia {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private TransaccionBloqueoDesbloqueo transaccionBloqueoDesbloqueo;

    public TransaccionBloqueoDesbloqueo getTransaccionBloqueoDesbloqueo() {
        return transaccionBloqueoDesbloqueo;
    }

    public void setTransaccionBloqueoDesbloqueo(TransaccionBloqueoDesbloqueo transaccionBloqueoDesbloqueo) {
        this.transaccionBloqueoDesbloqueo = transaccionBloqueoDesbloqueo;
    }

    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }

    @Override
    public TransaccionBloqueoDesbloqueo getTransaccion() {
        return this.getTransaccionBloqueoDesbloqueo();
    }

    @Override
    public Date getFechaSolicitudServicio() {
        return this.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio();
    }

    @Override
    public TransaccionReferencia getTransaccionReferencia() {
        return transaccionBloqueoDesbloqueo.getSolicitudServicio().getTransaccionReferencia();
    }

}
