package ar.com.osde.cv.model;

public class CredencialSocio {

	private String numeroAsociado = "";
	private String numeroBeneficiario = "";
	private String prefijoAsociado = "";

	// TODO analizar la validez de este campo dado que es la concatenacion de
	// prefijo y n�mero de socio
	private String numeroContrato = "";

	public String getNumeroAsociado() {
		return numeroAsociado;
	}

	public void setNumeroAsociado(String numeroAsociado) {
		this.numeroAsociado = numeroAsociado;
	}

	public String getNumeroBeneficiario() {
		return numeroBeneficiario;
	}

	public void setNumeroBeneficiario(String numeroBeneficiario) {
		this.numeroBeneficiario = numeroBeneficiario;
	}

	public String getPrefijoAsociado() {
		return prefijoAsociado;
	}

	public void setPrefijoAsociado(String prefijoAsociado) {
		this.prefijoAsociado = prefijoAsociado;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

}
