package ar.com.osde.cv.pos.transformer;

import ar.com.osde.centroValidador.pos.PosMessageResponse;
import ar.com.osde.centroValidador.pos.SocioPosMessageBase;

/**
 * Realiza la trasnformacion de una transaccion a lo que corresponda
 * @author VA27789605
 *
 */
public interface TransaccionTransformer {

	/**
	 * Transforma el mensaje a un string de formato fijo
	 * @param posResponse
	 * @return
	 */
	String toFixedFormat(PosMessageResponse posResponse);
	
	/**
	 * Transforma el mensaje de respuesta  a un XML
	 * @param posResponse
	 * @return
	 */
	String toXMLFormat(PosMessageResponse posResponse);

	
	/**
	 * Transforma un string que representa un mensaje en formato fijo a un 
	 * @param stream
	 * @return
	 */
	SocioPosMessageBase fromStreamToFixedFormat(String stream);
	
}
