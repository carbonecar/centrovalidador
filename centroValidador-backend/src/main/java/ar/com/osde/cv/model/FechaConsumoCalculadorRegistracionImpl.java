package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.transaccion.TipoTransaccion;

/**
 * Clase helper para calcular la fecha de consumo
 * 
 * @author MT27789605
 * 
 */
public class FechaConsumoCalculadorRegistracionImpl implements FechaConsumoCalculador {


    public FechaConsumoCalculadorRegistracionImpl() {

    }

   
    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.model.FechaConsumoCalculador#buildFechaConsumo
     * (ar.com.osde.centroValidador.pos.PosMessage,
     * ar.com.osde.centroValidador.model.TransaccionPos)
     */
    public Date buildFechaConsumo(PosMessage posMessage, TransaccionPos transaccion) {

        Date fechaConsumo = posMessage.getFechaPracticaOffLine() != null ? posMessage.getFechaPracticaOffLine()
                .toDateTime(posMessage.getHoraTransaccion()).toDate() : null;
        TipoTransaccion tipoTransaccion = transaccion.getTransaccion().getTipoTransaccion();
        
        //Si no hay fecha de consumo o no es una transaccion offline
        if (fechaConsumo == null || !tipoTransaccion.isOffLne()) {
            fechaConsumo = (transaccion.getTransaccionConsumo().getSolicitudConsumo().getFechaSolicitudServicio());
        }

        return fechaConsumo;
    }

}
