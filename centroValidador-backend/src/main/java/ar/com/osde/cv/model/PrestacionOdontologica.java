package ar.com.osde.cv.model;

/**
 * 
 * @author VA27789605
 *
 */
public class PrestacionOdontologica extends Prestacion {
	private int cara;
	private int pieza;
	private int sector;

	public int getCara() {
		return cara;
	}

	public void setCara(int cara) {
		this.cara = cara;
	}

	public int getPieza() {
		return pieza;
	}

	public void setPieza(int pieza) {
		this.pieza = pieza;
	}

	public int getSector() {
		return sector;
	}

	public void setSector(int sector) {
		this.sector = sector;
	}

}
