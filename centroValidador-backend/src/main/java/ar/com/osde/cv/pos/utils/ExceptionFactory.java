package ar.com.osde.cv.pos.utils;

import java.util.List;
import java.util.Set;

import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoAptitud;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoErrorMDR;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoRulesException;
import ar.com.osde.centroValidador.pos.exception.TimeOutRulesException;
import ar.com.osde.centroValidador.pos.exception.TipoAtributoInvalido;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.entities.aptitud.AtributoInconsistencia;
import ar.com.osde.entities.aptitud.ObservacionDetalle;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.transaccionService.InconsistenciasFactory;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;

/**
 * Factoria de excepciones
 * 
 * @author MT27789605
 * 
 */
public class ExceptionFactory extends ar.com.osde.common.exception.ExceptionFactory {

    /**
     * Crea la excepcion en funcion del codigo de aptitud. Devuelve null en caso
     * que no aplique crearla
     * 
     * @param code
     * @param mensaje
     * @param respuesta
     */
    public static TimeOutRulesException createTimeOutExceptionByErrorAptitudCode(ObservacionDetalle observacionDetalle,
            RespuestaCentroValidador respuesta) {
        TimeOutRulesException exception = null;
        int code = observacionDetalle.getCodigo();
        String mensaje = observacionDetalle.getDescripcion();
        switch (code) {
        case InconsistenciasFactory.MDR_ERROR_CODE:
            exception = new TimeOutForzadoErrorMDR(mensaje, respuesta);
            break;
        case InconsistenciasFactory.CODIGO_TIMEOUT:
            exception = new TimeOutForzadoRulesException(collectAtributos(observacionDetalle), mensaje, respuesta);
            break;
        case InconsistenciasFactory.ERROR_APTITUDSERVICE_CODE:
            exception = new TimeOutForzadoAptitud(mensaje, respuesta);
            break;
        default:
            break;
        }
        return exception;
    }

    public static void throwsOnCondition(boolean shouldThrow, RuntimeException ex) {
        if (shouldThrow && ex != null) {
            throw ex;
        }
    }

    public static void createFixedFormatException(boolean condition, String message) {
        if (condition) {
            throw new FixedFormatException(message);
        }
    }

    public static void notNullMessage(IPosMessage posMessage, String message) {

        if (posMessage == null) {
            throw new RuntimeException(message);

        }
    }

    public static void createUnsupportedMessage(boolean condition, String message) {
        if (condition) {
            throw new UnsupportedPosMessageException(message);
        }
    }

    public static void createFilteredMessage(boolean condition, String message) {
        if (condition) {
            throw new FilteredMessageException(message);
        }
    }

    public static void createBusinessException(boolean condition, String message) throws BusinessException {
        if (condition) {
            throw new BusinessException(message);
        }
    }

    public static void createTipoAtributoInvalido(boolean condition, String message) {
        if (condition) {
            throw new TipoAtributoInvalido(message);
        }

    }

    /**
     * Retorna la descripcion de los atributos que participaron en el error y
     * son del tipo AtrDetallesLst
     * 
     * @param observacionDetalle
     * @return
     */
    private static List<String> collectAtributos(ObservacionDetalle observacionDetalle) {
        Set<AtributoInconsistencia> atributos = observacionDetalle.getAtributos();
        AtrDetalleLstCollector atrCollector = new AtrDetalleLstCollector();
        if (atributos != null) {
            for (AtributoInconsistencia atributoInconsistencia : atributos) {
                atributoInconsistencia.accept(atrCollector);
            }
        }
        return atrCollector.getDetalles();
    }
}
