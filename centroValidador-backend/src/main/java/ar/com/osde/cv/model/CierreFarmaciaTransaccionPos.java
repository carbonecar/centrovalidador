package ar.com.osde.cv.model;

import java.util.Date;

import ar.com.osde.entities.transaccion.TransaccionCierre;

/**
 * Clase que decora. 
 * a) PosMessage (mensaje parseado)
 * b) TransaccionCierre
 * @author MT27789605
 *
 */
public class CierreFarmaciaTransaccionPos extends AbstractTransaccionPos {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private TransaccionCierre transaccionCierre;
    public void accept(TransaccionPosVisitor bo) {
        bo.visit(this);

    }
    

    public TransaccionCierre getTransaccionCierre() {
        return transaccionCierre;
    }


    public void setTransaccionCierre(TransaccionCierre transaccionCierre) {
        this.transaccionCierre = transaccionCierre;
    }


    @Override
    public TransaccionCierre getTransaccion() {
        return transaccionCierre;
    }

    
    public void setTransaccion(TransaccionCierre transaccionCierre) {
        this.transaccionCierre = transaccionCierre;
    }

    @Deprecated
    @Override
    public Date getFechaSolicitudServicio() {
        return this.transaccionCierre.getSolicitudServicio().getFechaSolicitudServicio();
    }

}
