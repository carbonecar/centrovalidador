package ar.com.osde.cv.migracionas400.dao;

/**
 * Builder para el filtro
 * @author MT27789605
 *
 */
public class TransaccionMigracionFilterBuilder {

    private TransaccionMigracionFilter filter = new TransaccionMigracionFilter();

    public TransaccionMigracionFilter build() {
        return filter;
    }

    public TransaccionMigracionFilterBuilder withCodigoFilial(String codigoFilial) {
        this.filter.setCodigoFilial(codigoFilial);
        return this;
    }

}
