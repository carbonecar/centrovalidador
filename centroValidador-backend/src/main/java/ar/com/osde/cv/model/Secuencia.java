package ar.com.osde.cv.model;

/**
 * 
 * @author VA27789605
 * 
 */
public class Secuencia {

	private int numeroSecuenciaMensaje;

	private int numeroTotalMensajes;

	public int getNumeroSecuenciaMensaje() {
		return numeroSecuenciaMensaje;
	}

	public void setNumeroSecuenciaMensaje(int numeroSecuenciaMensaje) {
		this.numeroSecuenciaMensaje = numeroSecuenciaMensaje;
	}

	public int getNumeroTotalMensajes() {
		return numeroTotalMensajes;
	}

	public void setNumeroTotalMensajes(int numeroTotalMensajes) {
		this.numeroTotalMensajes = numeroTotalMensajes;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(numeroSecuenciaMensaje).append("/")
				.append(numeroTotalMensajes);
		return str.toString();
	}

}
