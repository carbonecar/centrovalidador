package ar.com.osde.cv.model;

public interface TransaccionSecuencialPosVisitor {
	public void visit(TransaccionPrestacionPos transaccion);

	public void visit(RegistracionMedicamentoMessage transaccion);
}
