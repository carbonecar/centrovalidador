package ar.com.osde.cv.dao.impl;

import ar.com.osde.cv.dao.CodigoExceptuableDAO;
import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.framework.business.crud.GenericCRUDBOImpl;
import ar.com.osde.framework.business.exception.BusinessException;

import java.util.List;

/**
 * Dao para los codigos que se except�an. Se decora el generic crud del FWK
 * @author MT27789605
 *
 */
public class CodigoExceptuableDAOImpl implements CodigoExceptuableDAO  {

    private GenericCRUDBOImpl<CodigoExceptuable> genericCrud;

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#saveNew(ar.com.osde.entities.CodigoExceptuable)
     */
    public void saveNew(CodigoExceptuable codigoExceptuable) throws BusinessException {
        getGenericCrud().saveNew(codigoExceptuable);
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#update(ar.com.osde.entities.CodigoExceptuable)
     */
    public void update(CodigoExceptuable codigoExceptuable) throws BusinessException {
        getGenericCrud().saveOrUpdate(codigoExceptuable);
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#getAllCodigos()
     */
    public List<CodigoExceptuable> getAllCodigos() throws BusinessException {
        return getGenericCrud().getAll();
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#deleteContainer(ar.com.osde.entities.CodigoExceptuable)
     */
    public void deleteContainer(CodigoExceptuable codigoExceptuable) throws BusinessException {
        getGenericCrud().delete(codigoExceptuable);
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#getGenericCrud()
     */
    public GenericCRUDBOImpl<CodigoExceptuable> getGenericCrud() {
        return genericCrud;
    }

    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#setGenericCrud(ar.com.osde.framework.business.crud.GenericCRUDBOImpl)
     */
    public void setGenericCrud(GenericCRUDBOImpl<CodigoExceptuable> genericCrud) {
        this.genericCrud = genericCrud;
    }


    /* (non-Javadoc)
     * @see ar.com.osde.cv.dao.impl.CodigoExceptuableDAO#getById(java.lang.Long)
     */
    public CodigoExceptuable getById(Long id) throws BusinessException {
        return this.genericCrud.getById(id);
    }
}
