package ar.com.osde.cv.model;

import java.util.Date;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractNumeroTransaccionAnterior;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler;

/**
 * Para las transacciones tipo 4 que utilizan una transaccion anterior. En caso
 * de ser web de gestion (WG) el operador es el de la terminal
 * 
 * @author MT27789605
 * 
 * @param <T>
 * @param <E>
 */
public abstract class AbstractTransaccion4XBuilderStrategy<T extends AbstractTransaccionReferencia, E extends AbstractNumeroTransaccionAnterior>
        extends AbstractTransaccionPosBuilderStrategyImpl<T, E> {

    public AbstractTransaccion4XBuilderStrategy() {
        super();
    }

    @Override
    protected T buildSolicitud(AbstractPosMessage posMessage, T transaccion) {
        super.buildSolicitud(posMessage, transaccion);
        // TODO evitar el casteo.
        AbstractNumeroTransaccionAnterior transaccionAnteriorPosMessage = (AbstractNumeroTransaccionAnterior) posMessage;
        if (FixedFormatMessageHandler.WEB_GESTION.equalsIgnoreCase(transaccion.getTransaccion().getCodigoOperadorTransaccion())) {
            transaccion.getTransaccionReferencia().setCodigoOperador(transaccionAnteriorPosMessage.getOperadorTransaccionReferencia());
        }
        if (transaccion.getTransaccionReferencia().getCodigoOperador() != null) {
            String codigoOperador = transaccion.getTransaccionReferencia().getCodigoOperador();
            if ("".equals(codigoOperador.trim()) || "0".equals(codigoOperador.trim())) {
                transaccion.getTransaccionReferencia().setCodigoOperador(null);
            }
        }

        Date localDate = transaccionAnteriorPosMessage.getFechaTransaccionReferencia();
        if (localDate != null) {
            transaccion.getTransaccionReferencia().setFechaTransaccionOriginal(localDate);
        }
        return transaccion;
    }

    public T buildTransaccion(Mapper beanMapper, E posMessage) {
        T trx = this.buildTransaccion(beanMapper, posMessage, (Class<T>) getClazz());
        return trx;
    }

    public abstract Class<? extends AbstractTransaccionPos > getClazz();

}
