package ar.com.osde.cv.migracionas400.dao;

import java.util.List;

import ar.com.osde.cv.migraciontoas400.persistence.MensajeMigracion;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;

/**
 * DAO para el recupero de los mensajes a migrar.
 * @author MT27789605
 *
 */
public interface MensajeMigracionDAO extends GenericCRUDDao<MensajeMigracion>{

    public List<MensajeMigracion> findByFilter(TransaccionMigracionFilter filter);
    public Integer crearMensajesMQ(final TransaccionMigracionFilter filter);
    public Integer deleteAllMessage(TransaccionMigracionFilter filter);
}
