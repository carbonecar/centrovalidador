package ar.com.osde.cv.model;

import java.util.Date;

import org.dozer.Mapper;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.CierreFarmaciaPosMessage;
import ar.com.osde.entities.aptitud.TipoConsultaTransaccion;
import ar.com.osde.entities.transaccion.FormatoPublicacion;

public class TransaccionCierreBuilderStrategy extends
        AbstractTransaccionPosBuilderStrategyImpl<CierreFarmaciaTransaccionPos, CierreFarmaciaPosMessage> {

    public CierreFarmaciaTransaccionPos buildTransaccion(Mapper beanMapper, CierreFarmaciaPosMessage posMessage) {
        CierreFarmaciaTransaccionPos trx = this.buildTransaccion(beanMapper, posMessage, CierreFarmaciaTransaccionPos.class);

        // TODO: ver si posMessage y trx.getPosMessage es el mismo, si es asi
        // hay que sacar el parametro!
        CierreFarmaciaPosMessage cierrePosMessage = (CierreFarmaciaPosMessage) trx.getPosMessage();
        if (CierreFarmaciaPosMessage.PUNTO_VENTA.equals(cierrePosMessage.getPuntoVentaPrestador())) {
            trx.getTransaccion().getSolicitudServicio().setPuntoVenta(true);
        }
        if (CierreFarmaciaPosMessage.CSV.equals(cierrePosMessage.getFormatoPublicacion())) {
            trx.getTransaccion().setFormatoPublicacion(FormatoPublicacion.CSV);
        } else {
            trx.getTransaccion().setFormatoPublicacion(FormatoPublicacion.PDF);
        }
        trx.getTransaccion().getSolicitudServicio().setTipoConsultaTransaccion(TipoConsultaTransaccion.PENDIENTE_CIERRE);
        return trx;
    }

    @Override
    protected CierreFarmaciaTransaccionPos buildFechas(AbstractPosMessage posMessage, CierreFarmaciaTransaccionPos transaccionPos) {
        CierreFarmaciaTransaccionPos cierreTransaccion = super.buildFechas(posMessage, transaccionPos);
        CierreFarmaciaPosMessage cierrePosMessage = (CierreFarmaciaPosMessage) posMessage;
        if (cierrePosMessage.getFechaHastaRecuperar() != null) {
            // Agregamos 1 dia y le restamos 1 milisegundo para que entren todas
            // las transacciones del dia de hoy, inclusive.
            Date fechaHastaRecuperar = cierrePosMessage.getFechaHastaRecuperar().toDateTimeAtStartOfDay().plusDays(1).minusMillis(1)
                    .toDate();
            // Si la fecha es mayor a hoy-1 dia, entonces le poneos como fecha
            // limite el dia hoy-1
            // construyo hoy -1
            // Construimos el dia de ayer:
            fechaHastaRecuperar = buildDateForPendientesAndCierre(fechaHastaRecuperar, transaccionPos.getTransaccion()
                    .getSolicitudServicio().getFechaSolicitudServicio());
            cierreTransaccion.getTransaccionCierre().getSolicitudServicio().setFechaTransaccionHasta(fechaHastaRecuperar);
        }
        return cierreTransaccion;
    }

    @Override
    protected CierreFarmaciaTransaccionPos buildSolicitud(AbstractPosMessage posMessage, CierreFarmaciaTransaccionPos message) {
        super.buildSolicitud(posMessage, message);
        return message;
    }

}
