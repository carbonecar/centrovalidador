package ar.com.osde.cv.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.entities.aptitud.TipoElemento;
import ar.com.osde.entities.aptoServicio.Matricula;
import ar.com.osde.entities.aptoServicio.PedidoMedico;
import ar.com.osde.entities.transaccion.ItemPrestador;

/**
 * Clase base para la construccion de las transacciones de pos con items de
 * consumo
 * 
 * @author MT27789605
 * 
 * @param <T>
 * @param <E>
 */
public abstract class AbstractPrestacionTransaccionPosBuilderStrategyImpl<T extends AbstractTransaccionPrestacionPos, E extends PosMessage>
        extends AbstractTransaccionConsumoPosBuilderStrategyImpl<T, E> {

    public AbstractPrestacionTransaccionPosBuilderStrategyImpl(FechaConsumoCalculador fechaConsumoCalculador) {
        super(fechaConsumoCalculador);
    }

    /**
     * Creacion de las prestaciones
     * 
     * @param posMessage
     * @param transaccion
     */
    protected void buildPrestaciones(PosMessage posMessage, T transaccion) {

        Vector<ItemPrestador> prestaciones = new Vector<ItemPrestador>();
        if (posMessage.getCodigoPrestacion1() != 0) {
            // TODO
            // VER SI LOS CAMPOS NO SON NULL Y SI ES ASI CARGAR UNA PRESTACION
            // ODONTOLOGICA, NOT NULL OR EMPTY
            // NO ESTA DEFINIDO QUE DETERMINA CUANDO UNA PRESTACION ES
            // ODONTOLOGICA O NO.
            prestaciones.add(buildItemPrestador1(posMessage, transaccion));

        }

        if (posMessage.getCodigoPrestacion2() != 0) {
            prestaciones.add(this.buildItemPrestador(posMessage.getCodigoPrestacion2(), posMessage.getArancelPrestacion2(), posMessage
                    .getFrecuenciaOCantidadPrestacion2(), posMessage.getTipoPrestacion2(), transaccion.getBeneficiarioBid().getOrden(),
                    posMessage, transaccion));
        }

        if (posMessage.getCodigoPrestacion3() != 0) {
            prestaciones.add(this.buildItemPrestador(posMessage.getCodigoPrestacion3(), posMessage.getArancelPrestacion3(), posMessage
                    .getFrecuenciaOCantidadPrestacion3(), posMessage.getTipoPrestacion3(), transaccion.getBeneficiarioBid().getOrden(),
                    posMessage, transaccion));
        }

        if (posMessage.getCodigoPrestacion4() != 0) {
            prestaciones.add(this.buildItemPrestador(posMessage.getCodigoPrestacion4(), posMessage.getArancelPrestacion4(), posMessage
                    .getFrecuenciaOCantidadPrestacion4(), posMessage.getTipoPrestacion4(), transaccion.getBeneficiarioBid().getOrden(),
                    posMessage, transaccion));
        }
        transaccion.getTransaccionConsumo().setItemsPrestador(prestaciones);

    }

    protected ItemPrestador buildItemPrestador1(PosMessage posMessage, T transaccion) {
        return this.buildItemPrestador(posMessage.getCodigoPrestacion1(), posMessage.getArancelPrestacion1(), posMessage
                .getFrecuenciaOCantidadPrestacion1(), posMessage.getTipoPrestacion1(), transaccion.getBeneficiarioBid().getOrden(),
                posMessage, transaccion);
    }

    /**
     * Template method para la construccion de los elementos particulares
     * 
     * @param posMessage
     * @param transaccion
     * @param p
     */
    protected abstract void buildElementosParticularesItem(PosMessage posMessage, T transaccion, ItemPrestador p);

    /**
     * Creacion de los items del prestador
     * 
     * @param codigoPrestacion
     * @param arancel
     * @param frecuencia
     * @param tipoPrestacion
     * @param orden
     * @param posMessage
     * @param transaccion
     * @return
     */
    protected ItemPrestador buildItemPrestador(int codigoPrestacion, int arancel, int frecuencia, int tipoPrestacion, String orden,
            PosMessage posMessage, T transaccion) {
        ItemPrestador p = new ItemPrestador();
        p.setCodigoPrestacionPrestador(String.format(ItemPrestador.FORMATO_PRESTACION, codigoPrestacion));
        p.setArancel(arancel);
        p.setFrecuencia(frecuencia);

        // TipoPrestacion.getTipoByValue(tipoPrestacion).getValue();
        // Comento el codigo porque se estaba buscando el tipo de prestacion
        // segun el tipo informado. El problema es que
        // el tipo de prestacion tiene la misma numeracion que el informado, asi
        // que no tiene sentido y por otro lado, cuando
        // no encuentra nada devuelve null
        p.setAmbito(tipoPrestacion);
        p.setOrdenBeneficiario(orden);
        p.setFechaConsumo(this.buildFechaConsumo(posMessage, transaccion));

        Matricula matricula = this.buildMatriculaMatricula(posMessage);
        if (matricula != null) {
            PedidoMedico pedidoMedico = new PedidoMedico();
            pedidoMedico.setMatriculaPrescriptor(matricula);
            p.setPedidoMedico(pedidoMedico);
        }
        if (posMessage.getCodigoFinalizacionTerapia() != 0) {
            // CodigoFinTerapia codigoFinTerapia =
            // CodigoFinTerapia.getFromValue(posMessage.getCodigoFinalizacionTerapia());
            p.setCodigoFinTerapia(posMessage.getCodigoFinalizacionTerapia());
        }
        List<String> codigosDiagnosticos = new ArrayList<String>();

        this.agregarCodigoDiagnostico(posMessage.getDiagnosticoPsicopatologia1(), codigosDiagnosticos);
        this.agregarCodigoDiagnostico(posMessage.getDiagnosticoPsicopatologia2(), codigosDiagnosticos);

        // p.getco posMessage.getDiagnosticoPsicopatologia1()
        // buildAutorizacion(transaccion, p);
        this.buildElementosParticularesItem(posMessage, transaccion, p);
        p.setTipoElemento(this.getTipoElemento());
        return p;
    }

    private void agregarCodigoDiagnostico(String codigo, List<String> codigos) {
        if (codigo != null && !"".equals(codigo.trim())) {
            codigos.add(codigo);
        }
    }

    // private void buildAutorizacion(T transaccion, ItemPrestador p) {
    // AutorizacionBid autorizacionBid = new AutorizacionBid();
    // autorizacionBid.setNumeroAutorizacion(transaccion.getOrden().getNumeroAutorizacion());
    // String delegacion = transaccion.getOrden().getDelegacion();
    // if (StringUtils.isNumeric(delegacion)) {
    // autorizacionBid.setNumeroEmisor(Integer.parseInt(delegacion));
    // } else {
    // LOG.warn("Se crea una autorizacion sin numero de emisor por venir informado un valor no numerico: "
    // + delegacion);
    // }
    //
    // p.setAutorizacionBid(autorizacionBid);
    // }
    protected abstract TipoElemento getTipoElemento();

}
