package ar.com.osde.comunes.utils;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.AbstractFixedFormatter;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;

public class LocalTimeFormatter extends AbstractFixedFormatter<LocalTime> {

	public LocalTime asObject(String string, FormatInstructions instructions)
			throws FixedFormatException {
		LocalTime result = null;
		try {
			DateTimeFormatter formatter = DateTimeFormat
					.forPattern(instructions.getFixedFormatPatternData()
							.getPattern());
			DateTime dateTime = formatter.parseDateTime(string);
			result = new LocalTime(dateTime);
		} catch (Exception e) {
			throw new FixedFormatException("Could not parse value[" + string
					+ "] by pattern["
					+ instructions.getFixedFormatPatternData().getPattern()
					+ "] to " + LocalTime.class.getName(), e);
		}
		return result;
	}

	public String asString(LocalTime time, FormatInstructions instructions) {
		String result = null;

		if (time != null) {
			DateTimeFormatter formatter = DateTimeFormat
					.forPattern(instructions.getFixedFormatPatternData()
							.getPattern());
			result = formatter.print(time);
		}

		return result;
	}
}
