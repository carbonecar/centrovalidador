package ar.com.osde.comunes.utils;

import java.util.Collection;
import java.util.HashMap;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ar.com.osde.centroValidador.factory.UtilFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import ar.com.osde.centroValidador.pos.mq.CustomDefaultMessageListenerContainer;
import ar.com.osde.centroValidador.pos.mq.PosMDP;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.cv.factory.HandlerFactory;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Clase encargada del ABM de DefaultMessageListenerContainer. Contiene en
 * memoria todos los container que hay.
 */
public class ListenerManager implements InitializingBean {

	private HashMap<Long, DefaultMessageListenerContainer> listenerHash = new HashMap<Long, DefaultMessageListenerContainer>();
	private HandlerFactory handlerFactory;
	private boolean acceptMessageWhileStoping = true;

	private TransaccionServiceStatistics serviceStatics;

	public TransaccionServiceStatistics getServiceStatics() {
		return serviceStatics;
	}

	public void setServiceStatics(TransaccionServiceStatistics serviceStatics) {
		this.serviceStatics = serviceStatics;
	}

	public HandlerFactory getHandlerFactory() {
		return handlerFactory;
	}

	public void setHandlerFactory(HandlerFactory handlerFactory) {
		this.handlerFactory = handlerFactory;
	}

	public void appendListener(ContainerConfiguration containerConfiguration) throws BusinessException {

		DefaultMessageListenerContainer listener = createRealContainer(containerConfiguration);
		listenerHash.put(containerConfiguration.getId(), listener);

		if (containerConfiguration.isStartOnLoad()) {
			listener.afterPropertiesSet();
		}
	}

	public void deleteListener(Long containerId) {

		DefaultMessageListenerContainer containerToRemove = listenerHash.get(containerId);
		containerToRemove.stop();
		// containerToRemove.destroy();
		listenerHash.remove(containerId);
	}

	public void updateListener(ContainerConfiguration containerConfiguration) throws BusinessException {

		deleteListener(containerConfiguration.getId());

		appendListener(containerConfiguration);
	}

	public Collection<DefaultMessageListenerContainer> getAllListenerOnMemory() {
		return listenerHash.values();

	}

	public DefaultMessageListenerContainer getListener(long id) {
		DefaultMessageListenerContainer container = listenerHash.get(id);
		if (container != null) {
			container.setAcceptMessagesWhileStopping(acceptMessageWhileStoping);
		}
		return container;
	}

	private DefaultMessageListenerContainer createRealContainer(ContainerConfiguration containerConfiguration)
	        throws BusinessException {
		// TODO: Cambiar el container por uno que sea inyectado u obtenido de
		// una factory donde se configuren los parametros por defecto. Usar el
		// custom
		CustomDefaultMessageListenerContainer container = new CustomDefaultMessageListenerContainer();
		container.setCacheLevel(DefaultMessageListenerContainer.CACHE_NONE);
		container.setStartOnLoad(containerConfiguration.isStartOnLoad());
		container.setConcurrentConsumers(containerConfiguration.getNumberOfConcurrentConsumers());
		container.setAcceptMessagesWhileStopping(acceptMessageWhileStoping);
		Destination destination = null;

		try {
			Context context = (Context) new InitialContext().lookup("");
			destination = (Destination) context.lookup(containerConfiguration.getQueueName());
		} catch (NamingException e) {
			throw new BusinessException(e.getMessage());
		}

		ConnectionFactory connectionFactoryContainer = UtilFactory
		        .obtainConnectionFactory(containerConfiguration.getConnectionFactoryName());
		// TODO: arrojar una exception porque no esta el DS para esto.
		container.setConnectionFactory(connectionFactoryContainer);
		container.setDestination(destination);

		// TODO: hacer una factory donde se cree esta interfaz por medio de IOC
		PosMDP messageListener = new PosMDP(containerConfiguration.getContainerDescription());
		messageListener.setStatics(this.serviceStatics);
		messageListener
		        .setErrorHandler(this.handlerFactory.createRealErrorHandler(containerConfiguration.getErrorHandler()));
		messageListener
		        .setMessageHandler(this.handlerFactory.createHandler(containerConfiguration.getPosMessageHandler()));

		container.setMessageListener(messageListener);

		return container;
	}

	public DefaultMessageListenerContainer getListenerOnMemory(Long id) {
		return this.listenerHash.get(id);
	}

	public void afterPropertiesSet() throws Exception {
		this.handlerFactory.setStadistics(this.serviceStatics);
	}

}