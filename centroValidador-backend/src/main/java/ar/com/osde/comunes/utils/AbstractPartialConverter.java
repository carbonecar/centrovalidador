package ar.com.osde.comunes.utils;

import org.dozer.DozerConverter;
import org.joda.time.base.AbstractPartial;

public class AbstractPartialConverter extends DozerConverter<AbstractPartial, AbstractPartial> {

	public AbstractPartialConverter() {
		super(AbstractPartial.class, AbstractPartial.class);
	}

	@Override
	public AbstractPartial convertFrom(AbstractPartial source,
			AbstractPartial destination) {
		return source;
	}

	@Override
	public AbstractPartial convertTo(AbstractPartial source,
			AbstractPartial destination) {
		return source;
	}

	
	/*
	public class AbstractPartialConverter implements CustomConverter
	 
	public Object convert(Object destination, Object source, 
			Class<?> destClass, Class<?> sourceClass) {

		if (source == null) {
			return null;
		} else if (source instanceof AbstractPartial) {
			// son immutable, con lo cual pueden usar el mismo objeto.
			return source;
		} else {
			throw new MappingException(
					"Converter AbstractPartialConverter used incorrectly. Arguments passed in were:"
							+ destination + " and " + source);
		}

	}*/
}
