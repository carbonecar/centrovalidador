package ar.com.osde.comunes.utils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import ar.com.osde.entities.aptitud.Inconsistencia;
import ar.com.osde.entities.aptoServicio.ItemConsumoRespuesta;

/**
 *
 */
public class ItemConsumoUtils {

    public static final String ITEM_ERROR="ER";
    private ItemConsumoUtils(){
        
    }
    /**
     * Determina si un item fue aprobado o no
     * 
     * @param itemRespuesta
     * @return
     */
    public static boolean isApproved(ItemConsumoRespuesta itemRespuesta) {
        // No esta aprobado si hay al menos una inconsistencia no exceputada
        List<Inconsistencia> inconsistencias = itemRespuesta.getInconsistencias();

        boolean isApproved = true;

        if (inconsistencias != null) {

            Inconsistencia inc = (Inconsistencia) CollectionUtils.find(inconsistencias, new Predicate() {

                public boolean evaluate(Object object) {
                    Inconsistencia inc = (Inconsistencia) object;
                    return !inc.isExceptuada();
                }
            });
            isApproved = inc == null;
        }
        return isApproved;
    }
    
    public static String approvedString(ItemConsumoRespuesta itemRespuesta){
        boolean isApproved=isApproved(itemRespuesta);
        String approvedValue="";
        if(!isApproved){
            approvedValue=ITEM_ERROR;
        }
        return approvedValue;
    }
}
