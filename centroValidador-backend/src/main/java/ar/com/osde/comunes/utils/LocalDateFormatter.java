package ar.com.osde.comunes.utils;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ancientprogramming.fixedformat4j.format.AbstractFixedFormatter;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;

public class LocalDateFormatter extends AbstractFixedFormatter<LocalDate> {

    private static final Logger LOG = Logger.getLogger(LocalDateFormatter.class);

    public LocalDate asObject(String string, FormatInstructions instructions) {
        LocalDate result = null;
        String pattern = instructions.getFixedFormatPatternData().getPattern();

        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
            DateTime dateTime = formatter.parseDateTime(string);
            result = new LocalDate(dateTime);
        } catch (Exception e) {
            LOG.error(e);
            try {
                DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
                DateTime dateTime = formatter.withZone(DateTimeZone.UTC).parseDateTime(string);
                result = new LocalDate(dateTime);

            } catch (Exception e2) {
                LOG.error(e2);
            }
        }
        return result;
    }

    public String asString(LocalDate date, FormatInstructions instructions) {
        String result = "";

        if (date != null) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern(instructions.getFixedFormatPatternData().getPattern());
            result = formatter.print(date);
        }

        return result;
    }
}
