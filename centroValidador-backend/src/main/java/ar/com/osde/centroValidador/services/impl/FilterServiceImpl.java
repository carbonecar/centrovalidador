package ar.com.osde.centroValidador.services.impl;

import java.lang.reflect.Method;
import java.util.*;

import ar.com.osde.centroValidador.factory.filter.configuration.IFilterConfigurationFactory;
import ar.com.osde.centroValidador.services.pos.HandlerService;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.framework.persistence.exception.PersistenceException;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.services.pos.FilterService;
import ar.com.osde.cv.dao.FilterStrategyDAO;
import ar.com.osde.cv.dao.impl.FilterDAOImpl;
import ar.com.osde.cv.dao.impl.FilterStrategyDAOImpl;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

import javax.jws.WebParam;

import org.apache.log4j.Logger;

public class FilterServiceImpl implements FilterService {
    private static final Logger LOG = Logger.getLogger(FilterServiceImpl.class);

    public FilterDAOImpl filterDao;
    public FilterStrategyDAO filterStrategyDao;
    private Map<String, IFilterConfigurationFactory> filterConfigurationFactory;

    private static final String SPACE = " ";
    private static final String NONE = "";
    private static final String EQUAL_OPERATOR = "==";
    private static final String AND_OPERATOR = "AND";
    private static final String OR_OPERATOR = "OR";
    private static final String NOT_OPERATOR = "NOT";
    private HandlerService handlerService;

    public void updateFilterStrategy(@WebParam(name = "filterStrategyConfiguration") FilterStrategyConfiguration filterStrategyConfiguration) throws ServiceException {
        try {
            this.filterStrategyDao.saveOrUpdate(filterStrategyConfiguration);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public FilterStrategyConfiguration getFilterStrategy(@WebParam(name = "id") Long idFilter) throws ServiceException {
        try {
            return this.filterStrategyDao.getById(idFilter);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public FilterConfiguration getParentFilterForFilter(@WebParam(name = "filterId") Long filterId) {
        return this.filterDao.getParentFilter(filterId);
    }

    public void updateFilter(@WebParam(name = "filterConfiguration") FilterConfiguration filterConfiguration) throws ServiceException {
        try {
            if (filterConfiguration != null) {
                this.filterDao.saveOrUpdate(filterConfiguration);
                this.updateFilterOnMemory(filterConfiguration);
            }
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateFilterOnMemory(FilterConfiguration filterConfiguration) throws ServiceException {
        boolean parentFilterFound;
        long filterId = filterConfiguration.getId();
        do {
            FilterConfiguration parentFilter = this.getParentFilterForFilter(filterId);
            parentFilterFound = parentFilter != null;
            if (parentFilterFound)
                filterId = parentFilter.getId();
        } while (parentFilterFound);

        this.handlerService.updateHandlerOnMemory(this.handlerService.getParentHandlerForFilter(filterId));
    }

    public FilterConfiguration getFilter(@WebParam(name = "id") Long id) throws ServiceException {
        try {
            return filterDao.getById(id);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<FilterConfiguration> createFilter(String expression) throws ServiceException {
        FilterConfiguration filter = crearFilter(expression);
        List<FilterConfiguration> filterList = new LinkedList<FilterConfiguration>();
        filterList.add(filter);
        return filterList;
    }

    public List<String> getPropertiesToFilter() {
        List<String> properties = new ArrayList<String>();
        Method[] methods = PosMessage.class.getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("get")) {
                properties.add(method.getName().trim() + ":" + method.getReturnType().getSimpleName());
            }
        }
        return properties;
    }

    // *********************************************************//
    // ****************Imp Interna******************************//
    // *********************************************************//

    /**
     * Aca es donde se parsea la expresion. Esto esta mal. hay que hacer una arbol de expresion como corresponde.
     */
    public FilterConfiguration crearFilter(String expresionInfija) throws ServiceException {
        LinkedList<String> expresionPolacaInversa = this.generarPolacaInversa(expresionInfija);
        /**
         * Se invierta porque es mas sencillo generar los filtros
         * */
        LinkedList<String> expresionPolacaInversaInvertida = this.invertir(expresionPolacaInversa);
        if (expresionPolacaInversaInvertida.size() > 0) {
            return this.extraerFilter(expresionPolacaInversaInvertida);
        } else {
            throw new ServiceException("La expresion no puede ser vacia");
        }

    }

    private FilterConfiguration extraerFilter(LinkedList<String> expresionPolacaInversaInvertida) throws ServiceException {
        String operador = expresionPolacaInversaInvertida.poll();
        IFilterConfigurationFactory filterConfigurationFactory = this.filterConfigurationFactory.get(operador);
        if (filterConfigurationFactory == null)
            throw new ServiceException(operador + " no es un operador. Revise la expresión formada.");
        FilterConfiguration filter = filterConfigurationFactory.createFilter(expresionPolacaInversaInvertida);
        for (int hijos = 0; hijos < filter.cantidadHijosNecesarios(); hijos++) {
            filter.agregarHijo(this.extraerFilter(expresionPolacaInversaInvertida));
        }
        return filter;
    }

    /**
     * EJEMPLO:
     * B 1 EQUALS B 2 EQUALS B 3 EQUALS AND B 4 EQUALS OR AND B 5 EQUALS NOT OR
     * En
     * OR NOT EQUALS 5 B AND OR EQUALS 4 B AND EQUALS 3 B EQUALS 2 B EQUALS 1 B
     */
    private LinkedList<String> invertir(LinkedList<String> expresionPolacaInversa) {
        LinkedList<String> expresionPolacaInversaInvertida = new LinkedList<String>();
        for (String elemento : expresionPolacaInversa) {
            expresionPolacaInversaInvertida.addFirst(elemento);
        }
        return expresionPolacaInversaInvertida;
    }

    /**
     * Transforma una expresion comun a una expresion en polaca inversa
     * EJEMPLO:
     * Transforma
     * B == 1 AND ( ( B == 2 AND B == 3 ) OR B == 4 ) OR NOT B == 5
     * En
     * B 1 EQUALS B 2 EQUALS B 3 EQUALS AND B 4 EQUALS OR AND B 5 EQUALS NOT OR
     */
    private LinkedList<String> generarPolacaInversa(String expresionInfija) throws ServiceException {
        String expresionTransformada = expresionInfija.trim().replaceAll(Operador.AND.getDescripcion(), Operador.AND.name()).replaceAll(Operador.OR.getDescripcion(), Operador.OR.name()).replaceAll(Operador.EQUALS.getDescripcion(), Operador.EQUALS.name()).replaceAll(Operador.PARENTESISDERECHO.getDescripcion(), Operador.PARENTESISDERECHO.name()).replaceAll(Operador.PARENTESISIZQUIERDO.getDescripcion(), Operador.PARENTESISIZQUIERDO.name());
        String[] listaDeExpresionNotacionInfija = expresionTransformada.split(SPACE);
        LinkedList<String> listaSalidaNotacionPostFija = new LinkedList<String>();
        LinkedList<String> pilaAuxiliar = new LinkedList<String>();
        for (String elemento : listaDeExpresionNotacionInfija) {
            if (!NONE.equals(elemento))
                if (elemento.equals(Operador.AND.name()) || elemento.equals(Operador.OR.name())) {
                    String elementoAux = pilaAuxiliar.poll();
                    elementoAux = procesarOperador(listaSalidaNotacionPostFija, pilaAuxiliar, elementoAux);
                    if (Operador.PARENTESISIZQUIERDO.name().equals(elementoAux))
                        pilaAuxiliar.push(elementoAux);
                    pilaAuxiliar.push(elemento);
                } else if (elemento.equals(Operador.NOT.name())) {
                    String elementoAux = pilaAuxiliar.poll();
                    while (elementoAux != null && !Operador.PARENTESISIZQUIERDO.name().equals(elementoAux) && !Operador.AND.name().equals(elementoAux) && !Operador.OR.name().equals(elementoAux)) {
                        listaSalidaNotacionPostFija.addLast(elementoAux);
                        elementoAux = pilaAuxiliar.poll();
                    }
                    if (Operador.PARENTESISIZQUIERDO.name().equals(elementoAux) || Operador.AND.name().equals(elementoAux) || Operador.OR.name().equals(elementoAux))
                        pilaAuxiliar.push(elementoAux);
                    pilaAuxiliar.push(elemento);
                } else if (elemento.equals(Operador.EQUALS.name()) || elemento.equals(Operador.PARENTESISIZQUIERDO.name())) {
                    pilaAuxiliar.push(elemento);
                } else if (elemento.equals(Operador.PARENTESISDERECHO.name())) {
                    String elementoAux = pilaAuxiliar.poll();
                    elementoAux = procesarOperador(listaSalidaNotacionPostFija, pilaAuxiliar, elementoAux);
                    if (!Operador.PARENTESISIZQUIERDO.name().equals(elementoAux)) {
                        throw new ServiceException("Cerro Paréntesis que no se abrió nunca");
                    }
                } else {
                    listaSalidaNotacionPostFija.addLast(elemento);
                }
        }
        for (String elemento : pilaAuxiliar) {
            if (!Operador.PARENTESISIZQUIERDO.name().equals(elemento))
                listaSalidaNotacionPostFija.addLast(elemento);
        }
        return listaSalidaNotacionPostFija;

    }

    private String procesarOperador(LinkedList<String> listaSalidaNotacionPostFija, LinkedList<String> pilaAuxiliar, String elementoAux) {
        while (elementoAux != null && !Operador.PARENTESISIZQUIERDO.name().equals(elementoAux)) {
            listaSalidaNotacionPostFija.addLast(elementoAux);
            elementoAux = pilaAuxiliar.poll();
        }
        return elementoAux;
    }

    public List<String> getAvailableOperators() {
        List<String> operators = new ArrayList<String>();
        operators.add(EQUAL_OPERATOR);
        operators.add(NOT_OPERATOR);
        operators.add(AND_OPERATOR);
        operators.add(OR_OPERATOR);
        return operators;
    }

    public FilterDAOImpl getFilterDao() {
        return filterDao;
    }

    public void setFilterDao(FilterDAOImpl filterDao) {
        this.filterDao = filterDao;
    }

    public FilterStrategyDAO getFilterStrategyDao() {
        return filterStrategyDao;
    }

    public void setFilterStrategyDao(FilterStrategyDAO filterStrategyDao) {
        this.filterStrategyDao = filterStrategyDao;
    }

    public Map<String, IFilterConfigurationFactory> getFilterConfigurationFactory() {
        return filterConfigurationFactory;
    }

    public void setFilterConfigurationFactory(Map<String, IFilterConfigurationFactory> filterConfigurationFactory) {
        this.filterConfigurationFactory = filterConfigurationFactory;
    }

    public HandlerService getHandlerService() {
        return handlerService;
    }

    public void setHandlerService(HandlerService handlerService) {
        this.handlerService = handlerService;
    }
}
