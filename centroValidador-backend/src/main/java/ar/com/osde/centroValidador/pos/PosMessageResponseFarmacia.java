package ar.com.osde.centroValidador.pos;

import java.math.BigDecimal;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class PosMessageResponseFarmacia extends SocioPosMessageResponse {

    
    PosMessageResponseFarmacia(){
        
    }
    // AUFPT1
    private BigDecimal precioLista1;
    // AUFPO1
    private double porcentajeCobertura1;
    // AUFPF1
    private BigDecimal precioACargoSocio1;
    // AUFOB1
    private String nombreYPresentacionMedicamento1;

    // AUFPT2
    private BigDecimal precioLista2;
     // AUFPO2
    private double porcentajeCobertura2;
    // AUFPF2
    private BigDecimal precioACargoSocio2;
    // AUFOB2
    private String nombreYPresentacionMedicamento2;

    // AUFPT3
    private BigDecimal precioLista3;
    // AUFPO3
    private double porcentajeCobertura3;
    // AUFPF3
    private BigDecimal precioACargoSocio3;
    // AUFOB3
    private String nombreYPresentacionMedicamento3;


    @Field(offset = 395, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioLista1() {
        return precioLista1;
    }

    public void setPrecioLista1(BigDecimal precioLista1) {
        this.precioLista1 = precioLista1;
    }

    @Field(offset = 404, length = 5, align = Align.RIGHT, paddingChar = '0')
    public double getPorcentajeCobertura1() {
        return porcentajeCobertura1;
    }

    public void setPorcentajeCobertura1(double porcentajeCobertura1) {
        this.porcentajeCobertura1 = porcentajeCobertura1;
    }

    @Field(offset = 409, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioACargoSocio1() {
        return precioACargoSocio1;
    }

    public void setPrecioACargoSocio1(BigDecimal precioACargoSocio1) {
        this.precioACargoSocio1 = precioACargoSocio1;
    }

    @Field(offset = 418, length = 40)
    public String getNombreYPresentacionMedicamento1() {
        return nombreYPresentacionMedicamento1;
    }

    public void setNombreYPresentacionMedicamento1(String nombreMedicamento1) {
        this.nombreYPresentacionMedicamento1 = nombreMedicamento1;
    }

    @Field(offset = 458, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioLista2() {
        return precioLista2;
    }

    public void setPrecioLista2(BigDecimal precioLista2) {
        this.precioLista2 = precioLista2;
    }

    @Field(offset = 467, length = 5, align = Align.RIGHT, paddingChar = '0')
    public double getPorcentajeCobertura2() {
        return porcentajeCobertura2;
    }

    public void setPorcentajeCobertura2(double porcentajeCobertura2) {
        this.porcentajeCobertura2 = porcentajeCobertura2;
    }

    @Field(offset = 472, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioACargoSocio2() {
        return precioACargoSocio2;
    }

    public void setPrecioACargoSocio2(BigDecimal precioACargoSocio2) {
        this.precioACargoSocio2 = precioACargoSocio2;
    }

    @Field(offset = 481, length = 40)
    public String getNombreYPresentacionMedicamento2() {
        return nombreYPresentacionMedicamento2;
    }

    public void setNombreYPresentacionMedicamento2(String nombreMedicamento2) {
        this.nombreYPresentacionMedicamento2 = nombreMedicamento2;
    }

    @Field(offset = 521, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioLista3() {
        return precioLista3;
    }

    public void setPrecioLista3(BigDecimal precioLista3) {
        this.precioLista3 = precioLista3;
    }

    @Field(offset = 530, length = 5, align = Align.RIGHT, paddingChar = '0')
    public double getPorcentajeCobertura3() {
        return porcentajeCobertura3;
    }

    public void setPorcentajeCobertura3(double porcentajeCobertura3) {
        this.porcentajeCobertura3 = porcentajeCobertura3;
    }

    @Field(offset = 535, length = 9, align = Align.RIGHT, paddingChar = '0')
    public BigDecimal getPrecioACargoSocio3() {
        return precioACargoSocio3;
    }

    public void setPrecioACargoSocio3(BigDecimal precioACargoSocio3) {
        this.precioACargoSocio3 = precioACargoSocio3;
    }

    @Field(offset = 544, length = 40)
    public String getNombreYPresentacionMedicamento3() {
        return nombreYPresentacionMedicamento3;
    }

    public void setNombreYPresentacionMedicamento3(String nombreMedicamento3) {
        this.nombreYPresentacionMedicamento3 = nombreMedicamento3;
    }
}
