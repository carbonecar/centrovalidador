package ar.com.osde.centroValidador.pos.mq;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.exception.BadFormatException;
import ar.com.osde.centroValidador.pos.exception.EasyNameSOAPFaultException;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.exception.MigracionOutRulesException;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoAptitud;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoErrorMDR;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoRulesException;
import ar.com.osde.centroValidador.pos.exception.TimeOutRulesException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedTransactionException;
import ar.com.osde.centroValidador.pos.handlers.ErrorHandler;
import ar.com.osde.centroValidador.pos.handlers.HandlerTime;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.entities.aptitud.ObservacionAptitud;
import ar.com.osde.entities.transaccion.MessagePosBuilder;
import ar.com.osde.transaccionService.InconsistenciasFactory;

import com.ancientprogramming.fixedformat4j.format.ParseException;

/**
 * Clase central para el procesamiento de los mensajes. Implementa listener, el
 * control de errores y estadísticas
 * 
 * @author MT27789605
 * 
 */
public class PosMDP implements MessageListener {
    public static final String CRITICAL_LOG = "critical-logger";
    private ErrorHandler errorHandler;
    private PosMessageHandler messageHandler;
    private static final long MIN_MS_TO_LOG = 500;
    public static final int DEFAULT_MAX_LONG_MESSAGE = 1024;

    private transient TransaccionServiceStatistics statistics;
    private String name;

    public TransaccionServiceStatistics getStatics() {
        return statistics;
    }

    public void setStatics(TransaccionServiceStatistics statics) {
        this.statistics = statics;
    }

    /**
     * Constructor vacio necesario para la ioc de spring.
     * */
    public PosMDP() {
    }

    /**
     * Descripcion del contenerdor
     * */
    public PosMDP(String containerDescription) {
        this.name = containerDescription;
    }

    public PosMessageHandler getMessageHandler() {
        return messageHandler;
    }

    public void setMessageHandler(PosMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void onMessage(Message message) {
        String textMessage = null;

        long tiempoTotal = 0;
        long init = System.currentTimeMillis();
        RespuestaCentroValidador respuestaAptitud = null;
        try {
            // validamos que sea un mensaje de texto no vacio para continuar el
            // flujo
            if (message instanceof TextMessage) {
                TextMessage textMsg = ((TextMessage) message);
                if (textMsg.getText() == null) {
                    this.statistics.addNullMessage();
                } else {
                    respuestaAptitud = messageHandler.process(message);
                }
            } else {
                this.doHandleCastError("No se soportan mensajes del tipo " + message.getClass().getSimpleName() + " listener"
                        + this.getName());
            }

        } catch (FilteredMessageException fex) {
            tiempoTotal = System.currentTimeMillis() - init;
            statistics.updateHandlersTime(fex.getHandlersTime());
            statistics.addTransaccionesRuteadas();
        } catch (UnsupportedPosMessageException u) {
            tiempoTotal = System.currentTimeMillis() - init;
            getLog().error("Mensaje descartado UnsupportedPosMessageException : " + u.getMessage());
            doHandleError(message, u.getMessage());
        } catch (EasyNameSOAPFaultException e) {
            tiempoTotal = System.currentTimeMillis() - init;
            getLog().error(e);
            getLog().error("Error Time out o conexion SOAPFaultException: " + e.getServiceName() + e);
            doHandleError(message, e.getServiceName() + e.getMessage());
        } catch (javax.xml.ws.soap.SOAPFaultException e) {
            // TODO: VER SI SE PUEDE MOSTRAR CUAL ES EL
            // SERVCIO QUE ESTA FALLANDO
            tiempoTotal = System.currentTimeMillis() - init;
            getLog().error("Error Time out o conexion SOAPFaultException: " + e, e);
            this.statistics.addSoapFault();
            doHandleError(message, "SOAPFaultException" + e.getMessage());
        } catch (ParseException ex) {
            tiempoTotal = System.currentTimeMillis() - init;
            getLog().error("Imposible parsear el mensaje ParseException" + "error: " + ex.getMessage());
            getLog().error("Transaccion: " + textMessage);
            doHandleError(message, ex.getMessage());
        } catch (BadFormatException be) {
            tiempoTotal = System.currentTimeMillis() - init;
            getLog().error("Imposible parsear el mensaje BadFormatException" + "error: " + be.toString());
            getLog().error("Transaccion: " + textMessage);
            doHandleError(message, be.toString());
        } catch (MigracionOutRulesException ex) {
            tiempoTotal = System.currentTimeMillis() - init;
            // this.stadistics.addTimeOut(); no son timeout, son errores de
            // migracion
            this.getLog().error(ex.getMessage());
            this.getLog().error(ex);
            this.doHandlerErrorMigracion(message);
            respuestaAptitud = ex.getRespuestaCentroValidador();
        } catch (TimeOutForzadoErrorMDR ex) {
            tiempoTotal = System.currentTimeMillis() - init;
            this.statistics.addErrorMDR(true);
            // this.doHandleErrorTimeOut(message, ex.toString());
            respuestaAptitud = ex.getRespuestaCentroValidador();
        } catch (TimeOutForzadoAptitud e) {
            tiempoTotal = System.currentTimeMillis() - init;
            this.statistics.addSoapFault();
            this.getLog().error(e);
            respuestaAptitud = e.getRespuestaCentroValidador();
            doHandleError(message, e.toString());
        } catch (TimeOutForzadoRulesException ex) {
            tiempoTotal = System.currentTimeMillis() - init;
            this.statistics.addTimeoutForzado(ex.getServiciosFallidos());
            this.getLog().error(ex);
            // this.doHandleErrorTimeOutForzado(message, ex.getMessage());
            respuestaAptitud = ex.getRespuestaCentroValidador();
        } catch (TimeOutRulesException ex) {
            // this.stadistics.addTimeOut(); no son timeout, son errores de
            // timeout
            tiempoTotal = System.currentTimeMillis() - init;
            this.getLog().error(ex.toString());
            this.doHandleErrorTimeOut(message, ex.toString());
            respuestaAptitud = ex.getRespuestaCentroValidador();
        } catch (UnsupportedTransactionException e) {
            tiempoTotal = System.currentTimeMillis() - init;
            this.statistics.addUnsupportedTransaccion();
            // enviamos el mensaje a la cola de errores para no perderlo. Pero
            // no tendria que haber mensajes en dicha cola
            // correspondientes a transacciones no soportadas
            // lo comento porque las 4A son muchas. Hay ver que hacer con los TO
            // de las 4A porque pueden generar problemas
            // doHandleError(message, e.toString());
        } catch (Exception t) {
            tiempoTotal = System.currentTimeMillis() - init;
            getLogForCritical().error("Imposible procesar el mensaje " + t.getClass().getName() + " mensaje:" + textMessage);
            getLogForCritical().error("Transaccion: " + textMessage);
            getLogForCritical().error("error grave:", t);
            doHandleError(message, t.getClass().getCanonicalName().concat(t.getMessage() != null ? t.getMessage() : ""));
        }
        if (tiempoTotal == 0) {
            tiempoTotal = System.currentTimeMillis() - init;
        }
        // obtener los errores de persistencia

        if (respuestaAptitud != null && respuestaAptitud.getDecoratee() != null
                && respuestaAptitud.getDecoratee().getRespuestaAptitud() != null) {
            List<ObservacionAptitud> observaciones = MessagePosBuilder.collectObservaciones(respuestaAptitud.getDecoratee()
                    .getRespuestaAptitud());
            for (ObservacionAptitud observacionAptitud : observaciones) {
                if (observacionAptitud != null) {
                    int codigo = observacionAptitud.getCodigo();
                    if (codigo == InconsistenciasFactory.ERROR_PERSISTENCIA) {
                        this.doHandleError(message, "persistencia: ");
                    }
                }
            }
        }

        buildStatistics(textMessage, tiempoTotal, respuestaAptitud);
    }

    private void buildStatistics(String textMessage, long tiempoTotal, RespuestaCentroValidador respuestaCentroValidador) {
        StringBuilder strBuffTiempos = new StringBuilder();

        if (respuestaCentroValidador != null) {
            AbstractPosMessage posMessage = respuestaCentroValidador.getPosMessage();
            if (respuestaCentroValidador.getPosMessage() != null) {
                if ("A".equals(posMessage.getAtributoCodigoTransaccion()) && 1 == posMessage.getCodigoTransaccion()) {
                    this.statistics.updateHandlerTime(new HandlerTime(AptoServicioBO.END_TO_END_APTOSOCIO, tiempoTotal));
                    this.statistics.updateTimeEndToEndAptoSocio(tiempoTotal);
                } else {
                    this.statistics.updateTimeEndToEnd(tiempoTotal);
                }
            } else {
                this.statistics.updateTimeEndToEnd(tiempoTotal);
            }
            List<HandlerTime> handlersTime = respuestaCentroValidador.getHandlersTime();

            for (HandlerTime handlerTime : handlersTime) {
                strBuffTiempos.append(handlerTime.toString());
                this.statistics.updateHandlerTime(handlerTime);
            }
        } else {
            this.statistics.updateTimeEndToEnd(tiempoTotal);

        }

        if (tiempoTotal > MIN_MS_TO_LOG) {
            getLog().info(",Mensaje transaccionado en:, " + tiempoTotal + ", milisegundos " + "mensaje " + textMessage);
            getLog().info("Segregacion tiempos: " + strBuffTiempos);
        }
    }

    public String getName() {
        return this.name;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    // ***********Impl. Interna***********************//
    // ***********************************************//
    // ***********************************************//

    private Log getLogForCritical() {
        return LogFactory.getLog(CRITICAL_LOG);
    }

    private Log getLog() {
        return LogFactory.getLog(PosMDP.class);
    }

    private void doHandleCastError(String message) {
        errorHandler.process(message);
        this.statistics.addMessageNotSupportedError();
    }

    private void doHandlerErrorMigracion(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            errorHandler.processTimeOut(textMessage.getText());
        } catch (JMSException e) {
            errorHandler.process(message);
        }

    }

    private void doHandleErrorTimeOut(Message message, String error) {
        TextMessage textMessage = (TextMessage) message;
        try {
            errorHandler.processTimeOut("timeout: " + textMessage.getText());
        } catch (JMSException e) {
            errorHandler.process(message);
        }

    }

    private void doHandleError(Message message, String error) {

        this.statistics.addError();
        TextMessage textMessage = (TextMessage) message;
        try {
            StringBuffer realMessage = new StringBuffer();
            realMessage.append(" Error: ").append(error).append(" Mensaje: ").append(textMessage.getText());
            String strMessage = realMessage.toString();
            if (strMessage.length() > DEFAULT_MAX_LONG_MESSAGE) {
                strMessage = strMessage.substring(0, DEFAULT_MAX_LONG_MESSAGE - 1);
            }
            errorHandler.process(strMessage);
        } catch (JMSException e) {
            errorHandler.process(message);
        }

    }
}
