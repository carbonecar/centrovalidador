package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase "Holder" para la respuesta de "ingreso/egreso"
 * @author MT27789605
 *
 */
@Record
public class PosMessageIngresoEgresoResponse extends SocioPosMessageResponse {

}
