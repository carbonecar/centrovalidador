package ar.com.osde.centroValidador.pos;

import org.hibernate.search.annotations.ProvidedId;
import org.joda.time.LocalTime;

import ar.com.osde.centroValidador.pos.utils.PosLocalTimeFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase para el parseo de los mensajes MQ del pos para el mensaje de Ingreso y
 * Egreso
 * 
 * @author MT27789605
 * 
 */
@ProvidedId
@Record
public class IngresoEgresoPosMessage extends PosMessage {

    // AUMOTA Motivo de egreso. Este campo es opcional en el egreso (2E)
    private String motivoAlta;

    // AUHORP //hora en la cual ingreso o egreso
    private LocalTime horaPractica;
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public IngresoEgresoPosMessage() {
        super();
    }

    @Field(offset = 354, length = 2)
    public String getMotivoAlta() {
        return motivoAlta;
    }

    public void setMotivoAlta(String p) {
        this.motivoAlta = p;
    }

    @Field(offset = 348, length = 6, formatter = PosLocalTimeFormatter.class)
    @FixedFormatPattern("HHmmss")
    public LocalTime getHoraPractica() {
        return horaPractica;
    }

    public void setHoraPractica(LocalTime p) {
        this.horaPractica = p;
    }

}
