package ar.com.osde.centroValidador.factory.filter.configuration;

import java.util.LinkedList;

import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

/**
 * Clase base para la creaci�n de los fitlros
 * @author MT27789605
 *
 */
public abstract class AbstractFilterConfigurationFactory implements IFilterConfigurationFactory{

    public AbstractFilterConfigurationFactory() {
        super();
    }

    /**
     * Crea un NotFilter con las expresiones que se pasan en la lista
     */
    public FilterConfiguration createFilter(LinkedList<String> expresion) throws ServiceException {
        FilterConfiguration filter = this.createRealFilterConfiguration();
        filter.setClassName(getRealClassName());
        return filter;
    }

    /**
     * Nombre real de la clase que hay que crear
     * @return
     */
    protected abstract String getRealClassName();

    /**
     * FilterConfiguration real a crear
     * @return
     */
    protected abstract FilterConfiguration createRealFilterConfiguration();

}