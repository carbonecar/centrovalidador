package ar.com.osde.centroValidador.services.impl;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jws.WebParam;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;
import ar.com.osde.centroValidador.pos.mq.CustomDefaultMessageListenerContainer;
import ar.com.osde.centroValidador.pos.mq.PosMDP;
import ar.com.osde.centroValidador.services.exception.InThreadingException;
import ar.com.osde.centroValidador.services.pos.ImplementacionAsis;
import ar.com.osde.comunes.utils.ListenerManager;
import ar.com.osde.cv.migracionas400.bo.TransaccionMigracionBO;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

public class ImplementacionAsisImpl implements ImplementacionAsis {
    private ListenerManager manager;
    private Log LOGGER = LogFactory.getLog(ImplementacionAsisImpl.class);
    private TransaccionMigracionBO transaccionMigracionBO;

    public boolean isImplementada(@WebParam(name = "filial") Integer filial) throws ServiceException {
        if (filial == null) {
            return false;
        }
        Collection<DefaultMessageListenerContainer> listenerOnMemory = manager.getAllListenerOnMemory();
        for (DefaultMessageListenerContainer container : listenerOnMemory) {
            if (((CustomDefaultMessageListenerContainer) container).isStartOnLoad()) {
                PosMessageHandler posMessageHandler = ((PosMDP) container.getMessageListener()).getMessageHandler();
                if (posMessageHandler.procesaMensaje()) {
                    for (PosMessageFilter filter : posMessageHandler.getMessageFilters()) {
                        if (filter.implementaPropiedadValor("getFilialInstalacionPOS", filial))
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public TransaccionMigracionBO getTransaccionMigracionBO() {
        return transaccionMigracionBO;
    }

    public void setTransaccionMigracionBO(TransaccionMigracionBO transaccionMigracionBO) {
        this.transaccionMigracionBO = transaccionMigracionBO;
    }

    public void iniciarRollback(final String codigoFilial) throws ServiceException {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(new Runnable() {

            public void run() {
                try {
                    ImplementacionAsisImpl.this.transaccionMigracionBO.migrarTransacciones(codigoFilial);
                } catch (BusinessException e) {
                    LOGGER.error(e);
                    throw new InThreadingException(e);
                }
            }
        });
        executor.shutdown();

    }

    public ListenerManager getManager() {
        return manager;
    }

    public void setManager(ListenerManager manager) {
        this.manager = manager;
    }

    public int crearMensajesMigracion(String codigoFilial) throws ServiceException {
        return transaccionMigracionBO.crearMensajesMigracion(codigoFilial);
    }

    public int elminarMensajesMigracion(String codigoFilial) throws ServiceException {

        return transaccionMigracionBO.eliminarMensajesMigracion(codigoFilial);
    }
}
