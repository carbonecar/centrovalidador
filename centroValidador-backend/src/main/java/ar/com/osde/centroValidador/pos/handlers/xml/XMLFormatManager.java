package ar.com.osde.centroValidador.pos.handlers.xml;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import ar.com.osde.cv.pos.utils.ExceptionFactory;
import ar.com.osde.cv.transaccion.ObjectFactory;
import ar.com.osde.cv.transaccion.Transacciones;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

/**
 * Recibe un String lo parsea como xml y genera una transaccion.
 * 
 * @author VA27789605
 * 
 */
public class XMLFormatManager implements FixedFormatManager {
    private Logger LOG = Logger.getLogger(XMLFormatManager.class);

    private String fileName;

    @SuppressWarnings("unchecked")
    public <T> T load(Class<T> paramClass, String xml) throws FixedFormatException {

        ByteArrayInputStream byteMessage = new ByteArrayInputStream(xml.getBytes());
        JAXBElement<Transacciones> jaxbElement = null;

        try {
            JAXBContext jc = JAXBContext.newInstance("ar.com.osde.centroValidador");

            String xsdVersion = getXsdVersion(xml);

            Unmarshaller unmarshaller = jc.createUnmarshaller();

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.XMLNS_ATTRIBUTE_NS_URI);
            URL schemaURL = getClass().getResource("/" + xsdVersion + "/" + fileName);
            ExceptionFactory.createFixedFormatException(schemaURL == null, "no existe la version del schema");
            Schema schema = schemaFactory.newSchema(schemaURL);
            unmarshaller.setSchema(schema);

            jaxbElement = (JAXBElement<Transacciones>) unmarshaller.unmarshal(byteMessage);

        } catch (SAXException e) {
            LOG.error(e);
            // TODO: Esto no es una FixedFormat es una FormatException o
            // ParseException, pero no fixed
            throw new FixedFormatException("No es posible obtener el texto del mensaje jms. El schema no es valido: "
                    + e.toString());
        } catch (JAXBException e) {
            LOG.error(e);
            throw new FixedFormatException("No es posible obtener el texto del mensaje jms: " + e.toString());
        }
        return (T) jaxbElement.getValue();
    }

    public <T> String export(T transaccion) throws FixedFormatException {

        StringWriter stringWriter = null;

        try {
            JAXBContext jc = JAXBContext.newInstance("ar.com.osde.centroValidador");

            String xsdVersion = ((Transacciones) transaccion).getVersionXsd();

            Marshaller marshaller = jc.createMarshaller();

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.XMLNS_ATTRIBUTE_NS_URI);
            Schema schema = schemaFactory.newSchema(getClass().getResource("/" + xsdVersion + "/" + fileName));

            marshaller.setSchema(schema);

            stringWriter = new StringWriter();

            ObjectFactory objectFactory = new ObjectFactory();

            JAXBElement<Transacciones> jaxbElement = objectFactory.createTransacciones((Transacciones) transaccion);

            marshaller.marshal(jaxbElement, stringWriter);

        } catch (JAXBException e) {
            throw new FixedFormatException("No es posible serializar la transaccion: " + e.getCause());
        } catch (SAXException e) {
            throw new FixedFormatException("Error al crear el schema para la transaccion." + e.getCause());
        }

        return stringWriter.toString();
    }

    public <T> String export(String paramString, T paramT) throws FixedFormatException {
        throw new UnsupportedOperationException("No implementado: refactorizar la interfaz");
    }

    private String getXsdVersion(String xml) {

        String startString = "<versionXsd>";
        String endString = "</versionXsd>";

        int initialPosition = xml.indexOf(startString);
        int endPosition = xml.indexOf(endString, initialPosition);

        return xml.substring(initialPosition + startString.length(), endPosition);
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
