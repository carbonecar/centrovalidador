package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Transaccion para modelar la 3A.
 * @author MT27789605
 *
 */

@Record
public class PosMessageRescate extends SocioPosMessageBase {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String delegacionOrden;
    private int numeroAutorizacionReferencia;

    /**
     * Este valor puede ser o bien delegacion de la orden o bien el
     * tipoTratamientoOdontologico o bien el codigo de
     * 
     * @return
     */
    @Field(offset = 74, length = 2)
    public String getDelegacionOrden() {
        return delegacionOrden;
    }

    public void setDelegacionOrden(String p) {
        this.delegacionOrden = p;
    }

    @Field(offset = 76, length = 6)
    public int getNumeroAutorizacionReferencia() {
        return numeroAutorizacionReferencia;
    }

    public void setNumeroAutorizacionReferencia(int p) {
        this.numeroAutorizacionReferencia = p;
    }

    
    
}
