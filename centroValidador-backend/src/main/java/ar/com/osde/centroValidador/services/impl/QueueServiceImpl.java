package ar.com.osde.centroValidador.services.impl;

import java.util.LinkedList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.services.pos.QueueService;
import ar.com.osde.framework.services.ServiceException;

public class QueueServiceImpl implements QueueService {

    private static final Log LOG=LogFactory.getLog(QueueServiceImpl.class);
    public List<String> getAllQueueNames() throws ServiceException {

        List<String> queueNames = null;

        try {
            Context context = (Context) new InitialContext().lookup("");
            NamingEnumeration<NameClassPair> list = context.list("");

            queueNames = filterByName(list, "queue:");

        } catch (NamingException e) {
            throw new ServiceException("Error al obtener los nombres.");
        }

        return queueNames;
    }

    public List<String> getAllConnectionFactories() throws ServiceException {
    	  List<String> connectionFactoryNames = null;

          try {
              Context context = (Context) new InitialContext().lookup("java:");
              NamingEnumeration<NameClassPair> list = context.list("");

              connectionFactoryNames = filterByName(list, "MQCON");

          } catch (NamingException e) {
              throw new ServiceException("Error al obtener los nombres.");
          }

          return connectionFactoryNames;
    }
    
    /**
     * 
     * @param nameList
     * @param queueName
     * @return
     * @throws NamingException
     */
    private List<String> filterByName(NamingEnumeration<NameClassPair> nameList, String queueName) throws NamingException {

        List<String> queueNameList = new LinkedList<String>();

        while (nameList.hasMore()) {
            NameClassPair item = (NameClassPair) nameList.next();
            String name = item.getName();

            if (name.startsWith(queueName)) {
                queueNameList.add(name);
            }
        }

        return queueNameList;
    }

}
