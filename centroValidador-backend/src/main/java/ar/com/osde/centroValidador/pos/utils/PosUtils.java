package ar.com.osde.centroValidador.pos.utils;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;

public class PosUtils {
	private static final String NULL_CHAR = "0";

	
	private static String nullString(int length) {
		return StringUtils.repeat(NULL_CHAR, length).intern();
	}
	private static boolean isNullValue(String value) {
		return value.equals(nullString(value.length()));
	}
	private static boolean isControlString(String value) {
		for (int i = 0; i < value.length(); i++) {
			if (!Character.isISOControl(value.charAt(i))){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isNullString(String value) {
		return value == null 
				|| isNullValue(value)
				|| isControlString(value);
	}
	
	
	/**
	 * Verifica si el dato es nulo antes de pasarlo a date
	 * @param localDate
	 * @return
	 */
	public static Date toDateMidnight(LocalDate localDate){
	    Date myDate=null;
	    if(localDate!=null){
	        myDate=localDate.toDateMidnight().toDate();
	    }
	    
	    return myDate;
	}
	
}
