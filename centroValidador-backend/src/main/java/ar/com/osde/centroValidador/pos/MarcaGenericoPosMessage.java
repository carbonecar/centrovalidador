package ar.com.osde.centroValidador.pos;

import ar.com.osde.centroValidador.formatter.LongFormatterTrimmed;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase para el parseo de los mensajes MQ del pos para el mensaje de Marca
 * Generico
 * 
 * @author MT27789605
 * 
 */
@Record
public class MarcaGenericoPosMessage extends AbstractNumeroTransaccionAnterior {

    // TODO unificar con PosMessageFarmacia

    // AUTRO1
    private int codigoTroquelMedicamento1;

    // AUBAR1
    private long codigoBarrasMedicamento1;

    // AUGEN1
    private String marcaGenerico1;

    // AUTRO2
    private int codigoTroquelMedicamento2;

    // AUBAR2
    private long codigoBarrasMedicamento2;

    // AUGEN2
    private String marcaGenerico2;

    // AUTRO3
    private int codigoTroquelMedicamento3;

    // AUBAR3
    private long codigoBarrasMedicamento3;

    // AUGEN3
    private String marcaGenerico3;

    // AUALB1
    private String numeroDeRegistroAlfabeta1;

    // AUALB2
    private String numeroDeRegistroAlfabeta2;

    // AUALB3
    private String numeroDeRegistroAlfabeta3;

    @Field(offset = 445, length = 1, align = Align.RIGHT)
    public String getMarcaGenerico1() {
        return marcaGenerico1;
    }

    public void setMarcaGenerico1(String marcaGenerico1) {
        this.marcaGenerico1 = marcaGenerico1;
    }

    @Field(offset = 446, length = 1, align = Align.RIGHT)
    public String getMarcaGenerico2() {
        return marcaGenerico2;
    }

    public void setMarcaGenerico2(String marcaGenerico2) {
        this.marcaGenerico2 = marcaGenerico2;
    }

    @Field(offset = 446, length = 1, align = Align.RIGHT)
    public String getMarcaGenerico3() {
        return marcaGenerico3;
    }

    public void setMarcaGenerico3(String marcaGenerico3) {
        this.marcaGenerico3 = marcaGenerico3;
    }

    @Field(offset = 208, length = 7, align = Align.RIGHT)
    public int getCodigoTroquelMedicamento1() {
        return codigoTroquelMedicamento1;
    }

    public void setCodigoTroquelMedicamento1(int p) {
        this.codigoTroquelMedicamento1 = p;
    }

    @Field(offset = 215, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento1() {
        return codigoBarrasMedicamento1;
    }

    public void setCodigoBarrasMedicamento1(long p) {
        this.codigoBarrasMedicamento1 = p;
    }

    @Field(offset = 241, length = 7)
    public int getCodigoTroquelMedicamento2() {
        return codigoTroquelMedicamento2;
    }

    public void setCodigoTroquelMedicamento2(int p) {
        this.codigoTroquelMedicamento2 = p;
    }

    @Field(offset = 248, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento2() {
        return codigoBarrasMedicamento2;
    }

    public void setCodigoBarrasMedicamento2(long p) {
        this.codigoBarrasMedicamento2 = p;
    }

    @Field(offset = 274, length = 7)
    public int getCodigoTroquelMedicamento3() {
        return codigoTroquelMedicamento3;
    }

    public void setCodigoTroquelMedicamento3(int p) {
        this.codigoTroquelMedicamento3 = p;
    }

    @Field(offset = 281, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento3() {
        return codigoBarrasMedicamento3;
    }

    public void setCodigoBarrasMedicamento3(long p) {
        this.codigoBarrasMedicamento3 = p;
    }

    @Field(offset = 476, length = 6)
    public String getNumeroDeRegistroAlfabeta1() {
        return numeroDeRegistroAlfabeta1;
    }

    public void setNumeroDeRegistroAlfabeta1(String p) {
        this.numeroDeRegistroAlfabeta1 = p;
    }

    @Field(offset = 482, length = 6)
    public String getNumeroDeRegistroAlfabeta2() {
        return numeroDeRegistroAlfabeta2;
    }

    public void setNumeroDeRegistroAlfabeta2(String p) {
        this.numeroDeRegistroAlfabeta2 = p;
    }

    @Field(offset = 488, length = 6)
    public String getNumeroDeRegistroAlfabeta3() {
        return numeroDeRegistroAlfabeta3;
    }

    public void setNumeroDeRegistroAlfabeta3(String p) {
        this.numeroDeRegistroAlfabeta3 = p;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
