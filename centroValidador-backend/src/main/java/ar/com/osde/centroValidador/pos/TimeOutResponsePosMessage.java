package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class TimeOutResponsePosMessage implements IPosMessage {
    // AUAUTO Nro. de Autorizaci�n o Solicitud de OSDE Dec 6.0 174
    private int nroAutorizacion;
    // AUWHOO Firma del Generador del Nro. de Autorizaci�n Char 2 180
    private String generadorRespuesta;
    // AUREST C�digo de Respuesta Transaccional Char 2 182
    private String respuestaTransaccion;
    // AURES1 C�digo de Respuesta 1 Char 2 184
    private String codigoRespuestaItem1;
    // AURES2 C�digo de Respuesta 2 Char 2 186
    private String codigoRespuestaItem2;
    // AURES3 C�digo de Respuesta 3 Char 2 188
    private String codigoRespuestaItem3;
    // AURES4 C�digo de Respuesta 4 Char 2 190
    private String codigoRespuestaItem4;
    private String originalMessage;

    @Field(offset = 174, length = 6, align = Align.RIGHT)
    public int getNroAutorizacion() {
        return nroAutorizacion;
    }

    public void setNroAutorizacion(int nroAutorizacion) {
        this.nroAutorizacion = nroAutorizacion;
    }

    @Field(offset = 180, length = 2, align = Align.RIGHT)
    public String getGeneradorRespuesta() {
        return generadorRespuesta;
    }

    public void setGeneradorRespuesta(String generadorRespuesta) {
        this.generadorRespuesta = generadorRespuesta;
    }

    @Field(offset = 182, length = 2, align = Align.RIGHT)
    public String getRespuestaTransaccion() {
        return respuestaTransaccion;
    }

    public void setRespuestaTransaccion(String respuestaTransaccion) {
        this.respuestaTransaccion = respuestaTransaccion;
    }

    @Field(offset = 184, length = 2, align = Align.RIGHT)
    public String getCodigoRespuestaItem1() {
        return codigoRespuestaItem1;
    }

    public void setCodigoRespuestaItem1(String codigoRespuestaItem1) {
        this.codigoRespuestaItem1 = codigoRespuestaItem1;
    }

    @Field(offset = 186, length = 2, align = Align.RIGHT)
    public String getCodigoRespuestaItem2() {
        return codigoRespuestaItem2;
    }

    public void setCodigoRespuestaItem2(String codigoRespuestaItem2) {
        this.codigoRespuestaItem2 = codigoRespuestaItem2;
    }

    @Field(offset = 188, length = 2, align = Align.RIGHT)
    public String getCodigoRespuestaItem3() {
        return codigoRespuestaItem3;
    }

    public void setCodigoRespuestaItem3(String codigoRespuestaItem3) {
        this.codigoRespuestaItem3 = codigoRespuestaItem3;
    }

    @Field(offset = 190, length = 2, align = Align.RIGHT)
    public String getCodigoRespuestaItem4() {
        return codigoRespuestaItem4;
    }

    public void setCodigoRespuestaItem4(String codigoRespuestaItem4) {
        this.codigoRespuestaItem4 = codigoRespuestaItem4;
    }

    public String getOriginalMessage() {
        return this.originalMessage;
    }

    public void setOriginalMessage(String originalMessage) {
        this.originalMessage = originalMessage;

    }

}
