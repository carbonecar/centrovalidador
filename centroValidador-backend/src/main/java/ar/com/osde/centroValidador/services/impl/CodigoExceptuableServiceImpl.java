package ar.com.osde.centroValidador.services.impl;

import ar.com.osde.centroValidador.services.pos.CodigoExceptuableService;
import ar.com.osde.cv.dao.CodigoExceptuableDAO;
import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

import java.util.List;

/**
 * Service para devolver los codigos que son suceptibles de ser exceputados
 * @author MT27789605
 *
 */
public class CodigoExceptuableServiceImpl implements CodigoExceptuableService {

    private CodigoExceptuableDAO codigoExceptuableDao;

    
    public List<CodigoExceptuable> getAllCodigos() throws ServiceException {
        try {
            return codigoExceptuableDao.getAllCodigos();
        } catch (BusinessException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void saveNew(CodigoExceptuable codigoExceptuable) throws ServiceException {
        try {
            this.codigoExceptuableDao.saveNew(codigoExceptuable);
        } catch (BusinessException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void update(CodigoExceptuable codigoExceptuable) throws ServiceException {
        try {
            this.codigoExceptuableDao.update(codigoExceptuable);
        } catch (BusinessException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void delete(CodigoExceptuable codigoExceptuable) throws ServiceException {
        try {
            this.codigoExceptuableDao.deleteContainer(codigoExceptuable);
        } catch (BusinessException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public CodigoExceptuableDAO getCodigoExceptuableDao() {
        return codigoExceptuableDao;
    }

    public void setCodigoExceptuableDao(CodigoExceptuableDAO codigoExceptuableDao) {
        this.codigoExceptuableDao = codigoExceptuableDao;
    }
}
