package ar.com.osde.centroValidador.pos.handlers;

import ar.com.osde.rules.paralelo.ResponseRecord;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

public class StatisticalOldSystemResponse extends RespuestaCentroValidador {


	public StatisticalOldSystemResponse(RespuestaTransaccion respuestaTransaccion) {
		super(respuestaTransaccion);
		setSystemName(ResponseRecord.SystemName.POS.getName());
	}

	
	public void processLoguer(LoggerMessageRecordHandler loggerMessageRecordHandler) {
		loggerMessageRecordHandler.logMe(this);
		
	}
}
