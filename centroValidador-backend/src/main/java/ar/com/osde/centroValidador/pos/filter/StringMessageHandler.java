package ar.com.osde.centroValidador.pos.filter;


/**
 * Examina el mensaje y lo pasa a otra cola o lo descarta.
 * @author VA27789605
 *
 */
public interface StringMessageHandler{

	//TODO: crear N destinos que sean tomados via JNDI

	/**
	 * Parsea el mensaje y lo rutea a la cola indicada
	 * @param message
	 */
	public void handle(String message);
	
}
