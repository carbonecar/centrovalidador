package ar.com.osde.centroValidador.pos.handlers;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jms.Message;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.MigracionPosMessage;
import ar.com.osde.centroValidador.pos.exception.MigracionOutRulesException;
import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionMigracionPos;
import ar.com.osde.entities.aptitud.ObservacionDetalle;
import ar.com.osde.entities.transaccion.ImportesCoberturaMedicamento;

public class MigracionMessageHandler extends TimeOutMessageHandler {

    private Set<Integer> codigosExceptuar;

    public MigracionMessageHandler() {
        this.codigosExceptuar = new HashSet<Integer>();
    }

    @Override
    public RespuestaCentroValidador process(Message message) throws PosMessageHandleException {
        RespuestaCentroValidador respuestaCentroValidador = super.process(message);

        List<Integer> codigos = new ArrayList<Integer>();
        codigos.addAll(this.codigosExceptuar);
        List<ObservacionDetalle> codigosExceptuar = StreamResponseMessageHandler.findAdvertencias(respuestaCentroValidador, codigos);

        if (!(codigosExceptuar == null || codigosExceptuar.isEmpty())) {
            throw new MigracionOutRulesException(respuestaCentroValidador.getPosMessage().getOriginalMessage());
        }
        return respuestaCentroValidador;
    }

    public AbstractTransaccionPos createTransaccion(AbstractPosMessage posMessage) {
        MigracionPosMessage migracionPosMessage = createTimeOutPosMessage(posMessage);
        TransaccionMigracionPos transaccionMigracionPos = TransaccionFactory.getInstance().createTransaccionMigracion(posMessage,
                this.getBeanMapper(), migracionPosMessage);
        if (migracionPosMessage.isValidoMedicamento1()) {
            ImportesCoberturaMedicamento importesCoberturaMedicamento = this.buildImportesCoberturaMedidamento(
                    migracionPosMessage.getImporteCobertura1(), migracionPosMessage.getImporteCargoSocio1(),
                    migracionPosMessage.getPorcentajeCobertura1(), migracionPosMessage.getPorcentajeCargoSocio1(),
                    migracionPosMessage.getValorFacturado1(), migracionPosMessage.getPorcentajeBonificacion1(),
                    migracionPosMessage.getImporteBonificado1(), migracionPosMessage.getValorContratado1(),
                    migracionPosMessage.getValorAPagar1(), migracionPosMessage.getPrecioAlfabeta1(),
                    migracionPosMessage.getImporteCargoSocioUnitario1(), migracionPosMessage.getImporteCoberturaUnitario1());

            transaccionMigracionPos.getTransaccioMigracion().setImportesCoberturaMedicamento1(importesCoberturaMedicamento);
        }

        if (migracionPosMessage.isValidoMedicamento2()) {
            ImportesCoberturaMedicamento importesCoberturaMedicamento = this.buildImportesCoberturaMedidamento(
                    migracionPosMessage.getImporteCobertura2(), migracionPosMessage.getImporteCargoSocio2(),
                    migracionPosMessage.getPorcentajeCobertura2(), migracionPosMessage.getPorcentajeCargoSocio2(),
                    migracionPosMessage.getValorFacturado2(), migracionPosMessage.getPorcentajeBonificacion2(),
                    migracionPosMessage.getImporteBonificado2(), migracionPosMessage.getValorContratado2(),
                    migracionPosMessage.getValorAPagar2(), migracionPosMessage.getPrecioAlfabeta2(),
                    migracionPosMessage.getImporteCargoSocioUnitario2(), migracionPosMessage.getImporteCoberturaUnitario2());

            transaccionMigracionPos.getTransaccioMigracion().setImportesCoberturaMedicamento2(importesCoberturaMedicamento);
        }

        if (migracionPosMessage.isValidoMedicamento3()) {
            ImportesCoberturaMedicamento importesCoberturaMedicamento = this.buildImportesCoberturaMedidamento(
                    migracionPosMessage.getImporteCobertura3(), migracionPosMessage.getImporteCargoSocio3(),
                    migracionPosMessage.getPorcentajeCobertura3(), migracionPosMessage.getPorcentajeCargoSocio3(),
                    migracionPosMessage.getValorFacturado3(), migracionPosMessage.getPorcentajeBonificacion3(),
                    migracionPosMessage.getImporteBonificado3(), migracionPosMessage.getValorContratado3(),
                    migracionPosMessage.getValorAPagar3(), migracionPosMessage.getPrecioAlfabeta3(),
                    migracionPosMessage.getImporteCargoSocioUnitario3(), migracionPosMessage.getImporteCoberturaUnitario3());

            transaccionMigracionPos.getTransaccioMigracion().setImportesCoberturaMedicamento3(importesCoberturaMedicamento);
        }
        transaccionMigracionPos.getTransaccion().setBloqueada(migracionPosMessage.getBloqueada());
        return transaccionMigracionPos;

    }

    @Override
    protected MigracionPosMessage createTimeOutPosMessage(AbstractPosMessage posMessage) {
        String orginalMessage = posMessage.getOriginalMessage();
        FixedPosFormatManager formatManager = new FixedPosFormatManager();
        MigracionPosMessage timeOutResponsePosMessage = formatManager.load(MigracionPosMessage.class, orginalMessage);
        return timeOutResponsePosMessage;
    }

    public Set<Integer> getCodigosExceptuar() {
        return codigosExceptuar;
    }

    public void setCodigosExceptuar(Set<Integer> codigosExceptuar) {
        this.codigosExceptuar = codigosExceptuar;
    }

    public void addCodigoExceptuable(Integer codigo) {
        this.codigosExceptuar.add(codigo);
    }

    // *********************************************//
    // **********Impl. Interna**********************//
    // *********************************************//
    private ImportesCoberturaMedicamento buildImportesCoberturaMedidamento(BigDecimal importeCobertura1, BigDecimal importeCargoSocio1,
            BigDecimal porcentajeCobertura1, BigDecimal porcentajeCargoSocio1, BigDecimal valorFacturado1,
            BigDecimal porcentajeBonificacion1, BigDecimal importeBonificado1, BigDecimal valorContratado1, BigDecimal valorAPagar1,
            BigDecimal precioAlfabeta1, BigDecimal importeCargoSocioUnitario, BigDecimal importeCargoOsdeUnitario) {

        return new ImportesCoberturaMedicamento(precioAlfabeta1, importeCargoSocio1, importeCobertura1, porcentajeCargoSocio1,
                porcentajeCobertura1, importeCargoSocioUnitario, importeCargoOsdeUnitario);

    }
}
