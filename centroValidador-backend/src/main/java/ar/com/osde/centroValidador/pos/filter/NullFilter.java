package ar.com.osde.centroValidador.pos.filter;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;

public class NullFilter implements PosMessageFilter {

    public void validate(SocioPosMessageBase posMessage) throws FilteredMessageException {
    }

    public boolean implementaPropiedadValor(String propertyName, Object value) {
        return false;
    }

    public boolean evaluaPropiedad(String propertyName) {
        return false;
    }
}
