package ar.com.osde.centroValidador.pos.utils;

import java.util.Date;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.DateFormatter;

public class PosDateFormatter extends DateFormatter {

    @Override
    public Date asObject(String string, FormatInstructions instructions)
            throws FixedFormatException {
        if (PosUtils.isNullString(string)) {
            return null;
        }
        
        return super.asObject(string, instructions);
    }
}
