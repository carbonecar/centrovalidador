package ar.com.osde.centroValidador.pos.mq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;


public class XMLMessageCreator extends SimpleTextMessageCreator {

    public Message createMessage(Session session) throws JMSException {
        
        TextMessage message = session.createTextMessage();
        message.setText(getManager().export(getRespuestaCentroValidador()));
        // TODO: es necesario setear el id?
        
        return message;
      }

	public long getTimeUsedToCreateMessage() {
	    // TODO crear la variable
	    return 0;
    }
    
        public String createMessageResponse() {
        // TODO Auto-generated method stub
        return null;
    }
}
