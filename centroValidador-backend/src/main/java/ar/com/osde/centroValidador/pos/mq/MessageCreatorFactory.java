package ar.com.osde.centroValidador.pos.mq;

import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.xml.ws.soap.SOAPFaultException;

import org.springframework.jms.core.MessageCreator;

import ar.com.osde.centroValidador.pos.exception.EasyNameSOAPFaultException;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.centroValidador.pos.jms.TimedMessageCreator;
import ar.com.osde.entities.transaccion.ConfiguracionInconsistenciasImpresion;
import ar.com.osde.services.otorgamiento.ConfiguracionService;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

/**
 * Factory para la creacion de mesajes
 * 
 * @author VA27789605
 * 
 */
public class MessageCreatorFactory {

    private ConfiguracionService configuracionService;
    private List<ConfiguracionInconsistenciasImpresion> inconsistenciasDescripcion;

    public void setConfiguracionService(ConfiguracionService configuracionService) {
        this.configuracionService = configuracionService;
    }

    /**
     * Factory para la creacion del mensaje.
     * 
     * @param respuesta
     * @return
     */
    public TimedMessageCreator createStreamMessageCreator(RespuestaCentroValidador respuesta, Map<String, String> operadores,
            FixedFormatManager manager) {
        if (inconsistenciasDescripcion == null) {
            try {
                inconsistenciasDescripcion = configuracionService.obtenerConfiguracionesInconsistenciasImpresion();
            } catch (SOAPFaultException e) {
                throw new EasyNameSOAPFaultException(e.getFault(), "obtenerConfiguracionesInconsistenciasImpresion");
            }
        }
        PosMessageResponseCreator messageCreator = new PosMessageResponseCreator(inconsistenciasDescripcion);
        messageCreator.setRespuestaCentroValidador(respuesta);
        messageCreator.setManager(manager);
        messageCreator.setOperadores(operadores);

        return messageCreator;
    }

    /**
     * Crea un simple mensaje de texto
     * 
     * @param message
     * @return
     */
    public MessageCreator createSimpleTextMessage(final String message) {
        return new MessageCreator() {
            public Message createMessage(Session paramSession) throws JMSException {
                return paramSession.createTextMessage(message);
            }
        };
    }


    
    public MessageCreator createStreamMessageCreator(Message message) {
        return new ForwardingMessageCreator(message);
    }

    /**
     * Crea el  constructor del mensaje en formato xml
     * @param respuesta
     * @param operadores
     * @param manager
     * @return
     */
    public TimedMessageCreator createXmlMessageCreator(RespuestaCentroValidador respuesta, Map<String, String> operadores,
            FixedFormatManager manager) {

        XMLMessageCreator messageCreator = new XMLMessageCreator();
        messageCreator.setRespuestaCentroValidador(respuesta);
        messageCreator.setManager(manager);
        messageCreator.setOperadores(operadores);

        return messageCreator;
    }

}
