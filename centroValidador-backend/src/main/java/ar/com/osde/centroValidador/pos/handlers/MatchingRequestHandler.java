package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoRulesException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.rules.paralelo.NivelValidez;
import ar.com.osde.rules.paralelo.ResponseMatcher;
import ar.com.osde.rules.paralelo.ResponseMessageMatcher;

public class MatchingRequestHandler implements PosMessageHandler {

	private ResponseMessageMatcher matcher;
	private TransaccionServiceStatistics stadistics;

	private static final Log LOG = LogFactory.getLog(MatchingRequestHandler.class);

	public ResponseMessageMatcher getMatcher() {
		return matcher;
	}

	public void setMatcher(ResponseMessageMatcher matcher) {
		this.matcher = matcher;
	}

	public TransaccionServiceStatistics getStadistics() {
		return stadistics;
	}

	public void setStadistics(TransaccionServiceStatistics stadistics) {
		this.stadistics = stadistics;
	}

	public RespuestaCentroValidador process(Message message) throws PosMessageHandleException {

		StatisticalNewSystemResponse respuesta = new StatisticalNewSystemResponse(null);
		stadistics.addTransaccion();
		
		try {
			String mensajeTxt = ((TextMessage) message).getText();
			respuesta.setRequestString(mensajeTxt);
			processMatcher(respuesta);
		} catch (JMSException e) {
			LOG.error(e);
		}

		return respuesta;

	}

	private void processMatcher(RespuestaCentroValidador respuesta) {
		ResponseMatcher responseMatcher = null;
		// no validamos el response
		String requestString = respuesta.getRequestString();
		if (requestString != null) {
			responseMatcher = matcher.addRequest(respuesta.getRequestString());
			LOG.debug(respuesta.getRequestString());
			stadistics.addMensajeToBeMatched();
			if (responseMatcher.isWasMatched()) {
				respuesta.setResponseMatcher(responseMatcher);
				stadistics.updateCantidadTransaccionesMatcheadas();
				if (NivelValidez.VALIDAS.equals(responseMatcher.getNivelValidez())) {
					stadistics.addTransaccionIdentica();
				} else {
					if (NivelValidez.DIFERENCIA_APTITUD.equals(responseMatcher.getNivelValidez())) {
						stadistics.addTransaccionDiferenciaHabilitado();
					} else {
						if (NivelValidez.DIFERENCIA_COBERTURA.equals(responseMatcher.getNivelValidez())) {
							stadistics.addDiferenciaPorcenajesCobertura();
						} else {
							if (NivelValidez.VALIDAS_CON_DIFERENCIA_N1.equals(responseMatcher.getNivelValidez())) {
								stadistics.addTransaccionDiferenciaN1();
							} else {
								stadistics.addTransaccionDiferencia();
							}
						}

					}
				}
			}
		}
	}

	public List<PosMessageFilter> getMessageFilters() {
		List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
		return messageFilters;

	}

	public String getHierarchyName() {
		return "MatchingRequestHandler";
	}

	public boolean procesaMensaje() {
		return false;
	}

}
