package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.SocioPosMessageBase;

/**
 * El tipoAtributo deberia de quitarse con la implementacion del xml.
 * Por esto esta clase tambien es temporal
 * @author MT27789605
 *
 */
public class TipoAtributoInvalido extends PosMessageHandleException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TipoAtributoInvalido(TipoAtributoInvalido e){
        super(e);
    }
    public TipoAtributoInvalido(String string) {
        super(string);
    }

    public TipoAtributoInvalido(String message,
            SocioPosMessageBase posMessage) {
        super(message, posMessage);
    }

    public TipoAtributoInvalido(Throwable cause,
            SocioPosMessageBase posMessage) {
        super(cause, posMessage);
    }

    public TipoAtributoInvalido(String message, Throwable cause,
            PosMessage posMessage) {

        super(message, cause, posMessage);
    }

}
