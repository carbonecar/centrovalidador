package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Message;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoRulesException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.rules.paralelo.NivelValidez;
import ar.com.osde.rules.paralelo.ResponseMatcher;
import ar.com.osde.rules.paralelo.ResponseMessageMatcher;
import ar.com.osde.rules.paralelo.ResponseRecord;

public class MatchingResponseHandler extends PosMessageHandlerInterceptor {

	private static final Log LOG = LogFactory.getLog(MatchingResponseHandler.class);
 
    private ResponseMessageMatcher matcher;

	private TransaccionServiceStatistics statistics;

    public String getDescription() {
        return "Intercepta a un handler y matchea la respuesta con alguna anterior que llegara";
    }

    public TransaccionServiceStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(TransaccionServiceStatistics statistics) {
        this.statistics = statistics;
    }

    public ResponseMessageMatcher getMatcher() {
        return matcher;
    }

    public void setMatcher(ResponseMessageMatcher matcher) {
        this.matcher = matcher;
    }

    public RespuestaCentroValidador process(Message message) throws PosMessageHandleException {
        RespuestaCentroValidador respuesta;
       
        try {        	
        	respuesta = this.getIntercepted().process(message);
        	if(!respuesta.getSystemName().equals(ResponseRecord.SystemName.SALUD_SOFT.getName())){
        		statistics.addTransaccion();        	
        	}
            processMatcher(respuesta);
        } catch (TimeOutForzadoRulesException toException) {
            // Por definicion funcional los time out no participan del matcheo
            // processMatcher(e.getRespuestaCentroValidador());
            throw toException;
        }
        return respuesta;
    }

    public String getHierarchyName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("MatchingResponseHandler\n");
        strBuff.append("\t").append(this.getIntercepted().getHierarchyName());
        return strBuff.toString();
    }

    private void processMatcher(RespuestaCentroValidador respuesta) {
        ResponseMatcher responseMatcher = null;
        // no validamos el response
        
        if (respuesta.getResponseString() != null) {
            if (! (respuesta.getResponseString().contains("Transaccion Recibida"))) {
            	LOG.debug(respuesta.getResponseString()+","+respuesta.getSystemName());
                responseMatcher = matcher.addResponse(respuesta.getResponseString(),respuesta.getSystemName());
                statistics.addMensajeToBeMatched();
                if (responseMatcher.isWasMatched()) {
					respuesta.setResponseMatcher(responseMatcher);
					statistics.updateCantidadTransaccionesMatcheadas();
                    if (NivelValidez.VALIDAS.equals(responseMatcher.getNivelValidez())) {
                    	statistics.addTransaccionIdentica();
                    } else {
                        if (NivelValidez.DIFERENCIA_APTITUD.equals(responseMatcher.getNivelValidez())) {
                        	statistics.addTransaccionDiferenciaHabilitado();
                        } else {
                            if (NivelValidez.DIFERENCIA_COBERTURA.equals(responseMatcher.getNivelValidez())) {
                            	statistics.addDiferenciaPorcenajesCobertura();
                            } else {
                                if (NivelValidez.VALIDAS_CON_DIFERENCIA_N1.equals(responseMatcher.getNivelValidez())) {
                                	statistics.addTransaccionDiferenciaN1();
                                } else {
                                	statistics.addTransaccionDiferencia();
                                }
                            }

                        }

                    }
                }
            }
        }
    }

    public List<PosMessageFilter> getMessageFilters() {
        List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
        messageFilters.addAll(this.getIntercepted().getMessageFilters());
        return messageFilters;

    }
}
