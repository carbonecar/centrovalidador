package ar.com.osde.centroValidador.factory.filter.configuration;

import ar.com.osde.centroValidador.pos.filter.NotFilter;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;

/**
 * Factory para ola clreacion de un NotFilter
 * 
 * @author MT27789605
 * 
 */
public class NotFilterConfigurationFactory extends AbstractFilterConfigurationFactory implements IFilterConfigurationFactory {

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#getRealClassName()
     */
    protected String getRealClassName() {
        return NotFilter.class.getName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#createRealFilterConfiguration()
     */
    protected FilterConfiguration createRealFilterConfiguration() {
        return new NotFilterConfiguration();
    }

}
