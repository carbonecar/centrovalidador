package ar.com.osde.centroValidador.services.exception;

/**
 * Enmascara la excepcion que ocurre dentro de un thread
 * @author MT27789605
 *
 */
public class InThreadingException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public InThreadingException(Exception e){
        super(e);
    }
    
}
