package ar.com.osde.centroValidador.motor;


/**
 * 
 * Clase responsable de procesar las entidades con Drools en forma embebida.
 *
 */
public class MotorEmbebido {
    
    public MotorEmbebido(){
        
    }
//
//    private RulesEngine ruleEngine;
//    private RulesTransformer<RespuestaAptitud> rulesTransformer;
//    private String changeSet = "Test";
//    private String entryPoint = "put anything here";
//
//    public RespuestaAptitud process(Object... objects) {
//
//        RulesRequest rulesRequest = getRulesTransformer().createRequest(objects);
//
//        rulesRequest.setEntryPoint(entryPoint);
//        rulesRequest.setRuleSeName(changeSet);
//
//        RulesResponse response = ruleEngine.process(rulesRequest);
//
//        return getRulesTransformer().parseResponse(response, (Object[]) null);
//    }
//
//    public RulesEngine getRuleEngine() {
//        return ruleEngine;
//    }
//
//    public void setRuleEngine(RulesEngine ruleEngine) {
//        this.ruleEngine = ruleEngine;
//    }
//
//    public RulesTransformer<RespuestaAptitud> getRulesTransformer() {
//        return rulesTransformer;
//    }
//
//    public void setRulesTransformer(RulesTransformer<RespuestaAptitud> rulesTransformer) {
//        this.rulesTransformer = rulesTransformer;
//    }
//
//    public String getChangeSet() {
//        return changeSet;
//    }
//
//    public void setChangeSet(String changeSet) {
//        this.changeSet = changeSet;
//    }
}
