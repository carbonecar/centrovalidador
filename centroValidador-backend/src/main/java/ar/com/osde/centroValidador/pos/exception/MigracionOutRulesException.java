package ar.com.osde.centroValidador.pos.exception;

public class MigracionOutRulesException extends TimeOutRulesException {

    public MigracionOutRulesException(String arg0) {
        super(arg0);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
