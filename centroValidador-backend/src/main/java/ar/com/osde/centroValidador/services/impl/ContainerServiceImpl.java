package ar.com.osde.centroValidador.services.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jws.WebParam;

import org.apache.log4j.Logger;
import org.springframework.jms.core.SessionCallback;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import ar.com.osde.centroValidador.bo.ContainerBo;
import ar.com.osde.centroValidador.pos.handlers.ErrorHandler;
import ar.com.osde.centroValidador.pos.mq.PosMDP;
import ar.com.osde.centroValidador.services.pos.ContainerService;
import ar.com.osde.common.colections.utils.ListFactory;
import ar.com.osde.comunes.utils.ListenerManager;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.QueueMessage;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

public class ContainerServiceImpl implements ContainerService {

    private ListenerManager manager;
    private ContainerBo containerBo;
    private static final Logger LOG = Logger.getLogger(ContainerServiceImpl.class);

    public ContainerConfiguration getContainer(@WebParam(name = "id") Long id) throws ServiceException {
        try {
            return containerBo.getById(id);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Crea el container detenido y lo persiste.
     *
     * @throws ServiceException
     */
    public void saveContainer(ContainerConfiguration containerConfiguration) throws ServiceException {
        try {
            containerBo.saveContainer(containerConfiguration);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Actualiza el estado del container a nivel base de datos.
     *
     * @throws ServiceException
     */
    public void updateContainer(ContainerConfiguration container) throws ServiceException {
        try {
            containerBo.updateContainer(container);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateContainerOnMemory(@WebParam(name = "idHandler") ContainerConfiguration container) throws ServiceException {
        try {
            this.containerBo.updateContainerOnMemory(container);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Obtiene todos los containers.
     *
     * @throws ServiceException
     */
    public List<ContainerConfiguration> getAllContainers() throws ServiceException {

        List<ContainerConfiguration> containerList = null;

        try {
            containerList = containerBo.getAllContainers();
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }

        return containerList;
    }

    /**
     * Detiene y borra un container.
     *
     * @throws ServiceException
     */
    public void deleteContainer(Long id) throws ServiceException {
        try {
            ContainerConfiguration container = containerBo.getById(id);
            if (container != null)
                containerBo.deleteContainer(container);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Arranca el container.
     *
     * @throws ServiceException
     */
    public void startContainer(Long id) throws ServiceException {
        try {
            ContainerConfiguration container = containerBo.getById(id);
            if (container != null)
                containerBo.startContainer(container);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void startContainersWithGroupName(@WebParam(name = "groupName") String groupName) throws ServiceException {
        try {
            List<ContainerConfiguration> list = this.containerBo.getAllContainersWithGroupName(groupName);
            for (ContainerConfiguration containerConfiguration : list) {
                if (!getManager().getListener(containerConfiguration.getId()).isRunning())
                    this.containerBo.startContainer(containerConfiguration);
            }
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void stopContainersWithGroupName(@WebParam(name = "groupName") String groupName) throws ServiceException {
        try {
            List<ContainerConfiguration> list = this.containerBo.getAllContainersWithGroupName(groupName);
            for (ContainerConfiguration containerConfiguration : list) {
                if (getManager().getListener(containerConfiguration.getId()).isRunning())
                    this.containerBo.stopContainer(containerConfiguration);
            }
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Detiene el container.
     */
    public void stopContainer(Long id) throws ServiceException {
        try {
            ContainerConfiguration container = containerBo.getById(id);
            if (container != null)
                containerBo.stopContainer(container);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void cloneContainer(Long idToClone) throws ServiceException {
        try {
            ContainerConfiguration containerToClone = this.containerBo.getById(idToClone);
            ContainerConfiguration containerClone = (ContainerConfiguration) containerToClone.clone();
            containerClone.cleanToSave();
            this.saveContainer(containerClone);
        } catch (BusinessException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        } catch (CloneNotSupportedException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<QueueMessage> getMessagesErrorHandlerQueue(Long idContainer) throws ServiceException {
        DefaultMessageListenerContainer container = this.manager.getListenerOnMemory(idContainer);
        PosMDP listener = (PosMDP) container.getMessageListener();
        final ErrorHandler errorHandler = listener.getErrorHandler();
        if (errorHandler != null && errorHandler.getTemplateResponse() != null && errorHandler.getQueue() != null)
            return (List<QueueMessage>) errorHandler.getTemplateResponse().execute(new SessionCallback() {
                public Object doInJms(Session session) throws JMSException {
                    QueueBrowser browser = session.createBrowser(errorHandler.getQueue());
                    Enumeration messages = browser.getEnumeration();
                    List<QueueMessage> mensajeDeLaColaInline = new ArrayList<QueueMessage>();
                    while (messages.hasMoreElements()) {
                        TextMessage message = (TextMessage) messages.nextElement();
                        mensajeDeLaColaInline.add(new QueueMessage(message.getJMSMessageID(), message.getText().trim()));
                    }
                    browser.close();
                    return mensajeDeLaColaInline;
                }
            });
        return new ArrayList<QueueMessage>();
    }

    public QueueMessage removeMessageFromErrorHandlerQueue(Long idContainer, final String idMessage) throws ServiceException {
        DefaultMessageListenerContainer container = this.manager.getListenerOnMemory(idContainer);
        PosMDP listener = (PosMDP) container.getMessageListener();
        final ErrorHandler errorHandler = listener.getErrorHandler();
        String messageSelector = "JMSMessageID = '" + idMessage + "'";
        TextMessage message = (TextMessage) errorHandler.getTemplateResponse().receiveSelected(messageSelector);
        QueueMessage queueMessage = null;
        if (message != null)
            try {
                queueMessage = new QueueMessage(idMessage, message.getText());
            } catch (JMSException e) {
                LOG.debug(e);
                throw new ServiceException(e.getMessage());
            }
        return queueMessage;
    }

    public ContainerBo getContainerBo() {
        return containerBo;
    }

    public void setContainerBo(ContainerBo containerBo) {
        this.containerBo = containerBo;
    }

    public void reloadAllContainers() throws ServiceException {
        List<ContainerConfiguration> containers = this.getAllContainers();
        List<Long> containersId = ListFactory.<Long>createList();

        for (Iterator<ContainerConfiguration> iterator = containers.iterator(); iterator.hasNext(); ) {
            ContainerConfiguration containerConfiguration = iterator.next();
            containersId.add(containerConfiguration.getId());
        }
        this.reloadContainer(containersId);
    }

    public void reloadContainer(List<Long> containersId) throws ServiceException {
        this.containerBo.realoadContainers(containersId);

    }


    public ContainerConfiguration getParentContainer(@WebParam(name = "idHandler") long idHandler) {
        return this.containerBo.getParentContainer(idHandler);
    }

    public ListenerManager getManager() {
        return manager;
    }

    public void setManager(ListenerManager manager) {
        this.manager = manager;
    }
}
