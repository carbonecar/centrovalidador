package ar.com.osde.centroValidador.formatter;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.format.AbstractFixedFormatter;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;

/**
 * Formater para quitar todos los espacios
 * @author MT27789605
 * 
 */
public class TrimFormatter extends AbstractFixedFormatter<String> {

    public TrimFormatter(){
        super();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.AbstractFixedFormatter#asObject
     * (java.lang.String,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */
    @Override
    public String asObject(String string, FormatInstructions instructions) {
        if (string != null) {
            string = string.trim();
        }
        int length = instructions.getLength();
        Align alignment = instructions.getAlignment();
        char paddingChar = instructions.getPaddingChar();
        if (length == 0) {
            for (int i = 0; i < length; i++) {
                string += paddingChar;
            }
        }
        while (string.length() < length) {
            if (alignment == Align.LEFT) {
                string += paddingChar;
            } else {
                string = paddingChar + string;
            }
        }
        return string;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.AbstractFixedFormatter#asString
     * (java.lang.Object,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */
    @Override
    public String asString(String paramT, FormatInstructions paramFormatInstructions) {
        return null;
    }
}
