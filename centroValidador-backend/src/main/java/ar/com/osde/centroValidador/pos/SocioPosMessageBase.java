package ar.com.osde.centroValidador.pos;

import ar.com.osde.centroValidador.formatter.TrimFormatter;


import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase utilizada para levantar los campos por los cuales se filtra una
 * transaccion.
 * 
 * @author VA27789605
 * 
 */
@Record
public class SocioPosMessageBase extends AbstractPosMessage  {

    private static final long serialVersionUID = 1L;
    // Derivado de la concatenacion del prefijo y el numero de Socio.
    //private String numeroContrato;
    // AUFILA
    private String prefijoAsociado;
    // AUASOA
    private String numeroAsociado;
    // AUBENA
    private String numeroBeneficiario;

 // AUMANA
    private String formaIngresoDelAsociado;

    // Se cambia para que complete con 0 a fines de que se pueda parsear a
	// entero cuando el valor es no esta informado
	@Field(offset = 33, length = 2)
    public String getPrefijoAsociado() {
       if (prefijoAsociado.equals("  ") || "".equals(prefijoAsociado)) {
			return "00";
		}
		return prefijoAsociado;
    }

    public void setPrefijoAsociado(String p) {
        this.prefijoAsociado = p;
    }

   	// Se cambia para que complete con 0 a fines de que se pueda parsear a
	// entero cuando el valor es no esta informado
	@Field(offset = 35, length = 7)
    public String getNumeroAsociado() {
        if ("       ".equals(numeroAsociado) || "".equals(numeroAsociado)) {
			return "0000000";
		}
		return numeroAsociado;
    }

    public void setNumeroAsociado(String p) {
        this.numeroAsociado = p;
    }

	// // Se cambia para que complete con 0 a fines de que se pueda parsear a
	// // entero cuando el valor es no esta informado
	// @Field(offset = 33, length = 9, paddingChar = '0')
	// public String getNumeroContrato() {
	// return numeroContrato;
	// }

	// public void setNumeroContrato(String p) {
	// this.numeroContrato = p;
	// }
	
	@Field(offset = 42, length = 2,align=Align.RIGHT,paddingChar='0',formatter=TrimFormatter.class)
	public String getNumeroBeneficiario() {
		if ("  ".equals(numeroBeneficiario) || "".equals(numeroBeneficiario)) {
			return "00";
		}
		return numeroBeneficiario;
	}
	
    public void setNumeroBeneficiario(String p) {
        this.numeroBeneficiario = p;
    }

   
    @Field(offset = 59, length = 1)
    public String getFormaIngresoDelAsociado() {
        return formaIngresoDelAsociado;
    }

    public void setFormaIngresoDelAsociado(String p) {
        this.formaIngresoDelAsociado = p;
    }

    @Override
    public String toString() {
        return getCodigoTransaccion() + getAtributoCodigoTransaccion() + " | " + prefijoAsociado + numeroAsociado
                + numeroBeneficiario;
    }
}
