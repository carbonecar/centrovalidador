package ar.com.osde.centroValidador.pos.filter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;

/**
 * Filtra las propiedades que NO cumplen con la igualdad. Son propiedades "basicas"
 * de java
 *
 * @author VA27789605
 */
public class PropertyEqualFilter implements PosMessageFilter {

    private String propertyName;
    private Object value;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void validate(SocioPosMessageBase filter) throws FilteredMessageException {

        try {
            Method m = filter.getClass().getMethod(propertyName);

            Object propertyValue = m.invoke(filter);
            if (!propertyValue.equals(value)) {
                throw new FilteredMessageException(propertyName + " = " + value + " Mensaje ");
            }
        } catch (SecurityException e) {
            throw new FilteredMessageException(e.getMessage(), filter,0);
        } catch (NoSuchMethodException e) {
            throw new FilteredMessageException(e.getMessage(), filter,0);
        } catch (IllegalArgumentException e) {
            throw new FilteredMessageException(e.getMessage(), filter,0);
        } catch (IllegalAccessException e) {
            throw new FilteredMessageException(e.getMessage(), filter,0);
        } catch (InvocationTargetException e) {
            throw new FilteredMessageException(e.getMessage(), filter,0);
        }

    }

    public boolean implementaPropiedadValor(String propertyName, Object value) {
        return this.propertyName.equals(propertyName) && this.value.equals(value);
    }

    public boolean evaluaPropiedad(String propertyName) {
        return this.propertyName.equals(propertyName);
    }

}
