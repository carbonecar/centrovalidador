package ar.com.osde.centroValidador.pos.handlers.xml;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPos;
import ar.com.osde.cv.transaccion.Transacciones;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;

/**
 * 
 * @author VA27789605
 * 
 */
public class XmlFormatMessageHandler implements PosMessageHandler {
    private static final Log LOG=LogFactory.getLog(XmlFormatMessageHandler.class);
    private AptoServicioBO aptoServicioBO;
    private XMLFormatManager xmlFormatManager;

    public AptoServicioBO getAptoServicioBO() {
        return aptoServicioBO;
    }

    public XMLFormatManager getXmlFormatManager() {
        return xmlFormatManager;
    }

    public void setXmlFormatManager(XMLFormatManager xmlFormatManager) {
        this.xmlFormatManager = xmlFormatManager;
    }

    public void setAptoServicioBO(AptoServicioBO aptoServicioBO) {
        this.aptoServicioBO = aptoServicioBO;
    }

    public RespuestaCentroValidador process(Message message) throws PosMessageHandleException {

        try {
            TextMessage textMessage = (TextMessage) message;
            Transacciones transacciones = xmlFormatManager.load(Transacciones.class, textMessage.getText());

            if (transacciones.getAptoSocioMessage() != null) {
                //TODO implementar
//                aptoServicioBO.isAptoSocio((TransaccionPos) TransaccionFactory.getInstance().createAptoSocio(
//                        transacciones.getAptoSocioMessage().getRequest()));
            }

        } catch (FixedFormatException e) {
            LOG.error(e);
            throw new UnsupportedPosMessageException(
                    "No es es posible parserar la transaccion, verifique contra el XSD que esta enviando la transaccion.: "
                            + e.getMessage());
        } catch (JMSException e) {
            throw new UnsupportedPosMessageException("No es posible obtener el texto del mensaje jms: " + e.getMessage());
        }

        RespuestaCentroValidador respuesta = null;

        return respuesta;
    }

    public String getHierarchyName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("XmlFormatMessageHandler\n");
        return strBuff.toString();
    }

    public boolean procesaMensaje() {
        return true;
    }

    public List<PosMessageFilter> getMessageFilters() {
        return new ArrayList<PosMessageFilter>();
    }
}
