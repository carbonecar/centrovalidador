package ar.com.osde.centroValidador.pos.filter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate102;
import org.springframework.jms.core.MessageCreator;

/**
 * Handler simple para pruebas. Solo responde a una cola
 * 
 * @author VA27789605
 * 
 */
public class TestingMessageHandler implements StringMessageHandler {

	private JmsTemplate102 jmsTemplateResponse;

	public TestingMessageHandler(JmsTemplate102 jmsTemplateResponse) {
		this.jmsTemplateResponse = jmsTemplateResponse;
	}

	public void handle(String message) {

		jmsTemplateResponse.send(new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage("response");
				return message;
			}
		});
	}

}
