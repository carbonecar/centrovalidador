package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * 
 * @author MT27789605
 * 
 */
@Record
public class CierreFarmaciaPosMessage extends AbstractConsultaFarmaciaPosMessage {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    // AUPUBL. Este campo no se usa
    private String publicaPedido;

    public CierreFarmaciaPosMessage() {
        super();
    }

    @Deprecated
    // publica pedido
    @Field(offset = 469, length = 1)
    public String getPublicaPedido() {
        return publicaPedido;
    }

    public void setPublicaPedido(String publicaPedido) {
        this.publicaPedido = publicaPedido;
    }

}
