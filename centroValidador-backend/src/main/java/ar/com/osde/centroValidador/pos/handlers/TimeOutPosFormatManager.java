package ar.com.osde.centroValidador.pos.handlers;

import ar.com.osde.centroValidador.pos.IPosMessage;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

/**
 * 
 * @author MT27789605
 * 
 */
public class TimeOutPosFormatManager implements FixedFormatManager {

    private FixedFormatManager fixedFormatManager = new FixedFormatManagerImpl();

    public <T> T load(Class<T> paramClass, String originalMessage) throws FixedFormatException {

        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();
        String stringMessage=timeOutMessageParser.parseRequest(originalMessage);
        
        T posMessage = fixedFormatManager.load(paramClass, stringMessage.replace("\0", " "));

        ((IPosMessage) posMessage).setOriginalMessage(originalMessage);

        return posMessage;
    }

    public <T> String export(T posMessage) throws FixedFormatException {
        return fixedFormatManager.export(posMessage);
    }

    public <T> String export(String paramString, T paramT) throws FixedFormatException {
        return fixedFormatManager.export(paramT);
    }

}
