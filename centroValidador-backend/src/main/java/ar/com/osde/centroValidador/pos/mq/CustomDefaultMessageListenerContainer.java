package ar.com.osde.centroValidador.pos.mq;

import javax.jms.JMSException;

import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.JmsUtils;

public class CustomDefaultMessageListenerContainer extends DefaultMessageListenerContainer {

    private boolean startOnLoad;

    protected void handleListenerSetupFailure(Throwable ex, boolean alreadyRecovered) {
		if (ex instanceof JMSException) {
			invokeExceptionListener((JMSException) ex);
		}
		if (ex instanceof SharedConnectionNotInitializedException) {
			if (!alreadyRecovered) {
				logger.debug("JMS message listener invoker needs to establish shared Connection");
			}
		} else {
			// Recovery during active operation..
			if (alreadyRecovered) {
				logger.debug("Setup of JMS message listener invoker failed - already recovered by other invoker", ex);
			} else {
				StringBuffer msg = new StringBuffer();
				msg.append("Setup of JMS message listener invoker failed for destination '");
				msg.append(getDestinationDescription()).append("' - trying to recover. Cause: ");
				try {
					msg.append(ex instanceof JMSException ? JmsUtils.buildExceptionMessage((JMSException) ex) : ex
					        .getMessage());
					if (logger.isDebugEnabled()) {
						logger.info(msg, ex);
					} else {
						logger.info(msg);
					}
				} catch (NullPointerException ne) {
					logger.info(msg, ne);
				}
			}
		}
	}

    public boolean isStartOnLoad() {
        return startOnLoad;
    }

    public void setStartOnLoad(boolean startOnLoad) {
        this.startOnLoad = startOnLoad;
    }
}
