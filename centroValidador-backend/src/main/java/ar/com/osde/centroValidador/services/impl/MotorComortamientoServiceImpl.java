package ar.com.osde.centroValidador.services.impl;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ar.com.osde.centroValidador.services.MotorComportamientoService;
import ar.com.osde.cv.pos.comportamiento.Comportamiento;

@WebService(endpointInterface = "ar.com.osde.centroValidador.services.MotorComportamientoService", serviceName = "MotorComportamientoService")
public class MotorComortamientoServiceImpl implements MotorComportamientoService {

    private Comportamiento comportamiento;
    
    public Comportamiento getComportamiento() {
        return comportamiento;
    }

    

    public void setComportamiento(Comportamiento comportamiento) {
        this.comportamiento = comportamiento;
    }
    @WebMethod
    public void iniciarMotor(Date fechaDesde) {
        if(fechaDesde!=null){
            comportamiento.setLastDayComputed(fechaDesde);
        }
       ExecutorService executor= Executors.newFixedThreadPool(1);
       executor.execute(comportamiento);
       executor.shutdown();

    }

    
}
