package ar.com.osde.centroValidador.pos;

import ar.com.osde.entities.transaccion.TipoTransaccion;

public class PosMessageResponseCreatorFactory {

    public static PosMessageResponse createPosMessageReponse() {
        return new SocioPosMessageResponse();
    }

    public static PosMessageResponse createPosMessageReponse(TipoTransaccion codigo) {

        if (TipoTransaccion.O2F.equals(codigo) || TipoTransaccion.O1F.equals(codigo) || TipoTransaccion.O2J.equals(codigo)) {
            return new PosMessageResponseFarmacia();
        }
        if (TipoTransaccion.O1A.equals(codigo) || TipoTransaccion.O1B.equals(codigo)) {
            return new PosMessageResponsePrestaciones();
        }

        if (TipoTransaccion.O2A.equals(codigo) || TipoTransaccion.O2Q.equals(codigo) || TipoTransaccion.O2B.equals(codigo)
                || TipoTransaccion.O2C.equals(codigo) || TipoTransaccion.O2D.equals(codigo) || TipoTransaccion.O2S.equals(codigo)
                || TipoTransaccion.O2L.equals(codigo)||TipoTransaccion.O1C.equals(codigo)) {
            return new PosMessageResponsePrestaciones();
        }
        if (codigo.getTipo() == TipoTransaccion.EXCLUSION) {
            return new PosMessageResponseExclusion();
        }

        if (TipoTransaccion.O5F.equals(codigo)) {
            return new PosMessageResponseCierreFarmacia();
        }

        if (TipoTransaccion.O3F.equals(codigo)) {
            return new PosMessageResponseConsulta();
        }

        if (TipoTransaccion.O3A.equals(codigo)) {
            return new PosMessageResponseRescate();
        }

        if (TipoTransaccion.O2I.equals(codigo) || TipoTransaccion.O2E.equals(codigo)) {
            return new PosMessageIngresoEgresoResponse();
        }

        if (TipoTransaccion.O2P.equals(codigo)) {
            return new PosMessageResponseEnvioDocumetacion();
        }
        return new PosMessageResponse();

    }

}
