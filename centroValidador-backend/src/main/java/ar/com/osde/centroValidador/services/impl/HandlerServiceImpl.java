package ar.com.osde.centroValidador.services.impl;

import java.util.LinkedList;
import java.util.List;

import javax.jws.WebParam;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.services.pos.ContainerService;
import ar.com.osde.centroValidador.services.pos.HandlerService;
import ar.com.osde.cv.dao.impl.PosMessageHandlerDAO;
import ar.com.osde.cv.factory.HandlerFactory;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.framework.persistence.exception.PersistenceException;
import ar.com.osde.framework.services.ServiceException;

public class HandlerServiceImpl implements HandlerService {

    private HandlerFactory handlerFactory;
    private PosMessageHandlerDAO posMessageHandlerDao;
    private ContainerService containerService;

    private static final Log LOG=LogFactory.getLog(HandlerServiceImpl.class);

    public PosMessageHandlerConfiguration save(@WebParam(name = "handler") PosMessageHandlerConfiguration messageHandlerConfiguration) throws ServiceException {
        try {
            return this.posMessageHandlerDao.saveNew(messageHandlerConfiguration);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public List<String> getHandlerTypes() {

        List<String> handlerNames = new LinkedList<String>();
        handlerNames.addAll(this.handlerFactory.getAllHandlersNames());
        return handlerNames;
    }

    public PosMessageHandlerConfiguration getHandler(long idHandler) throws ServiceException {
        try {
            return posMessageHandlerDao.getById(idHandler);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public PosMessageHandlerConfiguration getParentHandler(@WebParam(name = "idHandler") long idHandler) throws ServiceException {
        return this.posMessageHandlerDao.getParentHandler(idHandler);
    }

    public void deleteHandler(PosMessageHandlerConfiguration posMessageHandler) throws ServiceException{
        try{
            this.posMessageHandlerDao.delete(posMessageHandler);
        }catch(PersistenceException e){
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }
    
    public void updateFilteredHandler(FilteredMessageHandlerConfiguration oldfilter,FilteredMessageHandlerConfiguration newFilter)throws ServiceException{
        oldfilter=(FilteredMessageHandlerConfiguration) this.getHandler(oldfilter.getId());
        oldfilter.getFilters().clear();
        oldfilter.getFilterStrategyList().clear();
        this.updateHandler(oldfilter);
        oldfilter.setFilters(newFilter.getFilters());
        oldfilter.setFilterStrategyList(newFilter.getFilterStrategyList());
        this.updateHandler(oldfilter);
    }
    
    public void updateHandler(PosMessageHandlerConfiguration posMessageHandler) throws ServiceException {
        try {            
            this.posMessageHandlerDao.saveOrUpdate(posMessageHandler);
            this.updateHandlerOnMemory(posMessageHandler);
        } catch (PersistenceException e) {
            LOG.error(e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updateHandlerOnMemory(@WebParam(name = "handler") PosMessageHandlerConfiguration handler) throws ServiceException {
        boolean parentHandlerFound;
        long handlerId = handler.getId();
        do {
            PosMessageHandlerConfiguration parentHandler = this.getParentHandler(handlerId);
            parentHandlerFound = parentHandler != null;
            if (parentHandlerFound){
                handlerId = parentHandler.getId();
            }
        } while (parentHandlerFound);

        this.containerService.updateContainerOnMemory(this.containerService.getParentContainer(handlerId));
    }

    public PosMessageHandlerConfiguration getParentHandlerForFilter(Long idFilter) throws ServiceException {
        return this.posMessageHandlerDao.getParentHandlerForFilter(idFilter);
    }

    public PosMessageHandlerConfiguration getParentHandlerForFilterStrategy(Long idFilter) throws ServiceException {
        return this.posMessageHandlerDao.getParentHandlerForFilterStrategy(idFilter);
    }

    public HandlerFactory getHandlerFactory() {
        return handlerFactory;
    }

    public void setHandlerFactory(HandlerFactory handlerFactory) {
        this.handlerFactory = handlerFactory;
    }

    public PosMessageHandlerDAO getPosMessageHandlerDao() {
        return posMessageHandlerDao;
    }

    public void setPosMessageHandlerDao(PosMessageHandlerDAO posMessageHandlerDao) {
        this.posMessageHandlerDao = posMessageHandlerDao;
    }

    public ContainerService getContainerService() {
        return containerService;
    }

    public void setContainerService(ContainerService containerService) {
        this.containerService = containerService;
    }
}
