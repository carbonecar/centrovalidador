package ar.com.osde.centroValidador.pos.filter.estrategy;

import javax.jms.Message;


/**
 * Estrategy que no hace nada. Para unificar el comportamiento del cliente.
 * 
 * @author VA27789605
 */
public class NullFilterStrategy implements FilterStrategy {
    
    public void doFilterAction(Message message) {}

    public String getResolucionEstrategia() {
        return MENSAJE_ESTRATEGIA_NULA;
    }
}
