package ar.com.osde.centroValidador.pos.filter.estrategy;

import javax.jms.Message;

/**
 * Estrategia que define lo que se debe hacer con el mensaje filtrado.
 * 
 * @author VA27789605
 */
public interface FilterStrategy {
    static final String MENSAJE_ESTRATEGIA_NULA = "Estrategia no hace nada";
    static final String MENSAJE_ESTRATEGIA_CONTINUA_PROCESANDO = "Continua procesando con su manejador de mensajes configurado";
    static final String ESTRATEGIA_RUTEO = "Estrategia de ruteo ";

    /**
     * Devuelve lo que esta haciendo la estrategia.
     * 
     * @return
     */
    String getResolucionEstrategia();

    /**
     * Realiza la acci�n del filtro
     * 
     * @param message
     */
    void doFilterAction(Message message);

}
