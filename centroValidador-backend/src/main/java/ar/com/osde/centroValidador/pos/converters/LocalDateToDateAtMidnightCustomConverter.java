package ar.com.osde.centroValidador.pos.converters;

import java.util.Calendar;

import org.dozer.CustomConverter;
import org.joda.time.LocalDate;

/**
 * 
 * @author MT27789605
 * 
 */
public class LocalDateToDateAtMidnightCustomConverter implements CustomConverter {

    public Object convert(Object destination, Object source, Class<?> destinationClass, Class<?> sourceClass) {
        Object date = null;
        if (source != null) {
            LocalDate localDate = ((LocalDate) source);
            try {
                date = localDate.toDateMidnight().toDate();
            } catch (Exception e) {
                //date = localDate.toDateMidnight(DateTimeZone.UTC).toDate();
                Calendar cal=Calendar.getInstance();
                cal.set(localDate.getYear(),localDate.getMonthOfYear()-1,localDate.getDayOfMonth(),0,0,0);
                date=cal.getTime();
            }
        }
        return date;

    }
}
