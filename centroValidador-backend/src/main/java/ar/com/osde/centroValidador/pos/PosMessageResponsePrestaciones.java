package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class PosMessageResponsePrestaciones extends SocioPosMessageResponse {

    PosMessageResponsePrestaciones(){
        
    }
    
    //AURES4
    private String codigoRespuestaItem4;

    @Field(offset = 43, length = 2, align = Align.RIGHT, paddingChar = ' ')
    public String getCodigoRespuestaItem4() {
        return codigoRespuestaItem4;
    }

    public void setCodigoRespuestaItem4(String codigoRespuestaItem4) {
        this.codigoRespuestaItem4 = codigoRespuestaItem4;
    }

    
}
