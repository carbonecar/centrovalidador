package ar.com.osde.centroValidador.formatter;

import java.math.BigDecimal;

import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.BigDecimalFormatter;

/**
 * Formatter para reemplazar los espacios con un cero
 * 
 * @author MT27789605
 * 
 */
public class ReplaceSpaceWithCerosBigDecimalFormatter extends BigDecimalFormatter {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.impl.BigDecimalFormatter#
     * asObject(java.lang.String,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */
    public BigDecimal asObject(String string, FormatInstructions instructions) {
        if (string != null) {
            string = string.replaceAll(" ", "0");
        }

        return super.asObject(string, instructions);
    }
}
