package ar.com.osde.centroValidador.pos;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import ar.com.osde.centroValidador.pos.utils.PosLocalDateFormatter;
import ar.com.osde.centroValidador.pos.utils.PosLocalTimeFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class PosMessagePersistence {

	private String filler;// Free 1 5 5
	private String codigoOperador;// Oped 6 2 7 Codigo Operador
	private int filialInstalacionPos;// Filp 8 2 9 Filial Instalacion Pos
	private int delegacionTerminal;// Delp 10 2 11 Delegacion Prestador
	private int numeroPos;// Nrop 12 4 15 Numero de pos
	private String cuitPrestador;// Cuit 16 11 26 Cuit prestador
	private int nroIsoEmisorCredencial;// Isoa 27 6 32 Nro. ISO del Emisor de
										// Credencial
	private int filialSocio;// Fila 33 2 34 Filial operador
	private int nroAsociado;// Asoa 35 7 41 Nro Asociado
	private int numeroBeneficiario;// Bene 42 2 43 beneficiario
	private int digitoVerificador;// Diga 44 1 44 digito verficador?.
	private int codigoTransaccion;// Trac 45 2 46 Codigo transaccion (A)
	private String atributoTransaccion;// Atrc 47 1 47 atributo transaccion (1)
	private int numeroTransaccion;// Syst 48 6 53 Numero de transaccion
	// Tpdu 54 5 58 ???????????
	private char formaIngresoAsociado;// Mana 59 1 59 Forma de ingreso del
										// numero de asociado
	private LocalDate fechaTransaccion;// Fect 60 8 67 Fecha transaccion
	private LocalTime horaTransaccion;// Hort 68 6 73 hora transaccion
	private int numeroSecuencia;// Nsec 74 2 75 Nro Secuencia
	private int totalMensajes;// Ntot 76 2 77 Nro Total mensajes
	private int versionCredencial;// Verc 78 2 79 Version credencial
	private String codigoRespuestaTransaccion;// Resp 80 2 81 Codigo respuesta
												// Transaccion
	private String leyendaPrestador;// Leyd 82 40 121 Leyenda prestador
	private String leyendaImpresion;// Leyp 122 80 201 Leyenda Impresion

	private String vectorIncAdv;// 202 30 231 vector inconsistencias y advertencias sin discriminar

	@Field(offset = 1, length = 5)
	public String getFiller() {
		return filler;
	}

	public void setFiller(String filler) {
		this.filler = filler;
	}

	@Field(offset = 6, length = 2)
	public String getCodigoOperador() {
		return codigoOperador;
	}

	public void setCodigoOperador(String codigoOperador) {
		this.codigoOperador = codigoOperador;
	}

	@Field(offset = 8, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getFilialInstalacionPos() {
		return filialInstalacionPos;
	}

	public void setFilialInstalacionPos(int filialInstalacionPos) {
		this.filialInstalacionPos = filialInstalacionPos;
	}

	@Field(offset = 10, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getDelegacionTerminal() {
		return delegacionTerminal;
	}

	public void setDelegacionTerminal(int delegacionTerminal) {
		this.delegacionTerminal = delegacionTerminal;
	}

	@Field(offset = 12, length = 4,align=Align.RIGHT,paddingChar='0')
	public int getNumeroPos() {
		return numeroPos;
	}

	public void setNumeroPos(int numeroPos) {
		this.numeroPos = numeroPos;
	}

	@Field(offset = 16, length = 11)
	public String getCuitPrestador() {
		return cuitPrestador;
	}

	public void setCuitPrestador(String cuitPrestador) {
		this.cuitPrestador = cuitPrestador;
	}

	@Field(offset = 27, length = 6,align=Align.RIGHT,paddingChar='0')
	public int getNroIsoEmisorCredencial() {
		return nroIsoEmisorCredencial;
	}

	public void setNroIsoEmisorCredencial(int nroIsoEmisorCredencial) {
		this.nroIsoEmisorCredencial = nroIsoEmisorCredencial;
	}

	@Field(offset = 33, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getFilialSocio() {
		return filialSocio;
	}

	public void setFilialSocio(int filialSocio) {
		this.filialSocio = filialSocio;
	}

	@Field(offset = 35, length = 7,align=Align.RIGHT,paddingChar='0')
	public int getNroAsociado() {
		return nroAsociado;
	}

	public void setNroAsociado(int nroAsociado) {
		this.nroAsociado = nroAsociado;
	}

	@Field(offset = 42, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getNumeroBeneficiario() {
		return numeroBeneficiario;
	}

	public void setNumeroBeneficiario(int numeroBeneficiario) {
		this.numeroBeneficiario = numeroBeneficiario;
	}

	@Field(offset = 44, length = 1,align=Align.RIGHT,paddingChar='0')
	public int getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(int digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	@Field(offset = 45, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getCodigoTransaccion() {
		return codigoTransaccion;
	}

	public void setCodigoTransaccion(int codigoTransaccion) {
		this.codigoTransaccion = codigoTransaccion;
	}

	@Field(offset = 47, length = 1)
	public String getAtributoTransaccion() {
		return atributoTransaccion;
	}

	public void setAtributoTransaccion(String atributoTransaccion) {
		this.atributoTransaccion = atributoTransaccion;
	}

	@Field(offset = 48, length = 6,align=Align.RIGHT,paddingChar='0')
	public int getNumeroTransaccion() {
		return numeroTransaccion;
	}

	public void setNumeroTransaccion(int numeroTransaccion) {
		this.numeroTransaccion = numeroTransaccion;
	}

	@Field(offset = 59, length = 1)
	public char getFormaIngresoAsociado() {
		return formaIngresoAsociado;
	}

	public void setFormaIngresoAsociado(char formaIngresoAsociado) {
		this.formaIngresoAsociado = formaIngresoAsociado;
	}

	@Field(offset = 60, length = 8, formatter = PosLocalDateFormatter.class)
	@FixedFormatPattern("yyyyMMdd")
	public LocalDate getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(LocalDate fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	@Field(offset = 68, length = 6, formatter = PosLocalTimeFormatter.class)
    @FixedFormatPattern("HHmmss")
	public LocalTime getHoraTransaccion() {
		return horaTransaccion;
	}

	public void setHoraTransaccion(LocalTime horaTransaccion) {
		this.horaTransaccion = horaTransaccion;
	}

	@Field(offset = 74, length = 2)
	public int getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(int numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	@Field(offset = 76, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getTotalMensajes() {
		return totalMensajes;
	}

	public void setTotalMensajes(int totalMensajes) {
		this.totalMensajes = totalMensajes;
	}

	@Field(offset = 78, length = 2,align=Align.RIGHT,paddingChar='0')
	public int getVersionCredencial() {
		return versionCredencial;
	}

	public void setVersionCredencial(int versionCredencial) {
		this.versionCredencial = versionCredencial;
	}

	@Field(offset = 80, length = 2)
	public String getCodigoRespuestaTransaccion() {
		return codigoRespuestaTransaccion;
	}

	public void setCodigoRespuestaTransaccion(String codigoRespuestaTransaccion) {
		this.codigoRespuestaTransaccion = codigoRespuestaTransaccion;
	}

	@Field(offset = 82, length = 40)
	public String getLeyendaPrestador() {
		return leyendaPrestador;
	}

	public void setLeyendaPrestador(String leyendaPrestador) {
		this.leyendaPrestador = leyendaPrestador;
	}

	@Field(offset = 122, length = 80)
	public String getLeyendaImpresion() {
		return leyendaImpresion;
	}

	public void setLeyendaImpresion(String leyendaImpresion) {
		this.leyendaImpresion = leyendaImpresion;
	}

	@Field(offset=202,length=30)
	public String getVectorIncAdv() {
		return vectorIncAdv;
	}

	public void setVectorIncAdv(String vectorIncAdv) {
		this.vectorIncAdv = vectorIncAdv;
	}

	
}
