package ar.com.osde.centroValidador.formatter;

import org.apache.commons.lang.StringUtils;

import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.LongFormatter;

/**
 * Formatter que devuevle un 0 en caso que no sea un numero.
 * 
 * @author MT27789605
 * 
 */
public class TrunkToZeroFormmatter extends LongFormatter {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.impl.LongFormatter#asObject
     * (java.lang.String,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */
    @Override
    public Long asObject(String string, FormatInstructions instructions) {
        string = string.trim();
        if (StringUtils.isNumeric(string)) {
            return super.asObject(string, instructions);
        } else {
            return 0l;
        }
    }
}
