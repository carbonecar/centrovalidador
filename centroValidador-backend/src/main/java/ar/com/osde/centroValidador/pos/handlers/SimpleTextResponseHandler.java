package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.rules.paralelo.ResponseRecord;

/**
 * Handler que devuevle una respusta del CV con el texto de repuesta. No proceso
 * nada. Solo guarda la respuesta
 * 
 * @author VA27789605
 * 
 */
public class SimpleTextResponseHandler implements PosMessageHandler {

    private static final Log LOG=LogFactory.getLog(SimpleTextResponseHandler.class);

	public RespuestaCentroValidador process(javax.jms.Message message) throws PosMessageHandleException {
		StatisticalOldSystemResponse respuestaCV = new StatisticalOldSystemResponse(null);
		try {
			String mensajeTxt = ((TextMessage) message).getText();
			respuestaCV.setResponseString(mensajeTxt);
			respuestaCV.setSystemName(ResponseRecord.SystemName.POS.getName());
		} catch (JMSException e) {
		    LOG.error(e);
		}
		return respuestaCV;
	}

	public String getHierarchyName() {
		return "SimpleTextResponseHandler";
	}

    public boolean procesaMensaje() {
        return false;
    }

    public List<PosMessageFilter> getMessageFilters() {
        return new ArrayList<PosMessageFilter>();
    }

}
