package ar.com.osde.centroValidador.pos;

import org.joda.time.LocalDate;

import ar.com.osde.centroValidador.pos.utils.PosLocalDateFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;

public class AbstractConsultaFarmaciaPosMessage extends AbstractPosMessage {

    public static final String PUNTO_VENTA="V";
    public static final String PRESTADOR="P";
    
    public static final String CSV="P";
    public static final String PDF="T";
    
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // AUFCVF
    private LocalDate fechaHastaRecuperar;

    // AUFPUB
    private String formatoPublicacion;

    // AUPUVE
    private String puntoVentaPrestador;
    
    
    @Field(offset = 364, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaHastaRecuperar() {
        return fechaHastaRecuperar;
    }

    public void setFechaHastaRecuperar(LocalDate fechaHastaRecuperar) {
        this.fechaHastaRecuperar = fechaHastaRecuperar;
    }

    @Field(offset = 466, length = 1)
    public String getFormatoPublicacion() {
        return formatoPublicacion;
    }

    public void setFormatoPublicacion(String formatoPublicacion) {
        this.formatoPublicacion = formatoPublicacion;
    }
    
    /**
     * Indica si el cierre se hace por punto de venta o prestador.
     * @return
     */
    @Field(offset = 467, length = 1)
    public String getPuntoVentaPrestador() {
        return puntoVentaPrestador;
    }

    public void setPuntoVentaPrestador(String tipo) {
        this.puntoVentaPrestador = tipo;
    }
}
