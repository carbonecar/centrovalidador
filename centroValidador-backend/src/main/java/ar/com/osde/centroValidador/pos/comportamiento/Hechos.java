package ar.com.osde.centroValidador.pos.comportamiento;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/**
 * Representacion del estado de las variables correspondiente a un hecho
 * 
 * @author MT27789605
 * 
 */
public class Hechos {

    public static final String TIME_OUT_FORZADO = "timeOutForzado";
    public static final String ERROR_MDR="errorMDR";
    public static final String SOAP_FAULT="soapfault";
    private String name;

    private long cant = 0;
    private Date fechaUltimaNotification = DateUtils.add(new Date(),Calendar.MINUTE,-10);
    private long cantInLastInterval = 0;

    
    public Hechos(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getCant() {
        return cant;
    }

    public void setCant(long cant) {
        this.cant = cant;
    }

    public Date getFechaUltimaNotification() {
        return fechaUltimaNotification;
    }

    public void setFechaUltimaNotification(Date fechaUltimaNotification) {
        this.fechaUltimaNotification = fechaUltimaNotification;
    }

    public long getCantInLastInterval() {
        return cantInLastInterval;
    }

    public void setCantInLastInterval(long cantInLastInterval) {
        this.cantInLastInterval = cantInLastInterval;
    }

    public void addCantInLastInterval() {
        this.cantInLastInterval++;

    }

}
