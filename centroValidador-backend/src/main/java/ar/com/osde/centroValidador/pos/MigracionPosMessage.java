package ar.com.osde.centroValidador.pos;

import java.math.BigDecimal;

import ar.com.osde.centroValidador.formatter.ReplaceSpaceWithCerosBigDecimalFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatBoolean;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatDecimal;
import com.ancientprogramming.fixedformat4j.annotation.Record;
import com.ancientprogramming.fixedformat4j.format.impl.BooleanFormatter;

/**
 * Agregado de los campos
 * 
 * @author MT27789605
 * 
 */
@Record
public class MigracionPosMessage extends TimeOutResponsePosMessage {
    public static final BigDecimal CERO = BigDecimal.ZERO;
    public static final BigDecimal CERO_COMA_CEROCERO = new BigDecimal("0.00");

    private BigDecimal importeCobertura1;
    private BigDecimal importeCargoSocio1;
    private BigDecimal porcentajeCobertura1;
    private BigDecimal porcentajeCargoSocio1;
    private BigDecimal valorFacturado1;
    private BigDecimal porcentajeBonificacion1;
    private BigDecimal importeBonificado1;
    private BigDecimal valorContratado1;
    private BigDecimal valorAPagar1;
    private BigDecimal precioAlfabeta1;
    private BigDecimal importeCoberturaUnitario1;
    private BigDecimal importeCargoSocioUnitario1;
    private BigDecimal importeBonificadoUnitario1;
    
    
    private BigDecimal importeCobertura2;
    private BigDecimal importeCargoSocio2;
    private BigDecimal porcentajeCobertura2;
    private BigDecimal porcentajeCargoSocio2;
    private BigDecimal valorFacturado2;
    private BigDecimal porcentajeBonificacion2;
    private BigDecimal importeBonificado2;
    private BigDecimal valorContratado2;
    private BigDecimal valorAPagar2;
    private BigDecimal precioAlfabeta2;
    private BigDecimal importeCoberturaUnitario2;
    private BigDecimal importeCargoSocioUnitario2;
    private BigDecimal importeBonificadoUnitario2;
    

    private BigDecimal importeCobertura3;
    private BigDecimal importeCargoSocio3;
    private BigDecimal porcentajeCobertura3;
    private BigDecimal porcentajeCargoSocio3;
    private BigDecimal valorFacturado3;
    private BigDecimal porcentajeBonificacion3;
    private BigDecimal importeBonificado3;
    private BigDecimal valorContratado3;
    private BigDecimal valorAPagar3;
    private BigDecimal precioAlfabeta3;
    private BigDecimal importeCoberturaUnitario3;
    private BigDecimal importeCargoSocioUnitario3;
    private BigDecimal importeBonificadoUnitario3;
    

    private Boolean bloqueada = Boolean.FALSE;

    @Field(offset = 651, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCobertura1() {
        return importeCobertura1;
    }

    public void setImporteCobertura1(BigDecimal importeCobertura1) {
        this.importeCobertura1 = importeCobertura1;
    }

    @Field(offset = 658, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocio1() {
        return importeCargoSocio1;
    }

    public void setImporteCargoSocio1(BigDecimal importeCargoSocio1) {
        this.importeCargoSocio1 = importeCargoSocio1;
    }

    @Field(offset = 665, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCobertura1() {
        return porcentajeCobertura1;
    }

    public void setPorcentajeCobertura1(BigDecimal porcentajeCobertura1) {
        this.porcentajeCobertura1 = porcentajeCobertura1;
    }

    @Field(offset = 670, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCargoSocio1() {
        return porcentajeCargoSocio1;
    }

    public void setPorcentajeCargoSocio1(BigDecimal porcentajeCargoSocio1) {
        this.porcentajeCargoSocio1 = porcentajeCargoSocio1;
    }

    @Field(offset = 675, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorFacturado1() {
        return valorFacturado1;
    }

    public void setValorFacturado1(BigDecimal valorFacturado1) {
        this.valorFacturado1 = valorFacturado1;
    }

    @Field(offset = 682, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeBonificacion1() {
        return porcentajeBonificacion1;
    }

    public void setPorcentajeBonificacion1(BigDecimal porcentajeBonificacion1) {
        this.porcentajeBonificacion1 = porcentajeBonificacion1;
    }

    @Field(offset = 687, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificado1() {
        return importeBonificado1;
    }

    public void setImporteBonificado1(BigDecimal importeBonificado1) {
        this.importeBonificado1 = importeBonificado1;
    }

    @Field(offset = 694, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorContratado1() {
        return valorContratado1;
    }

    public void setValorContratado1(BigDecimal valorContratado1) {
        this.valorContratado1 = valorContratado1;
    }

    @Field(offset = 701, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorAPagar1() {
        return valorAPagar1;
    }

    public void setValorAPagar1(BigDecimal valorAPagar1) {
        this.valorAPagar1 = valorAPagar1;
    }

    @Field(offset = 708, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecioAlfabeta1() {
        return precioAlfabeta1;
    }

    public void setPrecioAlfabeta1(BigDecimal precioAlfabeta1) {
        this.precioAlfabeta1 = precioAlfabeta1;
    }

    
    @Field(offset = 715, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCoberturaUnitario1() {
        return importeCoberturaUnitario1;
    }

    public void setImporteCoberturaUnitario1(BigDecimal importeCoberturaUnitario1) {
        this.importeCoberturaUnitario1 = importeCoberturaUnitario1;
    }

    
    @Field(offset = 721, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocioUnitario1() {
        return importeCargoSocioUnitario1;
    }

    public void setImporteCargoSocioUnitario1(BigDecimal importeCargoSocioUnitario1) {
        this.importeCargoSocioUnitario1 = importeCargoSocioUnitario1;
    }

    @Field(offset = 727, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificadoUnitario1() {
        return importeBonificadoUnitario1;
    }

    public void setImporteBonificadoUnitario1(BigDecimal importeBonificadoUnitario1) {
        this.importeBonificadoUnitario1 = importeBonificadoUnitario1;
    }

    
    
    @Field(offset = 733, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCobertura2() {
        return importeCobertura2;
    }

    public void setImporteCobertura2(BigDecimal importeCobertura2) {
        this.importeCobertura2 = importeCobertura2;
    }

    @Field(offset = 740, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocio2() {
        return importeCargoSocio2;
    }

    public void setImporteCargoSocio2(BigDecimal importeCargoSocio2) {
        this.importeCargoSocio2 = importeCargoSocio2;
    }

    @Field(offset = 747, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCobertura2() {
        return porcentajeCobertura2;
    }

    public void setPorcentajeCobertura2(BigDecimal porcentajeCobertura2) {
        this.porcentajeCobertura2 = porcentajeCobertura2;
    }

    @Field(offset = 752, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCargoSocio2() {
        return porcentajeCargoSocio2;
    }

    public void setPorcentajeCargoSocio2(BigDecimal porcentajeCargoSocio2) {
        this.porcentajeCargoSocio2 = porcentajeCargoSocio2;
    }

    @Field(offset = 757, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorFacturado2() {
        return valorFacturado2;
    }

    public void setValorFacturado2(BigDecimal valorFacturado2) {
        this.valorFacturado2 = valorFacturado2;
    }

    @Field(offset = 764, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeBonificacion2() {
        return porcentajeBonificacion2;
    }

    public void setPorcentajeBonificacion2(BigDecimal porcentajeBonificacion2) {
        this.porcentajeBonificacion2 = porcentajeBonificacion2;
    }

    @Field(offset = 769, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificado2() {
        return importeBonificado2;
    }

    public void setImporteBonificado2(BigDecimal importeBonificado2) {
        this.importeBonificado2 = importeBonificado2;
    }

    @Field(offset = 776, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorContratado2() {
        return valorContratado2;
    }

    public void setValorContratado2(BigDecimal valorContratado2) {
        this.valorContratado2 = valorContratado2;
    }

    @Field(offset = 783, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorAPagar2() {
        return valorAPagar2;
    }

    public void setValorAPagar2(BigDecimal valorAPagar2) {
        this.valorAPagar2 = valorAPagar2;
    }

    @Field(offset = 790, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecioAlfabeta2() {
        return precioAlfabeta2;
    }

    public void setPrecioAlfabeta2(BigDecimal precioAlfabeta2) {
        this.precioAlfabeta2 = precioAlfabeta2;
    }

    @Field(offset = 797, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCoberturaUnitario2() {
        return importeCoberturaUnitario2;
    }

    public void setImporteCoberturaUnitario2(BigDecimal importeCoberturaUnitario2) {
        this.importeCoberturaUnitario2 = importeCoberturaUnitario2;
    }

    @Field(offset = 803, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocioUnitario2() {
        return importeCargoSocioUnitario2;
    }

    public void setImporteCargoSocioUnitario2(BigDecimal importeCargoSocioUnitario2) {
        this.importeCargoSocioUnitario2 = importeCargoSocioUnitario2;
    }

    @Field(offset = 809, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificadoUnitario2() {
        return importeBonificadoUnitario2;
    }

    public void setImporteBonificadoUnitario2(BigDecimal importeBonificadoUnitario2) {
        this.importeBonificadoUnitario2 = importeBonificadoUnitario2;
    }

    
    
    
    @Field(offset = 815, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCobertura3() {
        return importeCobertura3;
    }

    public void setImporteCobertura3(BigDecimal importeCobertura3) {
        this.importeCobertura3 = importeCobertura3;
    }

    @Field(offset = 822, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocio3() {
        return importeCargoSocio3;
    }

    public void setImporteCargoSocio3(BigDecimal importeCargoSocio3) {
        this.importeCargoSocio3 = importeCargoSocio3;
    }

    @Field(offset = 829, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCobertura3() {
        return porcentajeCobertura3;
    }

    public void setPorcentajeCobertura3(BigDecimal porcentajeCobertura3) {
        this.porcentajeCobertura3 = porcentajeCobertura3;
    }

    @Field(offset = 834, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeCargoSocio3() {
        return porcentajeCargoSocio3;
    }

    public void setPorcentajeCargoSocio3(BigDecimal porcentajeCargoSocio3) {
        this.porcentajeCargoSocio3 = porcentajeCargoSocio3;
    }

    @Field(offset = 839, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorFacturado3() {
        return valorFacturado3;
    }

    public void setValorFacturado3(BigDecimal valorFacturado3) {
        this.valorFacturado3 = valorFacturado3;
    }

    @Field(offset = 846, length = 5, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPorcentajeBonificacion3() {
        return porcentajeBonificacion3;
    }

    public void setPorcentajeBonificacion3(BigDecimal porcentajeBonificacion3) {
        this.porcentajeBonificacion3 = porcentajeBonificacion3;
    }

    @Field(offset = 851, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificado3() {
        return importeBonificado3;
    }

    public void setImporteBonificado3(BigDecimal importeBonificado3) {
        this.importeBonificado3 = importeBonificado3;
    }

    @Field(offset = 858, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorContratado3() {
        return valorContratado3;
    }

    public void setValorContratado3(BigDecimal valorContratado3) {
        this.valorContratado3 = valorContratado3;
    }

    @Field(offset = 865, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getValorAPagar3() {
        return valorAPagar3;
    }

    public void setValorAPagar3(BigDecimal valorAPagar3) {
        this.valorAPagar3 = valorAPagar3;
    }

    @Field(offset = 872, length = 7, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecioAlfabeta3() {
        return precioAlfabeta3;
    }

    public void setPrecioAlfabeta3(BigDecimal precioAlfabeta3) {
        this.precioAlfabeta3 = precioAlfabeta3;
    }


    @Field(offset = 879, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCoberturaUnitario3() {
        return importeCoberturaUnitario3;
    }
    
    public void setImporteCoberturaUnitario3(BigDecimal importeCoberturaUnitario3) {
        this.importeCoberturaUnitario3 = importeCoberturaUnitario3;
    }
    
    @Field(offset = 885, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteCargoSocioUnitario3() {
        return importeCargoSocioUnitario3;
    }
    
    public void setImporteCargoSocioUnitario3(BigDecimal importeCargoSocioUnitario3) {
        this.importeCargoSocioUnitario3 = importeCargoSocioUnitario3;
    }
    
    
    @Field(offset = 891, length = 6, align = Align.RIGHT, formatter = ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getImporteBonificadoUnitario3() {
        return importeBonificadoUnitario3;
    }
    
    public void setImporteBonificadoUnitario3(BigDecimal importeBonificadoUnitario3) {
        this.importeBonificadoUnitario3 = importeBonificadoUnitario3;
    }
    

    
    @Field(offset = 897, length = 1, formatter = BooleanFormatter.class)
    @FixedFormatBoolean(trueValue = "1", falseValue = "0")
    public Boolean getBloqueada() {
        return bloqueada;
    }
    
    
   
    public void setBloqueada(Boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    public boolean isValidoMedicamento1() {
        boolean valid = true;
        if (this.precioAlfabeta1 == null || CERO.equals(this.precioAlfabeta1) || CERO_COMA_CEROCERO.equals(this.precioAlfabeta1)) {
            valid = false;
        }
        return valid;
    }

    public boolean isValidoMedicamento2() {
        boolean valid = true;
        if (this.precioAlfabeta2 == null || CERO.equals(this.precioAlfabeta2) || CERO_COMA_CEROCERO.equals(this.precioAlfabeta2)) {
            valid = false;
        }
        return valid;
    }

    public boolean isValidoMedicamento3() {
        boolean valid = true;
        if (this.precioAlfabeta3 == null || CERO.equals(this.precioAlfabeta3) || CERO_COMA_CEROCERO.equals(this.precioAlfabeta3)) {
            valid = false;
        }
        return valid;
    }

}
