package ar.com.osde.centroValidador.pos.mq;

import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * MDP que no hace nada. Es para descartar los mensajes
 * @author VA27789605
 *
 */
public class NoProcessPosMDP implements MessageListener {

    public void onMessage(Message message) {}
}