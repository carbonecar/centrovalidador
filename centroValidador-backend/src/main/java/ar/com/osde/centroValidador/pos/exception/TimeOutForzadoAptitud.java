package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;

public class TimeOutForzadoAptitud extends TimeOutForzadoRulesException {


    private static final long serialVersionUID = 1L;

    public TimeOutForzadoAptitud(String string, RespuestaCentroValidador respuesta) {
        super(string,respuesta);
    }

    public TimeOutForzadoAptitud(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TimeOutForzadoAptitud(String arg0) {
        super(arg0);
    }

    public TimeOutForzadoAptitud(Throwable arg0) {
        super(arg0);
    }


}
