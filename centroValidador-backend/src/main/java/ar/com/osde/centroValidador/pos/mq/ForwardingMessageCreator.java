package ar.com.osde.centroValidador.pos.mq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.MessageCreator;

public class ForwardingMessageCreator implements MessageCreator {

	private Message message;

	public ForwardingMessageCreator(Message message) {
		this.message = message;
	}

	public Message createMessage(Session paramSession) throws JMSException {
		return message;
	}

}
