package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Modela las transacciones de tipo 4
 * @author MT27789605
 *
 */
@Record
public class PosMessageResponseExclusion extends PosMessageResponse {

}

