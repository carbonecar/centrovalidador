package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class PosMessageResponseConsulta extends PosMessageResponse {

    private String nombreArchivoPublicado;

    public PosMessageResponseConsulta() {
        super();
    }

    @Field(offset = 251, length = 60, align = Align.LEFT, paddingChar = ' ')
    public String getNombreArchivoPublicado() {
        return nombreArchivoPublicado;
    }

    public void setNombreArchivoPublicado(String nombreArchivoPublicado) {
        this.nombreArchivoPublicado = nombreArchivoPublicado;
    }

}
