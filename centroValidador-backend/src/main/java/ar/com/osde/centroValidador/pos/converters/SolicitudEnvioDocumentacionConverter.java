package ar.com.osde.centroValidador.pos.converters;

import org.dozer.CustomConverter;

import ar.com.osde.centroValidador.pos.ProtocoloPosMessage.DelegacionNumero;
import ar.com.osde.entities.aptoServicio.InformacionAutorizacion;
import ar.com.osde.entities.aptoServicio.InformacionTransaccion;
import ar.com.osde.entities.aptoServicio.SolicitudEnvioDocumentacion;

public class SolicitudEnvioDocumentacionConverter implements CustomConverter {

    public Object convert(Object paramObject1, Object paramObject2, Class<?> paramClass1, Class<?> paramClass2) {

        SolicitudEnvioDocumentacion solicitudEnvioDocumentacion = new SolicitudEnvioDocumentacion();
        DelegacionNumero delegacionNumero = (DelegacionNumero) paramObject2;
        if (delegacionNumero.representaAutorizacion()) {
            solicitudEnvioDocumentacion = new InformacionAutorizacion();
        }

        if (delegacionNumero.representaTransaccion()) {
            solicitudEnvioDocumentacion = new InformacionTransaccion();
        }

        return solicitudEnvioDocumentacion;
    }

}
