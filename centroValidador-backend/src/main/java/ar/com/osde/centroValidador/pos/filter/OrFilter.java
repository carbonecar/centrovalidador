package ar.com.osde.centroValidador.pos.filter;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.cv.pos.utils.ExceptionFactory;

/**
 * Si alguno de los filtros no arroja excepcion es valido.
 * 
 * @author VA27789605
 * 
 */
public class OrFilter extends CompositeFilter {

    public void validate(SocioPosMessageBase posMessage) {

        StringBuilder messageBuilder = new StringBuilder("30");
        int exceptionCount = 0;

        for (PosMessageFilter filter : getFiltros()) {
            try {
                filter.validate(posMessage);
            } catch (FilteredMessageException e) {
                messageBuilder.append(e.getMessage());
                exceptionCount++;
            }
        }

        // Si todos los filtros fallan, entonces es invalido.
        boolean esValido = getFiltros().size() == exceptionCount;

        ExceptionFactory.createFilteredMessage(esValido, messageBuilder.toString());
    }
}
