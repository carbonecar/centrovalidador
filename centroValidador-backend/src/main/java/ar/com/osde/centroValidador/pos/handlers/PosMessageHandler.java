package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.Message;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;

import java.util.List;

/**
 * Interfaz com�n para procesar los mensajes
 * @author MT27789605
 *
 */
public interface PosMessageHandler {

    /**
     * Procsamiento del mensaje recibido por MQ
     * @param message
     * @return
     * @throws PosMessageHandleException
     */
    RespuestaCentroValidador process(Message message) throws PosMessageHandleException;
    
    /**
     * Devuevle una lista de las clases que participan en la jerarquia
     * @return
     */
    String getHierarchyName();

    boolean procesaMensaje();

    /**
     * Lista de los filtros del mensaje
     * @return
     */
    List<PosMessageFilter> getMessageFilters();
}
