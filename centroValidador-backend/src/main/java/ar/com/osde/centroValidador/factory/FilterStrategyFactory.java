package ar.com.osde.centroValidador.factory;

import javax.jms.ConnectionFactory;

import ar.com.osde.centroValidador.pos.filter.estrategy.FilterStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.NullFilterStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.RouteFilterStrategy;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Clase Factory que permite crear FilterStrategy.
 * @deprecated// no sirve para nada esta clase. Toda la logica fue migrada a HandlerFactory.
 */
public class FilterStrategyFactory {

    public static FilterStrategy createFilterStrategy(FilterStrategyConfiguration filterStrategyConfiguration,
            ConnectionFactory connectionFactory) throws BusinessException {

        String className = filterStrategyConfiguration.getClass().getName();

        if (FilterStrategyConfiguration.class.getName().equals(className)) {
            return new NullFilterStrategy();
        } else if (RouteFilterStrategyConfiguration.class.getName().equals(className)) {
            return createRouteStrategy((RouteFilterStrategyConfiguration) filterStrategyConfiguration, connectionFactory);
        } else if (ProcessFilterStrategyConfiguration.class.getName().equals(className)) {
            return createProcessStrategy((ProcessFilterStrategyConfiguration) filterStrategyConfiguration, connectionFactory);
        }

        throw new BusinessException("No se puede crear el filter strategy.");
    }

    private static FilterStrategy createProcessStrategy(ProcessFilterStrategyConfiguration configuration, 
                                                        ConnectionFactory connectionFactory) throws BusinessException {
        
//        ProcessStrategy processStrategy = new ProcessStrategy();
//        processStrategy.setPosMessageHandler(HandlerFactory.createHandler(configuration.getHandlerConfiguration(), connectionFactory));
//
//        return processStrategy;
        return null;
    }

    private static FilterStrategy createRouteStrategy(RouteFilterStrategyConfiguration filterStrategyConfiguration, ConnectionFactory connectionFactory) {
        RouteFilterStrategy routeStrategy = new RouteFilterStrategy();
        routeStrategy.setTemplateResponse(UtilFactory.createJmsTemplate(filterStrategyConfiguration.getQueueName(), connectionFactory));

        return routeStrategy;
    }
}
