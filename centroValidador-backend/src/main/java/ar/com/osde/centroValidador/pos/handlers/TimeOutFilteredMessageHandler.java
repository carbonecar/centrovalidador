package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;

public class TimeOutFilteredMessageHandler extends FilteredMessageHandler {


    protected String getStrMessage(Message message) {
        try {
            this.getStatitstics().addTimeOut();
            String stringMessage = ((TextMessage) message).getText();
            TimeOutMessageParser timeOutMessageParser=new TimeOutMessageParser();
            String request=timeOutMessageParser.parseRequest(stringMessage);
            return request;
        } catch (JMSException e1) {
            throw new UnsupportedPosMessageException(e1.getMessage());
        }
    }
}
