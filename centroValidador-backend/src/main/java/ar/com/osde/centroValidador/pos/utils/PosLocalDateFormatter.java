package ar.com.osde.centroValidador.pos.utils;

import org.joda.time.LocalDate;

import ar.com.osde.comunes.utils.LocalDateFormatter;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;

public class PosLocalDateFormatter extends LocalDateFormatter {

	@Override
	public LocalDate asObject(String string, FormatInstructions instructions)
			throws FixedFormatException {
		if (PosUtils.isNullString(string)) {
			return null;
		}
		
		return super.asObject(string, instructions);
	}

}
