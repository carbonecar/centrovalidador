package ar.com.osde.centroValidador.factory.filter.configuration;

import ar.com.osde.centroValidador.pos.filter.PropertyEqualFilter;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

import java.util.LinkedList;

/**
 * 
 * @author MT27789605
 * 
 */
public class EqualsFilterConfigurationFactory extends AbstractFilterConfigurationFactory {

    /**
     * 
     */
    public FilterConfiguration createFilter(LinkedList<String> expresion) throws ServiceException {
        PropertyEqualFilterConfiguration filter = (PropertyEqualFilterConfiguration) super.createFilter(expresion);

        String value = expresion.poll();
        String propertyName = expresion.poll();
        if (FilterUtils.esUnOperador(value) || FilterUtils.esUnOperador(propertyName) || value == null || propertyName == null)
            throw new ServiceException("La propiedad == necesita un valor a derecha e izquierda. Por favor verifique la expresion.");
        try {
            Integer.parseInt(value);
            filter.setType("Integer");
        } catch (NumberFormatException e) {
            filter.setType("String");
        }
        filter.setPropertyName(propertyName);
        filter.setValue(value);
        return filter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#getRealClassName()
     */
    @Override
    protected String getRealClassName() {
        return PropertyEqualFilter.class.getName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#createRealFilterConfiguration()
     */
    @Override
    protected FilterConfiguration createRealFilterConfiguration() {
        return new PropertyEqualFilterConfiguration();
    }
}
