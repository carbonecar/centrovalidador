package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.rules.paralelo.NivelValidez;
import ar.com.osde.rules.paralelo.Response;
import ar.com.osde.rules.paralelo.ResponseMatcher;
import ar.com.osde.rules.paralelo.ResponseRecord;

/**
 * Logger para dejar el resultado en formato de registro
 * 
 * @author VA27789605
 * 
 */
public class LoggerMessageRecordHandler extends PosMessageHandlerInterceptor {
	public static final Logger LOG = Logger.getLogger(LoggerMessageRecordHandler.class);
    public static final String CRITICAL_LOG = "critical-logger";

	public static final String SEPARADOR = ",";
	public static final String MY_NAME = LoggerMessageRecordHandler.class.getSimpleName();
	public static final String FIELD_BOOKMARK = " # ";

	long inicioTotal = 0;
	long endTotal = 0;

	public RespuestaCentroValidador process(Message message) throws UnsupportedPosMessageException {

		RespuestaCentroValidador report = null;

		inicioTotal = System.currentTimeMillis();
		try {
			report = getIntercepted().process(message);
			endTotal = System.currentTimeMillis();
		} catch (RuntimeException e) {
			Logger.getLogger(CRITICAL_LOG).info(e);
			throw e;
		} finally {
			report.processLoguer(this);

		}
		return report;

	}

	public String getHierarchyName() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("LoggerMessageRecordHandler\n");
		strBuff.append("\t").append(this.getIntercepted().getHierarchyName());
		return strBuff.toString();
	}

	public List<PosMessageFilter> getMessageFilters() {
		List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
		messageFilters.addAll(this.getIntercepted().getMessageFilters());
		return messageFilters;

	}

	public void logMe(RespuestaCentroValidador report) {

		if (report != null) {
			long timeTotal = endTotal - inicioTotal;
			long timeMsTotal = timeTotal;
			StringBuffer recordRow = new StringBuffer();

			long initLocal = System.currentTimeMillis();
			// codigo de transaccion.
			// Time utilizado para esta transaccion
			recordRow.append(timeMsTotal).append(SEPARADOR);

			LOG.info(SEPARADOR + recordRow.toString());

			report.addHandlerTime(new HandlerTime(MY_NAME, System.currentTimeMillis() - initLocal));
		}
	}

	public void logMe(StatisticalNewSystemResponse report) {
		logResponse(report.getResponseMatcher());

	}

	public void logMe(StatisticalOldSystemResponse report) {
		logResponse(report.getResponseMatcher());
	}

	private void logResponse(ResponseMatcher report) {
		if (report != null && report.isWasMatched()) {
			StringBuffer loggerLine = new StringBuffer(8192);

			DateTimeFormatter parser = ISODateTimeFormat.dateTime();
			String dateTime = parser.print(new DateTime());
			loggerLine.append("ts=" + dateTime);

			ResponseRecord responseRecord = report.getResponseRecord();
			loggerLine.append(FIELD_BOOKMARK).append("request=").append(responseRecord.getRequest())
			        .append(FIELD_BOOKMARK)
			        .append("response-" + responseRecord.getFirstResponse().getSystemName() + "=")
			        .append(responseRecord.getFirstResponse().getTextResponse()).append(FIELD_BOOKMARK)
			        .append("response-" + responseRecord.getSecondResponse().getSystemName() + "=")
			        .append(responseRecord.getSecondResponse().getTextResponse()).append(FIELD_BOOKMARK)
			        .append(this.createComparisonVector(report));
			Response responseSS = responseRecord.getResonseBySystemName(ResponseRecord.SystemName.SALUD_SOFT);
			loggerLine.append(FIELD_BOOKMARK).append("tiempo-").append(responseSS.getSystemName()).append("=")
			        .append(responseSS.getResponseTime());
			LOG.info(loggerLine);
		}
	}

	private String createComparisonVector(ResponseMatcher responseMatcher) {
		String comparsionVector = new String();
		if (NivelValidez.VALIDAS.equals(responseMatcher.getNivelValidez())) {
			comparsionVector = "f1=1".concat(FIELD_BOOKMARK).concat("f2=0").concat(FIELD_BOOKMARK).concat("f3=0")
			        .concat(FIELD_BOOKMARK).concat("f4=0").concat(FIELD_BOOKMARK).concat("f5=0");
		}

		if (NivelValidez.DIFERENCIA_APTITUD.equals(responseMatcher.getNivelValidez())) {
			comparsionVector = "f1=0".concat(FIELD_BOOKMARK).concat("f2=1").concat(FIELD_BOOKMARK).concat("f3=0")
			        .concat(FIELD_BOOKMARK).concat("f4=0").concat(FIELD_BOOKMARK).concat("f5=0");
		}

		if (NivelValidez.DIFERENCIA_COBERTURA.equals(responseMatcher.getNivelValidez())) {
			comparsionVector = "f1=0".concat(FIELD_BOOKMARK).concat("f2=0").concat(FIELD_BOOKMARK).concat("f3=1")
			        .concat(FIELD_BOOKMARK).concat("f4=0").concat(FIELD_BOOKMARK).concat("f5=0");
		}

		if (NivelValidez.VALIDAS_CON_DIFERENCIA_N1.equals(responseMatcher.getNivelValidez())) {
			comparsionVector = "f1=0".concat(FIELD_BOOKMARK).concat("f2=0").concat(FIELD_BOOKMARK).concat("f3=0")
			        .concat(FIELD_BOOKMARK).concat("f4=1").concat(FIELD_BOOKMARK).concat("f5=0");
		}

		if (NivelValidez.VALIDAS_CON_DIFERENCIA.equals(responseMatcher.getNivelValidez())) {
			comparsionVector = "f1=0".concat(FIELD_BOOKMARK).concat("f2=0").concat(FIELD_BOOKMARK).concat("f3=0")
			        .concat(FIELD_BOOKMARK).concat("f4=0").concat(FIELD_BOOKMARK).concat("f5=1");
		}

		return comparsionVector;
	}
}
