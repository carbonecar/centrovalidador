package ar.com.osde.centroValidador.pos.handlers;

import java.util.List;

import javax.jms.Message;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.TimeOutResponsePosMessage;
import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.RespuestaTransaccionParcial;
import ar.com.osde.cv.model.SequentialMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionTimeOutPos;
import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.MensajePos;

/**
 * Handler para el timeout.
 * 
 * @author MT27789605
 * 
 */
public class TimeOutMessageHandler extends FixedFormatMessageHandler implements TransaccionPosMessageCreator {

    public RespuestaCentroValidador process(Message message) throws PosMessageHandleException {

        RespuestaCentroValidador respuestaCentroValidador = super.process(message);
        return respuestaCentroValidador;

    }

    public AbstractPosMessage createPosMessage(String posMessage) {
        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();
        String request = timeOutMessageParser.parseRequest(posMessage);
        return this.getFormatManager().load(FixedFormatMessageClassFactory.getClassInstance(request), posMessage);
    }

    public AbstractTransaccionPos createTransaccion(AbstractPosMessage posMessage) {
        TimeOutResponsePosMessage timeOutResponsePosMessage = createTimeOutPosMessage(posMessage);
        AbstractTransaccionPos abstractTrasnaccionPos = TransaccionFactory.getInstance().createTransaccion(posMessage,
                this.getBeanMapper(), timeOutResponsePosMessage);
        posMessage.setGeneradorRespuesta(timeOutResponsePosMessage.getGeneradorRespuesta());
        abstractTrasnaccionPos.setGeneradorRespuesta(timeOutResponsePosMessage.getGeneradorRespuesta());
        return abstractTrasnaccionPos;

    }

    @Override
    protected SequentialMessage createTransaccionSequentialMessages(AbstractPosMessage lastPosMessage, List<AbstractPosMessage> messages,
            AbstractTransaccionPos trx) {
        SequentialMessage sequentialMessage = super.createTransaccionSequentialMessages(lastPosMessage, messages, trx);
        TransaccionTimeOutPos trxTimeout = (TransaccionTimeOutPos) trx;
        ((TransaccionTimeOutPos) sequentialMessage.getAbstractTransaccionPos()).getTransaccionTimeOut()
                .setEstadoTransaccionRespondidaTimeOut(trxTimeout.getTransaccionTimeOut().getEstadoTransaccionRespondidaTimeOut());
        return sequentialMessage;
    }

    protected TimeOutResponsePosMessage createTimeOutPosMessage(AbstractPosMessage posMessage) {
        // Aca parseamos la transaccion para la cual se quiere informar el
        // timeout
        String orginalMessage = posMessage.getOriginalMessage();
        FixedPosFormatManager formatManager = new FixedPosFormatManager();
        TimeOutResponsePosMessage timeOutResponsePosMessage = formatManager.load(TimeOutResponsePosMessage.class, orginalMessage);
        return timeOutResponsePosMessage;
    }

    @Override
    protected RespuestaTransaccionParcial createRepuestaTransaccionParcial(RespuestaAptitud respuestaAptitud, MensajePos mensajePos) {
        RespuestaTransaccionParcial respuestaTransaccionParcial = super.createRepuestaTransaccionParcial(respuestaAptitud, mensajePos);
        respuestaTransaccionParcial.setGeneradorRespuesta(93);
        return respuestaTransaccionParcial;
    }

    public String getHierarchyName() {
        return "\tTimeOutMessageHandler\n";
    }

}
