package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Transacciones tipo 4. 
 * See ar.com.osde.centroValidador.model.TransaccionFactory
 * @author MT27789605
 *
 */

//TODO renombrar a anulacion
@Record
public class TipoCuatroPosMessage extends AbstractNumeroTransaccionAnterior {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
