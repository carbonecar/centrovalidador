package ar.com.osde.centroValidador.pos.exception;

/**
 * @see UnsoportedOperationException
 * @author MT27789605
 *
 */
public class UnsupportedTransactionException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UnsupportedTransactionException() {
        super();
    }

    public UnsupportedTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedTransactionException(String message) {
        super(message);
    }

    public UnsupportedTransactionException(Throwable cause) {
        super(cause);
    }
    

}
