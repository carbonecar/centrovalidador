package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.Destination;

import ar.com.osde.centroValidador.pos.jms.TimedMessageCreator;
import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;

public class XMLResponseMessageHandler extends ResponseMessageHandler {

    private MessageCreatorFactory messageCreatorFactory;

    @Override
    protected void enviar(Destination destination, RespuestaCentroValidador respuesta) {

        TimedMessageCreator messageCreator = messageCreatorFactory.createXmlMessageCreator(respuesta, getOperadores(),
                getFormatManager());
        getTemplateResponse().send(destination, messageCreator);
		
		respuesta.addHandlerTime(new HandlerTime(TimedMessageCreator.MY_NAME,
				messageCreator.getTimeUsedToCreateMessage()));
    }

    @Override
    protected void enviar(RespuestaCentroValidador respuesta) {
        TimedMessageCreator messageCreator = messageCreatorFactory.createXmlMessageCreator(respuesta, getOperadores(),
                getFormatManager());
        getTemplateResponse().send(messageCreator);

		respuesta.addHandlerTime(new HandlerTime(TimedMessageCreator.MY_NAME,
				messageCreator.getTimeUsedToCreateMessage()));
    }
    
    
    public String getHierarchyName() {
    	StringBuffer strBuff=new StringBuffer();
    	strBuff.append("XMLResponseMessageHandler\n");
    	return strBuff.toString();
    }
    
    public void setMessageCreatorFactory(MessageCreatorFactory messageCreatorFactory){
        this.messageCreatorFactory=messageCreatorFactory;
    }
}