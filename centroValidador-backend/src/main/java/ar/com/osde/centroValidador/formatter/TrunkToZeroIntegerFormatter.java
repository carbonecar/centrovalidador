package ar.com.osde.centroValidador.formatter;

import org.apache.commons.lang.StringUtils;

import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.IntegerFormatter;

/**
 * 
 * En caso de no ser un numero formatea a un 0 (entero)
 * 
 * @author MT27789605
 * 
 */
public class TrunkToZeroIntegerFormatter extends IntegerFormatter {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.impl.IntegerFormatter#asObject
     * (java.lang.String,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */

    @Override
    public Integer asObject(String string, FormatInstructions instructions) {
        string = string.trim();
        if (StringUtils.isNumeric(string)) {
            return super.asObject(string, instructions);
        } else {
            return 0;
        }
    }
}
