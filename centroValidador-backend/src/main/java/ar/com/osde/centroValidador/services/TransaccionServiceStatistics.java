package ar.com.osde.centroValidador.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import ar.com.osde.centroValidador.pos.handlers.HandlerTime;
import ar.com.osde.cv.comportamiento.NotificadorComportamiento;

@ManagedResource(objectName = "TransaccionServiceStatistics:type=statistics,name=TransaccionServiceStatistics", description = "Estadisticas del Servicio de Orquestacion Aptitud")
public class TransaccionServiceStatistics {

	private Vector<Long> tiempos = new Vector<Long>();
	private Object lock = new Object();

	private long cantTransaccionesRuteadas;
	private long cantTotalTransaccionesAtendidas;
	private long mensajesAgregadosAlMatcher;

	private long cantidadTransacciones;
	private long cantidadTransaccionesMatcheadas;
	private long cantidadTransaccionesValidas;
	private long cantidadTransaccionesConDiferencia;
	private long cantidadTransaccionesDiferenciaHabilitado;
	private long cantidadTransaccionesError;
	private long cantidadTransaccionesTimeOutForzado;

	private long cantidadTransaccionesDiferenciaPorcenajesCobertura;

	private long countAptoSocio;
	private long timeAptoSocio;

	private long endToEndAptitudSocio;
	private long endToEnd;
	private Date fechaReinicioContadores = new Date();

	private Map<String, SegmentationTime> timeSegments = new ConcurrentHashMap<String, SegmentationTime>();
	private int cantidadTransaccionesConDiferenciaN1;
	private long cantidadTotalTransaccionesAtendidasMigracion;
	private long cantTotalTransaccionesAtendidasTimeOut;

	private int cantNullMessage;
	/**
	 * Cantidad de timeout que se recibieron.
	 */
	private int cantTimeOut;
	private int messageNotSupportedError;
	private int cantErrorMDR;
	private long cantTransaccionesLongMenor2048;

	private NotificadorComportamiento notificador;
	private int unsupportedTransaccion;
	private int soapFault;
	
	private int nullMessageHandlerCounter;

	public NotificadorComportamiento getNotificador() {
		return notificador;
	}

	public void setNotificador(NotificadorComportamiento notificador) {
		this.notificador = notificador;
	}

	public int getCantTimeOut() {
		return cantTimeOut;
	}

	@ManagedAttribute(description = "Cant. Msj que proceso el handler")
	public int getNullMessageHandlerCounter() {
		return nullMessageHandlerCounter;
	}

	@ManagedAttribute(description = "Cant. Mensajes que dieron error en el motor de reglas")
	public int getCantErrorMDR() {
		return cantErrorMDR;
	}

	@ManagedAttribute(description = "Cantidad de mensajes nulos que envia el operador")
	public int getCantNullMessage() {
		return cantNullMessage;
	}

	@ManagedAttribute(description = "Cantidad de mensajes que no son TextMessage que envia el operador")
	public int getMessageNotSupportedError() {
		return messageNotSupportedError;
	}

	@ManagedAttribute(description = "Cantidad de mensajes que no son TextMessage que envia el operador")
	public long getCantTransaccionesLongMenor2048() {
		return cantTransaccionesLongMenor2048;
	}

	public synchronized void updateCantTransaccionesLongMenor2048(TextMessage textMessage) {
		try {
			if (textMessage.getText().length() < 2048) {
				this.cantTransaccionesLongMenor2048++;
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public Map<String, SegmentationTime> getTimeSegments() {
		return timeSegments;
	}

	public void setTimeSegments(Map<String, SegmentationTime> timeSegments) {
		this.timeSegments = timeSegments;
	}

	public TransaccionServiceStatistics() {

	}

	public synchronized void updateHandlersTime(List<HandlerTime> handlersTime) {
		for (HandlerTime handlerTime : handlersTime) {
			updateHandlerTime(handlerTime);
		}

	}

	public synchronized void updateHandlerTime(HandlerTime handlerTime) {
		SegmentationTime segmentationTime = timeSegments.get(handlerTime.getName());
		if (segmentationTime == null) {
			// TODO loggear un warning
		} else {
			segmentationTime.append(handlerTime.getTimeMS());
		}
	}

	@ManagedAttribute(description = "Cantidad transacciones No soportadas")
	public int getUnsupportedTransaccion() {
		return unsupportedTransaccion;
	}

	@ManagedAttribute(description = "Fecha de reinicio de los contadores")
	public Date getFechaReinicioContadores() {
		return this.fechaReinicioContadores;
	}

	@ManagedAttribute(description = "Cantidad total de llamadas al aptoSocio")
	public long getCountAptoSocio() {
		return countAptoSocio;
	}

	@ManagedAttribute(description = "Tiempo que toma el AptitudService.aptoSocio")
	public long getTimeAptoSocio() {
		return timeAptoSocio;
	}

	@ManagedAttribute(description = "Cantidad total de Atendidas")
	public long getACantTotalTransaccionesAtendidas() {
		return this.cantTotalTransaccionesAtendidas;
	}

	@ManagedAttribute(description = "Cantidad total de Atendidas Migracion")
	public long getCantidadTotalTransaccionesAtendidasMigracion() {
		return cantidadTotalTransaccionesAtendidasMigracion;
	}

	@ManagedAttribute(description = "Cantidad total de Atendidas Timeout")
	public long getCantTotalTransaccionesAtendidasTimeOut() {
		return cantTotalTransaccionesAtendidasTimeOut;
	}

	@ManagedAttribute(description = "Cantidad total de AtendidasTimeOut")
	public void updateCantTotalTransaccionesAtendidasTimeOut() {
		this.cantTotalTransaccionesAtendidasTimeOut++;

	}

	@ManagedAttribute(description = "Cantidad total de Atendidas Migracion")
	public void updateCantTotalTransaccionesAtendidasMigracion() {
		this.cantidadTotalTransaccionesAtendidasMigracion++;

	}

	@ManagedAttribute(description = "Cantidad total de mensajes agregadas para realizar el matecheo (debe ser el doble de las identicas+diferencia")
	public long getBMensajesAgregadasAlMatcher() {
		return mensajesAgregadosAlMatcher;
	}

	@ManagedAttribute(description = "cantidad total de transacciones que ingresaron al sistema")
	public long getCantidadTransacciones() {
		return this.cantidadTransacciones;
	}

	@ManagedAttribute(description = "Cantidad total de transacciones que se compararon")
	public long getCCantidadTransaccionesMatcheadas() {
		return cantidadTransaccionesMatcheadas;
	}

	@ManagedAttribute(description = "Cantidad de transacciones que al matchear dieron iguales. VALIDAS. Si matchean como iguales se determina que son VALIDAS. Esto implica que son identicas desde:  0-44 de la 164 a la 239 (incluyendo advertencias) y de la 393 a la 623 ")
	public long getDCantidadTransaccionesValidas() {
		return cantidadTransaccionesValidas;
	}

	@ManagedAttribute(description = "Cantidad de transacciones que al matchear como iguales dieron diferentes")
	public long getECantidadTransaccionesValidasConDiferencia() {
		return cantidadTransaccionesConDiferencia;
	}

	@ManagedAttribute(description = "Cantidad de transacciones que al matechar dieron diferentes pero esta identificada dicha diferencia y se valida como aceptable. VALIDAS_CON_DIFERENCIA_N1. Si dieron VALIDAS pero solo difirieron en las advertencias el nivel de validez es VALIDAS_CON_DIFERENCIA_N1")
	public long getDDCantidadTransaccionesValidasConDiferenciaN1() {
		return cantidadTransaccionesConDiferenciaN1;
	}

	@ManagedAttribute(description = "Cantidad de transacciones con errores en los porcejantes de cobertura. Solo aplica para aquellas transacciones que validan coberturas. Son transacciones que solo dieron igual en el apto y difirieron en las coberturas")
	public long getFCantidadTransaccionesDifCobertura() {
		return cantidadTransaccionesDiferenciaPorcenajesCobertura;
	}

	@ManagedAttribute(description = "Cantidad de transacciones que respondieron habilitado en forma diferente")
	public long getGCantidadTransaccionesDiferenciaAptitud() {
		return cantidadTransaccionesDiferenciaHabilitado;
	}

	@ManagedAttribute(description = "Cantidad de transacciones donde no se contesta porque algun servicio dio timeout")
	public long getHCantidadTransaccionesTimeOutForzado() {
		return cantidadTransaccionesTimeOutForzado;
	}

	@ManagedAttribute(description = "Cantidad de transacciones donde se dio un error ")
	public long getICantidadTransaccionesError() {
		return cantidadTransaccionesError;
	}

	@ManagedAttribute(description = "Tiempo total involucrado desde la recepcion hasta el procesamiento (EndToEndAptoSocio)")
	public long getEndToendAptoSocio() {
		return endToEndAptitudSocio;
	}

	@ManagedAttribute(description = "Tiempo total involucrado desde la recepcion hasta el procesamiento (EndToEnd)")
	public long getEndToEnd() {
		return endToEnd;
	}

	@ManagedAttribute(description = "Cantidad de transacciones que ruteadas hacia otra cola")
	public long getCantTransaccionesRuteadas() {
		return cantTransaccionesRuteadas;
	}

	@ManagedAttribute(description = "Cantidad de transacciones son FAULT")
	public int getSoapFault() {
		return soapFault;
	}

	public void addTime(long time) {
		synchronized (lock) {
			this.tiempos.add(time);
		}
	}

	/**
	 * Actualiza el tiempo de punta a punto.
	 * 
	 * @param endToEnd
	 */
	public synchronized void updateTimeEndToEnd(long endToEnd) {
		this.endToEnd = endToEnd;

	}

	public synchronized void updateCantTotalTransaccionesAtendidas() {
		this.cantTotalTransaccionesAtendidas++;
	}

	public synchronized void updateTimeEndToEndAptoSocio(long endToEndAptoSocio) {
		this.endToEndAptitudSocio = endToEndAptoSocio;
	}

	public synchronized void updateTimeAptoSocio(long timeAptoSocio) {
		this.timeAptoSocio = timeAptoSocio;
	}

	public synchronized void addMensajeToBeMatched() {
		this.mensajesAgregadosAlMatcher++;
	}

	public synchronized void addTransaccionIdentica() {
		this.cantidadTransaccionesValidas++;
	}

	public synchronized void addTransaccion() {
		this.cantidadTransacciones++;
	}

	public synchronized void addTransaccionesRuteadas() {
		this.cantTransaccionesRuteadas++;
	}

	public synchronized void addTransaccionDiferencia() {
		this.cantidadTransaccionesConDiferencia++;

	}

	public synchronized void addTransaccionDiferenciaN1() {
		this.cantidadTransaccionesConDiferenciaN1++;

	}

	public synchronized void addTransaccionDiferenciaHabilitado() {
		this.cantidadTransaccionesDiferenciaHabilitado++;
	}

	public synchronized void addTimeoutForzado(List<String> serviciosTimeout) {
		this.cantidadTransaccionesTimeOutForzado++;
		this.notificador.notificar(this, serviciosTimeout);
	}

	public synchronized void addSoapFault() {
		this.soapFault++;
		this.getNotificador().notificar(this);

	}

	public synchronized void addErrorMDR(boolean hasErrorMDR) {
		if (hasErrorMDR) {
			this.cantErrorMDR++;
			this.getNotificador().notificar(this);
		}
	}

	public synchronized void addError() {
		this.cantidadTransaccionesError++;

	}

	public synchronized void addDiferenciaPorcenajesCobertura() {
		this.cantidadTransaccionesDiferenciaPorcenajesCobertura++;
	}

	@ManagedOperation
	public void reiniciarContadores() {
		synchronized (this) {
			this.cantidadTransaccionesConDiferencia = 0;
			this.cantidadTransaccionesValidas = 0;
			this.mensajesAgregadosAlMatcher = 0;
			this.countAptoSocio = 0;
			this.cantidadTransaccionesDiferenciaHabilitado = 0;
			this.cantidadTransaccionesError = 0;
			this.cantidadTransaccionesTimeOutForzado = 0;
			this.fechaReinicioContadores = new Date();
			this.cantTotalTransaccionesAtendidas = 0;
			this.cantTransaccionesRuteadas = 0;
			this.cantidadTransaccionesMatcheadas = 0;
			this.cantidadTransaccionesDiferenciaPorcenajesCobertura = 0;
			this.cantidadTransaccionesConDiferenciaN1 = 0;
			this.cantidadTotalTransaccionesAtendidasMigracion = 0;
			this.cantTotalTransaccionesAtendidasTimeOut = 0;
			this.cantNullMessage = 0;
			this.messageNotSupportedError = 0;
			this.cantTimeOut = 0;
			this.unsupportedTransaccion = 0;
			this.soapFault = 0;
			this.cantErrorMDR = 0;
			this.cantidadTransacciones=0;
			this.nullMessageHandlerCounter=0;

		}
	}

	@ManagedOperation
	public void reiniciarTodosContadores() {
		this.reiniciarContadores();
		synchronized (this) {
			for (SegmentationTime sgTime : this.timeSegments.values()) {
				sgTime.resetCounters();
			}
		}
	}

	public synchronized void updateCantidadTransaccionesMatcheadas() {
		this.cantidadTransaccionesMatcheadas++;
	}

	public synchronized void addNullMessage() {
		this.cantNullMessage++;
	}

	public synchronized void addTimeOut() {
		this.cantTimeOut++;

	}

	public synchronized void addMessageNotSupportedError() {
		this.messageNotSupportedError++;
	}

	public synchronized void addUnsupportedTransaccion() {
		this.unsupportedTransaccion++;

	}

	public void addNullMessageHandlerCounter() {
		this.nullMessageHandlerCounter++;
		
	}

}
