package ar.com.osde.centroValidador.services;

import java.util.Date;

import org.springframework.jmx.export.annotation.ManagedResource;

import ar.com.osde.centroValidador.services.MBeanSegmentation;

/**
 * Clase para segmentar los valores
 */
@ManagedResource(objectName = "SegmentationTime:type=statistics,name=SegmentationTime", description = "Estadisticas segmentadas por tiempo")
public class SegmentationTime implements MBeanSegmentation {

    /**
     * Nombre de quien estamos calculando las estadísticas.
     */
    private String name;

    private int countUnder5MS = 0;
    private int countbetween5aAnd10MS = 0;
    private int countbetween10aAnd20MS = 0;
    private int countbetween20aAnd30MS = 0;
    private int countbetween30aAnd40MS = 0;
    private int countbetween40aAnd50MS = 0;
    private int countbetween50And100MS = 0;
    private int countbetween100And200MS = 0;

    private int countbetween200And400MS = 0;
    private int countbetween400And600MS = 0;
    private int countbetween600And800MS = 0;
    private int countbetween800And1000MS = 0;
    private int countbetween1000And1200MS = 0;
    private int countbetween1200nd1800MS = 0;
    private int countbetween1800And2200MS = 0;
    private int countbetween2200And2700MS = 0;
    private int countbetween2700And3500MS = 0;
    private int countbetween3500And5000MS = 0;
    private int countbetween5000And6000MS = 0;
    private int countbetween6000And7500MS = 0;
    private int moreThan7500MS = 0;

    private Date fechaInicio = new Date();

    private int maximo = 0;

    private long time=0;
   

    public void resetCounters() {
        synchronized (this) {
            countUnder5MS = 0;
            countbetween5aAnd10MS = 0;
            countbetween10aAnd20MS = 0;
            countbetween20aAnd30MS = 0;
            countbetween30aAnd40MS = 0;
            countbetween40aAnd50MS = 0;
            countbetween50And100MS = 0;
            countbetween100And200MS = 0;
            countbetween200And400MS = 0;
            countbetween400And600MS = 0;
            countbetween600And800MS = 0;
            countbetween800And1000MS = 0;
            countbetween1000And1200MS = 0;
            countbetween1200nd1800MS = 0;
            countbetween1800And2200MS = 0;
            countbetween2200And2700MS = 0;
            countbetween2700And3500MS = 0;
            countbetween3500And5000MS = 0;
            countbetween5000And6000MS = 0;
            countbetween6000And7500MS = 0;
            moreThan7500MS = 0;
            maximo = 0;
            this.fechaInicio = new Date();
        }
    }

    public int getACountUnder5MS() {
        return countUnder5MS;
    }
    
    public int getBCountbetween5aAnd10MS() {
        return countbetween5aAnd10MS;
    }

    public int getCountbetween10aAnd20MS() {
        return countbetween10aAnd20MS;
    }

    public int getDCountbetween20aAnd30MS() {
        return countbetween20aAnd30MS;
    }

    public int getECountbetween30aAnd40MS() {
        return countbetween30aAnd40MS;
    }

    public int getFCountbetween40aAnd50MS() {
        return countbetween40aAnd50MS;
    }

    public int getGCountbetween50And100MS() {
        return countbetween50And100MS;
    }

    public int getHCountbetween100And200MS() {
        return countbetween100And200MS;
    }

   
   
    public int getICountbetween200And400MS() {
        return countbetween200And400MS;
    }

    public int getJCountbetween400And600MS() {
        return countbetween400And600MS;
    }

    public int getKCountbetween600And800MS() {
        return countbetween600And800MS;
    }

    public int getLCountbetween800And1000MS() {
        return countbetween800And1000MS;
    }

    public int getMCountbetween1000And1200MS() {
        return countbetween1000And1200MS;
    }

    public int getNCountbetween1200nd1800MS() {
        return countbetween1200nd1800MS;
    }

    public int getOCountbetween1800And2200MS() {
        return countbetween1800And2200MS;
    }

    public int getPCountbetween2200And2700MS() {
        return countbetween2200And2700MS;
    }

    public int getQCountbetween2700And3500MS() {
        return countbetween2700And3500MS;
    }

    public int getRCountbetween3500And5000MS() {
        return countbetween3500And5000MS;
    }

    public int getSCountbetween5000And6000MS() {
        return countbetween5000And6000MS;
    }

    public int getTCountbetween6000And7500MS() {
        return countbetween6000And7500MS;
    }

    public int getUMoreThan7500MS() {
        return moreThan7500MS;
    }

    
    public SegmentationTime() {

    }

    public SegmentationTime(String name) {
        this.name = name;
    }

    public Date getXAFechaInicio() {
        return this.fechaInicio;
    }
    
    public int getXBMaximo() {
        return maximo;
    }

    public String getXCName() {
        return name;
    }

    public long XDgetLastTime() {
        return time;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.services.MBeanSegmentation#append(int)
     */
    public synchronized void append(int time) {
        // Calendar cal = Calendar.getInstance();
        // cal.setTime(this.fechaInicio);
        // cal.add(Calendar.HOUR_OF_DAY, 1);
        // if (cal.getTime().before(Calendar.getInstance().getTime())) {
        // this.resetCounters();
        // }
        this.time=time;
        if (maximo < time) {
            maximo = time;
        }

        if (time < 5) {
            countUnder5MS++;
            return;
        }

        if (time <= 10) {
            countbetween5aAnd10MS++;
            return;
        }
        if (time <= 20) {
            countbetween10aAnd20MS++;
            return;
        }
        if (time <= 30) {
            countbetween20aAnd30MS++;
            return;
        }

        if (time <= 40) {
            countbetween30aAnd40MS++;
            return;
        }

        if (time <= 50) {
            countbetween40aAnd50MS++;
            return;
        }
        if (time <= 100) {
            countbetween50And100MS++;
            return;
        }

        if (time <= 200) {
            countbetween100And200MS++;
            return;
        }
        if (time <= 400) {
            countbetween200And400MS++;
            return;
        }

        if (time <= 600) {
            countbetween400And600MS++;
            return;
        }

        if (time <= 600) {
            countbetween600And800MS++;
            return;
        }
        if (time <= 800) {
            countbetween800And1000MS++;
            return;
        }
        if (time <= 1200) {
            countbetween1000And1200MS++;
            return;
        }
        if (time <= 1800) {
            countbetween1200nd1800MS++;
            return;
        }
        if (time <= 2200) {
            countbetween1800And2200MS++;
            return;
        }
        if (time <= 2700) {
            countbetween2200And2700MS++;
            return;
        }
        if (time <= 3500) {
            countbetween2700And3500MS++;
            return;
        }
        if (time <= 5000) {
            countbetween3500And5000MS++;
            return;
        }
        if (time <= 6000) {
            countbetween5000And6000MS++;
            return;
        }
        if (time <= 7500) {
            countbetween6000And7500MS++;
            return;
        }
        moreThan7500MS++;

    }

}
