package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;


/**
 * Excepcion que ocurre cuando un servicio de timeout arroja alguna falla.
 */
public class TimeOutRulesException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private RespuestaCentroValidador respuestaCentroValidador;
    
    
    public RespuestaCentroValidador getRespuestaCentroValidador() {
        return respuestaCentroValidador;
    }

    public void setRespuestaCentroValidador(RespuestaCentroValidador respuestaCentroValidador) {
        this.respuestaCentroValidador = respuestaCentroValidador;
    }

    public TimeOutRulesException(String string, RespuestaCentroValidador respuesta) {
        this(string);
        this.respuestaCentroValidador=respuesta;
    }

    public TimeOutRulesException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TimeOutRulesException(String arg0) {
        super(arg0);
    }

    public TimeOutRulesException(Throwable arg0) {
        super(arg0);
    }
}
