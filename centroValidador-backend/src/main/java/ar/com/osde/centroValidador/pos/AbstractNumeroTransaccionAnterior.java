package ar.com.osde.centroValidador.pos;

import java.util.Date;

import ar.com.osde.centroValidador.pos.utils.PosDateFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;

/**
 * Clase basa para parsear las transacciones que tiene una transaccion de
 * referencia
 * 
 * @author MT27789605
 * 
 */
public abstract class AbstractNumeroTransaccionAnterior extends AbstractPosMessage {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // una tx 2Lo 4A, o una 2P donde el operador (que es delegacion) no viene
    // informado
    private int numeroTransaccionReferencia;

    private String operadorTransaccionReferencia;

    private Date fechaTransaccionReferencia;

    @Field(offset = 76, length = 6)
    public int getNumeroTransaccionReferencia() {
        return numeroTransaccionReferencia;
    }

    public void setNumeroTransaccionReferencia(int p) {
        this.numeroTransaccionReferencia = p;
    }

    // AUNREF Esta valor se conviente en un nro de transaccion en el caso de ser

    @Field(offset = 158, length = 8, formatter = PosDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public Date getFechaTransaccionReferencia() {
        return fechaTransaccionReferencia;
    }

    public void setFechaTransaccionReferencia(Date fechaTransaccionReferencia) {
        this.fechaTransaccionReferencia = fechaTransaccionReferencia;
    }

    @Field(offset = 74, length = 2)
    public String getOperadorTransaccionReferencia() {
        return operadorTransaccionReferencia;
    }

    public void setOperadorTransaccionReferencia(String operadorTransaccionReferencia) {
        this.operadorTransaccionReferencia = operadorTransaccionReferencia;
    }

}
