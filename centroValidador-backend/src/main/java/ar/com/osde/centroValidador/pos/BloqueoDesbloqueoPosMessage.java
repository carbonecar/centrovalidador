package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase "Holder" para el parseo de los mensajes
 * 
 * @author MT27789605
 * 
 */
@Record
public class BloqueoDesbloqueoPosMessage extends AbstractNumeroTransaccionAnterior {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
