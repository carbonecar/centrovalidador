package ar.com.osde.centroValidador.pos.jms;

import org.springframework.jms.core.MessageCreator;

public interface StringMessageCreator extends MessageCreator {
	public String createMessageResponse();

}
