package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;

public class SimpleNewSystemResponseHandler implements PosMessageHandler {

	private static final Log LOG = LogFactory.getLog(SimpleNewSystemResponseHandler.class);

	public RespuestaCentroValidador process(javax.jms.Message message) throws PosMessageHandleException {
		StatisticalNewSystemResponse respuestaCV = new StatisticalNewSystemResponse(null);
		try {
			String mensajeTxt = ((TextMessage) message).getText();
			respuestaCV.setResponseString(mensajeTxt);
		} catch (JMSException e) {
			LOG.error(e);
		}
		return respuestaCV;
	}

	public String getHierarchyName() {
		return "SimpleNewSystemResponseHandler";
	}

	public boolean procesaMensaje() {
		return false;
	}

	public List<PosMessageFilter> getMessageFilters() {
		return new ArrayList<PosMessageFilter>();
	}
}
