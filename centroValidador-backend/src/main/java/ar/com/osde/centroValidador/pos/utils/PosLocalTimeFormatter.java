package ar.com.osde.centroValidador.pos.utils;

import org.joda.time.LocalTime;

import ar.com.osde.comunes.utils.LocalTimeFormatter;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;

public class PosLocalTimeFormatter extends LocalTimeFormatter {

	@Override
	public LocalTime asObject(String string, FormatInstructions instructions)
			throws FixedFormatException {
		if (PosUtils.isNullString(string)) {
			return null;
		}
		
		return super.asObject(string, instructions);
	}

}
