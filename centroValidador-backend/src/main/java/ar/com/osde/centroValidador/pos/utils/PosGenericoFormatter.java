package ar.com.osde.centroValidador.pos.utils;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.StringFormatter;

public class PosGenericoFormatter extends StringFormatter {


    public static final String TRUE="1";
    public static final String FALSE="0";
    public String asObject(String segment, FormatInstructions instructions)
            throws FixedFormatException {
        if (PosUtils.isNullString(segment)) {
            return FALSE;
        }
        
        try {
            segment=segment.trim();
            if("".equals(segment)){
                segment=FALSE;
            }
            return segment;
            
        } catch (Exception e) {
            throw new FixedFormatException("Could not parse value[" + segment
                    + "] as a boolean.", e);
        }
    }

    
    
}
