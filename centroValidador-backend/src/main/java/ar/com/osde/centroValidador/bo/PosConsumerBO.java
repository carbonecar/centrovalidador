package ar.com.osde.centroValidador.bo;
/**
 * interfaz para determinar si esta levantado o no un listener
 * @author MT27789605
 *
 */
public interface PosConsumerBO {
	
    /**
     * true si esta levantado
     * @return
     */
	public boolean isStarted();

	
	public boolean switchConsumer();
	
}
