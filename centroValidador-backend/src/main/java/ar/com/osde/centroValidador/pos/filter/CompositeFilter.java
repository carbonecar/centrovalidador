package ar.com.osde.centroValidador.pos.filter;

import java.util.LinkedList;
import java.util.List;

public abstract class CompositeFilter implements PosMessageFilter {

    private List<PosMessageFilter> filtros = new LinkedList<PosMessageFilter>();

    public void addFilter(PosMessageFilter messageFilter) {
        getFiltros().add(messageFilter);
    }

    public List<PosMessageFilter> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<PosMessageFilter> filters) {
        this.filtros = filters;
    }

    public boolean implementaPropiedadValor(String propertyName, Object value) {
        for (PosMessageFilter filter : this.filtros) {
            if (filter.implementaPropiedadValor(propertyName, value))
                return true;
        }
        return false;
    }

    public boolean evaluaPropiedad(String propertyName) {
        for (PosMessageFilter filter : this.filtros) {
            if (filter.evaluaPropiedad(propertyName))
                return true;
        }
        return false;
    }
}
