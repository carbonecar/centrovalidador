package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Message;

import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;

public class NullMessageHandler implements PosMessageHandler {

	
    private transient TransaccionServiceStatistics statistics;	
    
    public NullMessageHandler(){
    }
	public RespuestaCentroValidador process(Message message)
			throws PosMessageHandleException {
		if(statistics!=null) //TODO sacar esta cochinada
			statistics.addNullMessageHandlerCounter();
		return new RespuestaCentroValidador(null);
	}

	public String getHierarchyName() {
		return "\tNullMessageHandler\n";
	}

    public boolean procesaMensaje() {
        return false;
    }

    public List<PosMessageFilter> getMessageFilters() {
        return new ArrayList<PosMessageFilter>();
    }
    
    
    public void setStatistics(TransaccionServiceStatistics statistics){
    	this.statistics=statistics;
    }
}