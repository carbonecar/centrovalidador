package ar.com.osde.centroValidador.formatter;

import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.LongFormatter;

/**
 * Formater para pasar a un long quitantdo los espacios
 * 
 * @author MT27789605
 * 
 */
public class LongFormatterTrimmed extends LongFormatter {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ancientprogramming.fixedformat4j.format.impl.LongFormatter#asObject
     * (java.lang.String,
     * com.ancientprogramming.fixedformat4j.format.FormatInstructions)
     */
    @Override
    public Long asObject(String string, FormatInstructions instructions) {
        string = string.trim();
        return super.asObject(string, instructions);
    }
}
