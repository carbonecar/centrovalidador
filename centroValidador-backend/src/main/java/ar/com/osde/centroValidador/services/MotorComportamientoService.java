package ar.com.osde.centroValidador.services;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ar.com.osde.framework.services.Service;

//TODO mover esta interface al war de services
@WebService(name = "MotorComportamientoService", targetNamespace = "http://osde.com.ar/services/MotorComportamientoService")
public interface MotorComportamientoService  extends Service{

    @WebMethod
    public void iniciarMotor(@WebParam(name = "fechaDesde", header = false)Date fechaDesde);
    
}
