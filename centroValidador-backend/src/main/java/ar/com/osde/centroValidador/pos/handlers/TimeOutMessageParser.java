package ar.com.osde.centroValidador.pos.handlers;

public class TimeOutMessageParser {

    public static final int INICIO_REQUEST = 1;
    //La respuesta que dio el operador  esta en el rango 174 a 193 inclusive
    public static final int INICIO_RESPONSE = 174;
    public static final int FIN_RESPONSE = 193;

    public String parseResponse(String message) {
        return message.substring(INICIO_RESPONSE - 1, FIN_RESPONSE - 1);
    }

    public String parseRequest(String message) {

        String requestFirst = message.substring(INICIO_REQUEST - 1, INICIO_RESPONSE-1);
        String requestSecond = message.substring(FIN_RESPONSE, message.length());

        return "     ".concat(requestFirst.concat(requestSecond));
    }

}
