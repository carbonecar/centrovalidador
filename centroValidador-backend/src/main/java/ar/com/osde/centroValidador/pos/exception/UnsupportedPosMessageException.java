package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.SocioPosMessageBase;

public class UnsupportedPosMessageException extends PosMessageHandleException {
	private static final long serialVersionUID = 20100512L;

	
	public UnsupportedPosMessageException(UnsupportedPosMessageException e){
		super(e);
	}
	public UnsupportedPosMessageException(String string) {
		super(string);
	}

	public UnsupportedPosMessageException(String message,
	        SocioPosMessageBase posMessage) {
		super(message, posMessage);
	}

	public UnsupportedPosMessageException(Throwable cause,
	        SocioPosMessageBase posMessage) {
		super(cause, posMessage);
	}

	public UnsupportedPosMessageException(String message, Throwable cause,
			PosMessage posMessage) {

		super(message, cause, posMessage);
	}

}
