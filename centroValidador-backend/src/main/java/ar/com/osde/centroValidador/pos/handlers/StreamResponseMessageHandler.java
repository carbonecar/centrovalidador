package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jms.Destination;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;

import ar.com.osde.centroValidador.pos.exception.TimeOutRulesException;
import ar.com.osde.centroValidador.pos.jms.TimedMessageCreator;
import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;
import ar.com.osde.cv.pos.utils.ExceptionFactory;
import ar.com.osde.entities.aptitud.ObservacionDetalle;
import ar.com.osde.entities.transaccion.MessagePosBuilder;
import ar.com.osde.transaccionService.InconsistenciasFactory;

/**
 * Handler para los registros fijos. Crea la respuesta y la envia.
 * 
 * @author VA27789605
 * 
 */
public class StreamResponseMessageHandler extends ResponseMessageHandler {
    public static final String MY_NAME = StreamResponseMessageHandler.class.getSimpleName();
    /**
     * Todos estos codigos deben ser configurables por pantalla TODO. ESTAN EN
     * LA BASE DE DATOS. SACAR ESTO DE ACA
     */
    public static final int CODIGO_PERSISTENCIA = 10001;
    public static final int VALIDATION_ERROR = 6002;
    public static final int CONTROL_FACTURACION = 4000;
    public static final int AUTORIZACIONES_SERVICE = 4001;
    public static final int AUTORIZACION_SERVICE_FINALIZAR = 4004;

    public static final int TRASCODIFICACION = 4003;

    private MessageCreatorFactory messageCreatorFactory;

    @Override
    protected void enviar(Destination destination, RespuestaCentroValidador respuesta) {
        enviarMensaje(destination, respuesta);
    }

    protected void enviar(RespuestaCentroValidador respuesta) {
        enviarMensaje(null, respuesta);

    }

    public String getHierarchyName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("StreamResponseMessageHandler\n");
        strBuff.append("\t").append(this.getIntercepted().getHierarchyName());
        return strBuff.toString();
    }

    public void setMessageCreatorFactory(MessageCreatorFactory messageCreatorFactory) {
        this.messageCreatorFactory = messageCreatorFactory;

    }

    // ***********************************************//
    // ************Impl. Interna****************//
    /**
     * Lista de codigos para los cuales no se responde
     * */
    private ArrayList<Integer> buildListCodigosExceptuar() {
        ArrayList<Integer> codigosExceptuar = new ArrayList<Integer>();
        codigosExceptuar.add(new Integer(InconsistenciasFactory.CODIGO_TIMEOUT));
        codigosExceptuar.add(new Integer(InconsistenciasFactory.MDR_ERROR_CODE));
        codigosExceptuar.add(new Integer(InconsistenciasFactory.ERROR_APTITUDSERVICE_CODE));
        codigosExceptuar.add(new Integer(InconsistenciasFactory.ERROR_PERSISTENCIA));
        return codigosExceptuar;
    }

    // ***********************************************//
    private void enviarMensaje(Destination destination, RespuestaCentroValidador respuesta) {
        TimedMessageCreator messageCreator = messageCreatorFactory.createStreamMessageCreator(respuesta, getOperadores(),
                getFormatManager());

        ArrayList<Integer> codigosExceptuar = buildListCodigosExceptuar();
        List<ObservacionDetalle> advExceptuarEncontradas = findAdvertencias(respuesta, codigosExceptuar);
        List<ObservacionDetalle> observaciones = MessagePosBuilder.collectObservaciones(respuesta.getDecoratee().getRespuestaAptitud());
        boolean hasTrxDuplicadaError = false;
        List<TimeOutRulesException> timeOutExceptions = new ArrayList<TimeOutRulesException>();
        for (Iterator<ObservacionDetalle> iterator = observaciones.iterator(); iterator.hasNext();) {
            ObservacionDetalle observacionDetalle = iterator.next();
            timeOutExceptions.add(ExceptionFactory.createTimeOutExceptionByErrorAptitudCode(observacionDetalle, respuesta));
            if (InconsistenciasFactory.ERROR_TRX_REPETIDA_CODE == observacionDetalle.getCodigo()) {
                hasTrxDuplicadaError = true;
            }

        }
        // TODO revisar esta l�gica. Es incorrecto no mandar el mensaje de
        // duplicada si hay advertencias.
        if (advExceptuarEncontradas.isEmpty() || hasTrxDuplicadaError) {
            getTemplateResponse().send(messageCreator);

        }
        
        for (Iterator<TimeOutRulesException> iterator = timeOutExceptions.iterator(); iterator.hasNext();) {
            TimeOutRulesException timeOutRulesException = iterator.next();
            ExceptionFactory.throwsOnCondition(!hasTrxDuplicadaError, timeOutRulesException);
        }

        respuesta.addHandlerTime(new HandlerTime(TimedMessageCreator.MY_NAME, messageCreator.getTimeUsedToCreateMessage()));
    }

    public static List<ObservacionDetalle> findAdvertencias(RespuestaCentroValidador respuesta,
            final List<Integer> codigosObservacionDetalles) {
        return (List<ObservacionDetalle>) CollectionUtils.select(
                MessagePosBuilder.collectObservaciones(respuesta.getDecoratee().getRespuestaAptitud()),
                new Predicate<ObservacionDetalle>() {
                    public boolean evaluate(ObservacionDetalle adv) {
                        boolean tieneObservacion = false;
                        for (Integer codigoObservacionDetalle : codigosObservacionDetalles) {
                            tieneObservacion = tieneObservacion || (adv.getCodigo() == codigoObservacionDetalle.intValue());
                        }
                        return tieneObservacion;
                    }
                });
    }

}
