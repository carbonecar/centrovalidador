package ar.com.osde.centroValidador.bo;

import java.util.List;

import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.framework.business.base.BusinessObject;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Bo para manejar al container. Mantiene actualizado los container en un
 * repositorio de persistencia permanente (RPM) y en memoria. Permite detener e
 * iniciar un container.
 * 
 * @author MT27789605
 * 
 */
public interface ContainerBo extends BusinessObject {

    /**
     * Guarda con container en un repositorio de presistencia permanente
     * 
     * @param containerConfiguration
     * @throws BusinessException
     */
    public void saveContainer(ContainerConfiguration containerConfiguration) throws BusinessException;

    /**
     * Actualiza un container en un repositorio de persistencia permanente
     * 
     * @param container
     * @throws BusinessException
     */
    public void updateContainer(ContainerConfiguration container) throws BusinessException;

    /**
     * Actualiza el container solamente en memoria
     * 
     * @param container
     * @throws BusinessException
     */
    public void updateContainerOnMemory(ContainerConfiguration container) throws BusinessException;

    /**
     * Recupera todos los container que hay, tanto en memoria como en el
     * repositorio permanente.
     * 
     * @return
     * @throws BusinessException
     */
    public List<ContainerConfiguration> getAllContainers() throws BusinessException;

    /**
     * Recupera todos los container que hay, tanto en memoria como en el
     * repositorio permanente que cumplen la condicion de pertenecer a @groupName
     * 
     * @param groupName
     * @return
     * @throws BusinessException
     */
    public List<ContainerConfiguration> getAllContainersWithGroupName(String groupName) throws BusinessException;

    /**
     * Elimina los container tanto de memoria como del repositorio de
     * persitencia permanente
     * 
     * @param container
     * @throws BusinessException
     */
    public void deleteContainer(ContainerConfiguration container) throws BusinessException;

    /**
     * Inicia el container para consumir los mensajes. Actualiza el estado en el
     * repositorio de persistencia permanente
     * 
     * @param container
     * @throws BusinessException
     */
    public void startContainer(ContainerConfiguration container) throws BusinessException;

    /**
     * 
     * Detiene el container actualizando el estado en el RPM
     * @param container
     * @throws BusinessException
     */
    public void stopContainer(ContainerConfiguration container) throws BusinessException;

    /**
     * Recarga los container con el estado en que se encuentran en la base de datos
     * @param containersId
     */
    public void realoadContainers(List<Long> containersId);

    /**
     * Recupera la configuracion de un container solo
     * @param id
     * @return
     * @throws BusinessException
     */
    public ContainerConfiguration getById(long id) throws BusinessException;

    /**
     * Devuelve la configuracion del container al que pertenece el handler que se pasa por parámetro
     * @param idHandler
     * @return
     */
    public ContainerConfiguration getParentContainer(long idHandler);
}
