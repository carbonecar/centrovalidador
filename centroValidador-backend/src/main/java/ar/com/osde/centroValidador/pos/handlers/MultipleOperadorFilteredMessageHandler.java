package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jms.Message;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;

/**
 * Filtros para multiples operadores. Contiene un mapa cuya key es el operador y
 * value el verdadero filtro que debe aplicar.
 * 
 * @author VA27789605
 * 
 */
public class MultipleOperadorFilteredMessageHandler extends PosMessageHandlerInterceptor {

    private Map<String, FilteredMessageHandler> handlers;

    public RespuestaCentroValidador process(Message message) {

        PosMessage posMessage = loadPosMessage(message);

        return handlers.get(posMessage.getCodigoOperador()).process(message);
    }

    public Map<String, FilteredMessageHandler> getHandlers() {
        return handlers;
    }

    public void setHandlers(Map<String, FilteredMessageHandler> handlers) {
        this.handlers = handlers;
    }
    
    public String getHierarchyName() {
    	StringBuilder strBuilder=new StringBuilder(50);
    	strBuilder.append("MultipleOperadorFilteredMessageHandler\n");
    	strBuilder.append("\t").append(super.getHierarchyName());
    	return strBuilder.toString();
    }

    public List<PosMessageFilter> getMessageFilters() {
        List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
        messageFilters.addAll(getIntercepted().getMessageFilters());
        return messageFilters;

    }

}
