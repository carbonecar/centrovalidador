package ar.com.osde.centroValidador.factory.filter.configuration;

import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

import java.util.LinkedList;

/**
 *
 */
public interface IFilterConfigurationFactory {
    public FilterConfiguration createFilter(LinkedList<String> expresion) throws ServiceException;
}
