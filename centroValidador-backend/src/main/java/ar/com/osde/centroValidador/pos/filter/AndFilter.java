package ar.com.osde.centroValidador.pos.filter;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.cv.pos.utils.ExceptionFactory;

/**
 * Valida que se cumplan TODAS las condiciones de los filtros. Si se cumplen
 * todas las condiciones, entonces es un mensaje soportado.
 * 
 * Si alguno de las condiciones no se cumple, entonces es un mensaje invalido y arrjoa una exception
 * 
 * En detalles de implementacion NINGUN filtro debe haber arrojado una
 * excepcion.
 * 
 * @author VA27789605
 * 
 */
public class AndFilter extends CompositeFilter {

    public void validate(SocioPosMessageBase posMessage) {

        StringBuilder messageBuilder = new StringBuilder();
        boolean isValid = false;

        for (PosMessageFilter filtro : getFiltros()) {
            try {
                filtro.validate(posMessage);
            } catch (FilteredMessageException e) {
                isValid = true;
                messageBuilder.append(e.getMessage());
            }
        }

        ExceptionFactory.createFilteredMessage(isValid, messageBuilder.toString());
    }
}
