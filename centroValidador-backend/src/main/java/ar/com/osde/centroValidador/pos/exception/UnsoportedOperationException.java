package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Esta excepcion representa operaciones de migracion que no pueden ser ejecutadas en el nuevo sistema. 
 * @author MT27789605
 *
 */
public class UnsoportedOperationException extends BusinessException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    
    
    

}
