package ar.com.osde.centroValidador.pos.exception;

import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

public class EasyNameSOAPFaultException extends SOAPFaultException {

    private String serviceName;

    public EasyNameSOAPFaultException(SOAPFault fault, String serviceName) {
        super(fault);
        this.serviceName = serviceName;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}
