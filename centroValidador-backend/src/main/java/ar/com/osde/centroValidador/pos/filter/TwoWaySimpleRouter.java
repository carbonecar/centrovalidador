package ar.com.osde.centroValidador.pos.filter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate102;
import org.springframework.jms.core.MessageCreator;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

/**
 * Rueto simple a 2 colas. Parsea el mensaje y decide hacer el ruteo en funcion
 * de las condiciones. TODO: hacer que los destionos sean N tomados por jndi
 * 
 * @author VA27789605
 * 
 */
@Deprecated
public class TwoWaySimpleRouter implements StringMessageHandler {

	private JmsTemplate102 templateOld;
	private JmsTemplate102 templateNew;
	private FixedFormatManager formatManager;
	private PosMessageFilter filter;

	public TwoWaySimpleRouter() {

	}

	public TwoWaySimpleRouter(JmsTemplate102 templateOld,
			JmsTemplate102 templateNew, FixedFormatManager fixedFormatManager) {
		this.templateOld = templateOld;
		this.templateNew = templateNew;
		this.formatManager = fixedFormatManager;
	}

	public JmsTemplate102 getTemplateOld() {
		return templateOld;
	}

	public void setTemplateOld(JmsTemplate102 templateOld) {
		this.templateOld = templateOld;
	}

	public JmsTemplate102 getTemplateNew() {
		return templateNew;
	}

	public void setTemplateNew(JmsTemplate102 templateNew) {
		this.templateNew = templateNew;
	}

	public FixedFormatManager getFormatManager() {
		return formatManager;
	}

	public void setFormatManager(FixedFormatManager formatManager) {
		this.formatManager = formatManager;
	}

	public PosMessageFilter getFilter() {
		return filter;
	}

	public void setFilter(PosMessageFilter filter) {
		this.filter = filter;
	}

	public void handle(final String message) {

		// TODO: cambiar esto cuando este la herencia de mensajes.
		PosMessage posMessage = formatManager.load(PosMessage.class, message);

		try {
			filter.validate(posMessage);
			templateNew.send(new MessageCreator() {
				public Message createMessage(Session paramSession)
						throws JMSException {

					return paramSession.createTextMessage(message);
				}
			});
		} catch (UnsupportedPosMessageException e) {
			System.out.println("ruteando..");
			templateOld.send(new MessageCreator() {
				public Message createMessage(Session paramSession)
						throws JMSException {

					return paramSession.createTextMessage(message);
				}
			});
		}
	}

}
