package ar.com.osde.centroValidador.pos.exception;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.filter.estrategy.RouteFilterStrategy;
import ar.com.osde.centroValidador.pos.handlers.HandlerTime;

public class FilteredMessageException extends PosMessageHandleException {

    private static final long serialVersionUID = 1L;

    private List<HandlerTime> handlersTime=new ArrayList<HandlerTime>();
    public FilteredMessageException(PosMessageHandleException e) {
        super(e);
    }

    public FilteredMessageException(SocioPosMessageBase message) {
        super(message);
    }

    public FilteredMessageException(String string) {
        super(string);
    }
    public FilteredMessageException(String message, SocioPosMessageBase posMessage, List<HandlerTime> handlersTime) {
        super(message, posMessage);
        this.handlersTime.addAll(handlersTime);
    }
    public FilteredMessageException(String message, SocioPosMessageBase posMessage, HandlerTime handlerTime) {
        super(message, posMessage);
        this.handlersTime.add(handlerTime);
    }

    public FilteredMessageException(String message, SocioPosMessageBase posMessage, long filteredTime) {
        super(message, posMessage);
        this.handlersTime.add(new HandlerTime(RouteFilterStrategy.MY_NAME, filteredTime));
    }

    public FilteredMessageException(Throwable cause, SocioPosMessageBase posMessage) {
        super(cause, posMessage);
    }

    public FilteredMessageException(String message, Throwable cause, PosMessage posMessage) {

        super(message, cause, posMessage);
    }

    public List<HandlerTime> getHandlersTime() {
        return handlersTime;
    }

}
