package ar.com.osde.centroValidador.pos;

/**
 * Intefaz comun para todos los mensajes MQ del Pos
 * @author MT27789605
 *
 */
public interface IPosMessage {

    /**
     * Devuelve el mensaje original
     * @return
     */
    public abstract String getOriginalMessage();

    /**
     * Set del mensaje original de MQ
     * @param originalMessage
     */
    public abstract void setOriginalMessage(String originalMessage);

}