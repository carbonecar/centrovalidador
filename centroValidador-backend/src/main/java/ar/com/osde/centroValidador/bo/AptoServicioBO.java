package ar.com.osde.centroValidador.bo;

import ar.com.osde.centroValidador.pos.handlers.HandlerTime;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AnulacionTransaccionPos;
import ar.com.osde.cv.model.BloqueoDesbloqueoTransaccionPos;
import ar.com.osde.cv.model.CierreFarmaciaTransaccionPos;
import ar.com.osde.cv.model.ConsultaTransaccionPos;
import ar.com.osde.cv.model.EnvioDocumentacionTransaccionPos;
import ar.com.osde.cv.model.MarcaGenericoTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionPos;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.cv.model.TransaccionRescatePos;
import ar.com.osde.entities.transaccion.TransaccionIngresoEgreso;
import ar.com.osde.entities.transaccion.TransaccionMigracion;
import ar.com.osde.entities.transaccion.TransaccionTimeOut;
import ar.com.osde.framework.business.base.BusinessObject;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;
import ar.com.osde.transaccionService.services.RespuestaAnulacion;
import ar.com.osde.transaccionService.services.RespuestaBloqueoDesbloqueo;
import ar.com.osde.transaccionService.services.RespuestaConsulta;
import ar.com.osde.transaccionService.services.RespuestaEnvioDocumentacion;
import ar.com.osde.transaccionService.services.RespuestaIngresoEgreso;
import ar.com.osde.transaccionService.services.RespuestaRegistracionConsumos;
import ar.com.osde.transaccionService.services.RespuestaRescate;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;
import ar.com.osde.transaccionService.services.RespuestaValidacionConsumos;

/**
 * BO para la validacion del apto servicio
 * 
 * @author VA27789605
 * 
 */
public interface AptoServicioBO extends BusinessObject {
    static final String APTITUD_SERIVCE_APTO_SOCIO = "APTITUD_SERVICE_APTO_SOCIO";
    static final String APTITUD_SERIVCE_APTO_SERVICIO = "APTITUD_SERVICE_APTO_SERVICIO";
    static final String PRESTADOR_SERVICE_CODIGO_RELACION = "PRESTADOR_SERVICE_CODIGO_RELACION";
    static final String PRESTADOR_SERVICE_PRESTADORES_CUIT_FILIAL = "PRESTADOR_SERVICE_PRESTADOR_CUIT_FILIAL";
    static final String TRANSACCION_SERIVCE_ANULACION = "TRANSACCION_SERVICE_ANULACION";
    static final String TRANSACCION_SERIVCE_CIERRE = "TRANSACCION_SERVICE_CIERRE";
    static final String TRANSACCION_SERVICE_CONSULTA = "TRANSACCION_SERVICE_CONSULTA";
    static final String TRANSACCION_SERVICE_INFORMARMARCAGENERICO = "TRANSACCION_SERVICE_INFORMARMARCAGENERICO";

    static final String END_TO_END_APTOSOCIO = "END_TO_END_APTOSOCIO";
    static final String TRANSACCION_SERIVCE_REGISTRAR_RESCATE = "TRANSACCION_SERVICE_REGISTRAR_RESCATE";
    static final String TRANSACCION_SERIVCE_REGISTRARBLOQUEODESBLOQUEO = "TRANSACCION_SERVICE_REGISTRARBLOQUEODESBLOQUEO";
    static final String TRANSACCION_SERIVCE_INFORMARINGRESOEGRESO = "TRANSACCION_SERVICE_INFORMARINGRESOEGRESO";
    static final String TRANSACCION_SERIVCE_CONSULTA = "TRANSACCION_SERIVCE_CONSULTA";
    static final String TRANSACCION_ENVIO_DOCUMENTACION = "TRANSACCION_SERVICE_ENVIO_DOCUMENTACION";

    static final String TRANSACCION_SERIVCE_REGISTRARTIMEOUT = "TRANSACCION_SERIVCE_REGISTRARTIMEOUT";

    /**
     * Devuelve el o los motivos por los cuales es o no apto el servicio y las
     * prestaciones que se quieren brindar.
     * 
     * @param transaccion
     * @return
     * @throws ServiceException
     */

    HandlerTime completarCodigoRelacion(AbstractTransaccionPos transaccionPos);

    RespuestaValidacionConsumos isAptoServicioValidacionSinPrestador(TransaccionPrestacionPos transaccionPos);

    RespuestaValidacionConsumos isAptoServicioValidacion(TransaccionPrestacionPos transaccionPos);

    RespuestaRegistracionConsumos isAptoServicioRegistracion(TransaccionPrestacionPos transaccionPos);

    RespuestaTransaccion isAptoRegistracionMedicamento(RegistracionMedicamentoMessage transaccion);

    RespuestaTransaccion isAptoSocio(TransaccionPos transaccion);

    RespuestaAnulacion registrarAnulacion(AnulacionTransaccionPos exclusionPos);

    RespuestaTransaccion registrarCierre(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos);

    RespuestaConsulta registrarConsulta(ConsultaTransaccionPos consultaTransaccionPos);

    RespuestaRescate registrarRescate(TransaccionRescatePos transaccionRescatePos);

    RespuestaBloqueoDesbloqueo registrarBloqueoDesbloqueo(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoPosMessage);

    RespuestaIngresoEgreso informarIngresoEgreso(TransaccionIngresoEgreso transaccion);

    RespuestaTransaccion registrarTimeOut(TransaccionTimeOut transaccionTimeOut) throws BusinessException;

    RespuestaTransaccion registrarMigracion(TransaccionMigracion transaccionMigracion) throws BusinessException;

    RespuestaTransaccion informarMarcaGenerico(MarcaGenericoTransaccionPos transaccionMarcaGenerico);

    RespuestaEnvioDocumentacion envioDocumentacion(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion);

}
