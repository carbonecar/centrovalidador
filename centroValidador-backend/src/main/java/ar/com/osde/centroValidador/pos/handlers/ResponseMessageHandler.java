package ar.com.osde.centroValidador.pos.handlers;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.exception.BadFormatException;
import ar.com.osde.centroValidador.pos.exception.TipoAtributoInvalido;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.MensajePos;
import ar.com.osde.entities.transaccion.MessagePosBuilder;
import ar.com.osde.entities.transaccion.RespuestaErrorConsistenciaTransaccion;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

public abstract class ResponseMessageHandler extends AbstractResponseMessageHandler {

    // Mapa de recodificación de operadores. Es mantenido por cuestiones de
    // compatibilidad.
    // cuando el mensaje en formato string ya no se implemente eliminar esta
    // propiedad
    private Map<String, String> operadores = new HashMap<String, String>();

    public static final String MY_NAME = ResponseMessageHandler.class.getSimpleName();
    public static final Log LOG= LogFactory.getLog(ResponseMessageHandler.class);

    public RespuestaCentroValidador process(Message message) throws UnsupportedPosMessageException {

        RespuestaCentroValidador respuesta = null;

        TextMessage textMessage = (TextMessage) message;

        String txtMessageString = null;
        try {
            txtMessageString = textMessage.getText();
        } catch (JMSException e1) {
            LOG.error(e1);
        }
        long init = 0;
        try {
            respuesta = getIntercepted().process(message);
            init = System.currentTimeMillis();
            Destination destination = message.getJMSReplyTo();
            if (destination != null) {
                enviar(destination, respuesta);
            } else {
                enviar(respuesta);
            }
        } catch (BadFormatException be) {
            String textoFallido = be.getParseException().getFailedText();
            int posicionReemplazar = be.getParseException().getFormatContext().getOffset();

            int longitud=be.getParseException().getFormatInstructions().getLength();
            StringBuilder textoCompleto = new StringBuilder(be.getParseException().getCompleteText());

            for(int i=0;i<longitud;i++){
                textoCompleto.setCharAt(posicionReemplazar-1+i, '0');
            }
            
            AbstractPosMessage posMessage = new FixedPosFormatManager().load(
                    FixedFormatMessageClassFactory.getClassInstance(textoCompleto.toString()), textoCompleto.toString());
            RespuestaTransaccion rt = new RespuestaErrorConsistenciaTransaccion();
            rt.setRespuestaAptitud(new RespuestaAptitud());
            MensajePos mensajePos = new MessagePosBuilder(TipoTransaccion.O1A).buildMensajeAtributosInvalidos(be.getCauseMethod(),
                    textoFallido, be.getExpectdTypeTranslated());
            mensajePos.agregarLeyendaImpresora(be.toString());
            rt.setMensajePos(mensajePos);
            respuesta = new RespuestaCentroValidador(rt);
            respuesta.setPosMessage((PosMessage) posMessage);
            enviar(respuesta);

        } catch (TipoAtributoInvalido ex) {
            AbstractPosMessage posMessage = new FixedPosFormatManager().load(
                    FixedFormatMessageClassFactory.getClassInstance(txtMessageString), txtMessageString);
            RespuestaTransaccion rt = new RespuestaErrorConsistenciaTransaccion();
            rt.setRespuestaAptitud(new RespuestaAptitud());

            rt.setMensajePos(new MessagePosBuilder(TipoTransaccion.O1A).buildMensajeAtributoInvalido());
            respuesta = new RespuestaCentroValidador(rt);
            respuesta.setPosMessage((PosMessage) posMessage);
            enviar(respuesta);
        } catch (JMSException e) {
            LOG.error(e);
            throw new UnsupportedPosMessageException("No es posible procesar el mensaje jms: " + e.getMessage());
        }

        respuesta.addHandlerTime(new HandlerTime(MY_NAME, System.currentTimeMillis() - init));
        return respuesta;
    }

    protected abstract void enviar(Destination destination, RespuestaCentroValidador respuesta);

    protected abstract void enviar(RespuestaCentroValidador respuesta);

    public Map<String, String> getOperadores() {
        return operadores;
    }

    public void setOperadores(Map<String, String> operadores) {
        this.operadores = operadores;
    }
}
