package ar.com.osde.centroValidador.pos;

import org.apache.commons.lang.StringUtils;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase para recuperar la transaccion 2P de envio de protocolo quirurgico.
 * 
 * @author MT27789605
 * 
 */
@Record
public class ProtocoloPosMessage extends CredencialPosMessage {

    private static final long serialVersionUID = 1L;

    private String delegacionOrden;

    private int numeroReferencia;

    private String tipoProtocolo;

    private String nombreImagen;

    /**
     * Este valor puede ser o bien delegacion de la orden o bien no va a venir
     * informado
     * 
     * 
     * @return
     */
    @Field(offset = 74, length = 2)
    public String getDelegacionOrden() {
        return delegacionOrden;
    }

    public void setDelegacionOrden(String p) {
        this.delegacionOrden = p;
    }

    /**
     * Puede ser un numero de orden o
     * 
     * @return
     */
    @Field(offset = 76, length = 6)
    public int getNumeroReferencia() {
        return numeroReferencia;
    }

    public void setNumeroReferencia(int p) {
        this.numeroReferencia = p;
    }

    @Field(offset = 150, length = 2)
    public String getTipoProtocolo() {
        return tipoProtocolo;
    }

    public void setTipoProtocolo(String p) {
        this.tipoProtocolo = p;
    }

    @Field(offset = 307, length = 30)
    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public DelegacionNumero getDelegacionNumero() {
        return new DelegacionNumero(delegacionOrden, numeroReferencia);
    }

    /**
     * Clase para encapsular mas de un parametro para determinar el tipo de
     * sublcase de SolicitudEnvioDocumentacion
     * 
     * @author MT27789605
     * 
     */
    public class DelegacionNumero {
        private String delegacion;
        private int numero;

        public DelegacionNumero(String delegacion, int numero) {
            this.delegacion = delegacion;
            this.numero = numero;
        }

        public String getDelegacion() {
            return delegacion;
        }

        public int getNumero() {
            return numero;
        }

        public boolean representaAutorizacion() {
            boolean isAutorizacion = true;
            if (("00".equals(delegacion) || "".equals(delegacion) || delegacion == null) || !StringUtils.isNumeric(delegacion)) {
                isAutorizacion = false;
            }

            return isAutorizacion;
        }

        public boolean representaTransaccion() {
            boolean isTransaccion = true;
            if (!(this.numero != 0 && !representaAutorizacion())) {
                isTransaccion = false;
            }
            return isTransaccion;
        }

    }

}
