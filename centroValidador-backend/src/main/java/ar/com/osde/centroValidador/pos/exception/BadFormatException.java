package ar.com.osde.centroValidador.pos.exception;

import java.util.HashMap;
import java.util.Map;

import com.ancientprogramming.fixedformat4j.format.ParseException;

public class BadFormatException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String causeMethod;
    private String causeText;
    private String expectedType;
    private ParseException parseException;
    private Map<Class<?>, String> namedType;

    private static final String CARACTER = "CARACTER";
    private static final String ENTERO = "ENTERO";
    private static final String DECIMAL = "DECIMAL";

    public BadFormatException() {
        namedType = new HashMap<Class<?>, String>();
        namedType.put(String.class, CARACTER);
        namedType.put(Integer.class, ENTERO);
        namedType.put(Long.class, ENTERO);
        namedType.put(Double.class, DECIMAL);
        namedType.put(int.class, ENTERO);
    }

    public BadFormatException(String causeText, String causeMethod, String expectdType) {
        this();
        this.causeText = causeText;
        this.causeMethod = causeMethod;
        this.expectedType = expectdType;
    }

    public BadFormatException(ParseException parseException) {
        this();
        this.parseException = parseException;
        this.setCauseMethod(parseException.getAnnotatedMethod().getName());
        this.setCauseText(parseException.getFailedText());
        this.setExpectedType(parseException.getAnnotatedMethod().getReturnType().toString());
    }

    public ParseException getParseException() {
        return parseException;
    }

    public String getCauseMethod() {
        return causeMethod;
    }

    public void setCauseMethod(String causeMethod) {
        this.causeMethod = causeMethod;
    }

    public String getCauseText() {
        return causeText;
    }

    public void setCauseText(String causeText) {
        this.causeText = causeText;
    }

    public String getExpectedType() {
        return expectedType;
    }

    public String getExpectdTypeTranslated() {
        return this.namedType.get(this.parseException.getAnnotatedMethod().getReturnType());
    }

    public void setExpectedType(String expectedType) {
        this.expectedType = expectedType;
    }

    @Override
    public String toString() {
        return "Excepcion causada por el texto '" + getCauseText() + "' al tratar de convertirlo en " + getExpectedType()
                + " para el metodo " + getCauseMethod();
    }
}
