package ar.com.osde.centroValidador.factory;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.jms.core.JmsTemplate;

import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Clase Factory util que permite crear un jmsTemplate/Queue de forma simple.
 */
public class UtilFactory {

    public static JmsTemplate createJmsTemplate(String queueName, String ConnectionFactoryName) {

        JmsTemplate jmsTemplate = new JmsTemplate();
        try {
            jmsTemplate.setConnectionFactory(UtilFactory.obtainConnectionFactory(ConnectionFactoryName));
            jmsTemplate.setDefaultDestination(UtilFactory.obtainQueue(queueName));
        } catch (BusinessException e) {
            // En caso de no pododer obtener la cola le seteo el nombre.
            jmsTemplate.setDefaultDestinationName(queueName);
        }

        return jmsTemplate;
    }
    
    
    public static JmsTemplate createJmsTemplate(String queueName, ConnectionFactory connectionFactory) {

        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        try {
            jmsTemplate.setDefaultDestination(UtilFactory.obtainQueue(queueName));
        } catch (BusinessException e) {
            // En caso de no pododer obtener la cola le seteo el nombre.
            jmsTemplate.setDefaultDestinationName(queueName);
        }

        return jmsTemplate;
    }

    public static Queue obtainQueue(String queueName) throws BusinessException {

        Queue queue = null;

        if (queueName != null) {
            try {
                Context context = (Context) new InitialContext().lookup("");
                queue = (Queue) context.lookup(queueName);
            } catch (NamingException e) {
                throw new BusinessException(e.getMessage());
            }
        }

        return queue;
    }

    public static ConnectionFactory obtainConnectionFactory(String connectionFactoryName, String connectionFactoryDefaultName)
            throws BusinessException {
        ConnectionFactory connectionFactory = obtainConnectionFactory(connectionFactoryName);
        if(connectionFactory==null){
            connectionFactory=obtainConnectionFactory(connectionFactoryDefaultName);
        }
        return connectionFactory;
        
    }

    public static ConnectionFactory obtainConnectionFactory(String connectionFactoryName) throws BusinessException {
        ConnectionFactory connectionFactory = null;

        if (connectionFactoryName != null) {
            try {
                connectionFactory = (ConnectionFactory) new InitialContext().lookup(connectionFactoryName);
            } catch (NamingException e) {
                throw new BusinessException(e.getMessage());
            }
        }

        return connectionFactory;
    }
}
