package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase para el parseo de la respuesta MQ a los mensajes de farmacia
 * 
 * @author MT27789605
 * 
 */
@Record
public class PosMessageResponseCierreFarmacia extends PosMessageResponse {

    // AUNICI N�mero de cierre Dec 6 180
    private int numeroCierre;

    @Field(offset = 180, length = 6, align = Align.RIGHT, paddingChar = '0')
    public int getNumeroCierre() {
        return numeroCierre;
    }

    public void setNumeroCierre(int numeroCierre) {
        this.numeroCierre = numeroCierre;
        // TODO hacer el refactor del prefijo del plan
    }

}
