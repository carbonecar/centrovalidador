package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.IPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;

public class PosMessageHandleException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private IPosMessage posMessage;

	public PosMessageHandleException(PosMessageHandleException ex) {
		super(ex);
	}

	public PosMessageHandleException(String string) {
		super(string);
	}

	public PosMessageHandleException(IPosMessage message) {
		super();
		this.posMessage = message;
	}

	public PosMessageHandleException(String message, IPosMessage posMessage) {
		super(message);
		this.posMessage = posMessage;
	}

	public PosMessageHandleException(Throwable cause,
	        IPosMessage posMessage) {
		super(cause);
		this.posMessage = posMessage;
	}

	public PosMessageHandleException(String message, Throwable cause,
			PosMessage posMessage) {
		super(message, cause);
		this.posMessage = posMessage;
	}

	public IPosMessage getPosMessage() {
		return posMessage;
	}

	public void setPosMessage(IPosMessage posMessage) {
		this.posMessage = posMessage;
	}
}
