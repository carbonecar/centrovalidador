package ar.com.osde.centroValidador.pos;

import org.joda.time.LocalDate;

import ar.com.osde.centroValidador.pos.utils.PosLocalDateFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * 
 * @author MT27789605
 * 
 */

@Record
public class ConsultaFarmaciaPosMessage extends AbstractConsultaFarmaciaPosMessage {

    public static final String ACEPTADAS="A";
    public static final String PENDIENTES="P";
    public static final String CIERRE_PARTICULAR="C";
    public static final String ANULADAS="X";
    public static final String BLOQUEADAS="B";
    
    
    private static final long serialVersionUID = 1L;

    // AUMOTA
    private LocalDate fechaDesdeRecuperar;

    // AUTCKS
    private Integer nroCierreRecuperar;

    //A= transacciones aceptadas, T=Todas, P= Transacciones pendientes de cierre,
    //C=un cierre, B=bloqueadas. Si el campo tipo de consulta (AUTICO) contiene una "C" (por cierre), 
    //el campo n�mero de transacci�n / cierre deber� estar indicado.
    
    // AUTICO
    private String tipoConsulta;

    
    @Field(offset = 356, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaDesdeRecuperar() {
        return fechaDesdeRecuperar;
    }

    public void setFechaDesdeRecuperar(LocalDate fechaDesdeRecuperar) {
        this.fechaDesdeRecuperar = fechaDesdeRecuperar;
    }

    @Field(offset = 375, length = 10)
    public Integer getNroCierreRecuperar() {
        return nroCierreRecuperar;
    }

    public void setNroCierreRecuperar(Integer nroCierreRecuperar) {
        this.nroCierreRecuperar = nroCierreRecuperar;
    }

    @Field(offset = 468, length = 1)
    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

}
