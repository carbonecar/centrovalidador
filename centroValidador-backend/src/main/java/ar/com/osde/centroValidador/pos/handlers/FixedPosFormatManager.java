package ar.com.osde.centroValidador.pos.handlers;

import ar.com.osde.centroValidador.pos.IPosMessage;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

public class FixedPosFormatManager implements FixedFormatManager {

    private FixedFormatManager fixedFormatManager = new FixedFormatManagerImpl();
    
    public <T> T load(Class<T> paramClass, String originalMessage) throws FixedFormatException {
        
        String messageToLoad=originalMessage.replace("\0", " ");
        T posMessage = fixedFormatManager.load(paramClass, messageToLoad);
        
        ((IPosMessage)posMessage).setOriginalMessage(originalMessage);

        return posMessage;
    }
    
    public <T> String export(T posMessage) throws FixedFormatException {
        return fixedFormatManager.export(posMessage);
    }

    public <T> String export(String paramString, T paramT) throws FixedFormatException {
        return fixedFormatManager.export(paramT);
    }
}
