package ar.com.osde.centroValidador.services.impl;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ar.com.osde.centroValidador.bo.PosConsumerBO;
import ar.com.osde.centroValidador.services.pos.PosConsumerService;

@WebService(endpointInterface = "ar.com.osde.centroValidador.services.pos.PosConsumerService", serviceName = "PosConsumerService")
public class PosConsumerServiceImpl implements PosConsumerService {
	private PosConsumerBO bo;
	
	@WebMethod
	public boolean isStarted() {
		return bo.isStarted();
	}
	
	@WebMethod
	public boolean switchConsumer() {
		return bo.switchConsumer();
	}

	public PosConsumerBO getBo() {
		return bo;
	}

	public void setBo(PosConsumerBO bo) {
		this.bo = bo;
	}

}
