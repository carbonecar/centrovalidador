package ar.com.osde.centroValidador.pos.mq;


public class MessageIDBuilder {

    public static final String buildMessageID(int nroTransaccion, String codigoOperador) {
        byte[] messageId = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] nroTransaccionBytes = String.valueOf(nroTransaccion).getBytes();

        int posInicio = (6 - nroTransaccionBytes.length < 0) ? 0 : 6 - nroTransaccionBytes.length;

        System.arraycopy(nroTransaccionBytes, 0, messageId, posInicio, nroTransaccionBytes.length - 1);

        if (codigoOperador != null) {
            byte[] operadorArray = codigoOperador.getBytes();
            System.arraycopy(operadorArray, 0, messageId, 6, 2);
        }

        StringBuilder strBuff = new StringBuilder(24);
        strBuff.append(String.format("%1$06d",nroTransaccion));
        strBuff.append(codigoOperador);
        for (int i = strBuff.length(); i < 24; i++) {
            strBuff.append("0");
        }

        return strBuff.toString();
    }
}
