package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.dozer.Mapper;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.exception.BadFormatException;
import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AptoServicioBOVisitor;
import ar.com.osde.cv.model.RespuestaTransaccionParcial;
import ar.com.osde.cv.model.SequentialMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.EstadoTransaccion;
import ar.com.osde.entities.transaccion.MensajePos;
import ar.com.osde.entities.transaccion.TransaccionesPropertyHidder;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.ParseException;

import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

public class FixedFormatMessageHandler implements PosMessageHandler, TransaccionPosMessageCreator {

    private static final String MY_NAME = FixedFormatMessageHandler.class.getSimpleName();
    private static final Logger LOG = Logger.getLogger(FixedFormatMessageHandler.class);
    public static final String WEB_GESTION = "WG";
    private static final long THRESHOLD_TIMEOUT = 5;
    /**
     * Entidad que parsea el string
     */
    private FixedFormatManager formatManager;
    /**
     * Entidad que mapea el string parseado a objetos
     */
    private Mapper beanMapper;
    private AptoServicioBO aptoServicioBO;
    private GenericCRUDDao<AbstractPosMessage> transaccionPrestacionCrud;

    public GenericCRUDDao<AbstractPosMessage> getTransaccionPrestacionCrud() {
        return transaccionPrestacionCrud;
    }

    public void setTransaccionPrestacionCrud(GenericCRUDDao<AbstractPosMessage> transaccionPrestacionCrud) {
        this.transaccionPrestacionCrud = transaccionPrestacionCrud;
    }

    public AptoServicioBO getAptoServicioBO() {
        return aptoServicioBO;
    }

    public void setAptoServicioBO(AptoServicioBO aptoServicioBO) {
        this.aptoServicioBO = aptoServicioBO;
    }

    /**
     * Procesa el mensaje de texto
     */
    public RespuestaCentroValidador process(Message message) throws BadFormatException {
        long inicio = System.currentTimeMillis();
        long end;

        TextMessage textMessage = (TextMessage) message;
        AbstractPosMessage posMessage;

        String stringMessage = null;
        try {
            stringMessage = textMessage.getText();
        } catch (JMSException e) {
            LOG.error(e);
            throw new UnsupportedPosMessageException("No es posible obtener el texto del mensaje jms: " + e.getMessage());
        }

        try {
            posMessage = createPosMessage(stringMessage);
        } catch (ParseException e) {
            LOG.error(e);
            throw new BadFormatException(e);
        }

        LOG.debug("*****************************************************RECIBIENDO MENSAJE: *******************************");
        RespuestaCentroValidador respuesta = null;
        if (posMessage.getNumeroTotalMensajes() > 1) {
            respuesta = this.processSequentialMessege(posMessage);
            end = System.currentTimeMillis();
            LOG.debug("Tiempo utilizado para procesar el mensaje: " + (end - inicio));
            return respuesta;
        } else {
            AbstractTransaccionPos trx = createTransaccion(posMessage);
            end = System.currentTimeMillis();
            respuesta = processTransaction(posMessage, trx);
            LOG.debug("Tiempo utilizado para procesar el mensaje: " + (end - inicio));
            respuesta.addHandlerTime(new HandlerTime(MY_NAME, end - inicio));
            return respuesta;
        }

    }

    public AbstractTransaccionPos createTransaccion(AbstractPosMessage posMessage) {
        return TransaccionFactory.getInstance().createTransaccion(posMessage, beanMapper);
    }

    protected AbstractPosMessage createPosMessage(String posMessage) {
        return formatManager.load(FixedFormatMessageClassFactory.getClassInstance(posMessage), posMessage);
    }

    private RespuestaCentroValidador processTransaction(AbstractPosMessage posMessage, AbstractTransaccionPos trx) {
        LOG.debug("*****************************************PROCESANDO UN MENSAJE **************************** : ");
        RespuestaTransaccion respuesta = null;
        String codigoOperadorTransaccion = trx.getTransaccion().getCodigoOperadorTransaccion();
        // TODO pasar al build transaccion de las estrategias de
        // TransaccionFactory
        if (WEB_GESTION.equals(codigoOperadorTransaccion)) {
            trx.getTransaccion().getPrestadorTransaccionador().getTerminalID().setCodigoOperador(posMessage.getOperadorTerminal());
        }

        TransaccionesPropertyHidder.setMensajeMQ(trx.getTransaccion(), posMessage.getOriginalMessage().replace("\0", " "));
        AptoServicioBOVisitor visitor = new AptoServicioBOVisitor(aptoServicioBO);
        visitor.visit(trx);
        respuesta = visitor.getRespuestaAptitud();

        RespuestaCentroValidador respuestaCV = new RespuestaCentroValidador(respuesta, posMessage, trx);
        List<HandlerTime> handlersTime = visitor.getHandlersTime();
        for (HandlerTime handlerTime : handlersTime) {
            respuestaCV.addHandlerTime(handlerTime);
        }
        return respuestaCV;
    }

    /**
     * Procesamiento de un mensaje que viene dado por partes.
     * 
     * @param posMessage
     * @return
     */
    private RespuestaCentroValidador processSequentialMessege(AbstractPosMessage posMessage) {
        AbstractTransaccionPos trx = createTransaccion(posMessage);
        LOG.debug("*****************************************RECIBIENDO UN MENSAJE SECUENCIAL**************************** : "
                + posMessage.getNumeroSecuenciaMensaje() + "DE " + posMessage.getNumeroTotalMensajes() + " Transaccion id: " + trx.getId()
                + "Transaccion " + trx);
        // Verificamos que sea la ultima parte. Si es asi procesamos todo el
        // mensaje.
        // En caso de no ser la ultima parte enviamos una respuesta parcial.
        RespuestaCentroValidador respuesta = null;
        if (posMessage.isLastMessage()) {
            long lifeValue = 1000;//((GenericCRUDDaoINFImpl) this.transaccionPrestacionCrud).getLifeValue();
            boolean timeout = false;
            long start = System.currentTimeMillis();
            SequentialMessage sequentialMessage = null;
            boolean wasProcessed = false;
            while (!wasProcessed) {
                List<AbstractPosMessage> messages = this.recuperarMensajesDesdeCache(posMessage);

                boolean completed = messages.size() + 1 == posMessage.getNumeroTotalMensajes();
                timeout = (((System.currentTimeMillis() - start) / 1000) + THRESHOLD_TIMEOUT >= lifeValue);

                if (completed || timeout) {
                    for (AbstractPosMessage posMessage2Delete : messages) {
                        transaccionPrestacionCrud.delete(posMessage2Delete);
                    }
                    sequentialMessage = this.createTransaccionSequentialMessages(posMessage, messages, trx);
                    trx = sequentialMessage.getAbstractTransaccionPos();
                    LOG.debug("****************************RECIBIDOS TODOS LOS MENSAJES SECUENCIALES. PROCESANDO********************"
                            + trx.getTransaccion().getTipoTransaccion().getAtributo() + trx.getTransaccion().getTipoTransaccion().getTipo()
                            + "id trx" + trx.getId() + " Transaccion: " + trx);
                    LOG.debug(sequentialMessage.getCantidadMensajes() + " " + sequentialMessage.getCantidadMensajes());
                    if (!completed) {
                        trx.getTransaccion().setEstado(EstadoTransaccion.INCOMPLETA);
                    }
                    respuesta = this.processTransaction(posMessage, trx);
                    wasProcessed = true;
                } else {
                    try {
                        Thread.sleep(1000);
                        LOG.info("esperando respuesta");
                    } catch (InterruptedException e) {
                        throw new PosMessageHandleException(e, posMessage);
                    }
                }
            }
        } else {
            LOG.info("******************************Guardando mensaje en la cache trxId: " + trx.getId() + " id pos: " + posMessage.getId());
            //TODO Verificar de manejar errores en esta instancia
            transaccionPrestacionCrud.saveOrUpdate(posMessage);
            LOG.info("*********************************MENSAJE GUARDADO by id*************************************************");
            MensajePos mensajePos = new MensajePos();
            mensajePos.setLeyendaDisplayPos("Transaccion Recibida");
            List<String> leyendaImpresora = new ArrayList<String>();
            leyendaImpresora.add("Transaccion Recibida");
            mensajePos.setLeyendaImpresora(leyendaImpresora);
            RespuestaAptitud respuestaAptitud = new RespuestaAptitud();
            respuestaAptitud.setApto(true);
            RespuestaTransaccionParcial respuestaTransaccionParcial = this.createRepuestaTransaccionParcial(respuestaAptitud, mensajePos);
            respuesta = new RespuestaCentroValidador(respuestaTransaccionParcial, posMessage, trx);
            respuesta.setIsRespuestaParcial(true);
        }
        return respuesta;
    }

    protected RespuestaTransaccionParcial createRepuestaTransaccionParcial(RespuestaAptitud respuestaAptitud, MensajePos mensajePos) {
        return new RespuestaTransaccionParcial(respuestaAptitud, mensajePos);
    }

    protected SequentialMessage createTransaccionSequentialMessages(AbstractPosMessage lastPosMessage, List<AbstractPosMessage> messages,
            AbstractTransaccionPos trx) {
        SequentialMessage sequentialMessage = new SequentialMessage();
        sequentialMessage.setTotalMensajes(lastPosMessage.getNumeroTotalMensajes());
        sequentialMessage.addPosMessages(messages);
        sequentialMessage.addPosMessage(lastPosMessage);       
        sequentialMessage.buildMessage(this.getBeanMapper(), this);

        return sequentialMessage;
    }

    public FixedFormatManager getFormatManager() {
        return formatManager;
    }

    public void setFormatManager(FixedFormatManager formatManager) {
        this.formatManager = formatManager;
    }

    public Mapper getBeanMapper() {
        return beanMapper;
    }

    public void setBeanMapper(Mapper mapper) {
        this.beanMapper = mapper;
    }

    public long toMs(long duration) {
        return TimeUnit.NANOSECONDS.toMillis(duration);
    }

    public String getHierarchyName() {
        return "\tFixedFormatMessageHandler\n";
    }

    public boolean procesaMensaje() {
        return true;
    }

    public List<PosMessageFilter> getMessageFilters() {
        return new ArrayList<PosMessageFilter>();
    }

    // *******************************************************//
    // ******************Impl Interna************************//
    // *******************************************************//

    private List<AbstractPosMessage> recuperarMensajesDesdeCache(AbstractPosMessage posMessage) {
        AbstractPosMessage posMessageFromCache = null;
        List<AbstractPosMessage> messages = new ArrayList<AbstractPosMessage>();
        for (int i = 1; i < posMessage.getNumeroTotalMensajes(); i++) {
            posMessageFromCache = transaccionPrestacionCrud.getById(posMessage.getIdSequential(i));
            if (posMessageFromCache != null) {
                messages.add(posMessageFromCache);
            }
        }

        return messages;
    }

}
