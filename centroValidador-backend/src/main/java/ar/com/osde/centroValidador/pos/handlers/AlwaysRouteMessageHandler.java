package ar.com.osde.centroValidador.pos.handlers;

import java.io.UnsupportedEncodingException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.jms.core.MessageCreator;

import ar.com.osde.centroValidador.pos.PosMessageResponse;
import ar.com.osde.centroValidador.pos.exception.PosMessageHandleException;

import com.ibm.msg.client.wmq.WMQConstants;

/**
 * Handler que siempre rutea y delega el procesamiento del mensaje en los
 * hadlers que lo decoran.
 * 
 * @author VA27789605
 * 
 */
public class AlwaysRouteMessageHandler extends AbstractResponseMessageHandler {

    public static final String MY_NAME = AlwaysRouteMessageHandler.class.getSimpleName();
    private static final Logger LOG = Logger.getLogger(AlwaysRouteMessageHandler.class);

    public RespuestaCentroValidador process(final Message message) throws PosMessageHandleException {

        long init = System.currentTimeMillis();
        try {
            LOG.trace("Mensaje reenviado: " + ((TextMessage) message).getText());
        } catch (JMSException e) {
            LOG.debug(e);
            throw new PosMessageHandleException("No es posible obtener el texto del mensaje");
        }

        this.getTemplateResponse().send(new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                TextMessage newMessage = session.createTextMessage();
                String strMessage = ((TextMessage) message).getText();

                newMessage.setText(strMessage);
                newMessage.setJMSMessageID(message.getJMSMessageID().replace("ID:", "").replace("f", "").replace("F", "")
                        .replace("id:", ""));
                try {
                    newMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_MSGID,
                            newMessage.getJMSMessageID().getBytes(PosMessageResponse.EBCDIC_ENCODING));
                } catch (UnsupportedEncodingException e) {
                    LOG.debug(e);
                }

                return newMessage;
            }
        });

        long end = System.currentTimeMillis();

        RespuestaCentroValidador respuesta = getIntercepted().process(message);

        respuesta.addHandlerTime(new HandlerTime(MY_NAME, end - init));
        return respuesta;
    }

    public String getHierarchyName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("AlwaysRouteMessageHandler\n");
        if (this.getIntercepted() != null) {
            strBuff.append("\t").append(this.getIntercepted().getHierarchyName());
        }
        return strBuff.toString();
    }
}
