package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.JMSException;
import javax.jms.Queue;

import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;

import com.ibm.msg.client.jms.JmsDestination;
import com.ibm.msg.client.wmq.WMQConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase base para el manejo de los mensajes.
 * 
 * @author VA27789605
 * 
 */
public abstract class AbstractResponseMessageHandler extends PosMessageHandlerInterceptor {
	private static final Logger LOG = Logger.getLogger(AbstractResponseMessageHandler.class);
	private JmsTemplate templateResponse;
	// TODO: Revisar esta propiedad. No deber�a estar, No se porque motivo se
	// agreg� al construir la GUI.
	private Queue queue;

	public JmsTemplate getTemplateResponse() {
		return templateResponse;
	}

	public void setTemplateResponse(JmsTemplate templateResponse) {
		this.templateResponse = templateResponse;
		try {
			if (templateResponse.getDefaultDestination() != null) {
				((JmsDestination) templateResponse.getDefaultDestination()).setBooleanProperty(
				        WMQConstants.WMQ_MQMD_WRITE_ENABLED, true);
			}else{
				LOG.error("Se esperaba una default destination");
			}
		} catch (JMSException e) {
			throw new RuntimeException("No se puede setear la propiedad WMQ_MQMD_WRITE_ENABLED");
		}

	}

	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

    public List<PosMessageFilter> getMessageFilters() {
        List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
        messageFilters.addAll(this.getIntercepted().getMessageFilters());
        return messageFilters;

    }

}
