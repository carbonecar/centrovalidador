package ar.com.osde.centroValidador.pos;

import java.io.Serializable;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import ar.com.osde.centroValidador.pos.utils.PosLocalDateFormatter;
import ar.com.osde.centroValidador.pos.utils.PosLocalTimeFormatter;
import ar.com.osde.transaccionService.services.GeneradorRespuesta;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;

public abstract class AbstractPosMessage implements Serializable, IPosMessage {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private transient String originalMessage;

    // AUFREE
    private String codigoInterno;

    // AUOPED
    private String codigoOperador;

    // AUFILP
    private int filialInstalacionPOS;

    // AUDELP
    private int delegacionInstalacionPOS;

    // AUNROP
    private int numeroPOS;

    // AUCUIT
    private String cuitPrestador;

    // AUTRAC
    private int codigoTransaccion;
    // AUATRC
    private String atributoCodigoTransaccion;

    // AUSYST
    private int numeroTransaccion;

    // AUFECT
    private LocalDate fechaTransaccion;

    // AUHORT
    private LocalTime horaTransaccion;

    // AUNSEQ
    private int numeroSecuenciaMensaje = 1;

    // AUNTOT
    private int numeroTotalMensajes = 1;

    // AUPRST
    private String codigoPrestador;

    // AUTPRS
    private int tipoPrestador;

    /**
     * Este valor no se toma del mensaje original a menos que sea un timeout
     */
    private String generadorRespuesta = String.valueOf(GeneradorRespuesta.OSDE.getCodigo());
        
    
    private String versionMensajeria;
    
     
    @Field(offset = 557, length = 10)
    public String getVersionMensajeria() {
        return versionMensajeria;
    }

    public void setVersionMensajeria(String versionMensajeria) {
        this.versionMensajeria = versionMensajeria;
    }

    @Field(offset = 448, length = 6)
    public String getCodigoPrestador() {
        return codigoPrestador;
    }

    public void setCodigoPrestador(String codigoPrestador) {
        this.codigoPrestador = codigoPrestador;
    }

    @Field(offset = 454, length = 1)
    public int getTipoPrestador() {
        return tipoPrestador;
    }

    public void setTipoPrestador(int p) {
        this.tipoPrestador = p;
    }

    // AUMOTA
    private String operadorTerminal;

    // AUTPDU
    private String numeroOperadorParaTerminalPOS;

    @Field(offset = 354, length = 2, paddingChar = '0', align = Align.LEFT)
    public String getOperadorTerminal() {
        return operadorTerminal;
    }

    public void setOperadorTerminal(String operadorTerminal) {
        this.operadorTerminal = operadorTerminal;
    }

    @Field(offset = 6, length = 2)
    public String getCodigoOperador() {
        return codigoOperador;
    }

    public void setCodigoOperador(String p) {
        this.codigoOperador = p;
    }

    @Field(offset = 8, length = 2)
    public int getFilialInstalacionPOS() {
        return filialInstalacionPOS;
    }

    public void setFilialInstalacionPOS(int p) {
        this.filialInstalacionPOS = p;
    }

    @Field(offset = 10, length = 2)
    public int getDelegacionInstalacionPOS() {
        return delegacionInstalacionPOS;
    }

    public void setDelegacionInstalacionPOS(int p) {
        this.delegacionInstalacionPOS = p;
    }

    @Field(offset = 12, length = 4)
    public int getNumeroPOS() {
        return numeroPOS;
    }

    public void setNumeroPOS(int p) {
        this.numeroPOS = p;
    }

    @Field(offset = 16, length = 11)
    // , formatter = CuitFormatter.class)
    public String getCuitPrestador() {
        return cuitPrestador;
    }

    public void setCuitPrestador(String p) {
        this.cuitPrestador = p;
    }

    @Field(offset = 45, length = 2)
    public int getCodigoTransaccion() {
        return codigoTransaccion;
    }

    public void setCodigoTransaccion(int p) {
        this.codigoTransaccion = p;
    }

    @Field(offset = 47, length = 1)
    public String getAtributoCodigoTransaccion() {
        return atributoCodigoTransaccion;
    }

    public void setAtributoCodigoTransaccion(String p) {
        this.atributoCodigoTransaccion = p;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.pos.IPosMessage#getOriginalMessage()
     */
    public String getOriginalMessage() {
        return originalMessage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.pos.IPosMessage#setOriginalMessage(java.lang
     * .String)
     */
    public void setOriginalMessage(String originalMessage) {
        this.originalMessage = originalMessage;
    }

    @Field(offset = 48, length = 6)
    public int getNumeroTransaccion() {
        return numeroTransaccion;
    }

    public void setNumeroTransaccion(int p) {
        this.numeroTransaccion = p;
    }

    @Field(offset = 60, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(LocalDate p) {
        this.fechaTransaccion = p;
    }

    @Field(offset = 68, length = 6, formatter = PosLocalTimeFormatter.class)
    @FixedFormatPattern("HHmmss")
    public LocalTime getHoraTransaccion() {
        return horaTransaccion;
    }

    public void setHoraTransaccion(LocalTime p) {
        this.horaTransaccion = p;
    }

    @Field(offset = 179, length = 2)
    public int getNumeroSecuenciaMensaje() {
        return numeroSecuenciaMensaje;
    }

    public void setNumeroSecuenciaMensaje(int p) {
        if (p != 0) {
            this.numeroSecuenciaMensaje = p;
        }
    }

    @Field(offset = 181, length = 2)
    public int getNumeroTotalMensajes() {
        return numeroTotalMensajes;
    }

    public void setNumeroTotalMensajes(int p) {
        if (p != 0) {
            this.numeroTotalMensajes = p;
        }
    }

    /**
     * 
     * @return true en caso que sea el primer mensaje de varios. False en caso
     *         contrario, incluso si es solo 1 mensaje de 1.
     */
    public boolean isFistMessage() {
        boolean isFirst=false;
        if (isPartOf() && this.numeroSecuenciaMensaje == 1) {
            isFirst=true;
        }

        return isFirst;
    }

    /**
     * @return true si es el �ltimo mensaje de varios. False en caso contrario,
     *         incluso si es el �nico
     */
    public boolean isLastMessage() {
        boolean isLast=false;
        if (isPartOf() && this.numeroSecuenciaMensaje == this.numeroTotalMensajes) {
            isLast=true;
        }
        return isLast;
    }

    /**
     * Indica si el mensaje es parte de un conjunto de mensajes que forman una
     * transaccion
     * 
     * @return
     */
    private boolean isPartOf() {
        boolean isPartOf=false;
        if (this.numeroTotalMensajes > 1) {
            isPartOf=true;
        }
        return isPartOf;
    }

    // @Id
    // @org.hibernate.search.annotations.Field(store=Store.NO,index=Index.UN_TOKENIZED)
    private long id = 0;

    public long getIdSequential(int sequential) {
        String concatedId = getStringIDSinSecuencial() + sequential;
        return concatedId.hashCode();
    }

    /**
     * Identificador unico para cada mensaje
     * 
     * @return
     */
    public long getId() {
        if (id == 0) {
            id = this.getIdSequential(getNumeroSecuenciaMensaje());
        }
        return id;
    }

    /**
     * Devuelve el id como string SIN el identificador secuencial
     */
    public String getStringIDSinSecuencial() {
        StringBuilder concatedCodigoOperador = new StringBuilder();
        for (int i = 0; i < getCodigoOperador().length(); i++) {
            concatedCodigoOperador.append(Integer.valueOf(getCodigoOperador().charAt(i)).toString());
        }

        int anno = this.getFechaTransaccion().getYear();
        int mes = this.getFechaTransaccion().getMonthOfYear();
        int dia = this.getFechaTransaccion().getDayOfMonth();
        String concatedId = concatedCodigoOperador.toString() + "" + anno + "" + mes + "" + dia + "" + getNumeroTransaccion()
                + generadorRespuesta;
        return concatedId;

    }

    public void setId(long id) {
        this.id = id;
    }

    @Field(offset = 1, length = 5)
    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String p) {
        this.codigoInterno = p;
    }

    @Deprecated
    // no se usa. Se mantiene por compatibilidad
    @Field(offset = 54, length = 5)
    public String getNumeroOperadorParaTerminalPOS() {
        return numeroOperadorParaTerminalPOS;
    }

    @Deprecated
    // no se usa
    public void setNumeroOperadorParaTerminalPOS(String p) {
        this.numeroOperadorParaTerminalPOS = p;
    }

    // Estos campos no vienen en el mensaje original a menos que sea un timeout
    public String getGeneradorRespuesta() {
        return generadorRespuesta;
    }

    public void setGeneradorRespuesta(String generadorRespuesta) {
        this.generadorRespuesta = generadorRespuesta;
    }

}
