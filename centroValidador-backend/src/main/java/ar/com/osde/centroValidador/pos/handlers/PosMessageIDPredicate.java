package ar.com.osde.centroValidador.pos.handlers;

import org.apache.commons.collections15.Predicate;

import ar.com.osde.centroValidador.pos.PosMessage;

/**
 * Utilizados para comparar mensajes por su clave de identificacion. Este
 * predicado debe ser utilizado solo para mensajes en un lapso corto de tiempo
 * (menos de 1 hora).
 * 
 * @author VA27789605
 * 
 */
public class PosMessageIDPredicate implements Predicate<PosMessage> {

	private PosMessage posMessage;

	public PosMessageIDPredicate(PosMessage posMessage) {
		this.posMessage = posMessage;
	}

	public boolean evaluate(PosMessage otherMessage) {

		if (otherMessage == null)
			return false;

		boolean sameMessage = otherMessage.getCodigoOperador().equals(
				posMessage.getCodigoOperador())
				&& otherMessage.getDelegacionInstalacionPOS() == posMessage
						.getDelegacionInstalacionPOS()
				&& otherMessage.getFilialInstalacionPOS() == posMessage
						.getFilialInstalacionPOS()
				&& otherMessage.getNumeroPOS() == posMessage.getNumeroPOS()
				&& otherMessage.getNumeroTransaccion() == posMessage
						.getNumeroTransaccion();

		return sameMessage;
	}

}
