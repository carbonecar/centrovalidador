package ar.com.osde.centroValidador.pos.mq;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.ibm.msg.client.wmq.WMQConstants;

import ar.com.osde.centroValidador.pos.PosMessageResponse;
import ar.com.osde.centroValidador.pos.PosMessageResponseCierreFarmacia;
import ar.com.osde.centroValidador.pos.PosMessageResponseConsulta;
import ar.com.osde.centroValidador.pos.PosMessageResponseCreatorFactory;
import ar.com.osde.centroValidador.pos.PosMessageResponseFarmacia;
import ar.com.osde.centroValidador.pos.PosMessageResponsePrestaciones;
import ar.com.osde.centroValidador.pos.SocioPosMessageResponse;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.comunes.utils.ItemConsumoUtils;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AnulacionTransaccionPos;
import ar.com.osde.cv.model.BloqueoDesbloqueoTransaccionPos;
import ar.com.osde.cv.model.CierreFarmaciaTransaccionPos;
import ar.com.osde.cv.model.CodigoRespuestaTransaccion;
import ar.com.osde.cv.model.ConsultaTransaccionPos;
import ar.com.osde.cv.model.EnvioDocumentacionTransaccionPos;
import ar.com.osde.cv.model.MarcaGenericoTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.SocioPosMessage;
import ar.com.osde.cv.model.TransaccionIngresoEgresoPos;
import ar.com.osde.cv.model.TransaccionMigracionPos;
import ar.com.osde.cv.model.TransaccionPosVisitor;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.cv.model.TransaccionRescatePos;
import ar.com.osde.cv.model.TransaccionTimeOutPos;
import ar.com.osde.entities.aptoServicio.ArancelRespuesta;
import ar.com.osde.entities.aptoServicio.ItemConsumoRespuesta;
import ar.com.osde.entities.aptoServicio.MedicamentoRespuesta;
import ar.com.osde.entities.aptoServicio.PracticaRespuesta;
import ar.com.osde.entities.aptoServicio.TransaccionReferencia;
import ar.com.osde.entities.as400.ProgramaEspecial;
import ar.com.osde.entities.as400.Sexo;
import ar.com.osde.entities.as400.Socio;
import ar.com.osde.entities.transaccion.ConfiguracionInconsistenciasImpresion;
import ar.com.osde.entities.transaccion.InconsistenciaDescription;
import ar.com.osde.entities.transaccion.MessagePosBuilder;
import ar.com.osde.entities.transaccion.NombreReporte;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.entities.transaccion.TransaccionConsulta;
import ar.com.osde.transaccionService.services.RespuestaCierre;

/**
 * Crea un mensaje de respuesta hacia los sistemas POS. Estos tienen sus
 * particularidades como ser set de caracteres y protocolo en cuanto a la forma
 * de construir el mensaje. Esta clase no puede ser singleton.
 * 
 * @author VA27789605
 * 
 */
public class PosMessageResponseCreator extends SimpleTextMessageCreator implements TransaccionPosVisitor {

    private static final Logger LOG = Logger.getLogger(PosMessageResponseCreator.class);
    public static final String ERROR_ASOCIADO_NO_HABILITADO = "ERROR, ASOCIADO NO  HABILITADO";
    public static final String OK_PRESTACION_REGISTRADA = "OK, PRESTACION RE-  GISTRADA";
    private RespuestaCentroValidador respuestaCentroValidador;
    private long timeUsedToCreateMessage = 0;

    private PosMessageResponse response = PosMessageResponseCreatorFactory.createPosMessageReponse();

    private Map<String, InconsistenciaDescription> codigoInconsistenciaDescripcion;
    private String stringResponse;
    private List<ConfiguracionInconsistenciasImpresion> inconsistenciasDescripcion;

    public long getTimeUsedToCreateMessage() {
        return timeUsedToCreateMessage;
    }

    public void setRespuestaCentroValidador(RespuestaCentroValidador respuesta) {
        this.respuestaCentroValidador = respuesta;
    }

    public RespuestaCentroValidador getRespuestaCentroValidador() {
        return respuestaCentroValidador;
    }

    // No cambiar la visibilidad
    PosMessageResponseCreator(List<ConfiguracionInconsistenciasImpresion> inconsistenciasDescripcion) {
        this.inconsistenciasDescripcion = inconsistenciasDescripcion;
    }

    public Message createMessage(Session session) throws JMSException {
        long init = System.currentTimeMillis();
        TextMessage message = session.createTextMessage();

        String responseStr = createMessageResponse();
        // if(responseStr.length()>1001){
        // responseStr=responseStr.substring(0,1000);
        // }
        message.setText(responseStr);

        String codigoOperador = getOperadores().get(respuestaCentroValidador.getPosMessage().getCodigoOperador());
        if (codigoOperador == null) {
            codigoOperador = respuestaCentroValidador.getPosMessage().getCodigoOperador();
        }
        if (codigoOperador == null) {

            codigoOperador = respuestaCentroValidador.getTransaccionXML().getCodigoOperadorTransaccion();
        }
        message.setJMSMessageID(MessageIDBuilder.buildMessageID(respuestaCentroValidador.getPosMessage().getNumeroTransaccion(),
                codigoOperador));
        try {
            message.setObjectProperty(WMQConstants.JMS_IBM_MQMD_MSGID,
                    message.getJMSMessageID().getBytes(PosMessageResponse.EBCDIC_ENCODING));
        } catch (UnsupportedEncodingException e) {
            LOG.error(e);
            throw new UnsupportedPosMessageException(e.getMessage());
        }

        this.timeUsedToCreateMessage = System.currentTimeMillis() - init;
        return message;
    }

    public String createMessageResponse() {
        if (this.stringResponse == null) {
            AbstractTransaccionPos transaccion = respuestaCentroValidador.getTransaccion();
            this.visit(transaccion);
            stringResponse = getManager().export(response);
            respuestaCentroValidador.setResponseString(stringResponse);
        }

        LOG.debug(stringResponse);

        return stringResponse;
    }

    // ************************************************************//
    // *************Implementacion del visitor para construir la
    // repuesta en funcion del tipo de mensaje enviado************//
    // ************************************************************//

    public void visit(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos) {
        Integer numeroCierre = ((RespuestaCierre) respuestaCentroValidador.getDecoratee()).getNumeroCierre();
        if (numeroCierre != null) {
            ((PosMessageResponseCierreFarmacia) this.response).setNumeroCierre(numeroCierre);
        }
    }

    public void visit(AbstractTransaccionPos transaccionPos) {
        if (transaccionPos != null) {
            this.response = PosMessageResponseCreatorFactory.createPosMessageReponse(transaccionPos.getTransaccion().getTipoTransaccion());
        } else {
            this.response = PosMessageResponseCreatorFactory.createPosMessageReponse();
        }
        response.setCodigoInterno(respuestaCentroValidador.getPosMessage().getCodigoInterno());
        response.setCodigoOperador(respuestaCentroValidador.getPosMessage().getCodigoOperador());
        response.setFilialInstalacionPOS(respuestaCentroValidador.getPosMessage().getFilialInstalacionPOS());
        response.setDelegacionInstalacionPOS(respuestaCentroValidador.getPosMessage().getDelegacionInstalacionPOS());
        response.setNumeroPOS(respuestaCentroValidador.getPosMessage().getNumeroPOS());
        response.setNroTransaccion(respuestaCentroValidador.getPosMessage().getNumeroTransaccion());

        response.setNroInternoPos(respuestaCentroValidador.getPosMessage().getNumeroOperadorParaTerminalPOS());
        response.setNroSecuencia(respuestaCentroValidador.getPosMessage().getNumeroSecuenciaMensaje());
        response.setTotalMensajes(respuestaCentroValidador.getPosMessage().getNumeroTotalMensajes());
        response.setFirmaGeneradorAutorizaciones(CodigoRespuestaTransaccion.GENERADOR_RESPUESTA_PROPIO.getCodigo());

        // ver al construir la respuesta que se cree el mensaje pos.
        // correctamente.
        response.setLeyendaDesplegar(respuestaCentroValidador.getDecoratee().getMensajePos().getLeyendaDisplayPos());
        List<String> leyendasImpresora = respuestaCentroValidador.getDecoratee().getMensajePos().getLeyendaImpresora();

        if (leyendasImpresora != null && !leyendasImpresora.isEmpty()) {
            int index = 1;
            for (String leyenda : leyendasImpresora) {
                response.setLeyendaImpresionRenglon1(leyenda, index++);

            }
        }

        response.setCodigoRespuestaTransaccion(respuestaCentroValidador.getRespuestaTransaccion().getCodigo());
        if (transaccionPos != null) {
            List<InconsistenciaDescription> inconsistencias = null;

            try {
                this.codigoInconsistenciaDescripcion = new HashMap<String, InconsistenciaDescription>();
                for (Iterator iterator = inconsistenciasDescripcion.iterator(); iterator.hasNext();) {
                    ConfiguracionInconsistenciasImpresion configuracionInconsistenciasImpresion = (ConfiguracionInconsistenciasImpresion) iterator
                            .next();
                    codigoInconsistenciaDescripcion.put(String.valueOf(configuracionInconsistenciasImpresion.getCodigoMotor()),
                            configuracionInconsistenciasImpresion.getInconsistenciaDescription());
                }
            } catch (Exception ex) {
                LOG.warn("No esta disponible el servicio. No se mapean inconsistencias");
            }
            // Si no es apto muestro las inconsistencias
            if (!respuestaCentroValidador.isApto()) {
                inconsistencias = MessagePosBuilder.collectInconsistenciaDescriptionFromObservaciones(respuestaCentroValidador
                        .getDecoratee().getRespuestaAptitud(), this.codigoInconsistenciaDescripcion);
            } else {// Sino muestro las advertencias nada mas
                inconsistencias = MessagePosBuilder.collectInconsistenciaDescriptionFromAdvertencias(respuestaCentroValidador
                        .getDecoratee().getRespuestaAptitud(), this.codigoInconsistenciaDescripcion);
            }
            int numeroInc = 1;
            for (InconsistenciaDescription inconsistenciaDescription : inconsistencias) {
                switch (numeroInc++) {
                case 1:
                    response.setInconsistencia1(Integer.parseInt(inconsistenciaDescription.getCodigo()));
                    break;
                case 2:
                    response.setInconsistencia2(Integer.parseInt(inconsistenciaDescription.getCodigo()));
                    break;
                default:
                    break;
                }
            }

            transaccionPos.accept(this);
        }
    }

    public void visit(TransaccionPrestacionPos transaccion) {
        this.visit(((SocioPosMessage) transaccion));
        if (respuestaCentroValidador.isRespuestaParcial() && respuestaCentroValidador.isApto()) {
            response.setLeyendaDesplegar(respuestaCentroValidador.getDecoratee().getMensajePos().getLeyendaDisplayPos());
        }
        this.evaluarItemsPrestacion();
    }

    public void visit(RegistracionMedicamentoMessage transaccion) {
        this.visit((SocioPosMessage) transaccion);
        if (respuestaCentroValidador.isApto()) {
            response.setLeyendaDesplegar(OK_PRESTACION_REGISTRADA);
        }
        evaluarItemsFarmacia();

    }

    public void visit(AnulacionTransaccionPos anulacionTransaccionPos) {
        TransaccionReferencia transaccionReferencia = anulacionTransaccionPos.getTransaccionReferencia();
        if (transaccionReferencia != null) {
            this.response.setNroAutorizacion((int) transaccionReferencia.getNumeroTransaccion());
        }

    }

    public void visit(TransaccionRescatePos transaccionRescatePos) {
        // nothing to do
    }

    public void visit(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoPosMessage) {
        TransaccionReferencia transaccionReferencia = bloqueoDesbloqueoPosMessage.getTransaccionReferencia();
        if (transaccionReferencia != null) {
            this.response.setNroAutorizacion((int) transaccionReferencia.getNumeroTransaccion());
        }

    }

    public void visit(TransaccionIngresoEgresoPos transaccionIngresoEgresoPos) {
        // nothing to do

    }

    public void visit(SocioPosMessage socioPosMessage) {
        if (!respuestaCentroValidador.isRespuestaParcial()) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Socio socio = respuestaCentroValidador.getSocio();
            response.setLeyendaDesplegar(respuestaCentroValidador.getDecoratee().getMensajePos().getLeyendaDisplayPos());

            // Todo este codigo tiene que pasar al TransaccionesService
            // datos del socio.
            if (socio != null) {
                SocioPosMessageResponse socioResponse = (SocioPosMessageResponse) response;
                if (socio.getPlanServicio() != null) {
                    socioResponse.setPrefijoPlan(socio.getPlanServicio().getPrefijo().toString());
                    socioResponse.setPlanSocio(String.format("%3s", socio.getPlanServicio().getPlan()).replace(' ', '0'));
                }
                response.setNombreAsociado(socio.getApellido() + " " + socio.getNombre());
                if (Sexo.MASCULINO.equals(socio.getSexo())) {
                    response.setSexoAsociado('M');
                } else {
                    response.setSexoAsociado('F');
                }

                if (socio.getFechaNacimiento() != null) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(socio.getFechaNacimiento());
                    cal.add(Calendar.HOUR, 4);

                    response.setFechaNacimiento(Integer.valueOf(sdf.format(cal.getTime())));
                }
                if (socio.getRotuloIva() != null) {
                    String codigoRotuloIVA = socio.getRotuloIva().getCodigo();
                    if ("1".equals(codigoRotuloIVA) || "5".equals(codigoRotuloIVA)) {
                        socioResponse.setTipoSocio('O'); // no grabado
                    } else {
                        socioResponse.setTipoSocio('D'); // grabado
                    }
                }
                List<ProgramaEspecial> programasEspeciales = socio.getProgramasEspeciales();
                if (programasEspeciales != null) {
                    for (Iterator<ProgramaEspecial> iterator = programasEspeciales.iterator(); iterator.hasNext();) {
                        ProgramaEspecial programaEspecial = iterator.next();
                        if (ar.com.osde.cv.model.ProgramaEspecial.CRONICO.getCodigoTraducido().equals(programaEspecial.getTipo())) {
                            response.setCodigoCronico(programaEspecial.getTipo());
                        }
                        if (ar.com.osde.cv.model.ProgramaEspecial.MATERNO_INFANTIL.getCodigoTraducido().equals(programaEspecial.getTipo())) {
                            response.setCubrePlanMaterno('S');
                        }
                    }
                }
            }
        }
        this.respuestaCentroValidador.setPosMessageResponse(response);
    }

    public void visit(TransaccionTimeOutPos transaccionTimeoutPos) {
        // nothing to do

    }

    public void visit(TransaccionMigracionPos transaccionMigracionPos) {
        // nothing to do

    }

    // *******************************************************//
    // ********************Impl. Interna**********************//
    // *******************************************************//
    private void evaluarItemsPrestacion() {
        List<ItemConsumoRespuesta> itemsRespuesta = respuestaCentroValidador.getDecoratee().getRespuestaAptitud()
                .getItemsConsumoRespuesta();
        if (itemsRespuesta != null) {
            for (ItemConsumoRespuesta itemConsumoRespuesta : itemsRespuesta) {
                if ((itemConsumoRespuesta instanceof PracticaRespuesta)) {
                    PracticaRespuesta practica = (PracticaRespuesta) itemConsumoRespuesta;
                    ArancelRespuesta arancel = practica.getArancelesRespuesta().get(0);

                    switch (itemConsumoRespuesta.getNroItem()) {
                    case 1:
                        this.response.setArancelPrestacion1(arancel.getCodArancel());
                        this.response.setDescripcionPrestacion1(practica.getDescripcion());
                        response.setCodigoRespuestaItem1(ItemConsumoUtils.approvedString(itemConsumoRespuesta));
                        break;
                    case 2:
                        this.response.setArancelPrestacion2(arancel.getCodArancel());
                        this.response.setDescripcionPrestacion1(practica.getDescripcion());
                        response.setCodigoRespuestaItem2(ItemConsumoUtils.approvedString(itemConsumoRespuesta));

                        break;
                    case 3:
                        this.response.setArancelPrestacion3(arancel.getCodArancel());
                        this.response.setDescripcionPrestacion1(practica.getDescripcion());
                        response.setCodigoRespuestaItem3(ItemConsumoUtils.approvedString(itemConsumoRespuesta));

                        break;
                    case 4:
                        this.response.setArancelPrestacion4(arancel.getCodArancel());
                        this.response.setDescripcionPrestacion1(practica.getDescripcion());
                        ((PosMessageResponsePrestaciones) response).setCodigoRespuestaItem4(ItemConsumoUtils
                                .approvedString(itemConsumoRespuesta));
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }

    private void evaluarItemsFarmacia() {

        PosMessageResponseFarmacia response = (PosMessageResponseFarmacia) this.response;
        List<ItemConsumoRespuesta> itemsRespuesta = respuestaCentroValidador.getDecoratee().getRespuestaAptitud()
                .getItemsConsumoRespuesta();
        if (itemsRespuesta != null) {
            for (Iterator<ItemConsumoRespuesta> iterator = itemsRespuesta.iterator(); iterator.hasNext();) {
                ItemConsumoRespuesta item = iterator.next();
                if (item instanceof MedicamentoRespuesta) {
                    MedicamentoRespuesta itemConsumoRespuesta = (MedicamentoRespuesta) item;
                    int nroItem = itemConsumoRespuesta.getNroItem();
                    switch (nroItem) {
                    case 1:
                        response.setCodigoRespuestaItem1(ItemConsumoUtils.approvedString(itemConsumoRespuesta));
                        if (this.respuestaCentroValidador.getDecoratee().getRespuestaAptitud().isApto()) {
                            response.setPrecioLista1(BigDecimal.valueOf(itemConsumoRespuesta.getImporte()));
                            if (itemConsumoRespuesta.getCoberturaOtorgada() != null) {
                                response.setPorcentajeCobertura1(itemConsumoRespuesta.getCoberturaOtorgada().getPorcentajeCobertura());
                            }
                            response.setPrecioACargoSocio1(BigDecimal.valueOf(itemConsumoRespuesta.getImporteACargoDelSocio()));
                            response.setNombreYPresentacionMedicamento1(itemConsumoRespuesta.getDescripcion() + " "
                                    + itemConsumoRespuesta.getPresentacion());
                        }
                        break;
                    case 2:
                        response.setCodigoRespuestaItem2(ItemConsumoUtils.approvedString(itemConsumoRespuesta));
                        if (this.respuestaCentroValidador.getDecoratee().getRespuestaAptitud().isApto()) {
                            response.setPrecioLista2(BigDecimal.valueOf(itemConsumoRespuesta.getImporte()));
                            if (itemConsumoRespuesta.getCoberturaOtorgada() != null) {
                                response.setPorcentajeCobertura2(itemConsumoRespuesta.getCoberturaOtorgada().getPorcentajeCobertura());
                            }
                            response.setPrecioACargoSocio2(BigDecimal.valueOf(itemConsumoRespuesta.getImporteACargoDelSocio()));
                            response.setNombreYPresentacionMedicamento2(itemConsumoRespuesta.getDescripcion() + " "
                                    + itemConsumoRespuesta.getPresentacion());
                        }
                        break;
                    case 3:
                        response.setCodigoRespuestaItem3(ItemConsumoUtils.approvedString(itemConsumoRespuesta));
                        if (this.respuestaCentroValidador.getDecoratee().getRespuestaAptitud().isApto()) {
                            response.setPrecioLista3(BigDecimal.valueOf(itemConsumoRespuesta.getImporte()));
                            if (itemConsumoRespuesta.getCoberturaOtorgada() != null) {
                                response.setPorcentajeCobertura3(itemConsumoRespuesta.getCoberturaOtorgada().getPorcentajeCobertura());
                            }
                            response.setPrecioACargoSocio3(BigDecimal.valueOf(itemConsumoRespuesta.getImporteACargoDelSocio()));
                            response.setNombreYPresentacionMedicamento3(itemConsumoRespuesta.getDescripcion() + " "
                                    + itemConsumoRespuesta.getPresentacion());
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }

    public void visit(ConsultaTransaccionPos consultaTransaccionPos) {
        // si es una consulta de farmacia entonces cargo el nombre del archivo.
        TransaccionConsulta trxConsulta = consultaTransaccionPos.getTransaccion();
        if (TipoTransaccion.O3F.equals(trxConsulta.getTipoTransaccion())) {
            String nombreReporte = NombreReporte.crearNombre(trxConsulta);
            if ((this.response instanceof PosMessageResponseConsulta)) {
                ((PosMessageResponseConsulta) this.response).setNombreArchivoPublicado(nombreReporte);
            }
        }
    }

    public void visit(MarcaGenericoTransaccionPos transaccionMarcaGenerico) {
        // nothing to do

    }

    public void visit(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion) {
        // nothing to do

    }

}
