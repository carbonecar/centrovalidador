package ar.com.osde.centroValidador.pos.filter.estrategy;

import javax.jms.Message;

import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;

/**
 * Estrategia que continua procesando seg�n como haya configurado el manejador
 * de mensajes.
 * 
 */
public class ProcessStrategy implements FilterStrategy {

    private PosMessageHandler posMessageHandler;

    public void doFilterAction(Message message) {
        posMessageHandler.process(message);
    }

    public PosMessageHandler getPosMessageHandler() {
        return posMessageHandler;
    }

    public void setPosMessageHandler(PosMessageHandler posMessageHandler) {
        this.posMessageHandler = posMessageHandler;
    }

    public String getResolucionEstrategia() {
        return MENSAJE_ESTRATEGIA_CONTINUA_PROCESANDO;
    }
}
