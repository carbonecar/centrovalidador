package ar.com.osde.centroValidador.factory;

import java.util.LinkedList;
import java.util.List;

import ar.com.osde.centroValidador.pos.filter.AndFilter;
import ar.com.osde.centroValidador.pos.filter.CompositeFilter;
import ar.com.osde.centroValidador.pos.filter.NotFilter;
import ar.com.osde.centroValidador.pos.filter.NullFilter;
import ar.com.osde.centroValidador.pos.filter.OrFilter;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.filter.PropertyEqualFilter;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * Clase Factory que permite crear PosMessageFilter.
 */
public class FilterFactory {

    public static PosMessageFilter createFilter(FilterConfiguration filterConfiguration) throws BusinessException {

        String className = filterConfiguration.getClassName();

        if (NullFilter.class.getName().equals(className)) {
            return new NullFilter();
        } else
            if ((AndFilter.class.getName().equals(className) || (OrFilter.class.getName().equals(className)))
                    && filterConfiguration instanceof CompositeFilterConfiguration) {
                return createCompositeFilter((CompositeFilterConfiguration) filterConfiguration);
            } else
                if (NotFilter.class.getName().equals(className) && filterConfiguration instanceof NotFilterConfiguration) {
                    return new NotFilter(createFilter(((NotFilterConfiguration) filterConfiguration).getFilterConfiguration()));
                } else
                    if (PropertyEqualFilter.class.getName().equals(className)
                            && filterConfiguration instanceof PropertyEqualFilterConfiguration) {
                        return createPropertyFilter((PropertyEqualFilterConfiguration) filterConfiguration);
                    }

        throw new BusinessException("No se puede crear el filtro.");
    }

    private static PosMessageFilter createPropertyFilter(PropertyEqualFilterConfiguration filterConfiguration) {

        PropertyEqualFilter equalFilter = new PropertyEqualFilter();

        equalFilter.setPropertyName(filterConfiguration.getPropertyName());
        equalFilter.setValue(filterConfiguration.getRealValue());

        return equalFilter;
    }

    private static PosMessageFilter createCompositeFilter(CompositeFilterConfiguration filterConfiguration) throws BusinessException {

        CompositeFilter filter = null;

        if (AndFilter.class.getName().equals(filterConfiguration.getClassName())) {
            filter = new AndFilter();
        } else
            if (OrFilter.class.getName().equals(filterConfiguration.getClassName())) {
                filter = new OrFilter();
            } else {
                throw new BusinessException("No se pudo crear el filtro compuesto.");
            }

        List<PosMessageFilter> filterList = new LinkedList<PosMessageFilter>();

        CompositeFilterConfiguration configuration =  filterConfiguration;

        for (FilterConfiguration config : configuration.getFilterConfigurationList()) {
            filterList.add(createFilter(config));
        }

        filter.setFiltros(filterList);

        return filter;
    }
}
