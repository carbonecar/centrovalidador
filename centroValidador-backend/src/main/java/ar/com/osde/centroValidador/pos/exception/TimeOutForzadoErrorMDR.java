package ar.com.osde.centroValidador.pos.exception;

import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;

public class TimeOutForzadoErrorMDR extends TimeOutForzadoRulesException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TimeOutForzadoErrorMDR(String string, RespuestaCentroValidador respuesta) {
        super(string,respuesta);
    }

    public TimeOutForzadoErrorMDR(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TimeOutForzadoErrorMDR(String arg0) {
        super(arg0);
    }

    public TimeOutForzadoErrorMDR(Throwable arg0) {
        super(arg0);
    }


}
