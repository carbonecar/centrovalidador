package ar.com.osde.centroValidador.pos;

import java.io.Serializable;

import org.hibernate.search.annotations.ProvidedId;
import org.joda.time.LocalDate;

import ar.com.osde.centroValidador.formatter.TrunkToZeroFormmatter;
import ar.com.osde.centroValidador.formatter.TrunkToZeroIntegerFormatter;
import ar.com.osde.centroValidador.pos.utils.CuitFormatter;
import ar.com.osde.centroValidador.pos.utils.PosGenericoFormatter;
import ar.com.osde.centroValidador.pos.utils.PosLocalDateFormatter;
import ar.com.osde.framework.entities.FrameworkEntityID;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@ProvidedId
// @Indexed(index = "PosMessage")
// comentado hasta poder solucionar el problema del directorio donde se esta
// grabando la cache.
@Record
public class PosMessage extends CredencialPosMessage implements Serializable, FrameworkEntityID {

    private static final long serialVersionUID = -4968698829157657676L;

    // AUWREF
    private String delegacionOrden;

    // AUNREF Esta valor se conviente en un nro de transaccion en el caso de ser
    // una tx 2Lo 4A
    private int numeroAutorizacionReferencia;

    // AUPRE1
    private int codigoPrestacion1;

    // AUTIP1
    private int tipoPrestacion1;

    // AUARA1
    private int arancelPrestacion1;

    // AUCAN1
    private int frecuenciaOCantidadPrestacion1;

    // AUODP1
    private String piezaoSectorDental1;

    // AUODC1
    private String caraDental1;

    // AUPRE2
    private int codigoPrestacion2;

    // AUTIP2
    private int tipoPrestacion2;

    // AUARA2
    private int arancelPrestacion2;

    // AUCAN2
    private int frecuenciaOCantidadPrestacion2;

    // AUODP2
    private String piezaOSectorDental2;

    // AUODC2
    private String caradental2;

    // AUPRE3
    private int codigoPrestacion3;

    // AUTIP3
    private int tipoPrestacion3;

    // AUARA3
    private int arancelPrestacion3;

    // AUCAN3
    private int frecuenciaOCantidadPrestacion3;

    // AUODP3
    private String piezaOSectorDental3;

    // AUODC3
    private String caraDental3;

    // AUPRE4
    private int codigoPrestacion4;

    // AUTIP4
    private int tipoPrestacion4;

    // AUARA4
    private int arancelPrestacion4;

    // AUCAN4
    private int frecuenciaOCantidadPrestacion4;

    // AUODP4
    private String piezaOSectorDental4;

    // AUODC4
    private String caraDental4;

    // AUTMAT
    private String tipoMatriculaPrestador;

    // AUNMAT
    private int numeroMatriculaPrestador;

    // AUFECP
    private LocalDate fechaPracticaOffLine;

    // AUDSM1
    private String diagnosticoPsicopatologia1;

    // AUDSM2
    private String diagnosticoPsicopatologia2;

    // AUFINA
    private int codigoFinalizacionTerapia;

    // AUCUEF
    private String cuitDelEfector;

    // AUNTRP
    private long numeroTransaccionPrestador;

    // AUIMAG
    private String nombreImagenProtocolo;

    // AUCUPR
    private String cuitPrescriptorDerivador;

    // AUFCVI
    private LocalDate fechaDesdeARecuperar;

    // AUFCVF
    private LocalDate fechaHastaARecuperar;


    // AUTCKS
    private int numeroTransaccionCierreARecuperar;

    // AUPRTx
    private String fillerCARE2;

    // AUGEN1
    private String marcaGenerico1;

    // AUGEN2
    private String marcaGenerico2;

    // AUGEN3
    private String marcaGenerico3;


    // AUTTIPR
    private String tipoPrescriptor;

    // AUFPUB
    private String formatoPublicacion;

    // AUPUVE
    private String cierreConsultaPorPrestadorOPorPuntoDeVenta;

    // AUTICO
    private String tipoDeConsulta;

    // AUPUBL
    private String publicarElPedidoDeConsultaCierre;

    // AUPECI
    private int periodoACerrar;

    // AUALB1
    private String numeroDeRegistroAlfabeta1;

    // AUALB2
    private String numeroDeRegistroAlfabeta2;

    // AUALB3
    private String numeroDeRegistroAlfabeta3;

    // AUFILL
    private String filler;

    // AUPPR1
    private String codigoPrestacionPrestador1;

    // AUPPR2
    private String codigoPrestacionPrestador2;

    // AUPPR3
    private String codigoPrestacionPrestador3;

    // AUPPR4
    private String codigoPrestacionPrestador4;


    
    @Deprecated
    // se mantiene por compatibilidad con mensajes al AS400
    @Field(offset = 27, length = 6, align = Align.RIGHT)
    public int getNumeroISOEmisorCredencial() {
        return numeroISOEmisorCredencial;
    }

    public void setNumeroISOEmisorCredencial(int p) {
        this.numeroISOEmisorCredencial = p;
    }

  
    @Deprecated
    // se mantiene por compatibilidad con el AS400
    @Field(offset = 44, length = 1)
    public int getDigitoVerificadorAsociado() {
        return digitoVerificadorAsociado;
    }

    public void setDigitoVerificadorAsociado(int p) {
        this.digitoVerificadorAsociado = p;
    }

    /**
     * Este valor puede ser o bien delegacion de la orden o bien el
     * tipoTratamientoOdontologico o bien el codigo de
     * 
     * @return
     */
    @Field(offset = 74, length = 2)
    public String getDelegacionOrden() {
        return delegacionOrden;
    }

    public void setDelegacionOrden(String p) {
        this.delegacionOrden = p;
    }

    @Field(offset = 76, length = 6)
    public int getNumeroAutorizacionReferencia() {
        return numeroAutorizacionReferencia;
    }

    public void setNumeroAutorizacionReferencia(int p) {
        this.numeroAutorizacionReferencia = p;
    }

    @Field(offset = 82, length = 6)
    public int getCodigoPrestacion1() {
        return codigoPrestacion1;
    }

    public void setCodigoPrestacion1(int p) {
        this.codigoPrestacion1 = p;
    }

    @Field(offset = 88, length = 1)
    public int getTipoPrestacion1() {
        return tipoPrestacion1;
    }

    public void setTipoPrestacion1(int p) {
        this.tipoPrestacion1 = p;
    }
    /**
     *Aca los valores posibles son 0,1,2,4,8
     *0 implica que se seleccionan los aranceles que idico el prestador. 
     *Solo se pueden indicar aranceles del tipo 1, 2,4 u 8. Las sumatorias de estos numeros solo se indicarian en la respuesta.
     */
    @Field(offset = 89, length = 1)
    public int getArancelPrestacion1() {
        return arancelPrestacion1;
    }

    public void setArancelPrestacion1(int p) {
        this.arancelPrestacion1 = p;
    }

    @Field(offset = 90, length = 2)
    public int getFrecuenciaOCantidadPrestacion1() {
        return frecuenciaOCantidadPrestacion1;
    }

    public void setFrecuenciaOCantidadPrestacion1(int p) {
        this.frecuenciaOCantidadPrestacion1 = p;
    }

    @Field(offset = 92, length = 2)
    public String getPiezaoSectorDental1() {
        return piezaoSectorDental1;
    }

    public void setPiezaoSectorDental1(String p) {
        this.piezaoSectorDental1 = p;
    }

    @Field(offset = 94, length = 5)
    public String getCaraDental1() {
        return caraDental1;
    }

    public void setCaraDental1(String p) {
        this.caraDental1 = p;
    }

    @Field(offset = 99, length = 6, align = Align.RIGHT, paddingChar = ' ')
    public int getCodigoPrestacion2() {
        return codigoPrestacion2;
    }

    public void setCodigoPrestacion2(int p) {
        this.codigoPrestacion2 = p;
    }

    @Field(offset = 105, length = 1)
    public int getTipoPrestacion2() {
        return tipoPrestacion2;
    }

    public void setTipoPrestacion2(int p) {
        this.tipoPrestacion2 = p;
    }

    @Field(offset = 106, length = 1)
    public int getArancelPrestacion2() {
        return arancelPrestacion2;
    }

    public void setArancelPrestacion2(int p) {
        this.arancelPrestacion2 = p;
    }

    @Field(offset = 107, length = 2)
    public int getFrecuenciaOCantidadPrestacion2() {
        return frecuenciaOCantidadPrestacion2;
    }

    public void setFrecuenciaOCantidadPrestacion2(int p) {
        this.frecuenciaOCantidadPrestacion2 = p;
    }

    @Field(offset = 109, length = 2)
    public String getPiezaOSectorDental2() {
        return piezaOSectorDental2;
    }

    public void setPiezaOSectorDental2(String p) {
        this.piezaOSectorDental2 = p;
    }

    @Field(offset = 111, length = 5)
    public String getCaradental2() {
        return caradental2;
    }

    public void setCaradental2(String p) {
        this.caradental2 = p;
    }

    @Field(offset = 116, length = 6)
    public int getCodigoPrestacion3() {
        return codigoPrestacion3;
    }

    public void setCodigoPrestacion3(int p) {
        this.codigoPrestacion3 = p;
    }

    @Field(offset = 122, length = 1)
    public int getTipoPrestacion3() {
        return tipoPrestacion3;
    }

    public void setTipoPrestacion3(int p) {
        this.tipoPrestacion3 = p;
    }

    @Field(offset = 123, length = 1)
    public int getArancelPrestacion3() {
        return arancelPrestacion3;
    }

    public void setArancelPrestacion3(int p) {
        this.arancelPrestacion3 = p;
    }

    @Field(offset = 124, length = 2)
    public int getFrecuenciaOCantidadPrestacion3() {
        return frecuenciaOCantidadPrestacion3;
    }

    public void setFrecuenciaOCantidadPrestacion3(int p) {
        this.frecuenciaOCantidadPrestacion3 = p;
    }

    @Field(offset = 126, length = 2)
    public String getPiezaOSectorDental3() {
        return piezaOSectorDental3;
    }

    public void setPiezaOSectorDental3(String p) {
        this.piezaOSectorDental3 = p;
    }

    @Field(offset = 128, length = 5)
    public String getCaraDental3() {
        return caraDental3;
    }

    public void setCaraDental3(String p) {
        this.caraDental3 = p;
    }

    @Field(offset = 133, length = 6)
    public int getCodigoPrestacion4() {
        return codigoPrestacion4;
    }

    public void setCodigoPrestacion4(int p) {
        this.codigoPrestacion4 = p;
    }

    @Field(offset = 139, length = 1)
    public int getTipoPrestacion4() {
        return tipoPrestacion4;
    }

    public void setTipoPrestacion4(int p) {
        this.tipoPrestacion4 = p;
    }

    @Field(offset = 140, length = 1)
    public int getArancelPrestacion4() {
        return arancelPrestacion4;
    }

    public void setArancelPrestacion4(int p) {
        this.arancelPrestacion4 = p;
    }

    @Field(offset = 141, length = 2)
    public int getFrecuenciaOCantidadPrestacion4() {
        return frecuenciaOCantidadPrestacion4;
    }

    public void setFrecuenciaOCantidadPrestacion4(int p) {
        this.frecuenciaOCantidadPrestacion4 = p;
    }

    @Field(offset = 143, length = 2)
    public String getPiezaOSectorDental4() {
        return piezaOSectorDental4;
    }

    public void setPiezaOSectorDental4(String p) {
        this.piezaOSectorDental4 = p;
    }

    @Field(offset = 145, length = 5)
    public String getCaraDental4() {
        return caraDental4;
    }

    public void setCaraDental4(String p) {
        this.caraDental4 = p;
    }

    @Field(offset = 150, length = 2)
    public String getTipoMatriculaPrestador() {
        return tipoMatriculaPrestador;
    }

    public void setTipoMatriculaPrestador(String p) {
        this.tipoMatriculaPrestador = p;
    }

    @Field(offset = 152, length = 6,align=Align.RIGHT,paddingChar='0',formatter = TrunkToZeroIntegerFormatter.class)
    public int getNumeroMatriculaPrestador() {
        return numeroMatriculaPrestador;
    }

    public void setNumeroMatriculaPrestador(int p) {
        this.numeroMatriculaPrestador = p;
    }

    @Field(offset = 158, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaPracticaOffLine() {
        return fechaPracticaOffLine;
    }

    public void setFechaPracticaOffLine(LocalDate p) {
        this.fechaPracticaOffLine = p;
    }

    @Field(offset = 166, length = 6)
    public String getDiagnosticoPsicopatologia1() {
        return diagnosticoPsicopatologia1;
    }

    public void setDiagnosticoPsicopatologia1(String p) {
        this.diagnosticoPsicopatologia1 = p;
    }

    @Field(offset = 172, length = 6)
    public String getDiagnosticoPsicopatologia2() {
        return diagnosticoPsicopatologia2;
    }

    public void setDiagnosticoPsicopatologia2(String p) {
        this.diagnosticoPsicopatologia2 = p;
    }

    @Field(offset = 178, length = 1)
    public int getCodigoFinalizacionTerapia() {
        return codigoFinalizacionTerapia;
    }

    public void setCodigoFinalizacionTerapia(int p) {
        this.codigoFinalizacionTerapia = p;
    }


    @Field(offset = 185, length = 11, formatter = CuitFormatter.class)
    public String getCuitDelEfector() {
        return cuitDelEfector;
    }

    public void setCuitDelEfector(String p) {
        this.cuitDelEfector = p;
    }

    @Field(offset = 196, length = 12, formatter = TrunkToZeroFormmatter.class)
    public long getNumeroTransaccionPrestador() {
        return numeroTransaccionPrestador;
    }

    public void setNumeroTransaccionPrestador(long p) {
        this.numeroTransaccionPrestador = p;
    }

    @Field(offset = 307, length = 30)
    public String getNombreImagenProtocolo() {
        return nombreImagenProtocolo;
    }

    public void setNombreImagenProtocolo(String p) {
        this.nombreImagenProtocolo = p;
    }

    @Field(offset = 337, length = 11, formatter = CuitFormatter.class)
    public String getCuitPrescriptorDerivador() {
        return cuitPrescriptorDerivador;
    }

    public void setCuitPrescriptorDerivador(String p) {
        this.cuitPrescriptorDerivador = p;
    }

    @Field(offset = 356, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaDesdeARecuperar() {
        return fechaDesdeARecuperar;
    }

    public void setFechaDesdeARecuperar(LocalDate p) {
        this.fechaDesdeARecuperar = p;
    }

    @Field(offset = 364, length = 8, formatter = PosLocalDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public LocalDate getFechaHastaARecuperar() {
        return fechaHastaARecuperar;
    }

    public void setFechaHastaARecuperar(LocalDate p) {
        this.fechaHastaARecuperar = p;
    }

   

    @Field(offset = 375, length = 10)
    public int getNumeroTransaccionCierreARecuperar() {
        return numeroTransaccionCierreARecuperar;
    }

    public void setNumeroTransaccionCierreARecuperar(int p) {
        this.numeroTransaccionCierreARecuperar = p;
    }

    @Field(offset = 385, length = 60)
    public String getFillerCARE2() {
        return fillerCARE2;
    }

    public void setFillerCARE2(String p) {
        this.fillerCARE2 = p;
    }

    @Field(offset = 445, length = 1,paddingChar='0',formatter=PosGenericoFormatter.class)
    public String getMarcaGenerico1() {
        return marcaGenerico1;
    }

    public void setMarcaGenerico1(String p) {
        this.marcaGenerico1 = p;
    }

    @Field(offset = 446, length = 1)
    public String getMarcaGenerico2() {
        return marcaGenerico2;
    }

    public void setMarcaGenerico2(String p) {
        this.marcaGenerico2 = p;
    }

    @Field(offset = 447, length = 1)
    public String getMarcaGenerico3() {
        return marcaGenerico3;
    }

    public void setMarcaGenerico3(String p) {
        this.marcaGenerico3 = p;
    }


    
    @Field(offset = 465, length = 1)
    public String getTipoPrescriptor() {
        return tipoPrescriptor;
    }

    public void setTipoPrescriptor(String p) {
        this.tipoPrescriptor = p;
    }

    @Field(offset = 466, length = 1)
    public String getFormatoPublicacion() {
        return formatoPublicacion;
    }

    public void setFormatoPublicacion(String p) {
        this.formatoPublicacion = p;
    }

    @Field(offset = 467, length = 1)
    public String getCierreConsultaPorPrestadorOPorPuntoDeVenta() {
        return cierreConsultaPorPrestadorOPorPuntoDeVenta;
    }

    public void setCierreConsultaPorPrestadorOPorPuntoDeVenta(String p) {
        this.cierreConsultaPorPrestadorOPorPuntoDeVenta = p;
    }

    @Field(offset = 468, length = 1)
    public String getTipoDeConsulta() {
        return tipoDeConsulta;
    }

    public void setTipoDeConsulta(String p) {
        this.tipoDeConsulta = p;
    }

    @Field(offset = 469, length = 1)
    public String getPublicarElPedidoDeConsultaCierre() {
        return publicarElPedidoDeConsultaCierre;
    }

    public void setPublicarElPedidoDeConsultaCierre(String p) {
        this.publicarElPedidoDeConsultaCierre = p;
    }

    @Field(offset = 470, length = 6,formatter = TrunkToZeroIntegerFormatter.class)
    public int getPeriodoACerrar() {
        return periodoACerrar;
    }

    public void setPeriodoACerrar(int p) {
        this.periodoACerrar = p;
    }

    //TODO pasar esto a PosMessageFarmacia
    @Field(offset = 476, length = 6)
    public String getNumeroDeRegistroAlfabeta1() {
        return numeroDeRegistroAlfabeta1;
    }

    public void setNumeroDeRegistroAlfabeta1(String p) {
        this.numeroDeRegistroAlfabeta1 = p;
    }

    @Field(offset = 482, length = 6)
    public String getNumeroDeRegistroAlfabeta2() {
        return numeroDeRegistroAlfabeta2;
    }

    public void setNumeroDeRegistroAlfabeta2(String p) {
        this.numeroDeRegistroAlfabeta2 = p;
    }

    @Field(offset = 488, length = 6)
    public String getNumeroDeRegistroAlfabeta3() {
        return numeroDeRegistroAlfabeta3;
    }

    public void setNumeroDeRegistroAlfabeta3(String p) {
        this.numeroDeRegistroAlfabeta3 = p;
    }

    // codigo prestacion prestador 4/15
    @Field(offset = 494, length = 156)
    public String getFiller() {
        return filler;
    }

    public void setFiller(String p) {
        this.filler = p;
    }

    // AUISOA
    private int numeroISOEmisorCredencial;
    /*
     * //AUFILA private int prefijoAsociado;
     * 
     * //AUASOA private int numeroAsociado;
     * 
     * //AUBENA private int numeroBeneficiario;
     */
    // AUDIGA
    private int digitoVerificadorAsociado;
    /*
     * //AUTRAC private int codigoTransaccion;
     * 
     * //AUATRC private String atributoCodigoTransaccion;
     */


    // Este campo si bien es un numero puede venir con valores no numericos
    // AUCASO
    private String numeroCaso;

    @Field(offset = 494, length = 15)
    public String getCodigoPrestacionPrestador1() {
        return codigoPrestacionPrestador1;
    }

    public void setCodigoPrestacionPrestador1(String codigoPrestacionPrestador1) {
        this.codigoPrestacionPrestador1 = codigoPrestacionPrestador1;
    }

    @Field(offset = 509, length = 15)
    public String getCodigoPrestacionPrestador2() {
        return codigoPrestacionPrestador2;
    }

    public void setCodigoPrestacionPrestador2(String codigoPrestacionPrestador2) {
        this.codigoPrestacionPrestador2 = codigoPrestacionPrestador2;
    }

    @Field(offset = 524, length = 15)
    public String getCodigoPrestacionPrestador3() {
        return codigoPrestacionPrestador3;
    }

    public void setCodigoPrestacionPrestador3(String codigoPrestacionPrestador3) {
        this.codigoPrestacionPrestador3 = codigoPrestacionPrestador3;
    }

    @Field(offset = 539, length = 15)
    public String getCodigoPrestacionPrestador4() {
        return codigoPrestacionPrestador4;
    }

    public void setCodigoPrestacionPrestador4(String codigoPrestacionPrestador4) {
        this.codigoPrestacionPrestador4 = codigoPrestacionPrestador4;
    }

    @Field(offset = 2043, length = 6)    
    public String getNumeroCaso() {
        return numeroCaso;
    }
    
    public void setNumeroCaso(String numeroCaso) {
        this.numeroCaso = numeroCaso;
    }
    
    
    
}
