package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.BadFormatException;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.filter.estrategy.FilterStrategy;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;

import com.ancientprogramming.fixedformat4j.format.ParseException;

/**
 * Handler que filtra los mensajes. Evalua que alguna de las condiciones se
 * cumpla. Con que se cumpla una de las condiciones se filtra el mensaje.
 * 
 * @author VA27789605
 * 
 */
public class FilteredMessageHandler extends PosMessageHandlerInterceptor {

    public static final String MY_NAME = FilteredMessageHandler.class.getSimpleName();
    /**
     * Con que una de las condiciones se aplique se cumple el filtro.
     */
    private List<PosMessageFilter> filters;

    /**
     * Estrategias a utilizar sobre el mensaje filtrado. Que hace con los
     * mensajes que no pasaron el filtro
     */
    private List<FilterStrategy> filterStrategyList;
    private TransaccionServiceStatistics statistics;

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.pos.handlers.PosMessageHandler#process(javax
     * .jms.Message)
     */
    public RespuestaCentroValidador process(Message message) throws UnsupportedPosMessageException {
        long init = System.currentTimeMillis();
        String stringMessage = this.getStrMessage(message);

        SocioPosMessageBase posMessageHeader;

        try {
            posMessageHeader = this.getFormatManager().load(SocioPosMessageBase.class, stringMessage);
        } catch (ParseException e) {
            throw new BadFormatException(e);
        }

        try {

            for (PosMessageFilter filter : filters) {
                filter.validate(posMessageHeader);
            }
        } catch (FilteredMessageException e) {
            long end = System.currentTimeMillis();
            List<HandlerTime> handlersTime = new ArrayList<HandlerTime>();
            handlersTime.add(new HandlerTime(MY_NAME, end - init));
            StringBuilder resolucionFiltros = new StringBuilder();
            for (FilterStrategy filterStrategy : getFilterStrategyList()) {
                long initFilteredTime = System.currentTimeMillis();
                filterStrategy.doFilterAction(message);
                long endFilteredTime = System.currentTimeMillis() - initFilteredTime;
                resolucionFiltros.append(filterStrategy.getResolucionEstrategia()).append(" ");
                handlersTime.add(new HandlerTime(filterStrategy.getClass().getSimpleName(), endFilteredTime));
            }

            FilteredMessageException exception = new FilteredMessageException(e.getMessage() + resolucionFiltros.toString()
                    + " | Mensaje: " + stringMessage.substring(0, 49), posMessageHeader, handlersTime);
            getLog().debug("Mensaje Filtrado: " + exception.getMessage());
            throw exception;
        }
        // TODO: No es intercepted, esta al final. Hay que arreglar eso
        long end = System.currentTimeMillis();
        RespuestaCentroValidador resposne = getIntercepted().process(message);
        resposne.addHandlerTime(new HandlerTime(MY_NAME, end - init));
        return resposne;
    }

    /**
     * Retorna los filtros que componen el handler
     */
    public List<PosMessageFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<PosMessageFilter> filters) {
        this.filters = filters;
    }

    public List<FilterStrategy> getFilterStrategyList() {
        return filterStrategyList;
    }

    public void setFilterStrategyList(List<FilterStrategy> filterStrategyList) {
        this.filterStrategyList = filterStrategyList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.pos.handlers.PosMessageHandlerInterceptor
     * #getHierarchyName()
     */
    public String getHierarchyName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("FilteredMessageHandler\n");
        strBuff.append("\t").append(this.getIntercepted().getHierarchyName());
        return strBuff.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.pos.handlers.PosMessageHandler#getMessageFilters
     * ()
     */
    public List<PosMessageFilter> getMessageFilters() {
        List<PosMessageFilter> messageFilters = new ArrayList<PosMessageFilter>();
        messageFilters.addAll(this.filters);
        messageFilters.addAll(this.getIntercepted().getMessageFilters());
        return messageFilters;
    }

    public void setStatistics(TransaccionServiceStatistics statistics) {
        this.statistics = statistics;

    }

    public TransaccionServiceStatistics getStatitstics() {
        return this.statistics;
    }

    protected String getStrMessage(Message message) {
        try {
            String stringMessage = ((TextMessage) message).getText();
            return stringMessage;
        } catch (JMSException e1) {
            throw new UnsupportedPosMessageException(e1.getMessage());
        }
    }

    private Log getLog() {
        return LogFactory.getLog(FilteredMessageHandler.class);
    }

}
