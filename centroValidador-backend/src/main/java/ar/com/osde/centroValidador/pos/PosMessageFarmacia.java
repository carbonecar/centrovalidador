package ar.com.osde.centroValidador.pos;

import java.math.BigDecimal;
import java.util.Date;

import ar.com.osde.centroValidador.formatter.LongFormatterTrimmed;
import ar.com.osde.centroValidador.formatter.ReplaceSpaceWithCerosBigDecimalFormatter;
import ar.com.osde.centroValidador.pos.utils.PosDateFormatter;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatDecimal;
import com.ancientprogramming.fixedformat4j.annotation.FixedFormatPattern;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Mensaje para las transacciones 1F,2F,2J generico
 * 
 * @author VA27789605
 * 
 */
@Record
public class PosMessageFarmacia extends PosMessage {

    private static final long serialVersionUID = 1L;

    // AUTRO1
    private int codigoTroquelMedicamento1;

    // AUBAR1
    private long codigoBarrasMedicamento1;

    // AUCAF1
    private int cantidadMedicamento1;

    // AUPRC1
    private BigDecimal precio1;

    // AUTRO2
    private int codigoTroquelMedicamento2;

    // AUBAR2
    private long codigoBarrasMedicamento2;

    // AUCAF2
    private int cantidadMedicamento2;

    // AUPRC2
    private BigDecimal precio2;

    // AUTRO3
    private int codigoTroquelMedicamento3;

    // AUBAR3
    private long codigoBarrasMedicamento3;

    // AUCAF3
    private int cantidadMedicamento3;

    // AUPRC3
    private BigDecimal precio3;

    // AUFREC
    private Date fechaReceta;

    // AUFVEN
    private String formaDeVenta;

    // AUCRON
    private String coberturaEspecialOCondicionDelSocio;

    // AUTIPA
    private int fillerCARE1;


    /**
     * 
     * @return
     */
    @Field(offset = 208, length = 7, align = Align.RIGHT)
    public int getCodigoTroquelMedicamento1() {
        return codigoTroquelMedicamento1;
    }

    public void setCodigoTroquelMedicamento1(int p) {
        this.codigoTroquelMedicamento1 = p;
    }

    @Field(offset = 215, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento1() {
        return codigoBarrasMedicamento1;
    }

    public void setCodigoBarrasMedicamento1(long p) {
        this.codigoBarrasMedicamento1 = p;
    }

    @Field(offset = 228, length = 2)
    public int getCantidadMedicamento1() {
        return cantidadMedicamento1;
    }

    public void setCantidadMedicamento1(int p) {
        this.cantidadMedicamento1 = p;
    }

    @Field(offset = 230, length = 11, align = Align.RIGHT,formatter=ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecio1() {
        return precio1;
    }

    public void setPrecio1(BigDecimal p) {
        this.precio1 = p;
    }

    @Field(offset = 241, length = 7)
    public int getCodigoTroquelMedicamento2() {
        return codigoTroquelMedicamento2;
    }

    public void setCodigoTroquelMedicamento2(int p) {
        this.codigoTroquelMedicamento2 = p;
    }

    @Field(offset = 248, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento2() {
        return codigoBarrasMedicamento2;
    }

    public void setCodigoBarrasMedicamento2(long p) {
        this.codigoBarrasMedicamento2 = p;
    }

    @Field(offset = 261, length = 2)
    public int getCantidadMedicamento2() {
        return cantidadMedicamento2;
    }

    public void setCantidadMedicamento2(int p) {
        this.cantidadMedicamento2 = p;
    }

    @Field(offset = 263, length = 11, align = Align.RIGHT,formatter=ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecio2() {
        return precio2;
    }

    public void setPrecio2(BigDecimal p) {
        this.precio2 = p;
    }

    @Field(offset = 274, length = 7)
    public int getCodigoTroquelMedicamento3() {
        return codigoTroquelMedicamento3;
    }

    public void setCodigoTroquelMedicamento3(int p) {
        this.codigoTroquelMedicamento3 = p;
    }

    @Field(offset = 281, length = 13, formatter = LongFormatterTrimmed.class)
    public long getCodigoBarrasMedicamento3() {
        return codigoBarrasMedicamento3;
    }

    public void setCodigoBarrasMedicamento3(long p) {
        this.codigoBarrasMedicamento3 = p;
    }

    @Field(offset = 294, length = 2)
    public int getCantidadMedicamento3() {
        return cantidadMedicamento3;
    }

    public void setCantidadMedicamento3(int p) {
        this.cantidadMedicamento3 = p;
    }

    @Field(offset = 296, length = 11, align = Align.RIGHT,formatter=ReplaceSpaceWithCerosBigDecimalFormatter.class)
    @FixedFormatDecimal(decimals = 2)
    public BigDecimal getPrecio3() {
        return precio3;
    }

    public void setPrecio3(BigDecimal p) {
        this.precio3 = p;
    }

    @Field(offset = 372, length = 2)
    public String getCoberturaEspecialOCondicionDelSocio() {
        return coberturaEspecialOCondicionDelSocio;
    }

    public void setCoberturaEspecialOCondicionDelSocio(String p) {
        this.coberturaEspecialOCondicionDelSocio = p;
    }

    @Field(offset = 374, length = 1)
    public int getFillerCARE1() {
        return fillerCARE1;
    }

    public void setFillerCARE1(int p) {
        this.fillerCARE1 = p;
    }
    @Field(offset = 455, length = 8, formatter = PosDateFormatter.class)
    @FixedFormatPattern("yyyyMMdd")
    public Date getFechaReceta() {
        return fechaReceta;
    }

    public void setFechaReceta(Date p) {
        this.fechaReceta = p;
    }

    @Field(offset = 463, length = 2)
    public String getFormaDeVenta() {
        return formaDeVenta;
    }

    public void setFormaDeVenta(String p) {
        this.formaDeVenta = p;
    }

}
