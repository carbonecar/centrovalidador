package ar.com.osde.centroValidador.pos;

import org.apache.log4j.Logger;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;

public class PosMessageResponse {
    public static final String EBCDIC_ENCODING = "Cp1047";
    private static Logger LOG = Logger.getLogger(PosMessageResponse.class);
    private String codigoInterno;
    private String codigoOperador;
    private int filialInstalacionPOS;
    private int delegacionInstalacionPOS;
    private int numeroPOS;
    private int nroTransaccion;
    private int nroAutorizacion;
    private String firmaGeneradorAutorizaciones;
    private String nroInternoPos;
    private String codigoRespuestaTransaccion;
    private String leyenda;
    private String leyendaImpresion = "";
    private double montoTransaccion;
    private int nroSecuencia;
    private int totalMensajes;

    private int inconsistencia1;
    private int inconsistencia2;
    private String observacionesSolicitudProtesis;

    private int fechaNacimiento;
    private String descripcionPrestacion1;
    private int arancelPrestacion1;
    private String descripcionPrestacion2;
    private int arancelPrestacion2;
    private String descripcionPrestacion3;
    private int arancelPrestacion3;
    private String descripcionPrestacion4;
    private int arancelPrestacion4;
    private String codigoCronico;
    private char cubrePlanMaterno = 'N';

    private String observaciones;
    private String codigoCobertura1;
    private String codigoCobertura2;
    private String codigoCobertura3;

    private double bonifacionAOsde1;
    private char indicadorBonificacion;

    private double bonifacionAOsde2;
    private double bonifacionAOsde3;

    private String filler = "FILER";
    private String nombreAsociado;
    private char sexoAsociado = ' ';

    private String leyendaImpresionRenglon1;
    private String leyendaImpresionRenglon2;
    private String leyendaImpresionRenglon3;
    private String leyendaImpresionRenglon4;

    // *********Items*****************//

    // AURES1
    private String codigoRespuestaItem1;
    // AURES2
    private String codigoRespuestaItem2;
    // AURES3
    private String codigoRespuestaItem3;

    PosMessageResponse() {

    }

    @Field(offset = 85, length = 20)
    public String getLeyendaImpresionRenglon1() {
        return leyendaImpresionRenglon1;
    }

    public void setLeyendaImpresionRenglon1(String leyendaImpresionRenglon1) {
        this.leyendaImpresionRenglon1 = leyendaImpresionRenglon1;
    }

    @Field(offset = 105, length = 20)
    public String getLeyendaImpresionRenglon2() {
        return leyendaImpresionRenglon2;
    }

    public void setLeyendaImpresionRenglon2(String leyendaImpresionRenglon2) {
        this.leyendaImpresionRenglon2 = leyendaImpresionRenglon2;
    }

    @Field(offset = 125, length = 20)
    public String getLeyendaImpresionRenglon3() {
        return leyendaImpresionRenglon3;
    }

    public void setLeyendaImpresionRenglon3(String leyendaImpresionRenglon3) {
        this.leyendaImpresionRenglon3 = leyendaImpresionRenglon3;
    }

    @Field(offset = 145, length = 20)
    public String getLeyendaImpresionRenglon4() {
        return leyendaImpresionRenglon4;
    }

    public void setLeyendaImpresionRenglon4(String leyendaImpresionRenglon4) {
        this.leyendaImpresionRenglon4 = leyendaImpresionRenglon4;
    }

    @Field(offset = 1, length = 5)
    public String getCodigoInterno() {
        return this.codigoInterno;
    }

    public void setCodigoInterno(String p) {
        this.codigoInterno = p;
    }

    @Field(offset = 6, length = 2)
    public String getCodigoOperador() {
        return this.codigoOperador;
    }

    public void setCodigoOperador(String p) {
        this.codigoOperador = p;
    }

    @Field(offset = 8, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getFilialInstalacionPOS() {
        return this.filialInstalacionPOS;
    }

    public void setFilialInstalacionPOS(int p) {
        this.filialInstalacionPOS = p;
    }

    @Field(offset = 10, length = 2, paddingChar = '0', align = Align.RIGHT)
    public int getDelegacionInstalacionPOS() {
        return this.delegacionInstalacionPOS;
    }

    public void setDelegacionInstalacionPOS(int p) {
        this.delegacionInstalacionPOS = p;
    }

    @Field(offset = 12, length = 4, align = Align.RIGHT, paddingChar = '0')
    public int getNumeroPOS() {
        return this.numeroPOS;
    }

    public void setNumeroPOS(int p) {
        this.numeroPOS = p;
    }

    /**
     * Numero de orden que se recupero al operar con la transacción
     * 
     * @return
     */
    @Field(offset = 16, length = 6, paddingChar = '0', align = Align.RIGHT)
    public int getNroTransaccion() {
        return this.nroTransaccion;
    }

    public void setNroTransaccion(int nroTransaccion) {
        this.nroTransaccion = nroTransaccion;
    }

    @Field(offset = 22, length = 6, paddingChar = '0', align = Align.RIGHT)
    /**
     * Numero de autorizacion, solicitud o trx anterior
     */
    public int getNroAutorizacion() {
        return this.nroAutorizacion;
    }

    @Field(offset = 28, length = 2)
    public String getFirmaGeneradorAutorizaciones() {
        return this.firmaGeneradorAutorizaciones;
    }

    public void setFirmaGeneradorAutorizaciones(String firma) {
        this.firmaGeneradorAutorizaciones = firma;
    }

    @Field(offset = 30, length = 5, align = Align.RIGHT, paddingChar = '0')
    public String getNroInternoPos() {
        return this.nroInternoPos;
    }

    public void setNroInternoPos(String nroInternoPos) {
        try {
            this.nroInternoPos = Integer.valueOf(nroInternoPos).toString();
        } catch (NumberFormatException ex) {
            this.nroInternoPos = new String("37244");
            LOG.error(ex.getMessage());
        }
    }

    @Field(offset = 35, length = 2)
    public String getCodigoRespuestaTransaccion() {
        return this.codigoRespuestaTransaccion;
    }

    public void setCodigoRespuestaTransaccion(String codigoRespuestaTransaccion) {
        this.codigoRespuestaTransaccion = codigoRespuestaTransaccion;
    }

    @Field(offset = 45, length = 40)
    public String getLeyendaDesplegar() {
        return this.leyenda;
    }

    public void setLeyendaDesplegar(String leyenda) {
        this.leyenda = leyenda;
    }

    @Field(offset = 165, length = 11, paddingChar = '0')
    public double getMontoTransaccion() {
        return this.montoTransaccion;
    }

    public void setMontoTransaccion(double montoTransaccion) {
        this.montoTransaccion = montoTransaccion;
    }

    @Field(offset = 176, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getNroSecuencia() {
        return this.nroSecuencia;
    }

    public void setNroSecuencia(int nroSecuencia) {
        this.nroSecuencia = nroSecuencia;
    }

    @Field(offset = 178, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getTotalMensajes() {
        return this.totalMensajes;
    }

    public void setTotalMensajes(int totalMensajes) {
        this.totalMensajes = totalMensajes;
    }

    @Field(offset = 186, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getInconsistencia1() {
        return inconsistencia1;
    }

    public void setInconsistencia1(int inconsistencia1) {
        this.inconsistencia1 = inconsistencia1;
    }

    @Field(offset = 188, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getInconsistencia2() {
        return inconsistencia2;
    }

    public void setInconsistencia2(int inconsistencia2) {
        this.inconsistencia2 = inconsistencia2;
    }

    @Field(offset = 190, length = 40)
    public String getObservacionesSolicitudProtesis() {
        return observacionesSolicitudProtesis;
    }

    public void setObservacionesSolicitudProtesis(String observaciones) {
        this.observacionesSolicitudProtesis = observaciones;
    }

    @Field(offset = 240, length = 30)
    public String getNombreAsociado() {
        return nombreAsociado;
    }

    public void setNombreAsociado(String nombreAsociado) {
        this.nombreAsociado = nombreAsociado;
    }

    @Field(offset = 270, length = 1, paddingChar = ' ')
    public char getSexoAsociado() {
        return this.sexoAsociado;
    }

    public void setSexoAsociado(char sexo) {
        this.sexoAsociado = sexo;
    }

    @Field(offset = 271, length = 8, paddingChar = '0')
    public int getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(int fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Field(offset = 279, length = 26, paddingChar = ' ')
    public String getDescripcionPrestacion1() {
        return descripcionPrestacion1;
    }

    public void setDescripcionPrestacion1(String descripcionPrestacion1) {
        this.descripcionPrestacion1 = descripcionPrestacion1;
    }

    @Field(offset = 305, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getArancelPrestacion1() {
        return arancelPrestacion1;
    }

    public void setArancelPrestacion1(int arancelPrestacion1) {
        this.arancelPrestacion1 = arancelPrestacion1;
    }

    @Field(offset = 307, length = 26, paddingChar = ' ')
    public String getDescripcionPrestacion2() {
        return descripcionPrestacion2;
    }

    public void setDescripcionPrestacion2(String descripcionPrestacion2) {
        this.descripcionPrestacion2 = descripcionPrestacion2;
    }

    @Field(offset = 333, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getArancelPrestacion2() {
        return arancelPrestacion2;
    }

    public void setArancelPrestacion2(int arancelPrestacion2) {
        this.arancelPrestacion2 = arancelPrestacion2;
    }

    @Field(offset = 335, length = 26, paddingChar = ' ')
    public String getDescripcionPrestacion3() {
        return descripcionPrestacion3;
    }

    public void setDescripcionPrestacion3(String descripcionPrestacion3) {
        this.descripcionPrestacion3 = descripcionPrestacion3;
    }

    @Field(offset = 361, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getArancelPrestacion3() {
        return arancelPrestacion3;
    }

    public void setArancelPrestacion3(int arancelPrestacion3) {
        this.arancelPrestacion3 = arancelPrestacion3;
    }

    @Field(offset = 363, length = 26)
    public String getDescripcionPrestacion4() {
        return descripcionPrestacion4;
    }

    public void setDescripcionPrestacion4(String descripcionPrestacion4) {
        this.descripcionPrestacion4 = descripcionPrestacion4;
    }

    @Field(offset = 389, length = 2, align = Align.RIGHT, paddingChar = '0')
    public int getArancelPrestacion4() {
        return arancelPrestacion4;
    }

    public void setArancelPrestacion4(int arancelPrestacion4) {
        this.arancelPrestacion4 = arancelPrestacion4;
    }

    @Field(offset = 391, length = 3)
    public String getCodigoCronico() {
        return codigoCronico;
    }

    public void setCodigoCronico(String codigoCronico) {
        this.codigoCronico = codigoCronico;
    }

    @Field(offset = 394, length = 1)
    public char getCubrePlanMaterno() {
        return cubrePlanMaterno;
    }

    public void setCubrePlanMaterno(char cubrePlanMaterno) {
        this.cubrePlanMaterno = cubrePlanMaterno;
    }

    @Field(offset = 584, length = 40)
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Field(offset = 624, length = 2)
    public String getCodigoCobertura1() {
        return codigoCobertura1;
    }

    public void setCodigoCobertura1(String codigoCobertura1) {
        this.codigoCobertura1 = codigoCobertura1;
    }

    @Field(offset = 626, length = 2)
    public String getCodigoCobertura2() {
        return codigoCobertura2;
    }

    public void setCodigoCobertura2(String codigoCobertura2) {
        this.codigoCobertura2 = codigoCobertura2;
    }

    @Field(offset = 628, length = 2)
    public String getCodigoCobertura3() {
        return codigoCobertura3;
    }

    public void setCodigoCobertura3(String codigoCobertura3) {
        this.codigoCobertura3 = codigoCobertura3;
    }

    @Field(offset = 630, length = 5, align = Align.RIGHT, paddingChar = '0')
    public double getBonifacionAOsde1() {
        return bonifacionAOsde1;
    }

    public void setBonifacionAOsde1(double bonifacionAOsde1) {
        this.bonifacionAOsde1 = bonifacionAOsde1;
    }

    @Field(offset = 635, length = 1)
    public char getIndicadorBonificacion() {
        return indicadorBonificacion;
    }

    public void setIndicadorBonificacion(char indicadorBonificacion) {
        this.indicadorBonificacion = indicadorBonificacion;
    }

    @Field(offset = 636, length = 5)
    public double getBonifacionAOsde2() {
        return bonifacionAOsde2;
    }

    public void setBonifacionAOsde2(double bonifacionAOsde2) {
        this.bonifacionAOsde2 = bonifacionAOsde2;
    }

    @Field(offset = 641, length = 5)
    public double getBonifacionAOsde3() {
        return bonifacionAOsde3;
    }

    public void setBonifacionAOsde3(double bonifacionAOsde3) {
        this.bonifacionAOsde3 = bonifacionAOsde3;
    }

    @Field(offset = 646, length = 4)
    // La longitud es de 5, pero se pone en 4 para poder generar el mensaje de
    // 650
    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public void setNroAutorizacion(int nroAutorizacion) {
        this.nroAutorizacion = nroAutorizacion;
    }

    @Field(offset = 37, length = 2)
    public String getCodigoRespuestaItem1() {
        return codigoRespuestaItem1;
    }

    public void setCodigoRespuestaItem1(String codigoRespuestaMedicamento1) {
        this.codigoRespuestaItem1 = codigoRespuestaMedicamento1;
    }

    @Field(offset = 39, length = 2)
    public String getCodigoRespuestaItem2() {
        return codigoRespuestaItem2;
    }

    public void setCodigoRespuestaItem2(String codigoRespuestaMedicamento2) {
        this.codigoRespuestaItem2 = codigoRespuestaMedicamento2;
    }

    @Field(offset = 41, length = 2)
    public String getCodigoRespuestaItem3() {
        return codigoRespuestaItem3;
    }

    public void setCodigoRespuestaItem3(String codigoRespuestaMedicamento3) {
        this.codigoRespuestaItem3 = codigoRespuestaMedicamento3;
    }

    public void setLeyendaImpresionRenglon1(String leyenda, int index) {
        switch (index) {
        case 1:
            setLeyendaImpresionRenglon1(leyenda);
            break;
        case 2:
            setLeyendaImpresionRenglon2(leyenda);
            break;
        case 3:
            setLeyendaImpresionRenglon3(leyenda);
            break;
        case 4:
            setLeyendaImpresionRenglon4(leyenda);
            break;
        default:
            break;
        }

    }
}
