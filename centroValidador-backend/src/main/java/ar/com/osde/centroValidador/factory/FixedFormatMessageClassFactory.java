package ar.com.osde.centroValidador.factory;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.BloqueoDesbloqueoPosMessage;
import ar.com.osde.centroValidador.pos.CierreFarmaciaPosMessage;
import ar.com.osde.centroValidador.pos.ConsultaFarmaciaPosMessage;
import ar.com.osde.centroValidador.pos.IngresoEgresoPosMessage;
import ar.com.osde.centroValidador.pos.MarcaGenericoPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.PosMessageFarmacia;
import ar.com.osde.centroValidador.pos.PosMessageRescate;
import ar.com.osde.centroValidador.pos.ProtocoloPosMessage;
import ar.com.osde.centroValidador.pos.TipoCuatroPosMessage;
import ar.com.osde.cv.model.CodigoTransaccion;
import ar.com.osde.cv.model.TransaccionFactory;

/**
 * Para devolver la clase particular en funcion del mensaje que queremos
 * levantar.
 * 
 * 
 */
public class FixedFormatMessageClassFactory {

    // TODO hacer esto bien, apesta!
    public static Class<? extends AbstractPosMessage> getClassInstance(String message) {
        String tipoTransaccion = message.substring(45, 47);
        CodigoTransaccion codigoTransaccion = new CodigoTransaccion(Integer.parseInt(tipoTransaccion.substring(0, 1)),
                tipoTransaccion.substring(1, 2));
        if (codigoTransaccion.equals(TransaccionFactory.O1F) || codigoTransaccion.equals(TransaccionFactory.O2F)
                || codigoTransaccion.equals(TransaccionFactory.O2J)) {
            return PosMessageFarmacia.class;
        }

        if (TransaccionFactory.O4A.equals(codigoTransaccion)) {
            return TipoCuatroPosMessage.class;
        }

        if (TransaccionFactory.O4D.equals(codigoTransaccion) || TransaccionFactory.O4B.equals(codigoTransaccion)) {
            return BloqueoDesbloqueoPosMessage.class;
        }
        // int string
        // codigo, atributo
        if (codigoTransaccion.equals(TransaccionFactory.O5F)) {
            return CierreFarmaciaPosMessage.class;
        }

        if (TransaccionFactory.O3F.equals(codigoTransaccion)) {
            return ConsultaFarmaciaPosMessage.class;
        }

        if (TransaccionFactory.O2I.equals(codigoTransaccion) || TransaccionFactory.O2E.equals(codigoTransaccion)) {
            return IngresoEgresoPosMessage.class;
        }
        
        if(TransaccionFactory.O4G.equals(codigoTransaccion)){
            return MarcaGenericoPosMessage.class;
        }
        
        if(TransaccionFactory.O3A.equals(codigoTransaccion)){
            return PosMessageRescate.class;
        }
        
        if(TransaccionFactory.O2P.equals(codigoTransaccion)){
            return ProtocoloPosMessage.class;
        }
        return PosMessage.class;

    }
}
