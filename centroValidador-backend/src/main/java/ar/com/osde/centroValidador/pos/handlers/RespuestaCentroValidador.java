package ar.com.osde.centroValidador.pos.handlers;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.PosMessageResponse;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.CodigoRespuestaTransaccion;
import ar.com.osde.entities.as400.Socio;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.rules.paralelo.ResponseMatcher;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

public class RespuestaCentroValidador {

	private static final long serialVersionUID = 1L;
	// decorado
	private RespuestaTransaccion decoratee;
	private AbstractPosMessage posMessage;
	private AbstractTransaccionPos transaccionPos;
	private Transaccion transaccion;
	private PosMessageResponse posMessageResponse;
	// Respuesta generada como string.
	private String responseString;

	private boolean respuestaParcial;

	private List<HandlerTime> handlersTime = new ArrayList<HandlerTime>();
	private String systemName;


	
	private ResponseMatcher responseMatcher;
	private String requestString;
	
	
	

	public ResponseMatcher getResponseMatcher() {
		return responseMatcher;
	}

	public void setResponseMatcher(ResponseMatcher responseMatcher) {
		this.responseMatcher = responseMatcher;
	}

	public void addHandlerTime(HandlerTime handlerTime) {
		this.handlersTime.add(handlerTime);
	}

	public List<HandlerTime> getHandlersTime() {
		return handlersTime;
	}

	public void setIsRespuestaParcial(boolean isRespuestaParcial) {
		this.respuestaParcial = isRespuestaParcial;
	}

	public boolean isRespuestaParcial() {
		return this.respuestaParcial;
	}

	
	public String getSystemName() {
		return systemName;
	}
	
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public RespuestaCentroValidador(RespuestaTransaccion respuesta, AbstractPosMessage posMessage,
	        AbstractTransaccionPos transaccion) {
		this.decoratee = respuesta;
		this.posMessage = posMessage;
		this.transaccionPos = transaccion;
		this.transaccion = transaccion.getTransaccion();
	}

	public RespuestaCentroValidador(RespuestaTransaccion respuestaTransaccion) {
		this.decoratee = respuestaTransaccion;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public Transaccion getTransaccionXML() {
		return transaccion;
	}

	/**
	 * Transaccion que inicio o completo el request
	 * 
	 * @return
	 */
	public AbstractTransaccionPos getTransaccion() {
		return this.transaccionPos;
	}

	public CodigoRespuestaTransaccion getRespuestaTransaccion() {

		if (this.isRespuestaParcial()) {
			return CodigoRespuestaTransaccion.RESPUESTA_PARCIAL;
		} else {
			return this.getDecoratee().getRespuestaAptitud().isApto() ? CodigoRespuestaTransaccion.OK
			        : CodigoRespuestaTransaccion.FAIL;
		}
	}

	public boolean isApto() {
		return decoratee.getRespuestaAptitud().isApto();
	}

	/**
	 * Mensaje que inicio el request
	 * 
	 * @return
	 */
	public AbstractPosMessage getPosMessage() {
		return posMessage;
	}

	public void setPosMessage(PosMessage posMessage) {
		this.posMessage = posMessage;
	}

	public RespuestaTransaccion getDecoratee() {
		return this.decoratee;
	}

	public Socio getSocio() {
		if (this.getDecoratee().getRespuestaAptitud().getSocio() != null) {
			return this.getDecoratee().getRespuestaAptitud().getSocio().getSocio();
		}
		return null;
	}

	public PosMessageResponse getPosMessageResponse() {
		return posMessageResponse;
	}

	public void setPosMessageResponse(PosMessageResponse posMessageResponse) {
		this.posMessageResponse = posMessageResponse;
	}

	public void processLoguer(LoggerMessageRecordHandler loggerMessageRecordHandler) {
		loggerMessageRecordHandler.logMe(this);
		
	}

	public void setRequestString(String mensajeTxt) {
		this.requestString=mensajeTxt;
		
	}
	
	public String getRequestString() {
		return requestString;
	}

}
