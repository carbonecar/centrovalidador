package ar.com.osde.centroValidador.pos.exception;

import java.util.List;

import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;

/**
 * Exception para marcar que algun servicio dio time out
 * 
 * @author MT27789605
 * 
 */
public class TimeOutForzadoRulesException extends TimeOutRulesException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<String> serviciosFallidos;

    /**
     * Constructor con la lista de todos los servicios que fallan.
     * @param serviciosFallidos
     * @param mensajeError
     * @param respuesta
     */
    public TimeOutForzadoRulesException(List<String> serviciosFallidos, String mensajeError, RespuestaCentroValidador respuesta) {
        super(mensajeError, respuesta);
        this.serviciosFallidos = serviciosFallidos;
    }

    /**
     * Constructor con el mensaje de error y la respuesta
     * @param string
     * @param respuesta
     */
    public TimeOutForzadoRulesException(String string, RespuestaCentroValidador respuesta) {
        super(string, respuesta);
    }

    public TimeOutForzadoRulesException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public TimeOutForzadoRulesException(String arg0) {
        super(arg0);
    }

    public TimeOutForzadoRulesException(Throwable arg0) {
        super(arg0);
    }

    /**
     * Lista de servicios que fallaron para el error de timeoutforzado
     * @return
     */
    public List<String> getServiciosFallidos() {
        return this.serviciosFallidos;
    }
}
