package ar.com.osde.centroValidador.pos.handlers;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;

public interface TransaccionPosMessageCreator {
     AbstractTransaccionPos createTransaccion(AbstractPosMessage posMessage);

}
