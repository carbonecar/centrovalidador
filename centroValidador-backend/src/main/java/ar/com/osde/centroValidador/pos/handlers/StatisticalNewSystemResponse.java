package ar.com.osde.centroValidador.pos.handlers;

import ar.com.osde.rules.paralelo.ResponseRecord;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;

public class StatisticalNewSystemResponse extends RespuestaCentroValidador {

	public StatisticalNewSystemResponse(RespuestaTransaccion respuestaTransaccion) {
		super(respuestaTransaccion);
		setSystemName(ResponseRecord.SystemName.SALUD_SOFT.getName());
	}

	
	public void processLoguer(LoggerMessageRecordHandler loggerMessageRecordHandler) {
		loggerMessageRecordHandler.logMe(this);
		
	}


}
