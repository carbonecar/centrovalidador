package ar.com.osde.centroValidador.pos.filter;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.cv.pos.utils.ExceptionFactory;

/**
 * Negacion de los filtros.
 *
 */
public class NotFilter implements PosMessageFilter {

    private PosMessageFilter messageFilter;

    public NotFilter(PosMessageFilter messageFilter) {
        this.messageFilter = messageFilter;
    }

    public void validate(SocioPosMessageBase posMessage) throws FilteredMessageException {

        boolean throwException = true;
        
        try {
            messageFilter.validate(posMessage);
        } catch (FilteredMessageException e) {
            throwException = false;
        }
        
        ExceptionFactory.createFilteredMessage(throwException, "");
    }

    public boolean implementaPropiedadValor(String propertyName, Object value) {
        return this.evaluaPropiedad(propertyName) && !this.messageFilter.implementaPropiedadValor(propertyName, value);
    }

    public boolean evaluaPropiedad(String propertyName) {
        return this.messageFilter.evaluaPropiedad(propertyName);
    }
}
