package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

public abstract class PosMessageHandlerInterceptor implements PosMessageHandler {
    private PosMessageHandler intercepted = new NullMessageHandler();
    private FixedFormatManager formatManager;

    public FixedFormatManager getFormatManager() {
        return formatManager;
    }

    
    public void setFormatManager(FixedFormatManager formatManager) {
        this.formatManager = formatManager;
    }

    /**
     * Devuelve el handler que act�a como interceptor.
     * 
     * @return
     */
    public PosMessageHandler getIntercepted() {
        return intercepted;
    }

    public void setIntercepted(PosMessageHandler intercepted) {
        this.intercepted = intercepted;
    }

    /**
     * Levanta el mensaje de pos parseando el MQ y pasandolo a una clase
     * @param message
     * @return
     */
    protected PosMessage loadPosMessage(Message message) {
        String stringMessage = null;
        try {
            stringMessage = ((TextMessage) message).getText();
        } catch (JMSException e1) {
            throw new UnsupportedPosMessageException("El mensaje enviado por JMS no es un TextMessage: " + e1.getMessage());
        }

        PosMessage posMessage = this.formatManager.load(PosMessage.class, stringMessage);

        return posMessage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.pos.handlers.PosMessageHandler#getHierarchyName
     * ()
     */
    public String getHierarchyName() {
        StringBuilder strBuilder = new StringBuilder(50);
        strBuilder.append("PosMessageHandlerInterceptor\n");
        return strBuilder.toString();
    }

    public boolean procesaMensaje() {
        return intercepted.procesaMensaje();
    }
}
