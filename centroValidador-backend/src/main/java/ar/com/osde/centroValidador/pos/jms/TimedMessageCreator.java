package ar.com.osde.centroValidador.pos.jms;


/**
 * Interfaz para medir el tiempo utilizado en crear el mensaje.
 * 
 * @author VA27789605
 * 
 */
public interface TimedMessageCreator extends StringMessageCreator {

	public static final String MY_NAME=TimedMessageCreator.class.getSimpleName();
	public long getTimeUsedToCreateMessage();

}
