package ar.com.osde.centroValidador.factory.filter.configuration;

import ar.com.osde.centroValidador.services.impl.Operador;

/**
 * Utils para la creacion de los filters
 * 
 * @author MT27789605
 * 
 */
public class FilterUtils {
    /**
     * Devueve si es un operador valido
     * 
     * @param elemento
     * @return
     */
    public static boolean esUnOperador(String elemento) {
        return Operador.valuesString().contains(elemento);
    }
}
