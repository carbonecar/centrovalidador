package ar.com.osde.centroValidador.pos.handlers;

import javax.jms.Message;
import javax.jms.Queue;

import org.springframework.jms.core.JmsTemplate;

import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;

public class ErrorHandler {

    
    private JmsTemplate templateResponse;
    // TODO: ver si podemos quitar esta propiedad
    private Queue queue;
    private MessageCreatorFactory messageCreatorFactory;

    private Queue queueTimeOut;

    public Queue getQueueTimeOut() {
        return queueTimeOut;
    }

    public void setQueueTimeOut(Queue queueTimeOut) {
        this.queueTimeOut = queueTimeOut;
    }

    public void process(Message message) {

        this.getTemplateResponse().send(queue, messageCreatorFactory.createStreamMessageCreator(message));
    }

    public void processTimeOut(String message) {
        if (queueTimeOut != null) {
            this.getTemplateResponse().send(queueTimeOut, messageCreatorFactory.createSimpleTextMessage(message));
        }
    }

    public void process(String message) {
        this.getTemplateResponse().send(messageCreatorFactory.createSimpleTextMessage(message));
    }

    public JmsTemplate getTemplateResponse() {
        return templateResponse;
    }

    public void setTemplateResponse(JmsTemplate templateResponse) {
        this.templateResponse = templateResponse;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public void setMessageCreatorFactory(MessageCreatorFactory messageCreatorFactory) {
        this.messageCreatorFactory = messageCreatorFactory;

    }

}
