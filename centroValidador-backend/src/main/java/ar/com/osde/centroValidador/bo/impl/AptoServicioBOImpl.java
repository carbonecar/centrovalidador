package ar.com.osde.centroValidador.bo.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.drools.core.util.StringUtils;
import org.springframework.remoting.soap.SoapFaultException;

import ar.com.osde.centroValidador.bo.AptoServicioBO;
import ar.com.osde.centroValidador.pos.exception.UnsoportedOperationException;
import ar.com.osde.centroValidador.pos.handlers.HandlerTime;
import ar.com.osde.centroValidador.pos.handlers.xml.XMLFormatManager;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.common.colections.utils.ListFactory;
import ar.com.osde.cv.bo.impl.PrestadorOsdeMedicamentoSetter;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AnulacionTransaccionPos;
import ar.com.osde.cv.model.BloqueoDesbloqueoTransaccionPos;
import ar.com.osde.cv.model.CierreFarmaciaTransaccionPos;
import ar.com.osde.cv.model.ConsultaTransaccionPos;
import ar.com.osde.cv.model.EnvioDocumentacionTransaccionPos;
import ar.com.osde.cv.model.MarcaGenericoTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPos;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.cv.model.TransaccionRescatePos;
import ar.com.osde.cv.transaccion.Transacciones;
import ar.com.osde.entities.aptoServicio.Beneficiario;
import ar.com.osde.entities.aptoServicio.PracticaPensionTranslado;
import ar.com.osde.entities.aptoServicio.SolicitudConsumo;
import ar.com.osde.entities.aptoServicio.SolicitudServicio;
import ar.com.osde.entities.aptoServicio.mensajes.AptoSocio;
import ar.com.osde.entities.aptoServicio.mensajes.AptoSocioRequest;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.entities.transaccion.TransaccionAnulacion;
import ar.com.osde.entities.transaccion.TransaccionCierre;
import ar.com.osde.entities.transaccion.TransaccionConsulta;
import ar.com.osde.entities.transaccion.TransaccionConsumo;
import ar.com.osde.entities.transaccion.TransaccionIngresoEgreso;
import ar.com.osde.entities.transaccion.TransaccionMarcaGenerico;
import ar.com.osde.entities.transaccion.TransaccionMigracion;
import ar.com.osde.entities.transaccion.TransaccionTimeOut;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.SearchResult;
import ar.com.osde.framework.services.ServiceException;
import ar.com.osde.prestadores.exceptions.ObjectNotFoundException;
import ar.com.osde.prestadores.services.PrestadorService;
import ar.com.osde.services.otorgamiento.TransaccionesService;
import ar.com.osde.transaccionService.exception.NotSupportedOperation;
import ar.com.osde.transaccionService.services.GeneradorRespuesta;
import ar.com.osde.transaccionService.services.RespuestaAnulacion;
import ar.com.osde.transaccionService.services.RespuestaAptoSocio;
import ar.com.osde.transaccionService.services.RespuestaBloqueoDesbloqueo;
import ar.com.osde.transaccionService.services.RespuestaCierre;
import ar.com.osde.transaccionService.services.RespuestaConsulta;
import ar.com.osde.transaccionService.services.RespuestaEnvioDocumentacion;
import ar.com.osde.transaccionService.services.RespuestaIngresoEgreso;
import ar.com.osde.transaccionService.services.RespuestaMarcaGenerico;
import ar.com.osde.transaccionService.services.RespuestaRegistracionConsumos;
import ar.com.osde.transaccionService.services.RespuestaRescate;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;
import ar.com.osde.transaccionService.services.RespuestaValidacionConsumos;

/**
 * Tiene la responsabilidad de construir los objetos necesarios para la llamada
 * al servicio.
 * 
 * @author VA27789605
 * 
 */
public class AptoServicioBOImpl implements AptoServicioBO {

    private static final Logger LOG = Logger.getLogger(AptoServicioBOImpl.class);
    private static final Logger LOG_ERRORES_WEB_SERIVCES = Logger.getLogger("errores_web_services");

    private TransaccionesService transaccionService;
    // private MotorEmbebido motorEmbebido;
    private String xsdFilename;
    private String xsdVersion;
    private PrestadorService prestadorService;
    private TransaccionServiceStatistics transaccionStadistics;

    public AptoServicioBOImpl() {

    }

    public TransaccionServiceStatistics getTransaccionStadistics() {
        return transaccionStadistics;
    }

    public void setTransaccionStadistics(TransaccionServiceStatistics transaccionStadistics) {
        this.transaccionStadistics = transaccionStadistics;
    }

    public PrestadorService getPrestadorService() {
        return prestadorService;
    }

    public void setPrestadorService(PrestadorService prestadorService) {
        this.prestadorService = prestadorService;
    }

    public String getXsdFilename() {
        return xsdFilename;
    }

    public void setXsdFilename(String xsdFilename) {
        this.xsdFilename = xsdFilename;
    }

    public String getXsdVersion() {
        return xsdVersion;
    }

    public void setXsdVersion(String xsdVersion) {
        this.xsdVersion = xsdVersion;
    }

    public TransaccionesService getTransaccionService() {
        return transaccionService;
    }

    public void setTransaccionService(TransaccionesService transaccionService) {
        this.transaccionService = transaccionService;
    }

    public RespuestaValidacionConsumos isAptoServicioValidacion(TransaccionPrestacionPos transaccionPos) {
        TransaccionConsumo transaccionConsumo = transaccionPos.getTransaccionConsumo();

        try {

            // if(transaccionPos.getTransaccionConsumo().getTipoTransaccion())
            RespuestaValidacionConsumos respuestaValidacionConsumo = this.transaccionService.validarConsumos(transaccionConsumo);
            //TODO aspectos
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
            return respuestaValidacionConsumo;
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
        }
        return null;
    }

    public RespuestaValidacionConsumos isAptoServicioValidacionSinPrestador(TransaccionPrestacionPos transaccionPos) {
        TransaccionConsumo transaccionConsumo = transaccionPos.getTransaccionConsumo();

        try {

            // if(transaccionPos.getTransaccionConsumo().getTipoTransaccion())
            RespuestaValidacionConsumos respuestaValidacionConsumo = this.transaccionService.validarConsumos(transaccionConsumo);
            //TODO aspectos
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
            return respuestaValidacionConsumo;
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
        }
        return null;
    }

    public RespuestaRegistracionConsumos isAptoServicioRegistracion(TransaccionPrestacionPos transaccionPos) {
        TransaccionConsumo transaccionConsumo = transaccionPos.getTransaccionConsumo();
        try {
            RespuestaRegistracionConsumos respuestaValidacionConsumos = null;
            // parametrizar las transacciones que generan autorizacion
            if (TransaccionFactory.getInstance().generaAutorizacion(transaccionConsumo)) {
                respuestaValidacionConsumos = this.transaccionService.generarSolicitudAutorizacion(transaccionConsumo);
            } else {
                respuestaValidacionConsumos = this.transaccionService.registrarConsumos(transaccionConsumo);
            }
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
            return respuestaValidacionConsumos;
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
        }
        return null;
    }

    public RespuestaEnvioDocumentacion envioDocumentacion(EnvioDocumentacionTransaccionPos transaccionEnvioDocumentacion) {

        try {
            return this.transaccionService.envioDocumentacion(transaccionEnvioDocumentacion.getTransaccion());
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
        }
        return null;
    }

    public HandlerTime completarCodigoRelacion(AbstractTransaccionPos transaccionPos) {

        long init = System.currentTimeMillis();
        String operacionInvocada = AptoServicioBO.PRESTADOR_SERVICE_CODIGO_RELACION;
        String codigoRelacion = transaccionPos.getTransaccion().getPrestadorTransaccionador().getNumeroRelacion();
        
        if (StringUtils.isEmpty(codigoRelacion)) {

            long nroCuit = Long.valueOf(transaccionPos.getPrestador().getCuit().replace("-", ""));
            String codigoOperadorTerminal = transaccionPos.getPrestador().getTerminalID().getCodigoOperador();
            Integer codigoFilialTerminal = Integer.valueOf(transaccionPos.getPrestador().getTerminalID().getCodigoFilial());
            int numeroTerminal = transaccionPos.getPrestador().getTerminalID().getNumero();
            int agrupador = transaccionPos.getPrestador().getTerminalID().getAgrupador();
            try {
                String codigoRelacionFromService = this.prestadorService.getCodigoRelacionPrestador(nroCuit, codigoOperadorTerminal,
                        codigoFilialTerminal, numeroTerminal, agrupador);
                transaccionPos.getPrestador().setNumeroRelacion(codigoRelacionFromService);
            } catch (NumberFormatException e) {
                LOG_ERRORES_WEB_SERIVCES.error(e);
                LOG.error("Error fatal inseperado: " + e.getMessage());
                // TODO: MANEJAR LA EXCEPTION
            } catch (ObjectNotFoundException e) {
                LOG.info("No se encontro el codigo de relacion segun: cuit: " + nroCuit + " Codigo operador: " + codigoOperadorTerminal
                        + " Codigo filial terminal: " + codigoFilialTerminal + " numeroTerminal: " + numeroTerminal + " agrupador: "
                        + agrupador);
                LOG.info("se busca el primer prestador con cuit: " + nroCuit + " codigoFilialTerminal: " + codigoFilialTerminal);
                // Como no se encontro un prestador entonces busco todos los
                // prestadores por cuit y filial para luego elegir el primero
                // que este dado de alta.
                operacionInvocada = PRESTADOR_SERVICE_PRESTADORES_CUIT_FILIAL;
                SearchResult<ar.com.osde.rules.prestadores.common.entities.Prestador> searchResult = this.prestadorService
                        .searchPrestadoresByCuitFilial(nroCuit,
                                Integer.valueOf(transaccionPos.getPrestador().getTerminalID().getCodigoFilial()));
                List<ar.com.osde.rules.prestadores.common.entities.Prestador> prestadores = searchResult.getList();
                // FIX para el "bug" cxf donde devuelve null en lugar de una
                // lista vacia
                if (prestadores == null) {
                    prestadores = new ArrayList<ar.com.osde.rules.prestadores.common.entities.Prestador>();
                }
                Iterator<ar.com.osde.rules.prestadores.common.entities.Prestador> it = prestadores.iterator();
                ar.com.osde.rules.prestadores.common.entities.Prestador p = null;
                Date fechaSS = transaccionPos.getFechaSolicitudServicio();
                do {
                    if (!it.hasNext()) {
                        break;
                    }
                    p = it.next();
                } while (!(p.getFechaAlta().before(fechaSS) && p.getFechaBaja().after(fechaSS)));
                if (p != null) {
                    // TODO: este codigo esta duplicado hay que sacarlo de
                    // PrestadorService y de aca y mandarlo a
                    // otorgamiento-commons
                    String filialOrigen = String.format("%02d", Integer.valueOf(p.getFilialOrigen()));
                    String tipoPrestador = String.format("%02d", Integer.valueOf(p.getTipoPrestador()));
                    String codigoPrestador = String.format("%06d", Integer.valueOf(p.getCodigoPrestador()));

                    transaccionPos.getPrestador().setNumeroRelacion(filialOrigen + tipoPrestador + codigoPrestador);
                }

            } catch (Exception e) {
                LOG.error(e);
                LOG.error("Error fatal inseperado: " + e.getMessage());
                // TODO: MANEJAR LA EXCEPTION. No se esta arrojando una
                // objetNotFound, sino que se tira una xfire exception

            }
            // Seteamos el prestadorOSDE a los medicamentos
            transaccionPos.accept(new PrestadorOsdeMedicamentoSetter());

        }

        return new HandlerTime(operacionInvocada, System.currentTimeMillis() - init);
    }

    /**
     * Esto es para una transaccion del tipo 2F, 1F, 2J
     * */
    public RespuestaTransaccion isAptoRegistracionMedicamento(RegistracionMedicamentoMessage transaccion) {
        SolicitudConsumo solicitud = transaccion.getTransaccionConsumo().getSolicitudServicio();
        // solicitud.setFechaReceta(transaccion.getFechaReceta());

        RespuestaTransaccion respuestaAptitud = null;

        List<PracticaPensionTranslado> prestaciones = new ArrayList<PracticaPensionTranslado>();

        try {
            if (TipoTransaccion.O1F.equals(transaccion.getTransaccionConsumo().getTipoTransaccion())) {
                respuestaAptitud = this.transaccionService.validarConsumos(transaccion.getTransaccionConsumo());
            } else {
                respuestaAptitud = this.transaccionService.registrarConsumos(transaccion.getTransaccionConsumo());
            }
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();

        } catch (ServiceException e) {
            // hacer que esto sea un aspecto
            respuestaAptitud = this.handleServiceException(
                    e,
                    ListFactory.<Serializable> createBuilder().with(solicitud)
                            .with(transaccion.getTransaccionConsumo().getSolicitudServicio().getBeneficiarioBid()).with(prestaciones)
                            .build());
        }

        return respuestaAptitud;
    }

    public RespuestaTransaccion isAptoSocio(TransaccionPos transaccion) {
        RespuestaAptoSocio respuesta = null;
        TransaccionConsumo transaccionConsumo = transaccion.getTransaccionConsumo();
        try {

            long init = System.currentTimeMillis();
            respuesta = this.transaccionService.validarSocio(transaccionConsumo);
            this.transaccionStadistics.updateTimeAptoSocio(System.currentTimeMillis() - init);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
            // codigo de debug. Creamos una transaccion para poder generar xml
            // logTransactionAsXML(transaccion, beneficiario, solicitud);
            // fin de codigo de debug
        } catch (ServiceException se) {
            LOG.info("El servicio no repondio de forma correcta: " + se.getMessage());
            // respuesta = this.handleServiceException(se,
            // ListFactory.<Serializable>
            // createBuilder().with(solicitud).with(beneficiario)
            // .build());

        }
        return respuesta;

    }

    public RespuestaRescate registrarRescate(TransaccionRescatePos transaccionRescatePos) {
        transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        RespuestaRescate registrarRescate = null;
        try {
            registrarRescate = this.transaccionService.registrarRescate(transaccionRescatePos.getTransaccionRescate());
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
            return registrarRescate;
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
        }
        return null;
    }

    public RespuestaIngresoEgreso informarIngresoEgreso(TransaccionIngresoEgreso transaccionIngresoEgreso) {
        RespuestaIngresoEgreso respuesta = null;
        try {
            respuesta = this.transaccionService.informarIngresoEgresoInternacion(transaccionIngresoEgreso);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (ServiceException e) {
            LOG_ERRORES_WEB_SERIVCES.error(e);
            LOG.info("El servicio no repondio de forma correcta: " + e.getMessage());
        }
        return respuesta;
    }

    public RespuestaAnulacion registrarAnulacion(AnulacionTransaccionPos anulacionTransaccionPos) {
        RespuestaAnulacion respuesta = null;
        TransaccionAnulacion transaccion = anulacionTransaccionPos.getTransaccion();
        // TODO agregar estadisticas
        try {
            respuesta = this.transaccionService.registrarAnulacion(transaccion);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (ServiceException e) {
            LOG.info("El servicio no repondio de forma correcta: " + e.getMessage());
            // TODO manejar la excepcion
        }
        return respuesta;
    }

    public RespuestaBloqueoDesbloqueo registrarBloqueoDesbloqueo(BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoTransaccionPos) {
        RespuestaBloqueoDesbloqueo respuesta = null;
        try {
            respuesta = this.transaccionService.informarBloqueoDesbloqueo(bloqueoDesbloqueoTransaccionPos.getTransaccion());
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (ServiceException e) {
            LOG.info("El servicio no repondio de forma correcta: " + e.getMessage());
        }
        return respuesta;
    }

    public RespuestaTransaccion registrarCierre(CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos) {
        RespuestaCierre respuesta = null;
        TransaccionCierre transaccionCierre = cierreFarmaciaTransaccionPos.getTransaccion();
        // TODO aspectos
        try {
            respuesta = this.transaccionService.generarCierre(transaccionCierre);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (ServiceException se) {
            LOG.info("El servicio no repondio de forma correcta: " + se.getMessage());
            // TODO manejar la exception
        }
        return respuesta;
    }

    public RespuestaConsulta registrarConsulta(ConsultaTransaccionPos consultaTransaccionPos) {

        TransaccionConsulta transaccionConsulta = consultaTransaccionPos.getTransaccion();
        RespuestaConsulta respuestaConsulta = null;
        try {
            respuestaConsulta = this.transaccionService.registrarConsulta(transaccionConsulta);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (ServiceException se) {
            LOG.info("El servicio no repondio de forma correcta: " + se.getMessage());
        }
        return respuestaConsulta;
    }

    public RespuestaTransaccion informarMarcaGenerico(MarcaGenericoTransaccionPos marcaGenericoTransaccionPos) {

        TransaccionMarcaGenerico transaccionMarcaGenerico = marcaGenericoTransaccionPos.getTransaccionMarcaGenerico();
        RespuestaMarcaGenerico respuesta = null;
        try {
            respuesta = this.transaccionService.informarMarcaGenerico(transaccionMarcaGenerico);
            transaccionStadistics.updateCantTotalTransaccionesAtendidas();
        } catch (Exception exception) {
            LOG.info("El servicio no repondio de forma correcta: " + exception.getMessage());
        }

        return respuesta;
    }

    public RespuestaTransaccion registrarTimeOut(TransaccionTimeOut transaccionTimeOut) throws BusinessException {
        try {
            // TODO las transacciones con generadorRespusta=OSDE deberian ser
            // rechazas
            if (GeneradorRespuesta.OSDE.getCodigo() == transaccionTimeOut.getGeneradorRespuesta()) {
                transaccionTimeOut.setGeneradorRespuesta(GeneradorRespuesta.DESCONOCIDO.getCodigo());
            }
            RespuestaTransaccion respuesta = this.transaccionService.registrarTimeOut(transaccionTimeOut);
            transaccionStadistics.updateCantTotalTransaccionesAtendidasTimeOut();
            return respuesta;

        } catch (NotSupportedOperation e) {
            throw new UnsoportedOperationException();
        } catch (ServiceException e) {
            throw new BusinessException(e);
        } catch (SoapFaultException e) {
            throw new BusinessException(e);
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    public RespuestaTransaccion registrarMigracion(TransaccionMigracion transaccionMigracion) throws BusinessException {
        try {
            if (GeneradorRespuesta.OSDE.getCodigo() == transaccionMigracion.getGeneradorRespuesta()) {
                transaccionMigracion.setGeneradorRespuesta(GeneradorRespuesta.DESCONOCIDO.getCodigo());
            }
            RespuestaTransaccion respuesta = this.transaccionService.registrarMigracion(transaccionMigracion);
            transaccionStadistics.updateCantTotalTransaccionesAtendidasMigracion();
            return respuesta;
        } catch (ServiceException e) {
            throw new BusinessException(e);
        } catch (SoapFaultException e) {
            throw new BusinessException(e);
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    // ********************************************************//
    // *****************Impl. Interna**************************//
    // ********************************************************//

    private void logTransactionAsXML(AbstractTransaccionPos transaccion, Beneficiario beneficiario, SolicitudServicio solicitud) {
        AptoSocioRequest aptoSocioRequest = new AptoSocioRequest();
        aptoSocioRequest.setBeneficiario(beneficiario);
        Transacciones transacciones = new Transacciones();
        transacciones.setVersionXsd(this.xsdVersion);
        AptoSocio aptoSocio = new AptoSocio();
        aptoSocio.setRequest(aptoSocioRequest);
        transacciones.setAptoSocioMessage(aptoSocio);
        // TODO: CAMBIAR POR EL TRANSACCIONES SERVICE CUANDO ESTE TERMINADO
        // aptoSocioRequest.setSolicitudServicio(solicitud);
        XMLFormatManager formatManager = new XMLFormatManager();
        formatManager.setFileName(this.xsdFilename);
        String aptoSocioRequestString = formatManager.export(transacciones);
        LOG.trace(aptoSocioRequestString);
        LOG.trace(transaccion.getPosMessage().getOriginalMessage());
    }

    private RespuestaTransaccion handleServiceException(ServiceException e, List<Serializable> objectRequest) {
        RespuestaTransaccion respuestaAptitud = new RespuestaAptoSocio();
        return respuestaAptitud;

    }

}
