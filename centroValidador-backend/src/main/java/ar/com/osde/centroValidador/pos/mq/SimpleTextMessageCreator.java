package ar.com.osde.centroValidador.pos.mq;

import java.util.HashMap;
import java.util.Map;

import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.centroValidador.pos.jms.TimedMessageCreator;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

public abstract class SimpleTextMessageCreator implements TimedMessageCreator {
	// Mapa de codificacion de operadores
	private Map<String, String> operadores = new HashMap<String, String>();
	private FixedFormatManager manager;

	private RespuestaCentroValidador respuestaCentroValidador;

	public RespuestaCentroValidador getRespuestaCentroValidador() {
		return respuestaCentroValidador;
	}

	public void setRespuestaCentroValidador(
			RespuestaCentroValidador respuestaCentroValidador) {
		this.respuestaCentroValidador = respuestaCentroValidador;
	}

	public Map<String, String> getOperadores() {
		return operadores;
	}

	public void setOperadores(Map<String, String> operadores) {
		this.operadores = operadores;
	}

	public FixedFormatManager getManager() {
		return manager;
	}

	public void setManager(FixedFormatManager manager) {
		this.manager = manager;
	}

}
