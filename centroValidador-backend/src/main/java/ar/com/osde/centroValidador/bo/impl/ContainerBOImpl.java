package ar.com.osde.centroValidador.bo.impl;

import java.util.List;

import org.springframework.jms.listener.DefaultMessageListenerContainer;

import ar.com.osde.centroValidador.bo.ContainerBo;
import ar.com.osde.comunes.utils.ListenerManager;
import ar.com.osde.cv.dao.ContainerDAO;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.persistence.exception.PersistenceException;

/**
 * Implementacion base del container
 * 
 * @author MT27789605
 * 
 */
public class ContainerBOImpl implements ContainerBo {

	private ContainerDAO containerDao;
	private ListenerManager listenerManager;

	/**
	 * M�todo invocado por spring a inicialir el contexto
	 * 
	 * @throws BusinessException
	 */
	public void init() throws BusinessException {
		for (ContainerConfiguration containerConfiguration : getContainerDao().getAll()) {
			getListenerManager().appendListener(containerConfiguration);
		}
	}

	/**
	 * Actualiza el container en la base de datos y lo actualiza en memoria.
	 */
	public void updateContainer(ContainerConfiguration containerConfiguration) throws BusinessException {

		getContainerDao().saveOrUpdate(containerConfiguration);

		this.updateContainerOnMemory(containerConfiguration);
	}

	/**
	 * Actualiza solamente el container en memoria si guardarlo en la base de
	 * datos
	 */
	public void updateContainerOnMemory(ContainerConfiguration container) throws BusinessException {
		if (container != null) {
			getListenerManager().updateListener(container);
		}
	}

	/**
	 * Guarda un nuevo container y lo agregara al @see
	 * ar.com.osde.comunes.utils.ListenerManager en memoria
	 */
	public void saveContainer(ContainerConfiguration containerConfiguration) throws BusinessException {

		getContainerDao().saveNew(containerConfiguration);

		getListenerManager().appendListener(containerConfiguration);
	}

	/**
	 * Recupera todas las configuraciones de los containers de la base de datos.
	 * Si se encuentra ya en memoria le setea el mismo valor que tiene en la
	 * propiedad isRunning del containr en memoria.
	 */
	public List<ContainerConfiguration> getAllContainers() throws BusinessException {
		List<ContainerConfiguration> containersInDb = getContainerDao().getAll();
		for (ContainerConfiguration containerConfiguration : containersInDb) {
			DefaultMessageListenerContainer containerInMemory = this.listenerManager
			        .getListener(containerConfiguration.getId());
			if (containerInMemory != null) {
				containerConfiguration.setRunning(containerInMemory.isRunning());
			}
		}
		return containersInDb;
	}

	public List<ContainerConfiguration> getAllContainersWithGroupName(String groupName) throws BusinessException {
		return this.containerDao.getAllContainersWithGroupName(groupName);
	}

	public void deleteContainer(ContainerConfiguration container) throws BusinessException {
		getContainerDao().delete(container);

		getListenerManager().deleteListener(container.getId());
	}

	public void startContainer(ContainerConfiguration containerConfiguration) throws BusinessException {

		if (getListenerManager().getListener(containerConfiguration.getId()).isRunning()) {
			throw new BusinessException(
			        "El container ya se encuentra en funcionamiento. Si desea recargarlo utilize la opcion de reload");
		}

		containerConfiguration.setStartOnLoad(true);
		getContainerDao().saveOrUpdate(containerConfiguration);

		DefaultMessageListenerContainer container = getListenerManager().getListener(containerConfiguration.getId());
		container.afterPropertiesSet();
		container.start();
	}

	public void stopContainer(ContainerConfiguration containerConfiguration) throws BusinessException {

		if (!getListenerManager().getListener(containerConfiguration.getId()).isRunning()) {
			throw new BusinessException("El container ya se encuentra detenido.");
		}

		containerConfiguration.setStartOnLoad(false);
		getContainerDao().saveOrUpdate(containerConfiguration);

		DefaultMessageListenerContainer container = getListenerManager().getListener(containerConfiguration.getId());
		container.stop();
		container.shutdown();
	}

	public ContainerDAO getContainerDao() {
		return containerDao;
	}

	public void setContainerDao(ContainerDAO containerDao) {
		this.containerDao = containerDao;
	}

	public ListenerManager getListenerManager() {
		return listenerManager;
	}

	public void setListenerManager(ListenerManager listenerManager) {
		this.listenerManager = listenerManager;
	}

	public void realoadContainers(List<Long> containersId) {

		for (Long containerId : containersId) {
			try {
				ContainerConfiguration containerConfig = this.getContainerDao().getById(containerId);
				DefaultMessageListenerContainer container = this.listenerManager.getListener(containerId);
				if (container != null) {
					this.listenerManager.deleteListener(containerId);
				}
				listenerManager.appendListener(containerConfig);
			} catch (BusinessException e) {
				// si no esta en la base de datos vemos de borrarlo del manager.
				this.listenerManager.deleteListener(containerId);
			}
		}
	}

	public ContainerConfiguration getById(long id) throws BusinessException {
		try {
			return this.containerDao.getById(id);
		} catch (PersistenceException e) {
			throw new BusinessException(e.getMessage());
		}

	}

	public ContainerConfiguration getParentContainer(long idHandler) {
		return this.containerDao.getParentContainer(idHandler);
	}
}
