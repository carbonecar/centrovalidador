package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class SocioPosMessageResponse extends PosMessageResponse {

    private String prefijoPlan;

    private String planSocio;
    private char tipoSocio = 'D';

    
    //TODO este campo no es igual para todas las respuesta, refactorizar y poner en paddingChar ' '
    @Field(offset = 180, length = 2, align = Align.LEFT, paddingChar = ' ')
    public String getPrefijoPlan() {
        return prefijoPlan;
    }

    public void setPrefijoPlan(String prefijoPlan) {
        this.prefijoPlan = prefijoPlan;
    }

    //Campo modificado de '0' a ' '
    @Field(offset = 182, length = 3, align = Align.LEFT, paddingChar = ' ')
    public String getPlanSocio() {
        return this.planSocio;
    }

    public void setPlanSocio(String planSocio) {
        this.planSocio = planSocio;
    }

    // Divido los campos de prefijo y plan para poder formaterlo

    @Field(offset = 185, length = 1, align = Align.RIGHT, paddingChar = '0')
    public char getTipoSocio() {
        return this.tipoSocio;
    }

    public void setTipoSocio(char tipoSocio) {
        this.tipoSocio = tipoSocio;
    }

}
