package ar.com.osde.centroValidador.factory.filter.configuration;

import ar.com.osde.centroValidador.pos.filter.OrFilter;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

import java.util.LinkedList;

/**
 * Factory para la creacion de los filtros OR
 * 
 * @author MT27789605
 * 
 */
public class OrFilterConfigurationFactory extends AbstractFilterConfigurationFactory {

    /**
     * Crea un filtro con las expresiones que se pasan en la lista.
     */
    public FilterConfiguration createFilter(LinkedList<String> expresion) throws ServiceException {
        FilterConfiguration filter = new CompositeFilterConfiguration();
        filter.setClassName(OrFilter.class.getName());
        return filter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#getRealClassName()
     */
    @Override
    protected String getRealClassName() {
        return OrFilter.class.getName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.factory.filter.configuration.
     * AbstractFilterConfigurationFactory#createRealFilterConfiguration()
     */
    @Override
    protected FilterConfiguration createRealFilterConfiguration() {
        return new CompositeFilterConfiguration();
    }
}
