package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * 
 * @author MT27789605
 *
 */
@Record
public abstract class CredencialPosMessage extends SocioPosMessageBase {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // AUVERC
    private int versionCredencial;
    
    //AUCSEX
    private String codigoSeguridadExterno;

    
    @Field(offset = 183, length = 2)
    public int getVersionCredencial() {
        return versionCredencial;
    }

    public void setVersionCredencial(int p) {
        this.versionCredencial = p;
    }
    
    @Field(offset = 554, length = 3)
    public String getCodigoSeguridadExterno() {
        return codigoSeguridadExterno;
    }

    public void setCodigoSeguridadExterno(String codigoSeguridadExterno) {
        this.codigoSeguridadExterno = codigoSeguridadExterno;
    }

}
