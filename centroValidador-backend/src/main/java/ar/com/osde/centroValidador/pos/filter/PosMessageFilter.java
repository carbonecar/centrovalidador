package ar.com.osde.centroValidador.pos.filter;

import org.apache.log4j.Logger;

import ar.com.osde.centroValidador.pos.SocioPosMessageBase;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;

public interface PosMessageFilter {

    public static final Logger LOG = Logger.getLogger(PosMessageFilter.class);

    /**
     * 
     * @param posMessage
     * @param txtMessage
     *            PROVISORIO hasta que todo el texto se mapee en el filter asi
     *            luego se deserializa y se enviar como texto.
     * @throws UnsupportedPosMessageException
     */
    public void validate(SocioPosMessageBase posMessage)throws FilteredMessageException;


    boolean implementaPropiedadValor(String propertyName, Object value);

    boolean evaluaPropiedad(String propertyName);
}
