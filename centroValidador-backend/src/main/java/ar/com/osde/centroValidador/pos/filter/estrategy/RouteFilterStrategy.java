package ar.com.osde.centroValidador.pos.filter.estrategy;

import javax.jms.Message;

import org.springframework.jms.core.JmsTemplate;

import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;

/**
 * 
 * Configuracion de ruteo.
 * 
 * @author VA27789605
 * 
 */
public class RouteFilterStrategy implements FilterStrategy {
    
    private static final String RUTEANDO_COLA = "Ruteando a cola: ";
    private JmsTemplate templateResponse;
    private MessageCreatorFactory messageCreatorFactory;
    public static final String MY_NAME="RouteFilterStrategy";
    public void doFilterAction(Message message) {

        this.getTemplateResponse().send(messageCreatorFactory.createStreamMessageCreator(message));
    }

    public JmsTemplate getTemplateResponse() {
        return templateResponse;
    }

    public void setTemplateResponse(JmsTemplate templateResponse) {
        this.templateResponse = templateResponse;
    }

    public String getResolucionEstrategia() {
    	String queueName=templateResponse.getDefaultDestinationName();
    	if(queueName==null){
    		queueName=templateResponse.getDefaultDestination().toString();
    	}
        return ESTRATEGIA_RUTEO + RUTEANDO_COLA + queueName;
    }

    public void setMessageCreatorFactory(MessageCreatorFactory messageCreatorFactory) {
        this.messageCreatorFactory=messageCreatorFactory;
        
    }
}
