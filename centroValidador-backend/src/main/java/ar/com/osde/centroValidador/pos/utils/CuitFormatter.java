package ar.com.osde.centroValidador.pos.utils;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.impl.StringFormatter;

public class CuitFormatter extends StringFormatter {

    private static final char SEPARADOR = '-';

    public String asObject(String segment, FormatInstructions instructions) throws FixedFormatException {
        if (PosUtils.isNullString(segment)) {
            return segment;
        }

        try {
            segment = segment.trim();
            Long numeroCuit = Long.valueOf(segment);
            String format = MessageFormat.format("{0,number,00000000000}".intern(), numeroCuit);
            String cuit = format.substring(0, 2) + SEPARADOR + format.substring(2, 10) + SEPARADOR + format.substring(10);
            return cuit;

        } catch (NullPointerException e) {
            throw new FixedFormatException("Could not parse value[" + segment + "] as a CUIT.", e);
        }
    }

    public String asString(String cuit, FormatInstructions instructions) {
        return StringUtils.remove(cuit, SEPARADOR);
    }
}
