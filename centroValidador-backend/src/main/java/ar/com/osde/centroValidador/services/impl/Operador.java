package ar.com.osde.centroValidador.services.impl;

import java.util.ArrayList;
import java.util.List;

public enum Operador {
    AND(1, "AND"), OR(2, "OR"), NOT(3, "NOT"), EQUALS(4, "=="), PARENTESISIZQUIERDO(5, "\\("), PARENTESISDERECHO(6, "\\)");

    private String descripcion;
    private int id;

    Operador(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public static List<String> valuesString() {
        List<String> list = new ArrayList<String>();
        for (Operador op : Operador.values()) {
            list.add(op.name());
        }
        return list;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getId() {
        return id;
    }
}
