package ar.com.osde.centroValidador.services;

import java.util.Arrays;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ar.com.osde.centroValidador.factory.filter.configuration.AbstractFilterConfigurationFactory;
import ar.com.osde.centroValidador.factory.filter.configuration.EqualsFilterConfigurationFactory;
import ar.com.osde.centroValidador.factory.filter.configuration.OrFilterConfigurationFactory;
import ar.com.osde.centroValidador.services.impl.FilterServiceImpl;
import ar.com.osde.cv.factory.filter.configuration.AndFilterConfigurationFactory;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.framework.services.ServiceException;

public class FilterServiceImplTest {
    private FilterServiceImpl filterService;
    private OrFilterConfigurationFactory orFilterFactory;
    private AndFilterConfigurationFactory andFilterFactory;
    private EqualsFilterConfigurationFactory equalsFilterFactory;
    private AbstractFilterConfigurationFactory notFilterFactory;

    @Before
    public void before() {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "spring/test/test.xml");
        filterService = (FilterServiceImpl) context.getBean("filter.service");
        orFilterFactory = (OrFilterConfigurationFactory) context.getBean("orIFilterConfigurationFactory");
        andFilterFactory = (AndFilterConfigurationFactory) context.getBean("andFilterConfigurationFactory");
        equalsFilterFactory = (EqualsFilterConfigurationFactory) context.getBean("equalsFilterConfigurationFactory");
        notFilterFactory = (AbstractFilterConfigurationFactory) context.getBean("notFilterConfigurationFactory");

    }

    @Test
    public void testExpresionFilterSimple() throws ServiceException {
        String expresion = "C == 8 OR D == S";
        FilterConfiguration filterConfiguration = filterService.crearFilter(expresion);

        CompositeFilterConfiguration orFilter = (CompositeFilterConfiguration) orFilterFactory.createFilter(null);
        FilterConfiguration equalFilter1 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"8", "C"})));
        FilterConfiguration equalFilter2 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"S", "D"})));
        orFilter.agregarHijo(equalFilter2);
        orFilter.agregarHijo(equalFilter1);
        Arrays.asList("d","s");
        Assert.assertEquals(filterConfiguration, orFilter);
    }

    @Test
    public void testExpresionFilterCompleja() throws ServiceException {
        String expresion = "B == 1 AND ( ( B == 2 AND B == 3 ) OR B == 4 ) OR NOT B == 5";

        FilterConfiguration filterConfiguration = filterService.crearFilter(expresion);

        FilterConfiguration equalFilter1 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"1", "B"})));
        FilterConfiguration equalFilter2 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"2", "B"})));
        FilterConfiguration equalFilter3 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"3", "B"})));
        FilterConfiguration equalFilter4 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"4", "B"})));
        FilterConfiguration equalFilter5 = equalsFilterFactory.createFilter(new LinkedList<String>(Arrays.asList(new String[]{"5", "B"})));

        CompositeFilterConfiguration orFilter = (CompositeFilterConfiguration) orFilterFactory.createFilter(null);
        CompositeFilterConfiguration andFilter1 = (CompositeFilterConfiguration) andFilterFactory.createFilter(null);
        CompositeFilterConfiguration orFilter2 = (CompositeFilterConfiguration) orFilterFactory.createFilter(null);
        CompositeFilterConfiguration andFilter2 = (CompositeFilterConfiguration) andFilterFactory.createFilter(null);

        NotFilterConfiguration notFilter = (NotFilterConfiguration) notFilterFactory.createFilter(null);

        notFilter.agregarHijo(equalFilter5);
        andFilter2.agregarHijo(equalFilter2);
        andFilter2.agregarHijo(equalFilter3);
        orFilter2.agregarHijo(equalFilter4);
        orFilter2.agregarHijo(andFilter2);
        andFilter1.agregarHijo(equalFilter1);
        andFilter1.agregarHijo(orFilter2);
        orFilter.agregarHijo(notFilter);
        orFilter.agregarHijo(andFilter1);

        Assert.assertEquals(filterConfiguration, orFilter);
    }

    @Test(expected = ServiceException.class)
    public void testExpresionFilterSimpleFail() throws ServiceException {
        String expresion = "C == 8 D == S";
        FilterConfiguration filterConfiguration = filterService.crearFilter(expresion);
    }
}
