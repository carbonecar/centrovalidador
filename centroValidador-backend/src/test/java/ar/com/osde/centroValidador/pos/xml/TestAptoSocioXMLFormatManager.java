package ar.com.osde.centroValidador.pos.xml;

import org.junit.Assert;

import ar.com.osde.cv.transaccion.Transacciones;
import ar.com.osde.entities.aptoServicio.mensajes.AptoSocio;
import ar.com.osde.entities.aptoServicio.mensajes.AptoSocioRequest;

public class TestAptoSocioXMLFormatManager extends AbstractXMLFormatManager<AptoSocio> {

    @Override
    protected String getResourceName() {
        return "/testcases/aptosocio/transaccion_aptosocio.xml";
    }

    @Override
    protected AptoSocio getTransaccion(Transacciones transaccion) {
        return transaccion.getAptoSocioMessage();
    }

    @Override
    protected void makeAssertions(Transacciones transaccionSerializada, Transacciones transaccionoriginal) {
        AptoSocioRequest aptoSocioRequest = transaccionSerializada.getAptoSocioMessage().getRequest();
        AptoSocioRequest aptoSocioRequestOriginal = transaccionoriginal.getAptoSocioMessage().getRequest();

        Assert.assertEquals("El apellido del beneficiario no se parseo correctamente ", aptoSocioRequest
                .getBeneficiario().getApellido(), aptoSocioRequestOriginal.getBeneficiario().getApellido());
    }

}
