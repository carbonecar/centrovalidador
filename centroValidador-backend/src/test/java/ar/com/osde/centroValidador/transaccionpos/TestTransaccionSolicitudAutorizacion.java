package ar.com.osde.centroValidador.transaccionpos;

import junit.framework.Assert;

import org.dozer.Mapper;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

/**
 * Este test es para las transacciones de tipo 2C. Estas transacciones se usan para odontologia. 
 * Los asserts tienen que hacerse sobre los datos de odontologia.
 * @author MT27789605
 *
 */
@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionSolicitudAutorizacion extends AbstractTestTransactionFactory {

    
    private static final String ingresoMessage = "03399IT600001243054585245000448061705235101002I49438703399M20121029164811000000000000000000       0000000000       0000000000       0000000000       MC07093020121029            0010100000000000000000446471-9                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         2012-10-29-16.47.28.269000</WMQTool>";

    @Test
    public void testSimpleIngresoPosMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();
        Assert.assertNotNull(manager);
        Assert.assertNotNull(mapper);

//        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(ingresoMessage), ingresoMessage);
//        Assert.assertTrue(posMessage.getClass().getName(),posMessage instanceof IngresoEgresoPosMessage);
//        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
//        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
//        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
//        Assert.assertNotNull(message.getSecuencia());
//        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
//        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
//        Assert.assertEquals(494387, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());
//        
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
//        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());
        
    }
}
