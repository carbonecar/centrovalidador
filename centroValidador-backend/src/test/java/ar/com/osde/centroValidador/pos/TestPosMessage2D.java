package ar.com.osde.centroValidador.pos;

import junit.framework.Assert;

import org.junit.Test;

public class TestPosMessage2D extends AbstractPosMessageTest {

    public static final String msj2D = "     OS600001002007722005500000060831769102002D80920600000M20121128103826  0000004202014001       0000000000       0000000000       0000000000         00000020121128            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         000012";

    @Test
    public void testParseo2B() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2D);

        Assert.assertEquals("000012", msj.getNumeroCaso());
    }
}
