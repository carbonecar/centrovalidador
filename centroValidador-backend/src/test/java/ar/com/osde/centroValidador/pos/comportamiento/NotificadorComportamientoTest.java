package ar.com.osde.centroValidador.pos.comportamiento;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.commons.lang.time.DateUtils;

import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.common.webService.DespachadorMensajes;
import ar.com.osde.common.webService.Mensaje;
import ar.com.osde.common.webService.ResultadoMensaje;
import ar.com.osde.cv.comportamiento.Hechos;
import ar.com.osde.cv.comportamiento.NotificadorComportamiento;

public class NotificadorComportamientoTest extends TestCase {

    
    public void testNotificador() {
        DespachadorMensajesMock despachadorMensajes = new DespachadorMensajesMock();

        NotificadorComportamiento notificadorComportamiento = new NotificadorComportamiento();
        notificadorComportamiento.setDespachadorMensajesService(despachadorMensajes);
        notificadorComportamiento.setMailsSoporte("OS-SoporteESB@osde.com.ar");
        notificadorComportamiento.setMails("jose.carbone@osde.com.ar,fernando.martinez@osde.com.ar");

        TransaccionServiceStatistics transaccionServiceStatistics = new TransaccionServiceStatistics();
        transaccionServiceStatistics.setNotificador(notificadorComportamiento);
        transaccionServiceStatistics.addErrorMDR(true);
        //Como el envio de mail es asincronico hago el sleep
        this.sleep(2000);
        assertTrue(despachadorMensajes.resultadoMensaje.getMensaje(),despachadorMensajes.resultadoMensaje.getMensaje().contains(
                "Ocurrio un ERROR MDR  y en los 10 minutos desde la ultima notificacion : 0 errores "));

        notificadorComportamiento.getEventoHechos().get(Hechos.ERROR_MDR)
                .setFechaUltimaNotification(DateUtils.add(new Date(), Calendar.MINUTE, -30));

        transaccionServiceStatistics.addErrorMDR(true);
        this.sleep(2000);
        assertTrue(despachadorMensajes.resultadoMensaje.getMensaje().contains(
                "currio un ERROR MDR  y en los 30 minutos desde la ultima notificacion : 0 errores"));

        transaccionServiceStatistics.addErrorMDR(true);
        transaccionServiceStatistics.addErrorMDR(true);
        transaccionServiceStatistics.addErrorMDR(true);
        transaccionServiceStatistics.addErrorMDR(true);
        transaccionServiceStatistics.addErrorMDR(true);

        notificadorComportamiento.getEventoHechos().get(Hechos.ERROR_MDR)
                .setFechaUltimaNotification(DateUtils.add(new Date(), Calendar.MINUTE, -40));

        transaccionServiceStatistics.addErrorMDR(true);
        this.sleep(2000);
        assertTrue(despachadorMensajes.resultadoMensaje.getMensaje().contains(
                "Ocurrio un ERROR MDR  y en los 40 minutos desde la ultima notificacion : 5 errores "));
        
        for(int i=0;i<30;i++){
            transaccionServiceStatistics.addErrorMDR(true);
        }
        this.sleep(2000);
        assertTrue(despachadorMensajes.resultadoMensaje.getMensaje().contains(
        "20 transacciones dieron ERROR MDR . Verifique que todos los servicios funcionen correctamente"));
        assertEquals("jose.carbone@osde.com.ar,fernando.martinez@osde.com.ar",despachadorMensajes.destinatarios);
    }

    private void sleep(long msInterval) {

        try {
            Thread.sleep(msInterval);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private class DespachadorMensajesMock implements DespachadorMensajes {

        public ResultadoMensaje resultadoMensaje;
        public String destinatarios;
        public ResultadoMensaje mandarFax(Mensaje arg0) {
            return null;
        }

        public ResultadoMensaje mandarMail(Mensaje arg0) {
            System.out.println(arg0.getBody());
            resultadoMensaje = new ResultadoMensaje();
            resultadoMensaje.setMensaje(arg0.getBody());
            this.destinatarios=arg0.getDestinatario();
            return resultadoMensaje;
        }

        public ResultadoMensaje mandarSMS(Mensaje arg0) {
            return null;
        }

    }
}
