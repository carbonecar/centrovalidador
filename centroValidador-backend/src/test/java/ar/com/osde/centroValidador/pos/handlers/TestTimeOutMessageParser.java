package ar.com.osde.centroValidador.pos.handlers;

import junit.framework.TestCase;

public class TestTimeOutMessageParser extends TestCase {

    private String mensajeTimeOut =           "IT020000373070928404161053161904136501001F75463703463M20121105142939000000000000000000       0000000000       0000000000       0000000000       MC02221600000000            00000009201        1001010000000000000";
    private String mensajeTimeOutFinTerapia=  "AA370400592022826235900000061208855201002A38443050265M20130430191251000000004201011001                                                          0 00000000000000            10000009300        0101010020228262359000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000000000000             000000000000000000                                                                                                                                              ";
    public void testParseResponse() {

        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();

        String timeOutResponse = timeOutMessageParser.parseResponse(mensajeTimeOut);

        assertEquals("0000009201        1", timeOutResponse);

    }

    
    
    public void testParseResponseFinTerapia() {

        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();

        String timeOutResponse = timeOutMessageParser.parseResponse(mensajeTimeOutFinTerapia);

        assertEquals("0000009300        0", timeOutResponse);

    }
  
    
    
    public void testParseRequest() {

        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();

        String timeOutRequest = timeOutMessageParser.parseRequest(mensajeTimeOut);

        assertEquals(
                "     IT020000373070928404161053161904136501001F75463703463M20121105142939000000000000000000       0000000000       0000000000       0000000000       MC02221600000000            001010000000000000",
                timeOutRequest);

    }
    
    
    public void testParseRequesFinTerapiat() {

        TimeOutMessageParser timeOutMessageParser = new TimeOutMessageParser();

        String timeOutRequest = timeOutMessageParser.parseRequest(mensajeTimeOutFinTerapia);

        assertEquals(
                "     AA370400592022826235900000061208855201002A38443050265M20130430191251000000004201011001                                                          0 00000000000000            101010020228262359000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000000000000             000000000000000000                                                                                                                                              ",
                timeOutRequest);

    }
}
