package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessageRescate;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionRescatePos;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.aptoServicio.BeneficiarioBid;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionRescatePos extends AbstractTestTransactionFactory {

    public static final String TRANSACCION_RESCATE_SIMPLE = "SAF  AA370000712010335136800000061169218901003A94730600000M2013080117330300000000      0000             0000             0000             0000       0 00000000000000            001010020103351368                                                                                                                                             00000000000000000                                                                                              0000001                                                                                                                                                                                                        ";

    @Test
    public void testRescateSimple() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(TRANSACCION_RESCATE_SIMPLE),
                TRANSACCION_RESCATE_SIMPLE);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageRescate);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionRescatePos);
        TransaccionRescatePos consultaTransaccionPos = (TransaccionRescatePos) message;

        Assert.assertEquals("20103351368", consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getCuit());
        TerminalID terminalid = consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getTerminalID();
        Assert.assertNotNull(terminalid);
        Assert.assertEquals("37", terminalid.getCodigoFilial());
        Assert.assertEquals("AA", terminalid.getCodigoOperador());
        Assert.assertEquals(71, terminalid.getNumero());

        BeneficiarioBid beneficiarioBid = consultaTransaccionPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid();

        Assert.assertNotNull(beneficiarioBid);
        Assert.assertEquals("61", beneficiarioBid.getNroContrato().getPrefijo());
        Assert.assertEquals("1692189", beneficiarioBid.getNroContrato().getNumero());
        Assert.assertEquals("01", beneficiarioBid.getOrden());

        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionRescate());

        // Date fechaTransaccionHasta =
        // consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        // Assert.assertNotNull(fechaTransaccionHasta);
        // Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

    }

    public static final String TRANSACCION_RESCATE_CONORDEN = "     AA370000712010335136800000061169218901003A94730900000M2013080117483560947302      0000             0000             0000             0000       0 00000000000000            00000009301        2501010020103351368                                                                                                                                             00000000000000000                                                                               0000001                                                                                                                                                                                                   ";

    @Test
    public void testRescateConOrden() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(TRANSACCION_RESCATE_CONORDEN),
                TRANSACCION_RESCATE_CONORDEN);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageRescate);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionRescatePos);
        TransaccionRescatePos consultaTransaccionPos = (TransaccionRescatePos) message;

        Assert.assertNotNull(consultaTransaccionPos.getTransaccionRescate());
        Assert.assertEquals("20103351368", consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getCuit());
        TerminalID terminalid = consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getTerminalID();
        Assert.assertNotNull(terminalid);
        Assert.assertEquals("37", terminalid.getCodigoFilial());
        Assert.assertEquals("AA", terminalid.getCodigoOperador());
        Assert.assertEquals(71, terminalid.getNumero());

        BeneficiarioBid beneficiarioBid = consultaTransaccionPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid();

        Assert.assertNotNull(beneficiarioBid);
        Assert.assertEquals("61", beneficiarioBid.getNroContrato().getPrefijo());
        Assert.assertEquals("1692189", beneficiarioBid.getNroContrato().getNumero());
        Assert.assertEquals("01", beneficiarioBid.getOrden());

        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        AutorizacionBid autorizacionBid = consultaTransaccionPos.getTransaccionRescate().getSolicitudRescate().getAutorizacionBid();
        Assert.assertNotNull(autorizacionBid);

        Assert.assertEquals("37", autorizacionBid.getCodigoFilial());
        Assert.assertEquals(947302l, autorizacionBid.getNumeroAutorizacion());

        // Date fechaTransaccionHasta =
        // consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        // Assert.assertNotNull(fechaTransaccionHasta);
        // Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

    }

    public static final String TRANSACCION_RESCATE_NROTRANSACCION = "IVR  AI250000252013000688500000061031219603 03A26959100000M20130812113324001225960000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                                           ";

    @Test
    public void testRescateConTransaccion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(TRANSACCION_RESCATE_NROTRANSACCION),
                TRANSACCION_RESCATE_NROTRANSACCION);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageRescate);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionRescatePos);
        TransaccionRescatePos consultaTransaccionPos = (TransaccionRescatePos) message;

        Assert.assertNotNull(consultaTransaccionPos.getTransaccionRescate());
        Assert.assertEquals("20130006885", consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getCuit());
        TerminalID terminalid = consultaTransaccionPos.getTransaccion().getPrestadorTransaccionador().getTerminalID();
        Assert.assertNotNull(terminalid);
        Assert.assertEquals("25", terminalid.getCodigoFilial());
        Assert.assertEquals("AI", terminalid.getCodigoOperador());
        Assert.assertEquals(25, terminalid.getNumero());

        BeneficiarioBid beneficiarioBid = consultaTransaccionPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid();

        Assert.assertNotNull(beneficiarioBid);
        Assert.assertEquals("61", beneficiarioBid.getNroContrato().getPrefijo());
        Assert.assertEquals("0312196", beneficiarioBid.getNroContrato().getNumero());
        Assert.assertEquals("03", beneficiarioBid.getOrden());

        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        AutorizacionBid autorizacionBid = consultaTransaccionPos.getTransaccionRescate().getSolicitudRescate().getAutorizacionBid();
        Assert.assertNull(autorizacionBid);

        Assert.assertNotNull(consultaTransaccionPos.getTransaccionRescate().getSolicitudRescate().getTransaccionReferencia());
        Assert.assertEquals(122596l, consultaTransaccionPos.getTransaccionRescate().getSolicitudRescate().getTransaccionReferencia()
                .getNumeroTransaccion());
        Assert.assertEquals("AI", consultaTransaccionPos.getTransaccionRescate().getSolicitudRescate().getTransaccionReferencia()
                .getCodigoOperador());

        // Date fechaTransaccionHasta =
        // consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        // Assert.assertNotNull(fechaTransaccionHasta);
        // Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

    }

}
