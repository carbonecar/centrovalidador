package ar.com.osde.centroValidador.pos;

import junit.framework.Assert;

import org.junit.Test;

public class TestMarcaGenerico4GPosMessage extends AbstractPosMessageTest {


    private static final String parseoSimpleMarcaGenerico4G = "     WG600000142010833619700000000000000000004G00359300000 20130221151331WG0035900000000000       0000000000       0000000000       0000000000         00000020130219            001010000000000000000000000000994481677980690507550000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100000000000000000      0000000044816000000000000                                                                                                                                                            ";

    public static final String marcaGenerico4G="     WG370061002716415503500000000000000000004G00429500000 20130606115703CR0127600000000000       0000000000       0000000000       0000000000         00000020130606            001010000000000000000000000000513588177953021803230000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000CR0000000000000000  00000000000                                                            100000000000000000      0000000033100000000000000                                                                                                                                                            ";
    public static final String desmarcarGenerico4G="     WG370061002716415503500000000000000000004G00429600000 20130606124339CR0127600000000000       0000000000       0000000000       0000000000         00000020130606            001010000000000000000000000000513588177953021803230000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000CR0000000000000000  00000000000                                                            000000000000000000      0000000033100000000000000                                                                                                                                                                 ";
    
    @Test
    public void testParseoSimple(){
        MarcaGenericoPosMessage msj = getMananger().load(MarcaGenericoPosMessage.class, parseoSimpleMarcaGenerico4G);
    }
    
    @Test
    public void testParseoMarcaGenerico4G(){
        MarcaGenericoPosMessage msj = getMananger().load(MarcaGenericoPosMessage.class, parseoSimpleMarcaGenerico4G);
        Assert.assertEquals("G",msj.getAtributoCodigoTransaccion());
        Assert.assertEquals(4, msj.getCodigoTransaccion());
        Assert.assertEquals("WG", msj.getCodigoOperador());
        Assert.assertEquals(60, msj.getFilialInstalacionPOS());
        Assert.assertEquals(0, msj.getDelegacionInstalacionPOS());
        
        Assert.assertEquals(14, msj.getNumeroPOS());
        Assert.assertEquals("20108336197",msj.getCuitPrestador());
        Assert.assertEquals(3593, msj.getNumeroTransaccion());
        Assert.assertNotNull(msj.getFechaTransaccionReferencia());
        
        //TODO: continuar con todos los campos y hacer un super para los comunes.
    }
}
