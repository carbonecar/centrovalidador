package ar.com.osde.centroValidador.pos;

import junit.framework.Assert;

import org.junit.Test;

public class Test4APosMessage extends AbstractPosMessageTest {


    private static final String parseoSimple4A =            "FRM  AF6000005127174986008000000           004A05288700000 20120928163556  049952                                                                      00000020120928            0010100                                           00                               00                               00                                                            0000000000000000   0000000000                                                               000000000000000       000000                                                                                                                                                                              ";

    
    @Test
    public void testParseoSimple(){
        TipoCuatroPosMessage msj = getMananger().load(TipoCuatroPosMessage.class, parseoSimple4A);
    }
    
    @Test
    public void testParseo4A(){
        TipoCuatroPosMessage msj = getMananger().load(TipoCuatroPosMessage.class, parseoSimple4A);
        Assert.assertEquals("A",msj.getAtributoCodigoTransaccion());
        Assert.assertEquals(4, msj.getCodigoTransaccion());
        Assert.assertEquals("AF", msj.getCodigoOperador());
        Assert.assertEquals(60, msj.getFilialInstalacionPOS());
        Assert.assertEquals(0, msj.getDelegacionInstalacionPOS());
        
        Assert.assertEquals(51, msj.getNumeroPOS());
        Assert.assertEquals("27174986008",msj.getCuitPrestador());
        Assert.assertEquals(52887, msj.getNumeroTransaccion());
        //TODO: continuar con todos los campos y hacer un super para los comunes.
    }
    
    
   
}
