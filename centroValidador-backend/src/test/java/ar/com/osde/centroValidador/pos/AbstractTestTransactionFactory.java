package ar.com.osde.centroValidador.pos;

import org.dozer.Mapper;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

public abstract class AbstractTestTransactionFactory extends AbstractJUnit4SpringContextTests {

	public AbstractTestTransactionFactory() {
		super();
	}

	protected Mapper getMapper() {
    	return (Mapper) applicationContext.getBean("dozer.mapper");
    }

	protected FixedFormatManager getMannager() {
    	return (FixedFormatManager) applicationContext.getBean("beans.format.manager");
    }
	
	protected AbstractTransaccionPos getTransaccionPos(String mensajeMQ){
	  
	    
	    Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(mensajeMQ),
                mensajeMQ);
      
        return TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
	}

}