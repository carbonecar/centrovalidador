package ar.com.osde.centroValidador.transaccionpos;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.entities.aptoServicio.Credencial;
import ar.com.osde.entities.aptoServicio.ModoIngreso;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })

public class TestTransaccionPosCredencialProvisoaria extends AbstractTestTransactionFactory {

    private static final String msj2 = "SAF  AA000069332008447205100000060728835201002A93836900000M20100330181316  0000000004751001       0004121001       0004811001       0060941001         00000000000000            001040220084472051                                                                                                                                             00000000000000000  0000000000000                                                                                                                                                                                         013                                                                                                      ";

    private static final String msj6 = "43421IM600007112010828432461053161030791502902A34317300000A20101115173537000000004201011001       0000000000       0000000000       0000000000         00000000000000            0000002000000000000000000000000000000000000000000000        000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                   ";
    private static final String mensaje2P="IVR  AI600000102717031265700000061795707901 02P31747600000M20140815142942890890181103133001             0000       0000000000       0000000000       MC11752220140815            0010101                                                                                                                          8908901861795707901317476.TIF                                                                                                                058386                                                                                                    013                                                                                             ";

    
    @Test
    public void testVersionCredencial() {
        AbstractTransaccionPos transaccionPos = getTransaccionPos(msj6);

        Credencial credencial=transaccionPos.getTransaccion().getSolicitudServicio().getCredencial();
        
        Assert.assertNotNull(credencial);
        Assert.assertEquals(2, credencial.getVersion());
    }

    
    @Test
    public void testCodigoSeguridadExterno() {
        AbstractTransaccionPos transaccionPos = getTransaccionPos(msj2);

        Assert.assertEquals(ModoIngreso.M, transaccionPos.getTransaccion().getSolicitudServicio().getModoIngresoCredencial());

        Credencial credencial=transaccionPos.getTransaccion().getSolicitudServicio().getCredencial();
        
        Assert.assertNotNull(credencial);
        Assert.assertEquals("013", credencial.getCodigoSeguridadExterno());
    }
    

    @Test
    public void testVersionCredencialTrxProcololo() {
        AbstractTransaccionPos transaccionPos = getTransaccionPos(mensaje2P);

        Credencial credencial=transaccionPos.getTransaccion().getSolicitudServicio().getCredencial();
        
        Assert.assertNotNull(credencial);
        Assert.assertEquals(1, credencial.getVersion());
        Assert.assertEquals("013", credencial.getCodigoSeguridadExterno());
        Assert.assertEquals(ModoIngreso.M, transaccionPos.getTransaccion().getSolicitudServicio().getModoIngresoCredencial());

        
    }


}
