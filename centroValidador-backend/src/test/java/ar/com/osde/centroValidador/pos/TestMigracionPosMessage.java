package ar.com.osde.centroValidador.pos;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestMigracionPosMessage extends AbstractTestTransactionFactory {

    
    private String testMigracion2F="AF370100892005649145800000037200405402002F19013400000A20110803180206  000000                                                                    MB06274900000000            00064229800        0101010200000000000000000000000492801177953200509120300000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110803P M          031831000000000000                                                                                                                                              1000007200000730005400055000007600057000007800000790000071000007100006200006300006110000712000071300514005150000716005170000718000071900007200000710000620000630000621000072200007230052400525000072600527000072800007290000730000071000062000063000061";
    
    private String testMigracion2FSinBloquear="AF370100892005649145800000037200405402002F19013400000A20110803180206  000000                                                                    MB06274900000000            00064229800        0101010200000000000000000000000492801177953200509120300000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110803P M          031831000000000000                                                                                                                                              1000007200000730005400055000007600057000007800000790000071000007100006200006300006110000712000071300514005150000716005170000718000071900007200000710000620000630000621000072200007230052400525000072600527000072800007290000730000071000062000063000060";
    
    private String testMigracion2FUNImporteMedicamentoRespuesta="AF370100892005649145800000037200405402002F19013400000A20110803180206  000000                                                                    MB06274900000000            00064229800        0101010200000000000000000000000492801177953200509120300000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110803P M          031831000000000000                                                                                                                                              10000072000007300054000550000076000570000078000007900000710000071000062000063000060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    
   private String testMigracion2FImportesBlanco="AF370100892005649145800000037200405402002F19013400000A20110803180206  000000                                                                    MB06274900000000            00064229800        0101010200000000000000000000000492801177953200509120300000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110803P M          031831000000000000                                                                                                                                              1000007200000730005400055000007600057000007800000790000071000007100006200006300006                                                                                                                                                                      ";

    
    @Test
    public void testResponseTimeOut(){
        MigracionPosMessage responseTimeOut=getMannager().load(MigracionPosMessage.class, testMigracion2F);
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem1());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem2());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem3());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem4());
        Assert.assertEquals("98",responseTimeOut.getGeneradorRespuesta());
        Assert.assertEquals(6422,responseTimeOut.getNroAutorizacion());
        Assert.assertEquals("00",responseTimeOut.getRespuestaTransaccion());
        
        Assert.assertEquals(new BigDecimal("10000.07"), responseTimeOut.getImporteCobertura1());
        Assert.assertEquals(new BigDecimal("20000.07"), responseTimeOut.getImporteCargoSocio1());
        Assert.assertEquals(new BigDecimal("300.05"),responseTimeOut.getPorcentajeCobertura1());
        Assert.assertEquals(new BigDecimal("400.05"),responseTimeOut.getPorcentajeCargoSocio1());
        Assert.assertEquals(new BigDecimal("50000.07"),responseTimeOut.getValorFacturado1());
        Assert.assertEquals(new BigDecimal("600.05"),responseTimeOut.getPorcentajeBonificacion1());
        Assert.assertEquals(new BigDecimal("70000.07"),responseTimeOut.getImporteBonificado1());
        Assert.assertEquals(new BigDecimal("80000.07"),responseTimeOut.getValorContratado1());
        Assert.assertEquals(new BigDecimal("90000.07"),responseTimeOut.getValorAPagar1());
        Assert.assertEquals(new BigDecimal("10000.07"),responseTimeOut.getPrecioAlfabeta1());
        
        Assert.assertEquals(new BigDecimal("1000.06"),responseTimeOut.getImporteCoberturaUnitario1());
        Assert.assertEquals(new BigDecimal("2000.06"),responseTimeOut.getImporteCargoSocioUnitario1());
        Assert.assertEquals(new BigDecimal("3000.06"),responseTimeOut.getImporteBonificadoUnitario1());
        
        
        Assert.assertEquals(new BigDecimal("11000.07"), responseTimeOut.getImporteCobertura2());
        Assert.assertEquals(new BigDecimal("12000.07"), responseTimeOut.getImporteCargoSocio2());
        Assert.assertEquals(new BigDecimal("130.05"),responseTimeOut.getPorcentajeCobertura2());
        Assert.assertEquals(new BigDecimal("140.05"),responseTimeOut.getPorcentajeCargoSocio2());
        Assert.assertEquals(new BigDecimal("15000.07"),responseTimeOut.getValorFacturado2());
        Assert.assertEquals(new BigDecimal("160.05"),responseTimeOut.getPorcentajeBonificacion2());
        Assert.assertEquals(new BigDecimal("17000.07"),responseTimeOut.getImporteBonificado2());
        Assert.assertEquals(new BigDecimal("18000.07"),responseTimeOut.getValorContratado2());
        Assert.assertEquals(new BigDecimal("19000.07"),responseTimeOut.getValorAPagar2());
        Assert.assertEquals(new BigDecimal("20000.07"),responseTimeOut.getPrecioAlfabeta2());
        
        
        Assert.assertEquals(new BigDecimal("1000.06"),responseTimeOut.getImporteCoberturaUnitario2());
        Assert.assertEquals(new BigDecimal("2000.06"),responseTimeOut.getImporteCargoSocioUnitario2());
        Assert.assertEquals(new BigDecimal("3000.06"),responseTimeOut.getImporteBonificadoUnitario2());
        
        Assert.assertEquals(new BigDecimal("21000.07"), responseTimeOut.getImporteCobertura3());
        Assert.assertEquals(new BigDecimal("22000.07"), responseTimeOut.getImporteCargoSocio3());
        Assert.assertEquals(new BigDecimal("230.05"),responseTimeOut.getPorcentajeCobertura3());
        Assert.assertEquals(new BigDecimal("240.05"),responseTimeOut.getPorcentajeCargoSocio3());
        Assert.assertEquals(new BigDecimal("25000.07"),responseTimeOut.getValorFacturado3());
        Assert.assertEquals(new BigDecimal("260.05"),responseTimeOut.getPorcentajeBonificacion3());
        Assert.assertEquals(new BigDecimal("27000.07"),responseTimeOut.getImporteBonificado3());
        Assert.assertEquals(new BigDecimal("28000.07"),responseTimeOut.getValorContratado3());
        Assert.assertEquals(new BigDecimal("29000.07"),responseTimeOut.getValorAPagar3());
        Assert.assertEquals(new BigDecimal("30000.07"),responseTimeOut.getPrecioAlfabeta3());
        
        Assert.assertEquals(new BigDecimal("1000.06"),responseTimeOut.getImporteCoberturaUnitario3());
        Assert.assertEquals(new BigDecimal("2000.06"),responseTimeOut.getImporteCargoSocioUnitario3());
        Assert.assertEquals(new BigDecimal("3000.06"),responseTimeOut.getImporteBonificadoUnitario3());
        
        Assert.assertTrue(responseTimeOut.getBloqueada());
    }
    
    
    @Test
    public void testResponseMigracionSinBloquear(){
        MigracionPosMessage responseTimeOut=getMannager().load(MigracionPosMessage.class, testMigracion2FSinBloquear);
        
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem1());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem2());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem3());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem4());
        Assert.assertEquals("98",responseTimeOut.getGeneradorRespuesta());
        Assert.assertEquals(6422,responseTimeOut.getNroAutorizacion());
        Assert.assertEquals("00",responseTimeOut.getRespuestaTransaccion());
        
        Assert.assertFalse(responseTimeOut.getBloqueada());
    }

    
    @Test
    public void testResponseMigracionCeros2MedicamentosRespuesta(){
        MigracionPosMessage responseTimeOut=getMannager().load(MigracionPosMessage.class, testMigracion2FUNImporteMedicamentoRespuesta);
        
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem1());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem2());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem3());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem4());
        Assert.assertEquals("98",responseTimeOut.getGeneradorRespuesta());
        Assert.assertEquals(6422,responseTimeOut.getNroAutorizacion());
        Assert.assertEquals("00",responseTimeOut.getRespuestaTransaccion());
        
        Assert.assertEquals(new BigDecimal("10000.07"), responseTimeOut.getImporteCobertura1());
        Assert.assertEquals(new BigDecimal("20000.07"), responseTimeOut.getImporteCargoSocio1());
        Assert.assertEquals(new BigDecimal("300.05"),responseTimeOut.getPorcentajeCobertura1());
        Assert.assertEquals(new BigDecimal("400.05"),responseTimeOut.getPorcentajeCargoSocio1());
        Assert.assertEquals(new BigDecimal("50000.07"),responseTimeOut.getValorFacturado1());
        Assert.assertEquals(new BigDecimal("600.05"),responseTimeOut.getPorcentajeBonificacion1());
        Assert.assertEquals(new BigDecimal("70000.07"),responseTimeOut.getImporteBonificado1());
        Assert.assertEquals(new BigDecimal("80000.07"),responseTimeOut.getValorContratado1());
        Assert.assertEquals(new BigDecimal("90000.07"),responseTimeOut.getValorAPagar1());
        Assert.assertEquals(new BigDecimal("10000.07"),responseTimeOut.getPrecioAlfabeta1());
        
        
        Assert.assertEquals(new BigDecimal("0.00"), responseTimeOut.getImporteCobertura2());
        Assert.assertEquals(new BigDecimal("0.00"), responseTimeOut.getImporteCargoSocio2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeCobertura2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeCargoSocio2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorFacturado2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeBonificacion2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getImporteBonificado2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorContratado2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorAPagar2());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPrecioAlfabeta2());
        
        
        Assert.assertEquals(new BigDecimal("0.00"), responseTimeOut.getImporteCobertura3());
        Assert.assertEquals(new BigDecimal("0.00"), responseTimeOut.getImporteCargoSocio3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeCobertura3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeCargoSocio3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorFacturado3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPorcentajeBonificacion3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getImporteBonificado3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorContratado3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getValorAPagar3());
        Assert.assertEquals(new BigDecimal("0.00"),responseTimeOut.getPrecioAlfabeta3());
        
        
        Assert.assertFalse(responseTimeOut.getBloqueada());
    }

    
    
    @Test
    public void testResponseMigracionBlancos2MedicamentosRespuesta(){
        MigracionPosMessage responseTimeOut=getMannager().load(MigracionPosMessage.class, testMigracion2FImportesBlanco);
        
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem1());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem2());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem3());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem4());
        Assert.assertEquals("98",responseTimeOut.getGeneradorRespuesta());
        Assert.assertEquals(6422,responseTimeOut.getNroAutorizacion());
        Assert.assertEquals("00",responseTimeOut.getRespuestaTransaccion());
        
        Assert.assertEquals(new BigDecimal("10000.07"), responseTimeOut.getImporteCobertura1());
        Assert.assertEquals(new BigDecimal("20000.07"), responseTimeOut.getImporteCargoSocio1());
        Assert.assertEquals(new BigDecimal("300.05"),responseTimeOut.getPorcentajeCobertura1());
        Assert.assertEquals(new BigDecimal("400.05"),responseTimeOut.getPorcentajeCargoSocio1());
        Assert.assertEquals(new BigDecimal("50000.07"),responseTimeOut.getValorFacturado1());
        Assert.assertEquals(new BigDecimal("600.05"),responseTimeOut.getPorcentajeBonificacion1());
        Assert.assertEquals(new BigDecimal("70000.07"),responseTimeOut.getImporteBonificado1());
        Assert.assertEquals(new BigDecimal("80000.07"),responseTimeOut.getValorContratado1());
        Assert.assertEquals(new BigDecimal("90000.07"),responseTimeOut.getValorAPagar1());
        Assert.assertEquals(new BigDecimal("10000.07"),responseTimeOut.getPrecioAlfabeta1());
        
        
        Assert.assertEquals(new BigDecimal("0"), responseTimeOut.getImporteCobertura2());
        Assert.assertEquals(new BigDecimal("0"), responseTimeOut.getImporteCargoSocio2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeCobertura2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeCargoSocio2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorFacturado2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeBonificacion2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getImporteBonificado2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorContratado2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorAPagar2());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPrecioAlfabeta2());
        
        
        Assert.assertEquals(new BigDecimal("0"), responseTimeOut.getImporteCobertura3());
        Assert.assertEquals(new BigDecimal("0"), responseTimeOut.getImporteCargoSocio3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeCobertura3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeCargoSocio3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorFacturado3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPorcentajeBonificacion3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getImporteBonificado3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorContratado3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getValorAPagar3());
        Assert.assertEquals(new BigDecimal("0"),responseTimeOut.getPrecioAlfabeta3());
        
        
        Assert.assertFalse(responseTimeOut.getBloqueada());
    }
}
