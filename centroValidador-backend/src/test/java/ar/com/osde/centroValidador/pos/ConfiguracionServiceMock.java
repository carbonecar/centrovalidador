package ar.com.osde.centroValidador.pos;

import java.util.ArrayList;
import java.util.List;

import ar.com.osde.entities.transaccion.ConfiguracionInconsistenciasImpresion;
import ar.com.osde.services.otorgamiento.ConfiguracionService;

public class ConfiguracionServiceMock implements ConfiguracionService {

    public List<ConfiguracionInconsistenciasImpresion> obtenerConfiguracionesInconsistenciasImpresion() {
        return new ArrayList<ConfiguracionInconsistenciasImpresion>();
    }

}
