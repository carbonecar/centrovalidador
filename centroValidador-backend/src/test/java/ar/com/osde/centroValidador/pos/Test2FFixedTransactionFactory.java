package ar.com.osde.centroValidador.pos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.aptoServicio.Medicamento;
import ar.com.osde.entities.aptoServicio.SolicitudConsumo;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class Test2FFixedTransactionFactory extends AbstractTestTransactionFactory {

    private static final String medicamentoMessage = "11111OS600000112016444577222222260615434401802F01820233333M20101227112356  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001010000000000000000000000000       00000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020101201             013687000000000000                                                                                                                                                             ";

    private static final String firstMessage2F =  "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";
    private static final String secondMessage2F = "11111OS600000112016444577222222260615434401802F01826233333M20110106161552  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            002020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                            1  000000020110101             033100000000000000                                                                                                                                                             ";

    private static final String unMedicamentoMessage =                                "     WG600001173069213874700000060831769101002F00320500000M20130103112546  0000000000000000       0000000000       0000000000       0000000000       MB12345600000000            001010000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100001355120130103M M   0000000005455000000000000                                                                                                                                                            ";
    private static final String unMedicamentoMessageCodigoPrestadorConCeros =         "     WG600001173069213874700000060831769101002F00320500000M20130103112546  0000000000000000       0000000000       0000000000       0000000000       MB12345600000000            001010000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100  1355120130103M M   0000000005455000000000000                                                                                                                                                            ";
    
    private static final String unMedicamentoMessageCodigoPrestadorConCerosFilial01 = "     WG010001173069213874700000060831769101002F00320500000M20130103112546  0000000000000000       0000000000       0000000000       0000000000       MB12345600000000            001010000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100  1355120130103M M   0000000005455000000000000                                                                                                                                                            ";
    
    
    @Test
    public void testFirstMessage2FSimpleCreation() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(firstMessage2F), firstMessage2F);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(2, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertEquals(18262, registracionMedicamentoMessage.getTransaccionConsumo().getNumeroTransaccion());
        Assert.assertEquals(3,registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().size());
        Medicamento medicamento=(Medicamento) registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().get(0);
        Assert.assertFalse(medicamento.isGenerico());
    }

    @Test
    public void testSecondMessage2FSimpleCreation() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(secondMessage2F), secondMessage2F);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(2, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(2, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertEquals(18262, registracionMedicamentoMessage.getTransaccionConsumo().getNumeroTransaccion());

        Assert.assertEquals(1,registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().size());
        Medicamento medicamento=(Medicamento) registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().get(0);
        Assert.assertTrue(medicamento.isGenerico());
    }

    @Test
    public void testSimpleCreation() {

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(medicamentoMessage),
                medicamentoMessage);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
    }

    @Test
    public void testUnMedicamentoMessage() {

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(unMedicamentoMessage),
                unMedicamentoMessage);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        Assert.assertEquals(1, ((SolicitudConsumo) message.getTransaccion().getSolicitudServicio()).getItemsConsumo().size());
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        
        Assert.assertEquals("6001001355",message.getTransaccion().getPrestadorTransaccionador().getNumeroRelacion());
    }
    
    @Test
    public void testUnMedicamentoMessageCodigoPrestadorConCeros() {

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(unMedicamentoMessageCodigoPrestadorConCeros),
                unMedicamentoMessageCodigoPrestadorConCeros);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        Assert.assertEquals(1, ((SolicitudConsumo) message.getTransaccion().getSolicitudServicio()).getItemsConsumo().size());
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        
        Assert.assertEquals("6001001355",message.getTransaccion().getPrestadorTransaccionador().getNumeroRelacion());
    }
    
    @Test
    public void testUnMedicamentoMessageCodigoPrestadorConCerosFilial01() {

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(unMedicamentoMessageCodigoPrestadorConCerosFilial01),
                unMedicamentoMessageCodigoPrestadorConCerosFilial01);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        Assert.assertEquals(1, ((SolicitudConsumo) message.getTransaccion().getSolicitudServicio()).getItemsConsumo().size());
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        
        Assert.assertEquals("0101001355",message.getTransaccion().getPrestadorTransaccionador().getNumeroRelacion());
    }
}
