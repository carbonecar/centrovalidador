package ar.com.osde.centroValidador.pos.filter;

import junit.framework.Assert;

import org.junit.Test;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.filter.PropertyEqualFilter;

public class TestPropertyEqualFilter {

    @Test(expected = FilteredMessageException.class)
    public void notEqualFilter() {

        PropertyEqualFilter filter = new PropertyEqualFilter();

        final String PROPERTY_NAME = "getFilialInstalacionPOS";
        
        filter.setPropertyName(PROPERTY_NAME);
        
        Assert.assertEquals(filter.getPropertyName(), PROPERTY_NAME);
        
        filter.setValue(60);
        PosMessage posMessage = new PosMessage();
        posMessage.setFilialInstalacionPOS(61);

        filter.validate(posMessage);
    }
    
    @Test(expected = FilteredMessageException.class)
    public void testWrongProperty() {
        
        PropertyEqualFilter filter = new PropertyEqualFilter();
        filter.setPropertyName("wtf");
        filter.setValue("5");
        
        filter.validate(new PosMessage());
    }

    @Test
    public void equalFilter() {
        PropertyEqualFilter filter = new PropertyEqualFilter();

        filter.setPropertyName("getFilialInstalacionPOS");
        filter.setValue(60);
        PosMessage posMessage = new PosMessage();
        posMessage.setFilialInstalacionPOS(60);

        filter.validate(posMessage);
    }
}
