package ar.com.osde.centroValidador.transaccionpos;

import java.util.List;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessageFarmacia;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.aptitud.AutorizacionBid;
import ar.com.osde.entities.aptoServicio.Credencial;
import ar.com.osde.entities.aptoServicio.ItemConsumo;
import ar.com.osde.entities.aptoServicio.Medicamento;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestRegistracionMedicamentoPosMessage extends AbstractTestTransactionFactory {

    private static final String registracionMedicamentoPosMessage = "FRM  AF230001442716770454400000061949904301002F03634000000A20130410100036  000000                                                                    MS01313620130408            001010127167704544            2310271779801578013201           0000000             00           0000000             00                                                            0000000000000000   0000000000   ";

    private static final String autorizacionBidregistracionMedicamentoPosMessage98 = "FRM  AF230001442716770454400000061949904301002F03634000000A2013041010003698000012                                                                    MS01313620130408            001010127167704544            2310271779801578013201           0000000             00           0000000             00                                                            0000000000000000   0000000000   ";

    private static final String autorizacionBidregistracionMedicamentoPosMessage89 = "FRM  AF230001442716770454400000061949904301002F03634000000A2013041010003689000012                                                                    MS01313620130408            001010127167704544            2310271779801578013201           0000000             00           0000000             00                                                            0000000000000000   0000000000   ";

    private static final String autorizacionBidregistracionMedicamentoPosMessageOtro = "FRM  AF230001442716770454400000061949904301002F03634000000A2013041010003604000012                                                                    MS01313620130408            001010127167704544            2310271779801578013201           0000000             00           0000000             00                                                            0000000000000000   0000000000   ";
    private static final String registracionMedicamentoVersionMensajeria = "FRM  AF371301192322324621400000061554412504002F95179000000M20150219192325  000000                                                                    MC01511120150219            0010100                       5091151             01                               00                               00                                                            0000000000000000   0000000000                                                            1  050341 20150219  M    000000000000                                                                                2.0.0                                                                                   ";

    @Test
    public void testSimpleRegistracionMedicamento() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(registracionMedicamentoPosMessage),
                registracionMedicamentoPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(registracionMedicamentoMessage.getTransaccionConsumo());
        Credencial credencial = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getCredencial();
        Assert.assertNotNull(credencial);

        Assert.assertEquals(1, credencial.getVersion());

    }

    @Test
    public void testAutorizacionBidRegistracionMedicamentoEmisor98() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(autorizacionBidregistracionMedicamentoPosMessage98),
                autorizacionBidregistracionMedicamentoPosMessage98);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(registracionMedicamentoMessage.getTransaccionConsumo());
        Credencial credencial = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getCredencial();
        Assert.assertNotNull(credencial);
        List<ItemConsumo> itemsConsumo = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo();
        Assert.assertEquals(1, itemsConsumo.size());
        Medicamento medicamento = (Medicamento) itemsConsumo.get(0);

        Assert.assertNotNull(medicamento.getAutorizacionBidInformada());
        AutorizacionBid autorizacionBidInformada = medicamento.getAutorizacionBidInformada();

        Assert.assertEquals(98, autorizacionBidInformada.getNumeroEmisor());
        Assert.assertEquals(2, autorizacionBidInformada.getTipoEmisor());
        Assert.assertEquals(12l, autorizacionBidInformada.getNumeroAutorizacion());
        Assert.assertEquals(1, credencial.getVersion());
    }

    @Test
    public void testAutorizacionBidRegistracionMedicamentoEmisor89() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(autorizacionBidregistracionMedicamentoPosMessage89),
                autorizacionBidregistracionMedicamentoPosMessage89);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(registracionMedicamentoMessage.getTransaccionConsumo());
        Credencial credencial = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getCredencial();
        Assert.assertNotNull(credencial);
        List<ItemConsumo> itemsConsumo = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo();
        Assert.assertEquals(1, itemsConsumo.size());
        Medicamento medicamento = (Medicamento) itemsConsumo.get(0);

        Assert.assertNotNull(medicamento.getAutorizacionBidInformada());
        AutorizacionBid autorizacionBidInformada = medicamento.getAutorizacionBidInformada();

        Assert.assertEquals(89, autorizacionBidInformada.getNumeroEmisor());
        Assert.assertEquals(3, autorizacionBidInformada.getTipoEmisor());
        Assert.assertEquals(12l, autorizacionBidInformada.getNumeroAutorizacion());

        Assert.assertEquals("23", autorizacionBidInformada.getCodigoFilial());

        Assert.assertEquals(1, credencial.getVersion());
    }

    @Test
    public void autorizacionBidregistracionMedicamentoPosMessageOtro() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(autorizacionBidregistracionMedicamentoPosMessageOtro),
                autorizacionBidregistracionMedicamentoPosMessageOtro);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(registracionMedicamentoMessage.getTransaccionConsumo());
        Credencial credencial = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getCredencial();
        Assert.assertNotNull(credencial);
        List<ItemConsumo> itemsConsumo = registracionMedicamentoMessage.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo();
        Assert.assertEquals(1, itemsConsumo.size());
        Medicamento medicamento = (Medicamento) itemsConsumo.get(0);

        Assert.assertNotNull(medicamento.getAutorizacionBidInformada());
        AutorizacionBid autorizacionBidInformada = medicamento.getAutorizacionBidInformada();

        Assert.assertEquals(4, autorizacionBidInformada.getNumeroEmisor());
        Assert.assertEquals(1, autorizacionBidInformada.getTipoEmisor());
        Assert.assertEquals(12l, autorizacionBidInformada.getNumeroAutorizacion());
        Assert.assertEquals(1, credencial.getVersion());
    }

    @Test
    public void versionMensajeria() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(registracionMedicamentoVersionMensajeria),
                registracionMedicamentoVersionMensajeria);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage registracionMedicamentoMessage = (RegistracionMedicamentoMessage) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertEquals("2.0.0", registracionMedicamentoMessage.getTransaccion().getVersionMensajeria());
    }

}
