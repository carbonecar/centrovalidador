package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPrestacionPos;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionRegistracionConsumoLaboratorio extends AbstractTestTransactionFactory {

    private String transaccion2LSimple="SAF  AA600010002013808579200000061026243101002L94684700000M20130624163318000000000004751001             0000             0000             0000       0 00000000000000            001010020138085792                                                                                                                                             00000000000000000                                                                                              011227                                        000475                                                                                                                                                      ";
    @Test
    public void testSimpleTransaccion2J() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccion2LSimple),
                transaccion2LSimple);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionPrestacionPos);
        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;
        
        Assert.assertNotNull(transaccionPrestacionPos.getTransaccionConsumo());
        Assert.assertNotNull(transaccionPrestacionPos.getTransaccionConsumo().getSolicitudServicio());




        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }


}
