package ar.com.osde.centroValidador.pos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.exception.UnsupportedPosMessageException;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.handlers.PosMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.centroValidador.pos.handlers.StreamResponseMessageHandler;
import ar.com.osde.centroValidador.pos.mq.MessageCreatorFactory;
import ar.com.osde.common.colections.utils.ListBuilder;
import ar.com.osde.common.colections.utils.ListFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.pos.utils.JmsCollectorTemplate;
import ar.com.osde.cv.pos.utils.TextMessageAdapter;
import ar.com.osde.entities.aptoServicio.SocioRespuesta;
import ar.com.osde.entities.as400.Plan;
import ar.com.osde.entities.as400.RotuloIva;
import ar.com.osde.entities.as400.Socio;
import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.MensajePos;
import ar.com.osde.transaccionService.services.RespuestaAptoSocio;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

/**
 * Testeamos la generacion de respuesta.
 * 
 * @author VA27789605
 * 
 */

public class TestFixedResponseMessageCreator extends AbstractPosMessageTest {

    private String message2F1 = "11111OS600000112016444577222222260615434401802F01820233333M20101227112356  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            000000000000000000000000000000       00000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020101201             013687000000000000                                                                                                                                                             ";

    private String multimessage2F1 = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";
    private String multimessage2F2 = "11111OS600000112016444577222222260615434401802F01826233333M20110106161552  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            002020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             033100000000000000                                                                                                                                                             ";
    private String messageA1 = "     OS600000003054674125300000060788979802001A01477000000M20120209113540  0000000000000000       0000000000       0000000000       0000000000         00000000000000            001010000000000000                                                                                                         ";

    @Test
    public void aptoSocio() {
        StreamResponseMessageHandler handler = new StreamResponseMessageHandler();
        ListBuilder<String> listBuilder = ListFactory.<String> createBuilder();
        listBuilder.with("OK, ASOCIADO HABILI-").with("TADO");
        handler.setIntercepted(new DummyFixedFormatMessageHandler(new MensajePos(listBuilder.build(),
                "OK, ASOCIADO HABILI-TADO", "OK, ASOCIADO HABILI-TADO", "OK, ASOCIADO HABILI-TADO")));
        handler.setFormatManager(getMananger());

        handler.setTemplateResponse(new JmsCollectorTemplate());
        MessageCreatorFactory messageCreatorFactory=new MessageCreatorFactory();
        messageCreatorFactory.setConfiguracionService(new ConfiguracionServiceMock());
        handler.setMessageCreatorFactory(messageCreatorFactory);
        RespuestaCentroValidador respuesta;

        try {
            TextMessage message = new TextMessageAdapter();
            message.setText(messageA1);
            respuesta = handler.process(message);
            Assert.assertNotNull(respuesta);
            TextMessage txtMessage = (TextMessage) ((JmsCollectorTemplate) handler.getTemplateResponse())
                    .getMessageToSend();
            Assert.assertNotNull(txtMessage);
            Assert.assertTrue(txtMessage
                    .getText()
                    .contains(
                            "    OS60000000014770000000980000000        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-TADO                                                        "));// 000
                                                                                                                                                                                                    // 1
                                                                                                                                                                                                    // 1
                                                                                                                                                                                                    // 310
                                                                                                                                                                                                    // "));

        } catch (UnsupportedPosMessageException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }

    }

    @Test
    public void testRegistracionSimple() {
        StreamResponseMessageHandler handler = new StreamResponseMessageHandler();
        MessageCreatorFactory messageCreatorFactory=new MessageCreatorFactory();
        messageCreatorFactory.setConfiguracionService(new ConfiguracionServiceMock());
        handler.setMessageCreatorFactory(messageCreatorFactory);
        MensajePos mensajePos=new MensajePos();
        mensajePos.setLeyendaDisplayPos("OK. Medicamento Registrado");
        mensajePos.setLeyendaCompleta("OK. Medicamento Registrado");
        mensajePos.setInformacionAdicional("OK. Medicamento Registrado");
        mensajePos.agregarLeyendaImpresora("OK. Medicamento Registrado");
        
        handler.setIntercepted(new DummyFixedFormatMessageHandler(mensajePos));
        handler.setFormatManager(new FixedFormatManagerImpl());

        handler.setTemplateResponse(new JmsCollectorTemplate());

        RespuestaCentroValidador respuesta;

        try {
            TextMessage message = new TextMessageAdapter();
            message.setText(message2F1);
            respuesta = handler.process(message);
            Assert.assertNotNull(respuesta);
            TextMessage txtMessage = (TextMessage) ((JmsCollectorTemplate) handler.getTemplateResponse())
                    .getMessageToSend();
            Assert.assertNotNull(txtMessage);
            Assert.assertTrue(txtMessage.getText().contains("OK. Medicamento Registrado"));

        } catch (UnsupportedPosMessageException e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            e.printStackTrace();
        }

    }

    @Test
    public void testRegistracionMultimessage() {
        StreamResponseMessageHandler handler = new StreamResponseMessageHandler();
        MessageCreatorFactory messageCreatorFactory=new MessageCreatorFactory();
        messageCreatorFactory.setConfiguracionService(new ConfiguracionServiceMock());
        handler.setMessageCreatorFactory(messageCreatorFactory);

        MensajePos mensajePos=new MensajePos();
        mensajePos.setLeyendaDisplayPos("OK. Medicamento Registrado");
        mensajePos.setLeyendaCompleta("OK. Medicamento Registrado");
        mensajePos.setInformacionAdicional("OK. Medicamento Registrado");
        mensajePos.agregarLeyendaImpresora("OK. Medicamento Registrado");
        handler.setIntercepted(new DummyFixedFormatMessageHandler(mensajePos));
        handler.setFormatManager(new FixedFormatManagerImpl());
        handler.setTemplateResponse(new JmsCollectorTemplate());

        RespuestaCentroValidador respuesta;

        try {
            TextMessage message = new TextMessageAdapter();
            message.setText(multimessage2F1);
            respuesta = handler.process(message);

            Assert.assertTrue(respuesta.isApto());
            Assert.assertTrue(respuesta.isRespuestaParcial());

            message.setText(multimessage2F2);
            respuesta = handler.process(message);
            TextMessage txtMessage = ((TextMessage) ((JmsCollectorTemplate) handler.getTemplateResponse())
                    .getMessageToSend());

            Assert.assertTrue(respuesta.isApto());
            Assert.assertFalse(respuesta.isRespuestaParcial());
            Assert.assertTrue(txtMessage.getText().contains("OK. Medicamento Registrado"));
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

    }

    // *************************************************//
    // ************Helper classes***********************//
    // *************************************************//

    private class DummyFixedFormatMessageHandler implements PosMessageHandler{

        private MensajePos mensajePos;

        public DummyFixedFormatMessageHandler(MensajePos mensajePos) {
            this.mensajePos = mensajePos;
        }

        public RespuestaCentroValidador process(Message textMessage) throws UnsupportedPosMessageException {

            FixedFormatManager manager = new FixedFormatManagerImpl();

            Mapper mapper = getMapper();
            AbstractPosMessage message;
            try {
                String text = ((TextMessage) textMessage).getText();
                message = manager.load(FixedFormatMessageClassFactory.getClassInstance(text), text);
                AbstractTransaccionPos transaccion = TransaccionFactory.getInstance().createTransaccion(message, mapper);

                RespuestaAptoSocio respuesta = new RespuestaAptoSocio();
                RespuestaAptitud respuestaAptitud = new RespuestaAptitud();
                respuesta.setMensajePos(this.mensajePos);
                respuestaAptitud.setApto(true);
                respuesta.setRespuestaAptitud(respuestaAptitud);

                RespuestaCentroValidador respuestaCV = new RespuestaCentroValidador(respuesta, message, transaccion);

                respuestaCV.setIsRespuestaParcial(message.getNumeroSecuenciaMensaje() != message
                        .getNumeroTotalMensajes());
                Socio socio = new Socio();
                Plan planServicio = new Plan();
                planServicio.setPlan("310");
                planServicio.setPrefijo(2);
                socio.setPlanServicio(planServicio);
                Calendar cal=Calendar.getInstance();
                cal.set(2010, 1,30);
                socio.setFechaNacimiento(cal.getTime());
                RotuloIva rotuloIva=new RotuloIva();
                rotuloIva.setCodigo("1");
                socio.setRotuloIva(rotuloIva);
                SocioRespuesta socioRespuesta = new SocioRespuesta();
                socioRespuesta.setSocio(socio);
                respuesta.getRespuestaAptitud().setSocio(socioRespuesta);
                return respuestaCV;
            } catch (JMSException e) {
                throw new UnsupportedPosMessageException(e.getMessage());

            }
        }

        public String getHierarchyName() {
            return "DummyFixedFormatMessageHandler\n";
        }

        public boolean procesaMensaje() {
            return false;
        }

        public List<PosMessageFilter> getMessageFilters() {
            return new ArrayList<PosMessageFilter>();
        }
    }
}
