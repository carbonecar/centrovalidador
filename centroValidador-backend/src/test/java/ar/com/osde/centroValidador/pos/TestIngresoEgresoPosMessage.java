package ar.com.osde.centroValidador.pos;

import org.junit.Assert;
import org.junit.Test;

public class TestIngresoEgresoPosMessage extends AbstractPosMessageTest {


    private static final String parseoSimple2I = "03399IT600001243054585245000448061705235101002I49438703399M20121029164811000000000000000000       0000000000       0000000000       0000000000       MC07093020121029            0010100000000000000000446471-9                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         2012-10-29-16.47.28.269000</WMQTool>";

    @Test
    public void testParseoSimple() {
        IngresoEgresoPosMessage msj = getMananger().load(IngresoEgresoPosMessage.class, parseoSimple2I);
        Assert.assertNotNull(msj);
    }
}
