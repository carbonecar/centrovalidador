package ar.com.osde.centroValidador.transaccionpos;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.CredencialPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.commons.otorgamiento.DateUtils;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.entities.aptitud.ContextoEvaluacion;
import ar.com.osde.entities.aptoServicio.Credencial;
import ar.com.osde.entities.transaccion.ItemPrestador;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionConsumoDiferida extends AbstractTestTransactionFactory {

    private static final String transaccionUrgencias = "     OS600001002007722005500000060831769102002D80920600000M20121128103826  0000004202014001       0000000000       0000000000       0000000000         00000020121129            001010100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                        000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           000012";

    private static final String transaccionUrgenciasSinCaso = "     OS600001002007722005500000060831769102002D80920600000M20121128103826  0000004202014001       0000000000       0000000000       0000000000         00000020121128            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       abcdef";

    private static final String transaccionUrgenciasCasoBlanco = "     OS600001002007722005500000060831769102002D80920600000M20121128103826  0000004202014001       0000000000       0000000000       0000000000         00000020121128            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ";

    @Test
    public void testSimpleTransaccionUgencias() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccionUrgencias),
                transaccionUrgencias);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof CredencialPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionPrestacionPos);
        TransaccionPrestacionPos transaccionPrestacionPosUrgencias = (TransaccionPrestacionPos) message;
        Credencial credencial=transaccionPrestacionPosUrgencias.getTransaccion().getSolicitudServicio().getCredencial();
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes()); 
        Assert.assertEquals(ContextoEvaluacion.OFFLINE, transaccionPrestacionPosUrgencias.getTransaccionConsumo().getSolicitudServicio().getContextoEvaluacion());
        Assert.assertEquals(12, transaccionPrestacionPosUrgencias.getTransaccionConsumo().getSolicitudServicio().getCasoUrgencia()
                .getNumeroCaso());
        List<ItemPrestador> itemsPrestador = transaccionPrestacionPosUrgencias.getTransaccionConsumo().getItemsPrestador();

        Assert.assertEquals(1, itemsPrestador.size());
        ItemPrestador itemPrestador = itemsPrestador.get(0);
        Date fechaConsumo = null;
        try {
            fechaConsumo = DateUtils.createDateAAAAMMDD("20121129");
        } catch (ParseException e) {
            Assert.fail(e.getMessage());

        }
        Assert.assertEquals(fechaConsumo, DateUtils.getDateWithOutTime(itemPrestador.getFechaConsumo()));
        Assert.assertNotNull(credencial);
        
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo());
        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }

    @Test
    public void testTransaccionUgenciasSinCasoAsociado() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccionUrgenciasSinCaso),
                transaccionUrgenciasSinCaso);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionPrestacionPos);
        TransaccionPrestacionPos transaccionPrestacionPosUrgencias = (TransaccionPrestacionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());

        Assert.assertTrue(transaccionPrestacionPosUrgencias.getTransaccionConsumo().getSolicitudServicio().getCasoUrgencia() == null);
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo());
        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }

    @Test
    public void testTransaccionUgenciasCasoAsociadoBlanco() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccionUrgenciasCasoBlanco),
                transaccionUrgenciasCasoBlanco);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionPrestacionPos);
        TransaccionPrestacionPos transaccionPrestacionPosUrgencias = (TransaccionPrestacionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());

        Assert.assertTrue(transaccionPrestacionPosUrgencias.getTransaccionConsumo().getSolicitudServicio().getCasoUrgencia() == null);
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo());
        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }
}
