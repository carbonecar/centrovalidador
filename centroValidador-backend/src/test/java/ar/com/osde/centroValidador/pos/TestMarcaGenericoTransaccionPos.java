package ar.com.osde.centroValidador.pos;

import java.util.List;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.MarcaGenericoTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.aptoServicio.Medicamento;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestMarcaGenericoTransaccionPos extends AbstractTestTransactionFactory {

    @Test
    public void testSimpleMargaGenerico() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(TestMarcaGenerico4GPosMessage.marcaGenerico4G),
                TestMarcaGenerico4GPosMessage.marcaGenerico4G);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof MarcaGenericoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof MarcaGenericoTransaccionPos);
        MarcaGenericoTransaccionPos transaccionMarcaGenerico = (MarcaGenericoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());

        List<Medicamento> medicamentos = transaccionMarcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getMedicamentos();

        Assert.assertEquals(1, medicamentos.size());
        Medicamento medicamento=medicamentos.get(0);
      
        Assert.assertEquals("CR", transaccionMarcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getTransaccionReferencia().getCodigoOperador());
        Assert.assertTrue(medicamento.isGenerico());

    }


    @Test
    public void testSimpleDesMargaGenerico() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(TestMarcaGenerico4GPosMessage.desmarcarGenerico4G),
                TestMarcaGenerico4GPosMessage.desmarcarGenerico4G);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof MarcaGenericoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof MarcaGenericoTransaccionPos);
        MarcaGenericoTransaccionPos transaccionMarcaGenerico = (MarcaGenericoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());

        List<Medicamento> medicamentos = transaccionMarcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getMedicamentos();

        Assert.assertEquals(1, medicamentos.size());
        Medicamento medicamento=medicamentos.get(0);
        Assert.assertEquals("CR", transaccionMarcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getTransaccionReferencia().getCodigoOperador());
        Assert.assertFalse(medicamento.isGenerico());
    }
}
