package ar.com.osde.centroValidador.pos;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ar.com.osde.cv.model.Secuencia;
import ar.com.osde.cv.model.TransaccionPos;
import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.aptoServicio.PrestadorTransaccionador;
import ar.com.osde.entities.aptoServicio.SolicitudConsumo;
import ar.com.osde.entities.transaccion.TransaccionConsumo;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionPos extends AbstractJUnit4SpringContextTests {

	@Test
	public void testGenerateIDFromTransaccionPos() {
		TransaccionPos transaccionPos = new TransaccionPos();

		TerminalID terminal = new TerminalID();
		terminal.setCodigoOperador("10");
		terminal.setAgrupador(61);
		terminal.setCodigoFilial("60");
		terminal.setNumero(1234);
		PrestadorTransaccionador prestador = new PrestadorTransaccionador();
		prestador.setTerminalID(terminal);
		TransaccionConsumo transaccionConsumo = new TransaccionConsumo();
		transaccionConsumo.setPrestadorTransaccionador(prestador);
		transaccionConsumo.setNumeroTransaccion(555555);
		SolicitudConsumo solicitudConsumo = new SolicitudConsumo();
		Calendar cal=Calendar.getInstance();
		cal.set(2015, 1, 1, 10, 10,10);
		Date fechaSolicitudServicio=cal.getTime();
		solicitudConsumo.setFechaSolicitudServicio(fechaSolicitudServicio);
		transaccionConsumo.setSolicitudServicio(solicitudConsumo);
		transaccionPos.setTransaccionConsumo(transaccionConsumo);
		Secuencia secuencia = new Secuencia();
		secuencia.setNumeroSecuenciaMensaje(1);
		secuencia.setNumeroTotalMensajes(1);
		transaccionPos.setSecuencia(secuencia);
		//Assert.assertEquals(1142648009, transaccionPos.getId());
		Assert.assertEquals(1, transaccionPos.getSecuencia().getNumeroSecuenciaMensaje());

	}
	@Test
	public void testGeneratedIdFromPosMessage(){
	    PosMessage posMessage=new PosMessage();
	    LocalDate localDate=new LocalDate(2014, 3, 23);
	    posMessage.setFechaTransaccion(localDate);
	    posMessage.setCodigoOperador("AI");
	    posMessage.setNumeroTransaccion(535455);
	    posMessage.setNumeroTotalMensajes(3);
	    posMessage.setNumeroSecuenciaMensaje(1);
	    
	    Assert.assertEquals(1315690173,posMessage.getId());
	}
}
