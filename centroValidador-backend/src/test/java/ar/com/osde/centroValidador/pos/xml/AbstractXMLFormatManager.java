package ar.com.osde.centroValidador.pos.xml;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ar.com.osde.centroValidador.pos.handlers.xml.XMLFormatManager;
import ar.com.osde.centroValidador.pos.utils.StringUtil;
import ar.com.osde.cv.transaccion.Transacciones;

import com.ancientprogramming.fixedformat4j.exception.FixedFormatException;

public abstract class AbstractXMLFormatManager<T> {

    protected abstract String getResourceName();

    protected abstract T getTransaccion(Transacciones transaccion);

    protected abstract void makeAssertions(Transacciones transaccionSerializada, Transacciones transaccionoriginal);

    public AbstractXMLFormatManager() {
        super();
    }

    @Ignore
    @Test
    public void testLoadAndExport() throws IOException {

        String message = "";

        message = StringUtil.convertStreamToString(getClass().getResourceAsStream(this.getResourceName()));

        XMLFormatManager xmlFormatManager = new XMLFormatManager();
        xmlFormatManager.setFileName("transacciones.xsd");

        Transacciones transaccion = null;

        transaccion = xmlFormatManager.load(Transacciones.class, message);

        Assert.assertNotNull(transaccion);
        Assert.assertNotNull("Se esperaba una transaccion ", this.getTransaccion(transaccion));

        transaccion.setVersionXsd("1.0");

        String xmlTransaccion = xmlFormatManager.export(transaccion);

        Transacciones transaccionSerializada = xmlFormatManager.load(Transacciones.class, xmlTransaccion);

        Assert.assertEquals(transaccionSerializada.getVersionXsd(), "1.0");
        Assert.assertNotNull(transaccionSerializada);
        this.makeAssertions(transaccionSerializada, transaccion);
    }

    @Test(expected = FixedFormatException.class)
    public void testWrongSchema() throws IOException {

        String message = "";

        message = StringUtil.convertStreamToString(getClass().getResourceAsStream(
                "/testcases/badformed-transaccion.xml"));
        new XMLFormatManager().load(Transacciones.class, message);

    }

}