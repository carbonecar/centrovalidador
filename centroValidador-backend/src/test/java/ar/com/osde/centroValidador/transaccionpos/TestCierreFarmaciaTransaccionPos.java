package ar.com.osde.centroValidador.transaccionpos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.CierreFarmaciaPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.CierreFarmaciaTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = {"/spring/test/osde-framework-dozer-spring.xml"})
public class TestCierreFarmaciaTransaccionPos extends
        AbstractTestTransactionFactory {

    private static final String cierreFarmacia = "     WG609000140000000000000000000000000000005F00308700000 20121213164405  0000000000000000       0000000000       0000000000       0000000000         00000000000000            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AA0000000020120808  00000000000                                                            000000000000000000   TV 0000000000000000000000000                                                                                                                                                            ";

    @Test
    public void testSimpleCierre() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager
                .load(FixedFormatMessageClassFactory
                        .getClassInstance(cierreFarmacia), cierreFarmacia);
        Assert.assertTrue(posMessage.getClass().getName(),
                posMessage instanceof CierreFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance()
                .createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof CierreFarmaciaTransaccionPos);
        CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos = (CierreFarmaciaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia()
                .getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(cierreFarmaciaTransaccionPos
                .getTransaccionCierre());

        Date fechaHasta = cierreFarmaciaTransaccionPos.getTransaccionCierre()
                .getSolicitudServicio().getFechaTransaccionHasta();

        Calendar cal = Calendar.getInstance();
        cal.set(2012, 7, 8, 23, 59, 59);
        Assert.assertEquals(DateUtils.truncate(fechaHasta, Calendar.HOUR),
                DateUtils.truncate(cal.getTime(), Calendar.HOUR));
        Assert.assertNotNull(cierreFarmaciaTransaccionPos
                .getTransaccionCierre().getSolicitudServicio());
        Assert.assertNotNull(fechaHasta);

    }

    @Test
    public void testCierreConFechaMayorAyer() {

        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyyMMdd");
        String cierreFechaMayorAyer = cierreFarmacia.replace("20120808",
                dateFormater.format(new Date()));
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory
                        .getClassInstance(cierreFechaMayorAyer),
                cierreFechaMayorAyer);
        Assert.assertTrue(posMessage.getClass().getName(),
                posMessage instanceof CierreFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance()
                .createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof CierreFarmaciaTransaccionPos);
        CierreFarmaciaTransaccionPos cierreFarmaciaTransaccionPos = (CierreFarmaciaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia()
                .getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(cierreFarmaciaTransaccionPos
                .getTransaccionCierre());

        Date fechaHasta = cierreFarmaciaTransaccionPos.getTransaccionCierre()
                .getSolicitudServicio().getFechaTransaccionHasta();

        Date hoyHorasTruncadas = DateUtils.truncate(new Date(),
                Calendar.DAY_OF_MONTH);
        Date ayer = DateUtils.add(hoyHorasTruncadas, Calendar.MILLISECOND, -1);
//        Assert.assertEquals(ayer, cierreFarmaciaTransaccionPos.getTransaccion()
//                .getSolicitudServicio().getFechaTransaccionHasta());
        Assert.assertNotNull(cierreFarmaciaTransaccionPos
                .getTransaccionCierre().getSolicitudServicio());
        Assert.assertNotNull(fechaHasta);

    }

}
