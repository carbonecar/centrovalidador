package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.ProtocoloPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.EnvioDocumentacionTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.aptitud.TipoInformacionAdjunta;
import ar.com.osde.entities.aptoServicio.BeneficiarioBid;
import ar.com.osde.entities.aptoServicio.InformacionAutorizacion;
import ar.com.osde.entities.aptoServicio.InformacionTransaccion;
import ar.com.osde.entities.aptoServicio.SolicitudEnvioDocumentacion;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestEnvioDocumentacionTransaccionPos extends AbstractTestTransactionFactory {

    private static final String transacionEnvioDocumentacionConAutorizacion = "IVR  AI600000102717031265700000061795707901 02P31747600000M20140815142942890890181103133001             0000       0000000000       0000000000       MC11752220140815            0010100                                                                                                                          8908901861795707901317476.TIF                                                                                                                058386                                                                                                                                                                                                    ";

    private static final String transacionEnvioDocumentacionInformacionTransaccion = "IVR  AI600000112022046123900000060496689102 02P31830800000M20140815150153IM3183012001801001             0000       0000000000       0000000000       IM00000000000000            0010100                                                                                                                          IM31830160496689102318308.TIF                                                                                                                050968                                                                                                                                                                                                    ";

    private static final String transaccionEnvioDocumentacionInformacionTransaccion2 = "IVR  AI600000102717031265700000061795707901 02P31747600000M20140815142942  0890181103133001             0000       0000000000       0000000000       MC11752220140815            0010100                                                                                                                          8908901861795707901317476.TIF                                                                                                                058386                                                                                                                                                                                                    ";

    private static final String transaccionSolicitudEnvioDocumentacion = "IVR  AI360000103054674125300000061430340001 02P34797700000M20140819100420CC0000000000000000       0000000000       0000000000       0000000000       CC00000000000000            0010100                                                                                                                          CC00000061430340001347977.TIF                                                                                                                900000                                                                                                                                                                                                    ";

    @Test
    public void testSimpleTransaccionInformacionAutorizacion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(transacionEnvioDocumentacionConAutorizacion),
                transacionEnvioDocumentacionConAutorizacion);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ProtocoloPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof EnvioDocumentacionTransaccionPos);
        EnvioDocumentacionTransaccionPos envioDocumentacionTransaccionPos = (EnvioDocumentacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals(317476, envioDocumentacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertTrue(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio() instanceof InformacionAutorizacion);
        Assert.assertEquals(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getNombreImagen(),
                "8908901861795707901317476.TIF");
        BeneficiarioBid beneficiarioBid = envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid();

        Assert.assertNotNull(beneficiarioBid);
        Assert.assertEquals("61", beneficiarioBid.getNroContrato().getPrefijo());
        Assert.assertEquals("7957079", beneficiarioBid.getNroContrato().getNumero());
        Assert.assertEquals("01", beneficiarioBid.getOrden());

        Assert.assertEquals(TipoInformacionAdjunta.PROTOCOLO.name(), envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTipo());
    }

    @Test
    public void testSimpleTransaccionInformacionTransaccion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(transacionEnvioDocumentacionInformacionTransaccion),
                transacionEnvioDocumentacionInformacionTransaccion);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ProtocoloPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof EnvioDocumentacionTransaccionPos);
        EnvioDocumentacionTransaccionPos envioDocumentacionTransaccionPos = (EnvioDocumentacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals(318308, envioDocumentacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertTrue(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio() instanceof InformacionTransaccion);

        Assert.assertEquals(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getNombreImagen(),
                "IM31830160496689102318308.TIF");
        

        Assert.assertEquals(TipoInformacionAdjunta.INFORME_MEDICO.name(), envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTipo());
    }

    @Test
    public void testEspaciosTransaccionInformacionTransaccion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(transaccionEnvioDocumentacionInformacionTransaccion2),
                transaccionEnvioDocumentacionInformacionTransaccion2);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ProtocoloPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof EnvioDocumentacionTransaccionPos);
        EnvioDocumentacionTransaccionPos envioDocumentacionTransaccionPos = (EnvioDocumentacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals(317476, envioDocumentacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertTrue(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio() instanceof InformacionTransaccion);
        Assert.assertEquals(89018, ((InformacionTransaccion) envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio())
                .getTransaccionReferencia().getNumeroTransaccion());

        
        Assert.assertEquals(TipoInformacionAdjunta.INFORME_MEDICO.name(), envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTipo());
    }

    @Test
    public void testSoliciutdEnvioDocumentacion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(
                FixedFormatMessageClassFactory.getClassInstance(transaccionSolicitudEnvioDocumentacion),
                transaccionSolicitudEnvioDocumentacion);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ProtocoloPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof EnvioDocumentacionTransaccionPos);
        EnvioDocumentacionTransaccionPos envioDocumentacionTransaccionPos = (EnvioDocumentacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals(347977, envioDocumentacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertTrue(envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio() instanceof SolicitudEnvioDocumentacion);

        Assert.assertEquals(TipoInformacionAdjunta.CCC.name(), envioDocumentacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTipo());
    }

}
