package ar.com.osde.centroValidador.pos;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import javax.jws.WebParam;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import ar.com.osde.centroValidador.bo.impl.AptoServicioBOImpl;
import ar.com.osde.centroValidador.pos.exception.BadFormatException;
import ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.RespuestaCentroValidador;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.cv.pos.utils.PosMessageHashMapCrudImpl;
import ar.com.osde.cv.pos.utils.TextMessageAdapter;
import ar.com.osde.entities.aptoServicio.Beneficiario;
import ar.com.osde.entities.aptoServicio.Medicamento;
import ar.com.osde.entities.aptoServicio.PracticaPensionTranslado;
import ar.com.osde.entities.aptoServicio.ProductoMedico;
import ar.com.osde.entities.aptoServicio.SolicitudServicio;
import ar.com.osde.entities.otorgamiento.aptitud.RespuestaAptitud;
import ar.com.osde.entities.transaccion.TransaccionAnulacion;
import ar.com.osde.entities.transaccion.TransaccionBID;
import ar.com.osde.entities.transaccion.TransaccionBloqueoDesbloqueo;
import ar.com.osde.entities.transaccion.TransaccionCierre;
import ar.com.osde.entities.transaccion.TransaccionConsulta;
import ar.com.osde.entities.transaccion.TransaccionConsumo;
import ar.com.osde.entities.transaccion.TransaccionEnvioDocumentacion;
import ar.com.osde.entities.transaccion.TransaccionFichaCatastral;
import ar.com.osde.entities.transaccion.TransaccionIngresoEgreso;
import ar.com.osde.entities.transaccion.TransaccionMarcaGenerico;
import ar.com.osde.entities.transaccion.TransaccionMigracion;
import ar.com.osde.entities.transaccion.TransaccionRescate;
import ar.com.osde.entities.transaccion.TransaccionTimeOut;
import ar.com.osde.framework.services.ServiceException;
import ar.com.osde.services.otorgamiento.TransaccionesService;
import ar.com.osde.transaccionService.services.RespuestaAnulacion;
import ar.com.osde.transaccionService.services.RespuestaAptoSocio;
import ar.com.osde.transaccionService.services.RespuestaBloqueoDesbloqueo;
import ar.com.osde.transaccionService.services.RespuestaCierre;
import ar.com.osde.transaccionService.services.RespuestaConsulta;
import ar.com.osde.transaccionService.services.RespuestaEnvioDocumentacion;
import ar.com.osde.transaccionService.services.RespuestaGenerarSolicitudAutorizacion;
import ar.com.osde.transaccionService.services.RespuestaIngresoEgreso;
import ar.com.osde.transaccionService.services.RespuestaMarcaGenerico;
import ar.com.osde.transaccionService.services.RespuestaRegistracionConsumos;
import ar.com.osde.transaccionService.services.RespuestaRegistracionFichaCatastral;
import ar.com.osde.transaccionService.services.RespuestaRescate;
import ar.com.osde.transaccionService.services.RespuestaTransaccion;
import ar.com.osde.transaccionService.services.RespuestaValidacionConsumos;

public class TestFixedFormatMessageHandler extends AbstractPosMessageTest {
    

    @Test
    public void testSimpleMessageHandled() {

        String simpleMessage = "11111OS600000112016444577222222260615434401802F01820233333M20101227112356  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            000000000000000000000000000000       00000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020101201             013687000000000000                                                                                                                                                             ";

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        TextMessage textMessage = createTextMessage(simpleMessage);

        RespuestaCentroValidador respuesta = handler.process(textMessage);

        Assert.assertTrue(respuesta.isApto());
        Assert.assertNotNull(respuesta);
    }

    // TODO: Poner nombres mas significativos.
    @Test
    public void testSpecialCase1() {

        String specialCase = "ODO  AO600073852705711991300591260521933704002C19180000000 20110726115200OP0000008404121001--           0000             0000             0000         00000000000000            0010100                       INFERIOR                                                                                                                         00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          ";
        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        handler.process(textMessage);
    }

    @Test
    public void testSpecialCase2() {

        String specialCase = "01943IT600001243054585245000448061714298901002I44671101943M20110726144115000000000000000000       0000000000       0000000000       0000000000       MC12036020110726            0010100000000000000000432826-3                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         ";
        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        handler.process(textMessage);
    }

    @Test
    @Ignore
    public void testSpecialCase3() {

        String specialCase = "FRM  AF600001493069699885600000060739605802004A22598500000M20110726160701  225968                                                                      00000000000026            0010100                                           00                               00                               00                                                            0000000000000000   0000000000                                                               000000000000000       000000                                                                                                                                                                              ";
        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        handler.process(textMessage);
    }

    @Test
    public void testSpecialCase4() {

        String specialCase = "SAF  AA600005203059873954000000070554/00   001A22619100000M20110726160832  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          ";
        TextMessage textMessage = createTextMessage(specialCase);
        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        try {
            handler.process(textMessage);
        } catch (BadFormatException e) {
            Assert.assertEquals("int", e.getExpectedType());
            Assert.assertEquals("554/00 ", e.getCauseText());
        }
    }

    @Test
    @Ignore
    public void testSpecialCase5() {

        String specialCase = "SAF  AA60000000           000000000000000000   23031300000M20110726163802  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100O                                                                                                                                                                          ";
        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        handler.process(textMessage);
    }

    @Test
    public void testSpecialCase6() {

        String specialCase = "IVR  AI6000001127209879609000000#2261040139 01A39484300000M20110726164843  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                                           ";
        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Test
    public void testSpecialCase7() {
        String specialCase = "ODO  AO6000864620176995042051896610647802  001A23135200000 20110726164558  000000      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          ";

        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Test
    public void testSpecialCase8() {
        String specialCase = "SAF  AA6000691033561330609000000554        001A23163000000M20110726164822  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                        00000000000000000  0000000000000                                                                                                                                                                                                                                                                                          ";

        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Test
    @Ignore
    public void testSpecialCase9() {

        String specialCase = "03628IT600000453068822431061053161392477002002A45116403628M20110726165233XX0000000002981001       298A  1001       298B  1001       0061611001       MC09291000000000            001020000000000000000000000000                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         ";

        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Test
    @Ignore
    public void testSpecialCase10() {

        String specialCase = "08106TR600035923065842917161053160899597402902A880981TP2DUA20110726171430000000004202661001       0000000000       0000000000       0000000000         00000000000000            0    01MN-97261   000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ";

        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Test
    @Ignore
    public void testSpecialCase11() {

        String specialCase = "02359IT600000363054586675300000061394485101001A45189102359M2011072617130000  88880000000000       0000000000       0000000000       0000000000       MC12702300000000            001010000000000000000000073882                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         ";

        TextMessage textMessage = createTextMessage(specialCase);

        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();
        handler.process(textMessage);
    }

    @Ignore
    public void testMultipleMessageHandled() {

        String firstMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";

        String secondMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161552  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            002020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             033100000000000000                                                                                                                                                             ";
        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        TextMessage textMessage = createTextMessage(firstMessage);
        TextMessage textMessage2 = createTextMessage(secondMessage);

        RespuestaCentroValidador respuesta = handler.process(textMessage);

        Assert.assertNotNull(respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = handler.process(textMessage2);

        Assert.assertNotNull(respuesta);
        Assert.assertFalse(respuesta.isRespuestaParcial());
    }

    @Test
    @Ignore
    public void testUnorderedMultipleMessageHandled() {

        String secondMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";

        String firstMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161552  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            002020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             033100000000000000                                                                                                                                                             ";
        FixedFormatMessageHandler handler = createFixedFormatMessageHandler();

        FixedFormatHandlerThreadHelper t1 = new FixedFormatHandlerThreadHelper(handler, secondMessage);
        t1.start();

        FixedFormatHandlerThreadHelper t2 = new FixedFormatHandlerThreadHelper(handler, firstMessage);
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }
        RespuestaCentroValidador respuesta = t1.getRespuesta();
        Assert.assertNotNull("se esperaba una respuesta no nula", respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = t2.getRespuesta();
        Assert.assertNotNull(respuesta);
        Assert.assertFalse(respuesta.isRespuestaParcial());

    }

    @Test
    @Ignore
    public void testLargerMultipleMessage() {
        String secondMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            001050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";

        String firstMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            002050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";
        String thirdMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            003050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";
        String fourthMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            004050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";
        String fifthMessage = "11111OS600000112016444577222222260615434401802F01826233333M20110106161524  0000000000000000       0000000000       0000000000       0000000000       MN65432100000000            005050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020110101             023826026726026726                                                                                                                                                             ";

        FixedFormatMessageHandler handler = this.createFixedFormatMessageHandler();

        FixedFormatHandlerThreadHelper t1 = new FixedFormatHandlerThreadHelper(handler, firstMessage);
        t1.start();

        FixedFormatHandlerThreadHelper t2 = new FixedFormatHandlerThreadHelper(handler, secondMessage);
        t2.start();
        FixedFormatHandlerThreadHelper t3 = new FixedFormatHandlerThreadHelper(handler, thirdMessage);
        t3.start();
        FixedFormatHandlerThreadHelper t4 = new FixedFormatHandlerThreadHelper(handler, fourthMessage);
        t4.start();
        FixedFormatHandlerThreadHelper t5 = new FixedFormatHandlerThreadHelper(handler, fifthMessage);

        t5.start();
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }
        RespuestaCentroValidador respuesta = t1.getRespuesta();
        Assert.assertNotNull("Se esperaba una respuesta no nula: ", respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = t2.getRespuesta();
        Assert.assertNotNull("Se esperaba una respuesta no nula: ", respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = t3.getRespuesta();
        Assert.assertNotNull("Se esperaba una respuesta no nula: ", respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = t4.getRespuesta();
        Assert.assertNotNull("Se esperaba una respuesta no nula: ", respuesta);
        Assert.assertTrue(respuesta.isRespuestaParcial());

        respuesta = t5.getRespuesta();
        Assert.assertNotNull("Se esperaba una respuesta no nula: ", respuesta);
        Assert.assertFalse(respuesta.isRespuestaParcial());

    }

    private TextMessage createTextMessage(String text) {

        TextMessage textMessage = new TextMessageAdapter();

        try {
            textMessage.setText(text);
        } catch (JMSException e) {
            Assert.fail();
        }

        return textMessage;
    }

    private FixedFormatMessageHandler createFixedFormatMessageHandler() {
        FixedFormatMessageHandler handler = new FixedFormatMessageHandler();
        handler.setAptoServicioBO(new AptoServicioBOImpl());
        handler.setFormatManager(getMananger());
        handler.setBeanMapper(this.getMapper());
        handler.setTransaccionPrestacionCrud(new PosMessageHashMapCrudImpl());
        ((AptoServicioBOImpl) handler.getAptoServicioBO()).setTransaccionService(new DummyTransaccionService());
        ((AptoServicioBOImpl)handler.getAptoServicioBO()).setTransaccionStadistics(new TransaccionServiceStatistics());

        return handler;
    }

    // *************************************************//
    // ****************Helper clases********************//
    // *************************************************//

    private class FixedFormatHandlerThreadHelper extends Thread {

        private FixedFormatMessageHandler handler;
        private String message;
        private RespuestaCentroValidador respuesta;

        public FixedFormatHandlerThreadHelper(FixedFormatMessageHandler handler, String message) {
            this.handler = handler;
            this.message = message;
        }

        @Override
        public void run() {
            TextMessage textMessage = new TextMessageAdapter();
            try {
                textMessage.setText(message);
            } catch (JMSException e) {
                Assert.fail();
            }
            this.respuesta = handler.process(textMessage);
        }

        public RespuestaCentroValidador getRespuesta() {
            return this.respuesta;
        }
    }

    private class DummyTransaccionService /* extends DummyAptitudServiceImpl */implements TransaccionesService {

        public RespuestaAptitud isAptoServicio(@WebParam(name = "solicitud", header = false) SolicitudServicio solicitud,
                @WebParam(name = "beneficiario", header = false) Beneficiario beneficiario,
                @WebParam(name = "prestaciones", header = false) List<PracticaPensionTranslado> prestaciones,
                @WebParam(name = "medicamentosInformados") List<Medicamento> medicamentosInformados,
                @WebParam(name = "productoMedico") List<ProductoMedico> productosMedicos) throws ServiceException {
            RespuestaAptitud respuestaAptitud = new RespuestaAptitud();
            respuestaAptitud.setApto(true);
            return respuestaAptitud;
        }

        public RespuestaEnvioDocumentacion envioDocumentacion(TransaccionEnvioDocumentacion transaccionEnvioDocumentacion)
                throws ServiceException {
            return null;
        }

        public RespuestaEnvioDocumentacion envioDocumentacion(TransaccionEnvioDocumentacion transaccionEnvioDocumentacion, Byte[] imagen)
                throws ServiceException {
            return null;
        }

        public RespuestaGenerarSolicitudAutorizacion generarSolicitudAutorizacion(TransaccionConsumo transaccionConsumo)
                throws ServiceException {
            return null;
        }

        public RespuestaIngresoEgreso informarIngresoEgresoInternacion(TransaccionIngresoEgreso transaccionIngresoEgreso)
                throws ServiceException {
            return null;
        }

        public RespuestaRegistracionConsumos registrarConsumos(TransaccionConsumo transaccionConsumo) throws ServiceException {
            RespuestaRegistracionConsumos respuestaRegistracionConsumos = new RespuestaRegistracionConsumos();
            RespuestaAptitud respuestaAptitud = new RespuestaAptitud();
            respuestaAptitud.setApto(true);
            respuestaRegistracionConsumos.setRespuestaAptitud(respuestaAptitud);
            return respuestaRegistracionConsumos;
        }

        public RespuestaValidacionConsumos validarConsumos(TransaccionConsumo transaccionConsumo) throws ServiceException {
            return null;
        }

        public RespuestaAptoSocio validarSocio(TransaccionConsumo transaccionConsumo) throws ServiceException {
            return null;
        }

        public RespuestaRegistracionFichaCatastral registrarFichaCatastral(TransaccionFichaCatastral transaccionFichaCatastral)
                throws ServiceException {
            return null;
        }

        public RespuestaAnulacion registrarAnulacion(TransaccionAnulacion transaccionAnulacion) throws ServiceException {
            return null;
        }

        public RespuestaCierre generarCierre(TransaccionCierre trancaccionCierre) {
            return null;
        }

        public List<String> getEventosPublicados() {
            return null;
        }

        public RespuestaRescate registrarRescate(TransaccionRescate transaccionRescate) throws ServiceException {
            return null;
        }

        public RespuestaConsulta registrarConsulta(TransaccionConsulta transaccionConsulta) throws ServiceException {
            return null;
        }

        public RespuestaMarcaGenerico informarMarcaGenerico(TransaccionMarcaGenerico transaccionMarcaGenerico) throws ServiceException {
            return null;
        }

        public RespuestaBloqueoDesbloqueo informarBloqueoDesbloqueo(TransaccionBloqueoDesbloqueo transaccionBloqueoDesbloqueo)
                throws ServiceException {
            return null;
        }

        public RespuestaTransaccion registrarTimeOut(TransaccionTimeOut transaccionTimeout) throws ServiceException {
            return null;
        }

        public RespuestaTransaccion registrarMigracion(TransaccionTimeOut transaccionTimeout) throws ServiceException {
            return null;
        }

        public RespuestaTransaccion registrarMigracion(TransaccionMigracion transaccionMigracion) throws ServiceException {
            // TODO Auto-generated method stub
            return null;
        }

        public void republicarTransaccion(TransaccionBID transaccionBID) {
            // TODO Auto-generated method stub
            
        }
    }
}
