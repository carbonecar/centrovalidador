package ar.com.osde.centroValidador.pos;

import java.io.Serializable;

import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.comunes.utils.LocalDateFormatter;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.SocioPosMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.entities.aptoServicio.Matricula;
import ar.com.osde.entities.aptoServicio.Medicamento;
import ar.com.osde.entities.aptoServicio.SolicitudConsumo;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Sign;
import com.ancientprogramming.fixedformat4j.format.FormatInstructions;
import com.ancientprogramming.fixedformat4j.format.data.FixedFormatBooleanData;
import com.ancientprogramming.fixedformat4j.format.data.FixedFormatDecimalData;
import com.ancientprogramming.fixedformat4j.format.data.FixedFormatNumberData;
import com.ancientprogramming.fixedformat4j.format.data.FixedFormatPatternData;

public class TestPosMessage extends AbstractPosMessageTest {

    private static final String msj1 = "     OS600000003054674125300000061058695401001A03015000000M20100330171909  0000000000000000       0000000000       0000000000       0000000000         00000000000000            001010000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ";
    private static final String msj2 = "SAF  AA000069332008447205100000060728835201002A93836900000M20100330181316  0000000004751001       0004121001       0004811001       0060941001         00000000000000            001040020084472051                                                                                                                                             00000000000000000  0000000000000                                                                                                                                                                                         013                                                                                                      ";
    private static final String nullCantTrx = "     OS600000003054674125300000061058695401001A03015000000M20100330171909  0000000000000000       0000000000       0000000000       0000000000         00000000000000            0    0000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ";
    private static final String socioNoInformado = "02834IT6000079430551318296000000           001A13016602834A20120726100720000000000000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                        00000000000000000";
    private static final String msj3 = "FRM  AF6000000200000000020000000131313     002F93872500000M20100512121237  000000                                                                    MC04054920100512            0010100                       0449034779364023917801                               00                               00                                                            0000000000000000   0000000000                                                            0  000000020100512  M    000000023767                                                                                                                                                                         ";

    
    private static final String TRANSACCION_FRECUENCIA_CERO = "SAF  AA000069332008447205100000060728835201002A93836900000M20100330181316  0000000004751000       0004121001       0004811001       0060941001         00000000000000            001040020084472051                                                                                                                                             00000000000000000  0000000000000                                                                                                                                                                                         013                                                                                                      ";

    // private static final String msj4 =
    // "SAF  AA000069103356133060900000060671956201001A93872700000M20100512161047  0000000000000000       0000000000       0000000000       0000000000         00000000000000            001010033561330609                                                                                                                                             00000000000000000                                                                                                                                                                                                                                                                                                         ";
    // private static final String msj4 =
    // "SAF  AA0000058130619689255000000           004A93872800000M20100512181647  575281      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000                                                                                                                                                                                                                                                                                                         ";
    private static final String msj4 = "ODO  AO000083722712840676500504660398070702003A93872900000 20100512181648OD501083      0000             0000             0000             0000         00000000000000            0010100                                                                                                                                                        00000000000000000                                                                                                                                                                                                                                                                                                         ";
    // private static final String msj5 =
    // "SAF  AA600000000000000000000000060671956201000093874500000 201005121816540 0000004201011001             0000             0000             0000       0000000000000000000   306776901018507000000000            OSDE                                                                                                                             00000000000000000                                                                                                                                                                                                                                                                                                         ";

    private static final String msj5 = "43421IM600007112010828432461053161030791502902A34317300000A20101115173537000000004201011001       0000000000       0000000000       0000000000         00000000000000            000000200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                   ";

    private static final String msj6 = "43421IM600007112010828432461053161030791502902A34317300000A20101115173537000000004201011001       0000000000       0000000000       0000000000         00000000000000            0000002000000000000000000000000000000000000000000000        000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                   ";

    private static final String msg2F1 = "11111OS600000112016444577222222260615434401802F01820233333M20101227112356  0000000000000000       0000000000       0000000000       0000000000       MB65432100000000            000000000000000000000000000000136870000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000  0000000000000000   0000000000                                                               000000020101201             013687000000000000                                                                                                                                                             ";
    private static final String msj7 = "01170IT600011573052242816361053155060231001001A57464201170M20120413214204000000000000000000       0000000000       0000000000       0000000000         00000020120413            001010030522428163                                                                                                                                                                                                                                                                                                                                                                                                                                                                       2012-04-13-21.40.50.758000";

    private static final String fechaPracticaOflineInvalida = "02568IT600001273060511496905131961121106701001A58559102568M20120414115021000000000000000000       0000000000       0000000000       0000000000         000000120413            00001010000000000000000000004949                                                                                             ";

    private static final String msg2J = "FRM  AF600001603069213874700000061026243101002J94544400000M20130131100611  000000                                                                    MC00000120121115            0010101                       0000000             01                               00                               00                                                            0000000000000000C  0000000000                                                               000000020121114  M    000000044741                                                                                                                                                                        ";
    private static final String msg2JSinCoberturaEspecial = "FRM  AF600001603069213874700000061026243101002J94544400000M20130131100611  000000                                                                    MC00000120121115            0010101                       0000000             01                               00                               00                                                            0000000000000000   0000000000                                                               000000020121114  M    000000044741                                                                                                                                                                        ";

    private static final String BENEFICIARIO_MAL_FORMADO = "FRM  AF60000160306921387470000006102624311 002J94544400000M20130131100611  000000                                                                    MC00000120121115            0010101                       0000000             01                               00                               00                                                            0000000000000000C  0000000000                                                               000000020121114  M    000000044741                                                                                                                                                                        ";

    @Test
    public void testSerialize() {
        PosMessage msj = getMananger().load(PosMessage.class, msj6);
        Assert.assertTrue((msj instanceof Serializable));
    }

    @Test
    public void testVersionCredencial() {
        PosMessage msj = getMananger().load(PosMessage.class, msj6);
        Assert.assertEquals(2, msj.getVersionCredencial());
    }

    @Test
    public void testLocalDateFormatter() {
        String fechaString = "        ";
        LocalDate fecha = null;

        FixedFormatPatternData patternData = new FixedFormatPatternData("yyyyMMdd");
        FixedFormatBooleanData booleanData = new FixedFormatBooleanData("T", "F");
        FixedFormatNumberData numberData = new FixedFormatNumberData(Sign.NOSIGN, '+', '-');
        FixedFormatDecimalData decimalData = new FixedFormatDecimalData(2, false, '.');

        FormatInstructions instructions = new FormatInstructions(8, Align.LEFT, ' ', patternData, booleanData, numberData, decimalData);

        LocalDateFormatter formatter = new LocalDateFormatter();
        try {
            fecha = formatter.asObject(fechaString, instructions);
            Assert.fail("Deberia fallar por no poder parsear un string vacio");
        } catch (Throwable e) {
            Assert.assertNull(fecha);
        }

    }

    @Test
    public void testFechaPracticaOflineInvalida1A() {
        PosMessage msj = getMananger().load(PosMessage.class, fechaPracticaOflineInvalida);
        AbstractTransaccionPos transaccionPos = TransaccionFactory.getInstance().createTransaccion(msj, getMapper());

    }

    @Test
    public void testParseado() {
        PosMessage msj = (PosMessage) getMananger().load(FixedFormatMessageClassFactory.getClassInstance(msj2), msj2);

        Assert.assertEquals(2, msj.getCodigoTransaccion());
        Assert.assertEquals("A", msj.getAtributoCodigoTransaccion());

        Assert.assertEquals(2010, msj.getFechaTransaccion().getYear());
        Assert.assertEquals(3, msj.getFechaTransaccion().getMonthOfYear());
        Assert.assertEquals(30, msj.getFechaTransaccion().getDayOfMonth());

        Assert.assertEquals(18, msj.getHoraTransaccion().getHourOfDay());
        Assert.assertEquals(13, msj.getHoraTransaccion().getMinuteOfHour());
        Assert.assertEquals(16, msj.getHoraTransaccion().getSecondOfMinute());

        Assert.assertEquals("se esperaba prefijo del asociado 60", "60", msj.getPrefijoAsociado());
        Assert.assertEquals("7288352", msj.getNumeroAsociado());
        Assert.assertEquals("01", msj.getNumeroBeneficiario());
    }

    @Test
    public void testBeneficiarioMalFormado() {
        AbstractPosMessage msj = getMananger().load(FixedFormatMessageClassFactory.getClassInstance(BENEFICIARIO_MAL_FORMADO),
                BENEFICIARIO_MAL_FORMADO);

        Assert.assertEquals(2, msj.getCodigoTransaccion());
        Assert.assertEquals("J", msj.getAtributoCodigoTransaccion());
        SocioPosMessageBase posMessage = (SocioPosMessageBase) msj;
        Assert.assertEquals("se esperaba prefijo del asociado 01", "01", posMessage.getNumeroBeneficiario());

    }

    @Test
    public void testSocioNoInformado() {
        AbstractPosMessage msj = getMananger().load(FixedFormatMessageClassFactory.getClassInstance(socioNoInformado), socioNoInformado);

        Assert.assertEquals(1, msj.getCodigoTransaccion());
        Assert.assertEquals("A", msj.getAtributoCodigoTransaccion());
        SocioPosMessageBase posMessage = (SocioPosMessageBase) msj;
        Assert.assertEquals("se esperaba prefijo del asociado 00", "00", posMessage.getPrefijoAsociado());
        Assert.assertEquals("0000000", posMessage.getNumeroAsociado());
        Assert.assertEquals("00", posMessage.getNumeroBeneficiario());
    }

    @Test
    public void testSocioProvisorio() {
        PosMessageFarmacia msj = getMananger().load(PosMessageFarmacia.class, msj7);

        Assert.assertEquals("55", msj.getPrefijoAsociado());
        Assert.assertEquals("0602310", msj.getNumeroAsociado());
        Assert.assertEquals("01", msj.getNumeroBeneficiario());
        Assert.assertEquals(0l, msj.getCodigoBarrasMedicamento2());

    }

    @Test
    public void testNullCantTrx() {

        PosMessage msj = getMananger().load(PosMessage.class, nullCantTrx);
        AbstractTransaccionPos transaccionPos = TransaccionFactory.getInstance().createTransaccion(msj, getMapper());

        Assert.assertTrue(transaccionPos instanceof SocioPosMessage);
        SocioPosMessage socioPosMessae = (SocioPosMessage) transaccionPos;
        Assert.assertEquals(msj.getCodigoTransaccion(), socioPosMessae.getTransaccionConsumo().getTipoTransaccion().getTipo());
        Assert.assertEquals(msj.getAtributoCodigoTransaccion(), socioPosMessae.getTransaccionConsumo().getTipoTransaccion().getAtributo());

        Assert.assertEquals(msj.getPrefijoAsociado(), socioPosMessae.getBeneficiarioBid().getNroContrato().getPrefijo());
        Assert.assertEquals(String.valueOf(msj.getNumeroAsociado()), socioPosMessae.getBeneficiarioBid().getNroContrato().getNumero());

        Assert.assertEquals(String.valueOf(msj.getNumeroBeneficiario()), socioPosMessae.getBeneficiarioBid().getOrden());

        Assert.assertEquals("30546741253", msj.getCuitPrestador());
        Assert.assertEquals(1, msj.getNumeroTotalMensajes());
        Assert.assertEquals(1, msj.getNumeroSecuenciaMensaje());

    }

    @Test
    public void testMapeoSimple() {
        PosMessage msj = getMananger().load(PosMessage.class, msj1);
        AbstractTransaccionPos transaccionPos = TransaccionFactory.getInstance().createTransaccion(msj, getMapper());

        SocioPosMessage socioPosMessage = (SocioPosMessage) transaccionPos;
        Assert.assertEquals(msj.getCodigoTransaccion(), socioPosMessage.getTransaccionConsumo().getTipoTransaccion().getTipo());
        Assert.assertEquals(msj.getAtributoCodigoTransaccion(), socioPosMessage.getTransaccionConsumo().getTipoTransaccion().getAtributo());
        /*
         * assertEquals(2010, transaccionPos.getFechaTransaccion().getYear());
         * assertEquals(3,
         * transaccionPos.getFechaTransaccion().getMonthOfYear());
         * assertEquals(30,
         * transaccionPos.getFechaTransaccion().getDayOfMonth());
         * 
         * assertEquals(17, transaccionPos.getHoraTransaccion().getHourOfDay());
         * assertEquals(19,
         * transaccionPos.getHoraTransaccion().getMinuteOfHour());
         * assertEquals(9,
         * transaccionPos.getHoraTransaccion().getSecondOfMinute());
         */
        Assert.assertEquals(msj.getPrefijoAsociado(), socioPosMessage.getBeneficiarioBid().getNroContrato().getPrefijo());
        Assert.assertEquals(String.valueOf(msj.getNumeroAsociado()), socioPosMessage.getBeneficiarioBid().getNroContrato().getNumero());

        Assert.assertEquals(String.valueOf(msj.getNumeroBeneficiario()), socioPosMessage.getBeneficiarioBid().getOrden());

        Assert.assertEquals("30546741253", msj.getCuitPrestador());
        Assert.assertEquals(1, msj.getNumeroTotalMensajes());
        Assert.assertEquals(1, msj.getNumeroSecuenciaMensaje());
    }

    @Test
    public void testPrestaciones() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2);
        TransaccionPrestacionPos transaccionPos = getMapper().map(msj, TransaccionPrestacionPos.class);
        Assert.assertEquals(475, msj.getCodigoPrestacion1());
        Assert.assertEquals(1, msj.getTipoPrestacion1());
        Assert.assertEquals(0, msj.getArancelPrestacion1());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion1());

        Assert.assertEquals(412, msj.getCodigoPrestacion2());
        Assert.assertEquals(1, msj.getTipoPrestacion2());
        Assert.assertEquals(0, msj.getArancelPrestacion2());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(481, msj.getCodigoPrestacion3());
        Assert.assertEquals(1, msj.getTipoPrestacion3());
        Assert.assertEquals(0, msj.getArancelPrestacion3());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion3());

        Assert.assertEquals(6094, msj.getCodigoPrestacion4());
        Assert.assertEquals(1, msj.getTipoPrestacion4());
        Assert.assertEquals(0, msj.getArancelPrestacion4());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion4());

        Assert.assertEquals(msj.getCodigoPrestacion1(), transaccionPos.getCodigoPrestacion1());
        Assert.assertEquals(msj.getTipoPrestacion1(), transaccionPos.getTipoPrestacion1());
        Assert.assertEquals(msj.getArancelPrestacion1(), transaccionPos.getArancelPrestacion1());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion1(), transaccionPos.getFrecuenciaOCantidadPrestacion1());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());

        Assert.assertEquals("013", msj.getCodigoSeguridadExterno());
        Assert.assertEquals(4, transaccionPos.getPrestaciones().size());
        
        Assert.assertEquals(1, transaccionPos.getPrestaciones().get(0).getFrecuencia());

    }

    
    @Test
    public void testPrestacionesCantidadCero() {
        PosMessage msj = getMananger().load(PosMessage.class, TRANSACCION_FRECUENCIA_CERO);
        TransaccionPrestacionPos transaccionPos = getMapper().map(msj, TransaccionPrestacionPos.class);
        Assert.assertEquals(475, msj.getCodigoPrestacion1());
        Assert.assertEquals(1, msj.getTipoPrestacion1());
        Assert.assertEquals(0, msj.getArancelPrestacion1());
        Assert.assertEquals(0, msj.getFrecuenciaOCantidadPrestacion1());

        Assert.assertEquals(412, msj.getCodigoPrestacion2());
        Assert.assertEquals(1, msj.getTipoPrestacion2());
        Assert.assertEquals(0, msj.getArancelPrestacion2());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(481, msj.getCodigoPrestacion3());
        Assert.assertEquals(1, msj.getTipoPrestacion3());
        Assert.assertEquals(0, msj.getArancelPrestacion3());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion3());

        Assert.assertEquals(6094, msj.getCodigoPrestacion4());
        Assert.assertEquals(1, msj.getTipoPrestacion4());
        Assert.assertEquals(0, msj.getArancelPrestacion4());
        Assert.assertEquals(1, msj.getFrecuenciaOCantidadPrestacion4());

        Assert.assertEquals(msj.getCodigoPrestacion1(), transaccionPos.getCodigoPrestacion1());
        Assert.assertEquals(msj.getTipoPrestacion1(), transaccionPos.getTipoPrestacion1());
        Assert.assertEquals(msj.getArancelPrestacion1(), transaccionPos.getArancelPrestacion1());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion1(), transaccionPos.getFrecuenciaOCantidadPrestacion1());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals(msj.getCodigoPrestacion2(), transaccionPos.getCodigoPrestacion2());
        Assert.assertEquals(msj.getTipoPrestacion2(), transaccionPos.getTipoPrestacion2());
        Assert.assertEquals(msj.getArancelPrestacion2(), transaccionPos.getArancelPrestacion2());
        Assert.assertEquals(msj.getFrecuenciaOCantidadPrestacion2(), transaccionPos.getFrecuenciaOCantidadPrestacion2());

        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());

        Assert.assertEquals("013", msj.getCodigoSeguridadExterno());
        Assert.assertEquals(4, transaccionPos.getPrestaciones().size());
        
        Assert.assertEquals(0, transaccionPos.getPrestaciones().get(0).getFrecuencia());

    }
    
    @Test
    public void testMedicamentos() {
        PosMessageFarmacia msj = getMananger().load(PosMessageFarmacia.class, msg2F1);
        RegistracionMedicamentoMessage transaccionPos = (RegistracionMedicamentoMessage) TransaccionFactory.getInstance()
                .createTransaccion(msj, getMapper());
        Medicamento medicamento1 = ((Medicamento) transaccionPos.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().get(0));

        Assert.assertEquals(13687l, medicamento1.getCodMedicamento());

        Assert.assertNotNull(medicamento1.getPedidoMedico());
        Matricula matricula1 = medicamento1.getPedidoMedico().getMatriculaPrescriptor();
        Assert.assertNotNull(matricula1);
        Assert.assertEquals("M", matricula1.getEspecialidad());
    }

    @Test
    public void testTransaccion2J() {
        PosMessageFarmacia msj = getMananger().load(PosMessageFarmacia.class, msg2J);
        Assert.assertEquals("C", msj.getCoberturaEspecialOCondicionDelSocio());
        RegistracionMedicamentoMessage transaccionPos = (RegistracionMedicamentoMessage) TransaccionFactory.getInstance()
                .createTransaccion(msj, getMapper());
        Medicamento medicamento1 = ((Medicamento) transaccionPos.getTransaccionConsumo().getSolicitudServicio().getItemsConsumo().get(0));

        Assert.assertNotNull(medicamento1);
        SolicitudConsumo solicitudServicio = transaccionPos.getTransaccionConsumo().getSolicitudServicio();
        Assert.assertEquals("CX", solicitudServicio.getCoberturaEspecial());

    }

    @Test
    public void testTransaccion2JSinCoberturaEspecial() {
        PosMessageFarmacia msj = getMananger().load(PosMessageFarmacia.class, msg2JSinCoberturaEspecial);

        RegistracionMedicamentoMessage transaccionPos = (RegistracionMedicamentoMessage) TransaccionFactory.getInstance()
                .createTransaccion(msj, getMapper());
        Medicamento medicamento1 = ((Medicamento) transaccionPos.getTransaccionConsumo().getSolicitudConsumo().getItemsConsumo().get(0));

        SolicitudConsumo solicitudServicio = transaccionPos.getTransaccionConsumo().getSolicitudServicio();

    }

    @Test
    public void testParseadoCodigoDeBarra() {
        PosMessageFarmacia msj = getMananger().load(PosMessageFarmacia.class, msj3);
        long num = 7793640239178L;
        Assert.assertEquals(num, msj.getCodigoBarrasMedicamento1());
    }

    @Test
    public void testParseadoFechaDesdeRecuperar() {
        PosMessage msj = getMananger().load(PosMessage.class, msj4);

        Assert.assertEquals("ODO", msj.getCodigoInterno());
    }

}
