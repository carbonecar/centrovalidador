package ar.com.osde.centroValidador.pos;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPos;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.transaccion.ItemPrestador;

public class TestPosMessage2BCS extends AbstractPosMessageTest {

    public static final String msj2B =           "07659TR600036702012946504300000060711834104002B508818TP2DUM20110517164557100000003301651020       0000000000       0000000000       0000000000       6005220300000000  F989      0    0000000000000000000                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ";
    public static final String msj2C =           "SAF  AA370010002012193896100000061026243101002C94466100000M20121120152209  0000002501811001             0000             0000             0000       0 00000000000000            001010020121938961                                                                                                                                             00000000000000000  0000000000000";

    public static final String msj2CConEfector = "SAF  AA600010002014767165300000061026243101002S99579700000M20130411133506  0000000802033101I            0000             0000             0000       6005027720130411            001010020147671653                                                                                                                                             00000000000000000  0000000000000  ";

    @Test
    public void testParseo2CSinEfector() {
        // El efector es opcional y en este caso no viene

        PosMessage msj = getMananger().load(PosMessage.class, msj2C);
        AbstractTransaccionPos tr = TransaccionFactory.getInstance().createTransaccion(msj, getMapper());
        Assert.assertTrue(tr instanceof TransaccionPos);
        TransaccionPrestacionPos transaccion = (TransaccionPrestacionPos) tr;
        Assert.assertNotNull(transaccion.getPrestador());
        Assert.assertNotNull(transaccion.getPrestador().getTerminalID());

    }

    @Test
    public void testParseo2B() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2B);

        Assert.assertNotNull(msj);

        Assert.assertEquals("TR", msj.getCodigoOperador());
        Assert.assertEquals(60, msj.getFilialInstalacionPOS());
        Assert.assertEquals(0, msj.getDelegacionInstalacionPOS());
        Assert.assertEquals(3670, msj.getNumeroPOS());
        Assert.assertEquals("20129465043", msj.getCuitPrestador());
        Assert.assertEquals(0, msj.getNumeroISOEmisorCredencial());
        Assert.assertEquals("60", msj.getPrefijoAsociado());
        Assert.assertEquals("7118341", msj.getNumeroAsociado());
        Assert.assertEquals("04", msj.getNumeroBeneficiario());
        Assert.assertEquals(0, msj.getDigitoVerificadorAsociado());
        Assert.assertEquals(2, msj.getCodigoTransaccion());
        Assert.assertEquals("B", msj.getAtributoCodigoTransaccion());
        Assert.assertEquals(508818, msj.getNumeroTransaccion());
        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());
        // TODO: CREAR EL ASSERT PARA LocalDate
        Assert.assertEquals(330165, msj.getCodigoPrestacion1());
        Assert.assertEquals(1, msj.getTipoPrestacion1());
        Assert.assertEquals(0, msj.getArancelPrestacion1());
        Assert.assertEquals(20, msj.getFrecuenciaOCantidadPrestacion1());
        Assert.assertEquals("  F989", msj.getDiagnosticoPsicopatologia1());
        // En los casos de la 2B viene un valor num�rico que es la filial y en
        // numero de prestador.

        Assert.assertEquals("60", msj.getTipoMatriculaPrestador());
        Assert.assertEquals(52203, msj.getNumeroMatriculaPrestador());
    }

    @Test
    public void testCreacion2B() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2B);
        AbstractTransaccionPos tr = TransaccionFactory.getInstance().createTransaccion(msj, getMapper());
        Assert.assertTrue(tr instanceof TransaccionPos);
        TransaccionPrestacionPos transaccion = (TransaccionPrestacionPos) tr;
        Assert.assertNotNull(transaccion.getPrestador());
        Assert.assertNotNull(transaccion.getPrestador().getTerminalID());
        TerminalID terminal = transaccion.getPrestador().getTerminalID();

        Assert.assertEquals("TR", terminal.getCodigoOperador());
        Assert.assertEquals("60", terminal.getCodigoFilial());
        Assert.assertEquals(0, terminal.getAgrupador());
        Assert.assertEquals(3670, terminal.getNumero());

        Assert.assertEquals("60", transaccion.getBeneficiarioBid().getNroContrato().getPrefijo());
        Assert.assertEquals("7118341", transaccion.getBeneficiarioBid().getNroContrato().getNumero());
        Assert.assertEquals("04", msj.getNumeroBeneficiario());
        Assert.assertEquals(2, transaccion.getTransaccionConsumo().getTipoTransaccion().getTipo());
        Assert.assertEquals("B", transaccion.getTransaccionConsumo().getTipoTransaccion().getAtributo());
        Assert.assertEquals(508818, transaccion.getTransaccionConsumo().getNumeroTransaccion());
        // assertEquals("M", transaccion.getFormaIngresoDelAsociado());
        // TODO: CREAR EL ASSERT PARA LocalDate
        Assert.assertNotNull(transaccion.getTransaccionConsumo().getPrestadorTransaccionador().getEfector());
        Assert.assertNotNull(transaccion.getTransaccionConsumo().getSolicitudConsumo().getFechaSolicitudServicio());

        // TODO: las fechas de consumo son para cada uno de los items. Recorrer
        // cada uno de los items y ver que la fecha sea la misma para todos (en
        // el caso de los test para los STREAM)
        // Assert.assertNotNull(transaccion.getTransaccionConsumo().getSolicitudConsumo()
        Assert.assertEquals(1, transaccion.getPrestaciones().size());
        ItemPrestador prestacion = transaccion.getPrestaciones().get(0);
        Assert.assertEquals("330165", prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(1), prestacion.getAmbito());
        Assert.assertEquals(new Integer(0), prestacion.getArancel());
        Assert.assertEquals(20, prestacion.getFrecuencia());
        // assertEquals("  F989", transaccion.getDiagnosticoPsicopatologia1());
        Assert.assertEquals(transaccion.getPrestador().getCuit(), msj.getCuitPrestador());


        Assert.assertEquals(transaccion.getDiagnosticoPrincipal().getCodigo(), msj.getDiagnosticoPsicopatologia1());
        Assert.assertEquals(transaccion.getDiagnosticoSecundario().getCodigo(), msj.getDiagnosticoPsicopatologia2());
        Calendar cal=Calendar.getInstance();
        cal.set(2011, 4, 17, 16, 45,57);
        Date fechaSolicitudServicio=transaccion.getTransaccionConsumo().getSolicitudServicio().getFechaSolicitudServicio();
        
        Calendar calFromSolicitudServicio=Calendar.getInstance();
        calFromSolicitudServicio.setTime(fechaSolicitudServicio);
        Assert.assertEquals(cal.get(Calendar.YEAR) ,calFromSolicitudServicio.get(Calendar.YEAR));
        Assert.assertEquals(cal.get(Calendar.MONTH) ,calFromSolicitudServicio.get(Calendar.MONTH));
        Assert.assertEquals(cal.get(Calendar.DAY_OF_YEAR) ,calFromSolicitudServicio.get(Calendar.DAY_OF_YEAR));
        Assert.assertEquals(cal.get(Calendar.HOUR_OF_DAY) ,calFromSolicitudServicio.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(cal.get(Calendar.MINUTE) ,calFromSolicitudServicio.get(Calendar.MINUTE));
        Assert.assertEquals(cal.get(Calendar.SECOND) ,calFromSolicitudServicio.get(Calendar.SECOND));

    }
}
