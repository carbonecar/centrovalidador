package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.TipoCuatroPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AnulacionTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestAnulacionTransaccionPos extends AbstractTestTransactionFactory {

    
    private static final String anulacionWEBPosMessage = "     WG600001173069213874700000000000000000004A00385100000 20130509122515WG0290050000000000       0000000000       0000000000       0000000000         00000020130101            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            000000000000000000      0000000000000000000000000                                                                                                                                                            ";
    private static final String anulacionPosMessage=     "FRM  AF6000005127174986008000000           004A05288700000 20120928163556  049952                                                                      00000020120928            0010100                                           00                               00                               00                                                            0000000000000000   0000000000                                                               000000000000000       000000                                                                                                                                                                              ";
    @Test
    public void testSimpleTransaccionBloqueoDesbloqueoWEB() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(anulacionWEBPosMessage),
                anulacionWEBPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulacionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(anulacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getFechaSolicitudServicio());
        Assert.assertEquals(3851, anulacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertEquals("WG", anulacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTransaccionReferencia().getCodigoOperador());

    }

    
    
    @Test
    public void testSimpleTransaccionBloqueoDesbloqueo() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(anulacionPosMessage),
                anulacionPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulacionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulacionTransaccionPos.getTransaccion());

        Assert.assertNotNull(anulacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getFechaSolicitudServicio());
        Assert.assertEquals(52887, anulacionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertEquals("AF", anulacionTransaccionPos.getTransaccion().getSolicitudServicio()
                .getTransaccionReferencia().getCodigoOperador());

    }

}
