package ar.com.osde.centroValidador.mqjms;

import java.io.UnsupportedEncodingException;

import org.junit.Test;

import ar.com.osde.centroValidador.pos.PosMessageResponse;
import ar.com.osde.centroValidador.pos.mq.MessageIDBuilder;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Verificamos que el MessageID se envie de forma correcta
 * 
 * @author VA27789605
 * 
 */

public class TestMessageID extends TestCase {

    @Test
    public void testSendMessageID() {
//        byte[] messageId = new byte[] { 0, 0, 0, 0, 0, 0,0,0};
//        byte[] nroTransaccionBytes = String.valueOf(94440).getBytes();
//
//        int posInicio = (6 - nroTransaccionBytes.length < 0) ? 0 : 6 - nroTransaccionBytes.length;
//
//        System.arraycopy(nroTransaccionBytes, 0, messageId, posInicio, nroTransaccionBytes.length-1);
//        String operadorNumeric = "05";
//
//        if (operadorNumeric != null) {
//            byte[] operadorArray = operadorNumeric.getBytes();
//            System.arraycopy(operadorArray, 0, messageId,6, 2);
//        }
//
//        String newMessage = new String(messageId, Charset.forName("ISO-8859-1"));
//        StringBuffer strBuff=new StringBuffer(24);
//        
//       
//        for(int i=strBuff.length();i<24;i++){
//            strBuff.append("0");
//        }
//        
//        
//        System.out.println(strBuff.toString());
        
        
        Assert.assertEquals("944400050000000000000000", MessageIDBuilder.buildMessageID(944400, "05"));
        Assert.assertEquals("094440050000000000000000", MessageIDBuilder.buildMessageID(94440, "05"));
        

    }
}
