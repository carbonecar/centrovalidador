package ar.com.osde.centroValidador.pos.handlers;

import java.util.LinkedList;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;
import ar.com.osde.centroValidador.pos.filter.NotFilter;
import ar.com.osde.centroValidador.pos.filter.NullFilter;
import ar.com.osde.centroValidador.pos.filter.PosMessageFilter;
import ar.com.osde.centroValidador.pos.filter.estrategy.FilterStrategy;
import ar.com.osde.centroValidador.pos.filter.estrategy.NullFilterStrategy;
import ar.com.osde.centroValidador.services.TransaccionServiceStatistics;
import ar.com.osde.cv.pos.utils.TextMessageAdapter;

public class TestFilteredMessageHandler {

    private FilteredMessageHandler messageHandler;

    @Before
    public void initialize() {
        messageHandler = new FilteredMessageHandler();
        messageHandler.setFilterStrategyList(new LinkedList<FilterStrategy>());
        messageHandler.setFilters(new LinkedList<PosMessageFilter>());
        
        NullMessageHandler nullMesasgeHandler=new NullMessageHandler();
        nullMesasgeHandler.setStatistics(new TransaccionServiceStatistics());
        messageHandler.setIntercepted(nullMesasgeHandler);
        messageHandler.setFormatManager(new FixedPosFormatManager());
    }

    @Test
    public void testSimpleMessage() {

        String messageText = "02264IT600001733370845606905131960439777106001A48434702264M20110727172440000000000000000000       0000000000       0000000000       0000000000         00000020110727            001010000000000000000000004949                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         ";
        TextMessage textMessage = createTextMessage(messageText);

        RespuestaCentroValidador respuesta = messageHandler.process(textMessage);

        Assert.assertNotNull("Se esperaba una respuesta no nula", respuesta);
    }

    @Test(expected = FilteredMessageException.class)
    public void testMensajeFiltrado() {

        String messageText = "02264IT600001733370845606905131960439777106001A48434702264M20110727172440000000000000000000       0000000000       0000000000       0000000000         00000020110727            001010000000000000000000004949                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         ";
        TextMessage textMessage = createTextMessage(messageText);

        messageHandler.getFilters().add(new NotFilter(new NullFilter()));
        messageHandler.getFilterStrategyList().add(new NullFilterStrategy());

        messageHandler.process(textMessage);
    }

    //comentamos este test porque ahora es un string, con lo cual el formato numerico no se valida
    //pero hay que ver si es del todo correcto o bien se puede validar que sean un numerico alineado a derecha de 2 caracteres
//    @Test(expected = BadFormatException.class)
//    public void testWrongCase() {
//        
//        String messageText = "IVR  AI60000010201072883260000006063939707* 03A42040500000M20110728113516980656030000000000       0000000000       0000000000       0000000000         00000000000000            0010100                                                                                                                                                                           ";
//
//        TextMessage textMessage = createTextMessage(messageText);
//
//        RespuestaCentroValidador respuesta = messageHandler.process(textMessage);
//
//        Assert.assertNull(respuesta);
//    }
    
    public TextMessage createTextMessage(String messageText) {

        TextMessage textMessage = new TextMessageAdapter();

        try {
            textMessage.setText(messageText);
        } catch (JMSException e) {
            Assert.fail();
        }

        return textMessage;
    }
}
