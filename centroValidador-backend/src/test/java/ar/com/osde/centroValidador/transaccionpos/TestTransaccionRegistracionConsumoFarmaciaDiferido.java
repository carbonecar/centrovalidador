package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.RegistracionMedicamentoMessage;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.aptitud.ContextoEvaluacion;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionRegistracionConsumoFarmaciaDiferido extends AbstractTestTransactionFactory {

    private String transaccion2JSinMarcaCronico="FRM  AF600001603069213874700000061026243101002J94544400000M20130131100611  000000                                                                    MC00000120121115            0010101                       0000000             01                               00                               00                                                            0000000000000000   0000000000                                                               000000020121114  M    000000044741                                                                                                                                                                        ";
    private String transaccion2JConMarcaCronico="FRM  AF600002833069655353600000061117985603002J42936900000M20130617170626  000000                                                                    MB08132720130611            0010100                       2052041             01           2704393             01                               00                                                            0000000000000000M  0000000000                                                               000000020130521       000000000000000000                                                                                                                                                                  ";
    @Test
    public void testSimpleTransaccion2J() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccion2JSinMarcaCronico),
                transaccion2JSinMarcaCronico);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage transaccionMedicamentoDiferido = (RegistracionMedicamentoMessage) message;
        
        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo());
        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio());


        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio());
        Assert.assertEquals(ContextoEvaluacion.OFFLINE,transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio().getContextoEvaluacion());
        Assert.assertEquals("",transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio().getCoberturaEspecial());


        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }

    
    @Test
    public void testMarcaConicoTransaccion2J() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccion2JConMarcaCronico),
                transaccion2JConMarcaCronico);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof RegistracionMedicamentoMessage);
        RegistracionMedicamentoMessage transaccionMedicamentoDiferido = (RegistracionMedicamentoMessage) message;
        
        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo());
        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio());


        Assert.assertNotNull(transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio());
        Assert.assertEquals(ContextoEvaluacion.OFFLINE,transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio().getContextoEvaluacion());
        Assert.assertEquals("MI",transaccionMedicamentoDiferido.getTransaccionConsumo().getSolicitudServicio().getCoberturaEspecial());


        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }
}
