package ar.com.osde.centroValidador.spring;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml"})
public class TestSpring extends AbstractJUnit4SpringContextTests {

    @Test
    public void testDozerSpring() {
       Assert.assertNotNull(applicationContext.getBean("beans.format.manager"));
       Assert.assertNotNull(applicationContext.getBean("beans.format.manager.xml"));
       Assert.assertNotNull(applicationContext.getBean("dozer.mapper"));
    }
}