package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.MarcaGenericoPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.MarcaGenericoTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionMarcaGenerico;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionMarcaGenerico extends AbstractTestTransactionFactory {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String marcaGenericoPosMessage =          "     WG600000142010833619700000000000000000004G00359300000 20130221151331AI0035900000000000       0000000000       0000000000       0000000000         00000020130219            001010000000000000000000000000994481677980690507550000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100000000000000000      0000000044816000000000000                                                                                                                                                            ";

    private static final String marcaGenericoPosMessageFechaCero = "     AI600000142010833619700000000000000000004G00359300000 20130221151331AI0035900000000000       0000000000       0000000000       0000000000         00000000000000            001010000000000000000000000000994481677980690507550000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100000000000000000      0000000044816000000000000                                                                                                                                                            ";
    private static final String marcaGenericoPosMessageSinFecha =  "     AI600000142010833619700000000000000000004G00359300000 20130221151331AI0035900000000000       0000000000       0000000000       0000000000         000000                    001010000000000000000000000000994481677980690507550000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            100000000000000000      0000000044816000000000000                                                                                                                                                            ";

    @Test
    public void testSimpleMarcaGenericoTransaccionPos() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(marcaGenericoPosMessage),
                marcaGenericoPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof MarcaGenericoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof MarcaGenericoTransaccionPos);
        MarcaGenericoTransaccionPos marcaGenerico = (MarcaGenericoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(marcaGenerico.getTransaccion());
        Transaccion transaccion = marcaGenerico.getTransaccion();
        Assert.assertTrue(transaccion instanceof TransaccionMarcaGenerico);
        Assert.assertEquals(3593, marcaGenerico.getTransaccion().getNumeroTransaccion());
        Assert.assertNotNull(marcaGenerico.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());

        Assert.assertEquals("AI", marcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getTransaccionReferencia()
                .getCodigoOperador());
        
       Assert.assertNotNull(marcaGenerico.getTransaccionReferencia().getFechaTransaccionOriginal());

    }

    @Test
    public void testFechaCeroMarcaGenericoTransaccionPos() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(marcaGenericoPosMessageFechaCero),
                marcaGenericoPosMessageFechaCero);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof MarcaGenericoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof MarcaGenericoTransaccionPos);
        MarcaGenericoTransaccionPos marcaGenerico = (MarcaGenericoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(marcaGenerico.getTransaccion());
        Transaccion transaccion = marcaGenerico.getTransaccion();
        Assert.assertTrue(transaccion instanceof TransaccionMarcaGenerico);
        Assert.assertEquals(3593, marcaGenerico.getTransaccion().getNumeroTransaccion());
        Assert.assertNotNull(marcaGenerico.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());

        Assert.assertEquals("AI", marcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getTransaccionReferencia()
                .getCodigoOperador());
        
       Assert.assertNull(marcaGenerico.getTransaccionReferencia().getFechaTransaccionOriginal());

    }
    
    @Test
    public void testSinFechaMarcaGenericoTransaccionPos() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(marcaGenericoPosMessageSinFecha),
                marcaGenericoPosMessageSinFecha);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof MarcaGenericoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof MarcaGenericoTransaccionPos);
        MarcaGenericoTransaccionPos marcaGenerico = (MarcaGenericoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(marcaGenerico.getTransaccion());
        Transaccion transaccion = marcaGenerico.getTransaccion();
        Assert.assertTrue(transaccion instanceof TransaccionMarcaGenerico);
        Assert.assertEquals(3593, marcaGenerico.getTransaccion().getNumeroTransaccion());
        Assert.assertNotNull(marcaGenerico.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio());

        Assert.assertEquals("AI", marcaGenerico.getTransaccionMarcaGenerico().getSolicitudServicio().getTransaccionReferencia()
                .getCodigoOperador());
        
       Assert.assertNull(marcaGenerico.getTransaccionReferencia().getFechaTransaccionOriginal());

    }
}
