package ar.com.osde.centroValidador.pos;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestResponseTimeOutPosMessage extends AbstractTestTransactionFactory {

    private String timeOut2A="IT600001603070809250561053160541757102002A75463900000M20121105142940980000004201011001       0000000000       0000000000       0000000000         00000020121105            00000009200        0901010030708092505";
    
    
    @Test
    public void testResponseTimeOut(){
        TimeOutResponsePosMessage responseTimeOut=getMannager().load(TimeOutResponsePosMessage.class, timeOut2A);
        
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem1());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem2());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem3());
        Assert.assertEquals("",responseTimeOut.getCodigoRespuestaItem4());
        Assert.assertEquals("92",responseTimeOut.getGeneradorRespuesta());
        Assert.assertEquals(0,responseTimeOut.getNroAutorizacion());
        Assert.assertEquals("00",responseTimeOut.getRespuestaTransaccion());
    }
    
   
}
