package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.Orden;
import ar.com.osde.cv.model.SocioPosMessage;
import ar.com.osde.cv.model.TipoPrestacion;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.entities.aptitud.TerminalID;
import ar.com.osde.entities.aptoServicio.PrestadorTransaccionador;
import ar.com.osde.entities.transaccion.ItemPrestador;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestFixedTransaccionFactory extends AbstractTestTransactionFactory {

    private static final String message2ASecond = "01614IT600006443054590296261053160471392301302A43006601614M20110321140444000000004201012001       0000000000       0000000000       0000000000         00000020110321            001020000000000000000000000000                                                                                             ";
    private static final String message2AThird = "01614IT600006443054590296261053160471392301302A43006601614M20110321140444000000004201012203       4201011101       4201014101       4201013403         00000020110321            001030000000000000000000000000                                                                                             ";

    private static final String message2AFourth = "01614IT600006443054590296261053160471392301302A43006601614M20110321140444121234564201012203       4201011101       4201014101       4201013403         00000020110321            001040000000000000000000000000                                                                                             ";
    private static final String message2AFirst = "01614IT600006443054590296261053160471392301302A43006601614M20110321140444000000004201011001       0000000000       0000000000       0000000000         00000020110321            001010000000000000000000000000                                                                                             ";

    private static final String message2ABeneficiarioProvisorio = "01614IT600006443054590296261053199471392301302A43006601614M20110321140444000000004201011001       0000000000       0000000000       0000000000         00000020110321            001010000000000000000000000000                                                                                             ";

    private static final String message1A = "POS  AP140200642024269720161053161517285601001A36368500000A20140523204900  000000      0000             0000             0000             0000         00000000000000            0010102                                                                                                                                                        00000000000000000                                                                                              050295                                                                                                    013                                                                                                      2014-05-23-20.45.56.746000    ";

    @Test
    public void test1AMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        PosMessage posMessage = manager.load(PosMessage.class, message1A);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof SocioPosMessage);
        SocioPosMessage socioPosMessage = (SocioPosMessage) message;
        Assert.assertNotNull("Se espearaba una transaccion", socioPosMessage.getTransaccionConsumo());
        Assert.assertNotNull("Se esperaba un transaccionador ", socioPosMessage.getTransaccionConsumo().getPrestadorTransaccionador());
        Assert.assertEquals(363685, socioPosMessage.getTransaccionConsumo().getNumeroTransaccion());
        PrestadorTransaccionador prestador = socioPosMessage.getTransaccionConsumo().getPrestadorTransaccionador();
        Assert.assertEquals(2, prestador.getTerminalID().getAgrupador());
        Assert.assertEquals("AP", prestador.getTerminalID().getCodigoOperador());
        Assert.assertEquals(64, prestador.getTerminalID().getNumero());
        Assert.assertEquals("14", prestador.getTerminalID().getCodigoFilial());

        Assert.assertNotNull(socioPosMessage.getBeneficiarioBid());
        Assert.assertEquals("01", socioPosMessage.getBeneficiarioBid().getOrden());
        Assert.assertEquals("5172856", socioPosMessage.getBeneficiarioBid().getNroContrato().getNumero());
        Assert.assertEquals("61", socioPosMessage.getBeneficiarioBid().getNroContrato().getPrefijo());

        Assert.assertNotNull(socioPosMessage.getTransaccionConsumo().getSolicitudConsumo().getCredencial());
        Assert.assertEquals(2, socioPosMessage.getTransaccionConsumo().getSolicitudConsumo().getCredencial().getVersion());
        Assert.assertEquals("013", socioPosMessage.getTransaccionConsumo().getSolicitudConsumo().getCredencial()
                .getCodigoSeguridadExterno());
        // TODO Cuidado que esta definido que el mensaje del operador puede
        // traer M,A o bien un blanco. Blanco ==> A, Pero
        // poner eso puede implicar cambiar el mensaje que esta viniendo
        // originalmente.

    }

    @Ignore
    @Test
    public void test2AMessageSecond() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = (FixedFormatManager) applicationContext.getBean("beans.format.manager");

        PosMessage posMessage = manager.load(PosMessage.class, message2ASecond);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof TransaccionPrestacionPos);

        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;

        Assert.assertEquals(1, transaccionPrestacionPos.getPrestaciones().size());
        ItemPrestador prestacion = transaccionPrestacionPos.getPrestaciones().get(0);

        Assert.assertEquals(new Integer(TipoPrestacion.INTERNACION_CLINICA.getValue()), prestacion.getAmbito());
        Assert.assertEquals(420101, prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(0), prestacion.getArancel());
    }

    @Ignore
    @Test
    public void test2AMultiplePrestacion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = (FixedFormatManager) applicationContext.getBean("beans.format.manager");

        PosMessage posMessage = manager.load(PosMessage.class, message2AThird);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof TransaccionPrestacionPos);

        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;

        Assert.assertEquals(4, transaccionPrestacionPos.getPrestaciones().size());
        ItemPrestador prestacion = transaccionPrestacionPos.getPrestaciones().get(0);

        Assert.assertEquals(new Integer(TipoPrestacion.INTERNACION_CLINICA.getValue()), prestacion.getAmbito());
        Assert.assertEquals(420101, prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(2), prestacion.getArancel());
        Assert.assertEquals(3, prestacion.getFrecuencia());

        prestacion = transaccionPrestacionPos.getPrestaciones().get(1);
        Assert.assertEquals(new Integer(TipoPrestacion.AMBULATORIO.getValue()), prestacion.getAmbito());
        Assert.assertEquals(420101, prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(1), prestacion.getArancel());
        Assert.assertEquals(1, prestacion.getFrecuencia());

        prestacion = transaccionPrestacionPos.getPrestaciones().get(2);
        Assert.assertEquals(new Integer(TipoPrestacion.DOMICILIO.getValue()), prestacion.getAmbito());
        Assert.assertEquals(420101, prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(1), prestacion.getArancel());
        Assert.assertEquals(1, prestacion.getFrecuencia());

        prestacion = transaccionPrestacionPos.getPrestaciones().get(3);
        Assert.assertEquals(new Integer(TipoPrestacion.INTERNACION_QUIRURGICA.getValue()), prestacion.getAmbito());
        Assert.assertEquals(420101, prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(4), prestacion.getArancel());
        Assert.assertEquals(3, prestacion.getFrecuencia());

    }

    @Test
    public void test2AOrdenMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = (FixedFormatManager) applicationContext.getBean("beans.format.manager");

        PosMessage posMessage = manager.load(PosMessage.class, message2AFourth);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof TransaccionPrestacionPos);

        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;

        Orden orden = transaccionPrestacionPos.getOrden();
        Assert.assertNotNull(orden);
        Assert.assertEquals("12", orden.getDelegacion());
        Assert.assertEquals(123456, orden.getNumeroAutorizacion());

    }

    @Test
    public void test2AMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = (FixedFormatManager) applicationContext.getBean("beans.format.manager");

        PosMessage posMessage = manager.load(PosMessage.class, message2AFirst);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof TransaccionPrestacionPos);

        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;

        TerminalID terminal = transaccionPrestacionPos.getPrestador().getTerminalID();
        Assert.assertEquals("IT", terminal.getCodigoOperador());
        Assert.assertEquals("60", terminal.getCodigoFilial());
        Assert.assertEquals(0, terminal.getAgrupador());
        Assert.assertEquals(644, terminal.getNumero());
        Assert.assertEquals("30545902962", transaccionPrestacionPos.getPrestador().getCuit());

        Assert.assertEquals("60", transaccionPrestacionPos.getBeneficiarioBid().getNroContrato().getPrefijo());
        Assert.assertEquals("4713923", transaccionPrestacionPos.getBeneficiarioBid().getNroContrato().getNumero());
        Assert.assertEquals("01", transaccionPrestacionPos.getBeneficiarioBid().getOrden());

        Assert.assertEquals(2, transaccionPrestacionPos.getTransaccionConsumo().getTipoTransaccion().getTipo());

        Assert.assertEquals("A", transaccionPrestacionPos.getTransaccionConsumo().getTipoTransaccion().getAtributo());

        Assert.assertEquals(430066, transaccionPrestacionPos.getTransaccionConsumo().getNumeroTransaccion());

        Assert.assertEquals(0, transaccionPrestacionPos.getPrestador().getTerminalID().getAgrupador());
        Assert.assertEquals(0, transaccionPrestacionPos.getNroOrden());

        Assert.assertEquals(1, transaccionPrestacionPos.getPrestaciones().size());
        ItemPrestador prestacion = transaccionPrestacionPos.getPrestaciones().get(0);

        Assert.assertEquals(new Integer(TipoPrestacion.AMBULATORIO.getValue()), prestacion.getAmbito());
        Assert.assertEquals("420101", prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(0), prestacion.getArancel());

    }

    @Test
    public void test2AMessageProvisorio() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = (FixedFormatManager) applicationContext.getBean("beans.format.manager");

        PosMessage posMessage = manager.load(PosMessage.class, message2ABeneficiarioProvisorio);

        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);

        Assert.assertTrue(message instanceof TransaccionPrestacionPos);

        TransaccionPrestacionPos transaccionPrestacionPos = (TransaccionPrestacionPos) message;

        TerminalID terminal = transaccionPrestacionPos.getPrestador().getTerminalID();
        Assert.assertEquals("IT", terminal.getCodigoOperador());
        Assert.assertEquals("60", terminal.getCodigoFilial());
        Assert.assertEquals(0, terminal.getAgrupador());
        Assert.assertEquals(644, terminal.getNumero());
        Assert.assertEquals("30545902962", transaccionPrestacionPos.getPrestador().getCuit());

        Assert.assertEquals("99", transaccionPrestacionPos.getBeneficiarioBid().getNroContrato().getPrefijo());
        Assert.assertEquals("4713923", transaccionPrestacionPos.getBeneficiarioBid().getNroContrato().getNumero());
        Assert.assertEquals("01", transaccionPrestacionPos.getBeneficiarioBid().getOrden());

        Assert.assertEquals(2, transaccionPrestacionPos.getTransaccionConsumo().getTipoTransaccion().getTipo());

        Assert.assertEquals("A", transaccionPrestacionPos.getTransaccionConsumo().getTipoTransaccion().getAtributo());

        Assert.assertEquals(430066, transaccionPrestacionPos.getTransaccionConsumo().getNumeroTransaccion());

        Assert.assertEquals(0, transaccionPrestacionPos.getPrestador().getTerminalID().getAgrupador());
        Assert.assertEquals(0, transaccionPrestacionPos.getNroOrden());

        Assert.assertEquals(1, transaccionPrestacionPos.getPrestaciones().size());
        ItemPrestador prestacion = transaccionPrestacionPos.getPrestaciones().get(0);

        Assert.assertEquals(new Integer(TipoPrestacion.AMBULATORIO.getValue()), prestacion.getAmbito());
        Assert.assertEquals("420101", prestacion.getCodigoPrestacionPrestador());
        Assert.assertEquals(new Integer(0), prestacion.getArancel());

        Assert.assertNull(transaccionPrestacionPos.getTransaccion().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(transaccionPrestacionPos.getTransaccion().getSolicitudServicio().getBeneficiarioProvisorio());
        
        
        
    }

}
