package ar.com.osde.centroValidador.transaccionpos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.BloqueoDesbloqueoPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.BloqueoDesbloqueoTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestBloqueoDesbloqueoTransaccionPos extends AbstractTestTransactionFactory {

    private static final String bloqueoDesbloqueoPosMessageweb = "     WG600001173069213874700000000000000000004B00385200000 20130509122823AF0290050000000000       0000000000       0000000000       0000000000         00000020130101            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            000000000000000000      0000000000000000000000000                                                                                                                                                            ";
    private static final String desbloqueoDesbloqueoPosMessage=  "FRM  AF6000081730574409949000000           004B79409300000 20121029141446  739065                                                                      00000020121027            0010100                                           00                               00                               00                                                            0000000000000000   0000000000                                                               000000000000000       000000                                                                                                                                                                              ";
    @Test
    public void testSimpleTransaccionBloqueoDesbloqueoWEB() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(bloqueoDesbloqueoPosMessageweb),
                bloqueoDesbloqueoPosMessageweb);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof BloqueoDesbloqueoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof BloqueoDesbloqueoTransaccionPos);
        BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoTransaccionPos = (BloqueoDesbloqueoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo());

        Assert.assertNotNull(bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
                .getFechaSolicitudServicio());
        Assert.assertEquals(3852, bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());
        Assert.assertEquals("AF", bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
                .getTransaccionReferencia().getCodigoOperador());

    }

    
    
    @Test
    public void testSimpleTransaccionDesbloqueo() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(desbloqueoDesbloqueoPosMessage),
                desbloqueoDesbloqueoPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof BloqueoDesbloqueoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof BloqueoDesbloqueoTransaccionPos);
        BloqueoDesbloqueoTransaccionPos bloqueoDesbloqueoTransaccionPos = (BloqueoDesbloqueoTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo());

        Assert.assertNotNull(bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
                .getFechaSolicitudServicio());
        Assert.assertEquals(794093, bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());
        Assert.assertEquals("AF", bloqueoDesbloqueoTransaccionPos.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
                .getTransaccionReferencia().getCodigoOperador());

    }
}
