package ar.com.osde.centroValidador.motor;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ar.com.osde.entities.aptoServicio.Beneficiario;
import ar.com.osde.entities.aptoServicio.BeneficiarioBid;
import ar.com.osde.entities.aptoServicio.NroContrato;
import ar.com.osde.entities.aptoServicio.RotuloIva;

//@ContextConfiguration(locations = { "/spring/backend/osde-framework-rules-spring.xml",
//        "classpath:/spring/osde-framework-transformers-aptoSocio-spring.xml", "/spring/backend/osde-framework-motor-spring.xml" })
public class TestMotorEmbebido{// extends AbstractJUnit4SpringContextTests {

    @Test
    public void testSpring() {
        Assert.assertTrue(true);
//        MotorEmbebido motorEmbebido = (MotorEmbebido) this.applicationContext.getBean("MotorEmbebido");
//
//        Assert.assertNotNull(motorEmbebido);
        // Assert.assertNotNull(motorEmbebido.getRuleEngine());
        // Assert.assertNotNull(motorEmbebido.getRulesTransformer());
        //
        // String changeSet = motorEmbebido.getChangeSet();
        // motorEmbebido.setChangeSet(changeSet);
        // Assert.assertNotNull(changeSet, motorEmbebido.getChangeSet());
    }

    // @Test
    // public void testSimpleProcess() {
    //
    // MotorEmbebido motorEmbebido = (MotorEmbebido)
    // this.applicationContext.getBean("MotorEmbebido");
    //
    // Beneficiario beneficiario = crearBeneficiario();
    // RespuestaAptitud respuesta = motorEmbebido.process(beneficiario);
    //
    // Assert.assertNotNull(respuesta);
    // Assert.assertFalse(respuesta.isApto());
    // Assert.assertEquals(1, respuesta.getInconsistencias().size());
    // Assert.assertNotNull(respuesta.getInconsistencias().iterator().next().getDescripcion());
    // }

    private Beneficiario crearBeneficiario() {

        Beneficiario beneficiario = new Beneficiario();
        beneficiario.setApellido("apellido");

        BeneficiarioBid beneficiarioBid = new BeneficiarioBid();

        NroContrato contrato = new NroContrato("1234560");
        beneficiarioBid.setNroContrato(contrato);
        beneficiarioBid.setOrden("40");

        beneficiario.setBeneficiarioBid(beneficiarioBid);
        // beneficiario.setCoberturaEspecial(5);
        RotuloIva rotuloIva = new RotuloIva();
        rotuloIva.setCodigo(4);
        beneficiario.setRotuloIva(rotuloIva);
        // beneficiario.setCredencial(new Credencial());

        return beneficiario;
    }
}
