package ar.com.osde.centroValidador.transaccionpos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.ConsultaFarmaciaPosMessage;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.ConsultaTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestConsultaTransaccionPos extends AbstractTestTransactionFactory {

    private static final String consultaPendienteCierreFarmacia = "FRM  AF6000016030692138747000000           003F94545200000M20130201161040  000000                                                                      00000020130201            0010100                                           00                               00                               00                                                            2012120120130201   0000000000                                                               000000020130201  MTVP 000000                                                                                                                 ";

    private static final String consultaAceptadasCierreFarmacia = "FRM  AF6000016030692138747000000           003F94545200000M20130201161040  000000                                                                      00000020130201            0010100                                           00                               00                               00                                                            2012120120130201   0000000000                                                               000000020130201  MTVA 000000                                                                                                                 ";

    @Test
    public void testSimpleConsulta() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(consultaPendienteCierreFarmacia),
                consultaPendienteCierreFarmacia);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ConsultaFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof ConsultaTransaccionPos);
        ConsultaTransaccionPos consultaTransaccionPos = (ConsultaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta());

        Date fechaTransaccionHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        Assert.assertNotNull(fechaTransaccionHasta);
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

    }

    @Test
    public void testConsultaCierreFechaMayorAyer() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyyMMdd");
        String cierreFechaMayorAyer = consultaPendienteCierreFarmacia.replace("20130201", dateFormater.format(new Date()));

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(cierreFechaMayorAyer),
                cierreFechaMayorAyer);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ConsultaFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof ConsultaTransaccionPos);
        ConsultaTransaccionPos consultaTransaccionPos = (ConsultaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

        Date fechaTransaccionHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        Assert.assertNotNull(fechaTransaccionHasta);

        Date fechaHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();

        Date hoyHorasTruncadas = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        Date ayer = DateUtils.add(hoyHorasTruncadas, Calendar.MILLISECOND, -1);
        Assert.assertEquals(ayer, consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());
        Assert.assertNotNull(fechaHasta);

    }

    @Test
    public void testConsultaDistintaPendienteCierreFechaIgualHoy() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyyMMdd");
        String cierreFechaMayorAyer = consultaAceptadasCierreFarmacia.replace("20130201", dateFormater.format(new Date()));

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(cierreFechaMayorAyer),
                cierreFechaMayorAyer);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ConsultaFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof ConsultaTransaccionPos);
        ConsultaTransaccionPos consultaTransaccionPos = (ConsultaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

        Date fechaTransaccionHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        Assert.assertNotNull(fechaTransaccionHasta);

        Date fechaHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();

        Assert.assertEquals(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaSolicitudServicio(),
                consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());
        Assert.assertNotNull(fechaHasta);

    }

    /**
     * Este test valida que las consultas que son distintas de cierre y
     * pendientes de cierra cuando la fecha sea mayor a hoy se le coloque la
     * fecha de solicitud de servicio
     */
    @Test
    public void testConsultaDistintaPendienteCierreFechaMayorHoy() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyyMMdd");
        String cierreFechaMayorAyer = consultaAceptadasCierreFarmacia.replace("20130201",
                dateFormater.format(DateUtils.add(new Date(), Calendar.DAY_OF_MONTH, 1)));

        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(cierreFechaMayorAyer),
                cierreFechaMayorAyer);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof ConsultaFarmaciaPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof ConsultaTransaccionPos);
        ConsultaTransaccionPos consultaTransaccionPos = (ConsultaTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());

        Date fechaTransaccionHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();
        Assert.assertNotNull(fechaTransaccionHasta);

        Date fechaHasta = consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta();

        Date hoyHorasTruncadas = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
        Date manana = DateUtils.add(hoyHorasTruncadas, Calendar.DAY_OF_MONTH, 1);
        Assert.assertEquals(consultaTransaccionPos.getTransaccion().getSolicitudServicio().getFechaSolicitudServicio(),
                consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio().getFechaTransaccionHasta());
        Assert.assertNotNull(consultaTransaccionPos.getTransaccionConsulta().getSolicitudServicio());
        Assert.assertNotNull(fechaHasta);

    }

}
