package ar.com.osde.centroValidador.pos;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;
import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

public class TestPosMessageResponse extends AbstractPosMessageTest {
	private static final FixedFormatManager manager = new FixedFormatManagerImpl();

	private String messageResponseExpected = "02236IT60000047688762000000980223600        OK, ASOCIADO HABILI-TADO                OK, ASOCIADO HABILI-PLAN 2 210  GRAVADO                                         0000000000001012 210D4343                                                  LAZARTE ARIEL EDUARDO         M19753000                          00                          00                          00                          00   M                                                                                                                                                                                                                                           00000";
	private String tp2du = "15573TR6000379116305900000098TP2DU01        ERROR, ASOCIADO NO  HABILITADO          ASOC.INEXISTENTE                                                                000000000000101     D0100                                                                                 00000000                      ";

	public String message4BRequest=         "     WG600000142010833619700000000000000000004B00424900000 20130604160009WG0033460000000000       0000000000       0000000000       0000000000         00000020130116            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AF0000000000000000  00000000000                                                            000000000000000000      0000000000000000000000000                                                                                                                                                            ";
	public String message4BResponseExpected="     WG60000014004249000000980000000        OK, EXCLUSION REGISTRADA                TRANSACC. N�  3346EXCLUIDA                                                      000000000000101      0000                                                                                 00000000                          00                          00                          00                          00   N                                                                                                                                                                                                                                           00000";
	
	@Test
	public void testTP2DU() {
		//harcodear el numero
		PosMessageResponse posMessageResponse = manager.load(PosMessageResponseForTesting.class, tp2du);
		Assert.assertEquals("37244",posMessageResponse.getNroInternoPos());
	}

	@Test
	public void test420101OkResponse() {

	    PosMessageResponsePrestaciones response = new PosMessageResponsePrestaciones();

		response.setCodigoInterno("02236");
		response.setCodigoOperador("IT");
		response.setFilialInstalacionPOS(60);
		response.setDelegacionInstalacionPOS(0);
		response.setNumeroPOS(47);
		response.setNroTransaccion(688762);
		// response.setNroAutorizacion(123456); comentado porque es parte de
		// test ver que si no se carga nada, no lo borro para que quede como
		// template de otro
		response.setFirmaGeneradorAutorizaciones("98");
		// Viene informado en la posicion 54
		response.setNroInternoPos("2236");
		response.setCodigoRespuestaTransaccion("00");
		response.setLeyendaDesplegar("OK, ASOCIADO HABILI-TADO                ");
		response.setLeyendaImpresionRenglon1("OK, ASOCIADO HABILI-TADO");
		response.setLeyendaImpresionRenglon2("PLAN 2 210  GRAVADO ");
		response.setPrefijoPlan("2");
		response.setPlanSocio("210");
		response.setTipoSocio('D');
		response.setInconsistencia1(43);
		response.setInconsistencia2(43);
		response.setNombreAsociado("LAZARTE ARIEL EDUARDO");
		response.setSexoAsociado('M');
		
        Calendar cal=Calendar.getInstance();
		cal.set(1975, Calendar.DECEMBER, 3);
        String strFechaNacimiento=String.valueOf(cal.get(Calendar.YEAR));
        //strFechaNacimiento+=String.format("%, args)String.valueOf(cal.get(Calendar.MONTH)+1);
        strFechaNacimiento+=String.valueOf(cal.get(Calendar.DAY_OF_MONTH));  
        
		response.setFechaNacimiento(Integer.parseInt(strFechaNacimiento));
		response.setCubrePlanMaterno('M');
		response.setNroSecuencia(1);
		response.setTotalMensajes(1);

		String messageResponse = manager.export(response);
		Assert.assertEquals(649, messageResponse.length());
		System.out.println(messageResponse);
		
		Assert.assertEquals(messageResponseExpected, messageResponse.substring(0,634));

	}
}
