package ar.com.osde.centroValidador.pos;

import junit.framework.Assert;

import org.junit.Test;

public class TestPosMessage2P extends AbstractPosMessageTest {
    public static final String msj2PProtocoloInvalido = "IVR  AI600000102717031265700000061795707901 02P31747600000M20140815142942890890181103133001             0000       0000000000       0000000000       MC11752220140815            0010100                                                                                                                          8908901861795707901317476.TIF                                                                                                                058386                                                                                                                                                                                                    ";

    @Test
    public void testParseoP() {
        ProtocoloPosMessage msj = getMananger().load(ProtocoloPosMessage.class, msj2PProtocoloInvalido);

        Assert.assertEquals("MC", msj.getTipoProtocolo());
        Assert.assertEquals("8908901861795707901317476.TIF", msj.getNombreImagen());
    }
}
