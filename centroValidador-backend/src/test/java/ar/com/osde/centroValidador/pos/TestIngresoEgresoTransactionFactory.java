package ar.com.osde.centroValidador.pos;

import java.text.ParseException;
import java.util.Date;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.commons.otorgamiento.DateUtils;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionIngresoEgresoPos;
import ar.com.osde.entities.aptitud.ContextoEvaluacion;
import ar.com.osde.entities.aptoServicio.SolicitudIngresoEgreso;
import ar.com.osde.entities.transaccion.ItemPrestador;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestIngresoEgresoTransactionFactory extends AbstractTestTransactionFactory {

    private static final String ingresoMessageSinHora = "03399IT600001243054585245000448061705235101002I49438703399M20121029164811000000000000000000       0000000000       0000000000       0000000000       MC07093020121029            0010100000000000000000446471-9                                                                                                                                 00000000000000000                                                                                                                                                                                                                                                                                                         2012-10-29-16.47.28.269000</WMQTool>";

    /**
     * Este mensaje no existe, esta acomodado para que quede con ua prestacion.
     * No fue posible encontrar transacciones 2E con prestaciones
     */
    private static final String egresoMessageConPrestacion = "02827IT600000323054586767961053161508431002002E02485000000M20130619192900  0000004201011001       0000000000       0000000000       0000000000         00000020130619            30101000000000000000NSI1195898000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              000000000001929003 0000000000000000  00000000000                                                               000217100000000   TV O000000000000000000000000                                                                                                                                                             2013-06-19-19.27.22.152000</WMQTool>";

    /**
     * 
     */
    private static final String egresoMessageSinPrestacionConFechaYhoraOffline =   "02827IT600000323054586767961053161508431002002E02485000000M20130619192900  0000000000000000       0000000000       0000000000       0000000000         00000020130619            30101000000000000000NSI1195898000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              000000000001929003 0000000000000000  00000000000                                                               000217100000000   TV O000000000000000000000000                                                                                                                                                             2013-06-19-19.27.22.152000</WMQTool>";

    
    /**
     * 
     */
    private static final String egresoMessageSinPrestacionSinFechaYhoraOffline =   "02827IT600000323054586767961053161508431002002E02485000000M20130619192900  0000000000000000       0000000000       0000000000       0000000000         00000000000000            30101000000000000000NSI1195898000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              000000000000000003 0000000000000000  00000000000                                                               000217100000000   TV O000000000000000000000000                                                                                                                                                             2013-06-19-19.27.22.152000</WMQTool>";
    
    
    /**
     * 
     */
    private static final String egresoMessageSinPrestacionSinFechaOfflineConHora = "02827IT600000323054586767961053161508431002002E02485000000M20130619192900  0000000000000000       0000000000       0000000000       0000000000         00000000000000            30101000000000000000NSI1195898000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              000000000001930003 0000000000000000  00000000000                                                               000217100000000   TV O000000000000000000000000                                                                                                                                                             2013-06-19-19.27.22.152000</WMQTool>";
    @Test
    public void testSimpleIngresoPosMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(ingresoMessageSinHora),
                ingresoMessageSinHora);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof IngresoEgresoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
        Assert.assertEquals(494387, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());

        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());

        Assert.assertEquals(ContextoEvaluacion.OFFLINE, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio()
                .getContextoEvaluacion());
        SolicitudIngresoEgreso solicitudServicio = ingresoEgresoPosMessage.getTransaccion().getSolicitudServicio();
        Assert.assertNotNull(solicitudServicio.getFechaSolicitudServicio());
        Assert.assertNotNull(solicitudServicio.getFechaHora());
        // Date fechaConsumo = null;
        // try {
        // fechaConsumo = DateUtils.createDateAAAAMMDD("20130624");
        // } catch (ParseException e) {
        // Assert.fail(e.getMessage());
        //
        // }
        // Assert.assertEquals(fechaConsumo,
        // DateUtils.getDateWithOutTime(itemPrestador.getFechaConsumo()));

    }

    @Test
    public void testIngresoEgresoConPrestacion() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(egresoMessageConPrestacion),
                egresoMessageConPrestacion);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof IngresoEgresoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
        Assert.assertEquals(24850, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());

        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());

        Assert.assertEquals(ContextoEvaluacion.OFFLINE, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio()
                .getContextoEvaluacion());

        ItemPrestador itemPrestador = ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getItemPrestador();
        SolicitudIngresoEgreso solicitudServicio = ingresoEgresoPosMessage.getTransaccion().getSolicitudServicio();
        Assert.assertNotNull(solicitudServicio.getFechaSolicitudServicio());
        Date fechaHoraSolicitudIngreso = null;
        try {
            fechaHoraSolicitudIngreso = DateUtils.createDateAAAAMMDD("20130619");
        } catch (ParseException e1) {
            Assert.fail(e1.getMessage());
        }
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addHours(fechaHoraSolicitudIngreso, 19);
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addMinutes(fechaHoraSolicitudIngreso, 29);
        Assert.assertNotNull(solicitudServicio.getFechaHora());
        Assert.assertEquals(fechaHoraSolicitudIngreso, solicitudServicio.getFechaHora());
        Assert.assertNotNull(itemPrestador);
        Date fechaConsumo = null;
        try {
            fechaConsumo = DateUtils.createDateAAAAMMDD("20130619");
        } catch (java.text.ParseException e) {
            Assert.fail(e.getMessage());
        }

        Assert.assertEquals(solicitudServicio.getFechaHora(), itemPrestador.getFechaConsumo());

    }
    
    
    
    @Test
    public void testIngresoEgresoSinPrestacionFechaHoraOffline() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(egresoMessageSinPrestacionConFechaYhoraOffline),
                egresoMessageSinPrestacionConFechaYhoraOffline);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof IngresoEgresoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
        Assert.assertEquals(24850, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());

        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());

        Assert.assertEquals(ContextoEvaluacion.OFFLINE, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio()
                .getContextoEvaluacion());

        ItemPrestador itemPrestador = ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getItemPrestador();
        SolicitudIngresoEgreso solicitudServicio = ingresoEgresoPosMessage.getTransaccion().getSolicitudServicio();
        Assert.assertNotNull(solicitudServicio.getFechaSolicitudServicio());
        Date fechaHoraSolicitudIngreso = null;
        try {
            fechaHoraSolicitudIngreso = DateUtils.createDateAAAAMMDD("20130619");
        } catch (ParseException e1) {
            Assert.fail(e1.getMessage());
        }
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addHours(fechaHoraSolicitudIngreso, 19);
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addMinutes(fechaHoraSolicitudIngreso, 29);
        Assert.assertNotNull(solicitudServicio.getFechaHora());
        Assert.assertEquals(fechaHoraSolicitudIngreso, solicitudServicio.getFechaHora());
        Assert.assertNull(itemPrestador);


    }

    
    @Test
    public void testIngresoEgresoSinPrestacionSinFechaHoraOffline() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(egresoMessageSinPrestacionSinFechaYhoraOffline),
                egresoMessageSinPrestacionSinFechaYhoraOffline);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof IngresoEgresoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
        Assert.assertEquals(24850, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());

        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());

        Assert.assertEquals(ContextoEvaluacion.OFFLINE, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio()
                .getContextoEvaluacion());

        ItemPrestador itemPrestador = ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getItemPrestador();
        SolicitudIngresoEgreso solicitudServicio = ingresoEgresoPosMessage.getTransaccion().getSolicitudServicio();
        Assert.assertNotNull(solicitudServicio.getFechaSolicitudServicio());
        Assert.assertNotNull(solicitudServicio.getFechaHora());
        Assert.assertEquals(solicitudServicio.getFechaSolicitudServicio(), solicitudServicio.getFechaHora());
        Assert.assertNull(itemPrestador);
    }

    
    @Test
    public void testIngresoEgresoSinPrestacionSinFechaOfflineConHoraOffline() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(egresoMessageSinPrestacionSinFechaOfflineConHora),
                egresoMessageSinPrestacionSinFechaOfflineConHora);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof IngresoEgresoPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionIngresoEgresoPos);
        TransaccionIngresoEgresoPos ingresoEgresoPosMessage = (TransaccionIngresoEgresoPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso());
        Assert.assertEquals(24850, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getNumeroTransaccion());

        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getBeneficiarioBid());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getTipoServicio());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getFilialDeConsumo());
        Assert.assertNotNull(ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio().getModoIngresoCredencial());

        Assert.assertEquals(ContextoEvaluacion.OFFLINE, ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getSolicitudServicio()
                .getContextoEvaluacion());

        ItemPrestador itemPrestador = ingresoEgresoPosMessage.getTransaccionIngresoEgreso().getItemPrestador();
        SolicitudIngresoEgreso solicitudServicio = ingresoEgresoPosMessage.getTransaccion().getSolicitudServicio();
        Assert.assertNotNull(solicitudServicio.getFechaSolicitudServicio());
        Assert.assertNotNull(solicitudServicio.getFechaHora());
        
        Date fechaHoraSolicitudIngreso = null;
        try {
            fechaHoraSolicitudIngreso = DateUtils.createDateAAAAMMDD("20130619");
        } catch (ParseException e1) {
            Assert.fail(e1.getMessage());
        }
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addHours(fechaHoraSolicitudIngreso, 19);
        fechaHoraSolicitudIngreso = org.apache.commons.lang.time.DateUtils.addMinutes(fechaHoraSolicitudIngreso, 30);
        
        Assert.assertEquals(fechaHoraSolicitudIngreso, solicitudServicio.getFechaHora());
        Assert.assertNull(itemPrestador);
    }
}
