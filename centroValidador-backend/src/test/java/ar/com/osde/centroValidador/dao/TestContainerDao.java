package ar.com.osde.centroValidador.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ar.com.osde.centroValidador.pos.handlers.AlwaysRouteMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.FilteredMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.MigracionMessageHandler;
import ar.com.osde.cv.dao.impl.CodigoExceptuableDAOImpl;
import ar.com.osde.cv.dao.impl.ContainerDAOImpl;
import ar.com.osde.cv.util.DummyContainerFactory;
import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.ErrorHandlerConfiguration;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.MigracionMessageHandlerConfiguration;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

@ContextConfiguration(locations = { "/spring/backend/osde-framework-dao-spring.xml",
        "/spring/test/osde-framework-tx-spring.xml" })
public class TestContainerDao extends AbstractJUnit4SpringContextTests {

	@Test
	public void testABM() {

		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");
		ContainerConfiguration container = DummyContainerFactory.createSimpleContainer(errorQueue, containerQueue,
		        handlerQueue);

		containerDao.saveNew(container);
		long containerID = container.getId();
		Assert.assertEquals(1, containerDao.getAll().size());

		ContainerConfiguration containerConfiguration = containerDao.getAll().get(0);

		Assert.assertEquals(containerID, containerConfiguration.getId());
		Assert.assertEquals(containerQueue, containerConfiguration.getQueueName());
		Assert.assertEquals(AlwaysRouteMessageHandler.class.getCanonicalName(),
		        containerConfiguration.getPosMessageHandler().getClassName());
		Assert.assertEquals(errorQueue, containerConfiguration.getErrorHandler().getQueueName());

		containerDao.saveOrUpdate(container);

		Assert.assertEquals(1, containerDao.getAll().size());

		containerDao.delete(container);

		Assert.assertTrue(containerDao.getAll().isEmpty());

	}

	@Test
	public void testABMMigracionHandlerMessage() {
		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";
		CodigoExceptuableDAOImpl codigosDao = (CodigoExceptuableDAOImpl) this.applicationContext
		        .getBean("codigo.exceptuable.dao");

		try {
			CodigoExceptuable codigo = new CodigoExceptuable();
			codigo.setCodigo(100);
			codigo.setDescripcion("Nuevo1");
			codigosDao.saveNew(codigo);
			CodigoExceptuable codigo2 = new CodigoExceptuable();
			codigo2.setCodigo(100);
			codigo2.setDescripcion("Nuevo1");
			codigosDao.saveNew(codigo2);
		} catch (BusinessException e) {
			e.printStackTrace();
		}

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");

		MigracionMessageHandlerConfiguration handler = new MigracionMessageHandlerConfiguration();
		handler.setClassName("ar.com.osde.centroValidador.pos.handlers.MigracionMessageHandler");
		Set<CodigoExceptuable> codigos = new HashSet<CodigoExceptuable>();
		ContainerConfiguration container=null;
		try {
			codigos.addAll(codigosDao.getAllCodigos());
			handler.setCodigosExceptuar(codigos);
			container = DummyContainerFactory.createContainerWithHandler(errorQueue, containerQueue, handlerQueue,
			        handler);

			containerDao.saveNew(container);
			long containerID = container.getId();
			Assert.assertEquals(1, containerDao.getAll().size());

			ContainerConfiguration containerConfiguration = containerDao.getAll().get(0);

			Assert.assertEquals(containerID, containerConfiguration.getId());
			Assert.assertEquals(containerQueue, containerConfiguration.getQueueName());
			Assert.assertEquals(MigracionMessageHandler.class.getCanonicalName(),
			        containerConfiguration.getPosMessageHandler().getClassName());
			Assert.assertEquals(errorQueue, containerConfiguration.getErrorHandler().getQueueName());

			MigracionMessageHandlerConfiguration migracionMessageHandlerConfiguration = (MigracionMessageHandlerConfiguration) containerConfiguration
			        .getPosMessageHandler();
			Assert.assertEquals(2, migracionMessageHandlerConfiguration.getCodigosExceptuar().size());
			containerDao.saveOrUpdate(container);

			Assert.assertEquals(1, containerDao.getAll().size());

			containerDao.delete(container);

			Assert.assertTrue(containerDao.getAll().isEmpty());

		} catch (BusinessException e) {
			if (container != null) {
				containerDao.delete(container);
			}
			Assert.fail();
		} 
	}

	@Test
	public void testABMFilter() {

		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");
		ContainerConfiguration container = DummyContainerFactory.createContainerWithFilter(errorQueue, containerQueue,
		        handlerQueue);

		containerDao.saveNew(container);

		Assert.assertEquals(1, containerDao.getAll().size());

		FilteredMessageHandlerConfiguration filteredMessageHandler = (FilteredMessageHandlerConfiguration) containerDao
		        .getAll().get(0).getPosMessageHandler();

		Assert.assertEquals(FilteredMessageHandler.class.getCanonicalName(), filteredMessageHandler.getClassName());
		Assert.assertEquals(1, filteredMessageHandler.getFilters().size());
		Assert.assertEquals(1, filteredMessageHandler.getFilterStrategyList().size());

		containerDao.delete(container);

		Assert.assertTrue(containerDao.getAll().isEmpty());

	}

	@Test
	public void testABMCompositeFilter() {

		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");
		ContainerConfiguration container = DummyContainerFactory.createContainerWithCompositeFilter(errorQueue,
		        containerQueue, handlerQueue);

		containerDao.saveNew(container);

		Assert.assertEquals(1, containerDao.getAll().size());

		FilteredMessageHandlerConfiguration filteredMessageHandler = (FilteredMessageHandlerConfiguration) containerDao
		        .getAll().get(0).getPosMessageHandler();

		Assert.assertEquals(FilteredMessageHandler.class.getCanonicalName(), filteredMessageHandler.getClassName());
		Assert.assertEquals(1, filteredMessageHandler.getFilters().size());
		Assert.assertEquals(1, filteredMessageHandler.getFilterStrategyList().size());

		containerDao.delete(container);

		Assert.assertTrue(containerDao.getAll().isEmpty());

	}

	@Test
	public void testABMNotPropertyFilter() {

		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");
		ContainerConfiguration container = DummyContainerFactory.createContainerNotFilter(errorQueue, containerQueue,
		        handlerQueue);

		try {
			containerDao.saveNew(container);
			Assert.assertEquals(1, containerDao.getAll().size());

			ContainerConfiguration containerFromDB = containerDao.getAll().get(0);
			FilteredMessageHandlerConfiguration filteredMessageHandlerFromDB = (FilteredMessageHandlerConfiguration) containerFromDB
			        .getPosMessageHandler();
			List<FilterConfiguration> filtersFromDb = filteredMessageHandlerFromDB.getFilters();

			List<FilterConfiguration> filtersOriginal = ((FilteredMessageHandlerConfiguration) container
			        .getPosMessageHandler()).getFilters();
			Assert.assertEquals(filtersOriginal.size(), filtersFromDb.size());

			NotFilterConfiguration notFilterOriginal = (NotFilterConfiguration) filtersOriginal.get(0);
			NotFilterConfiguration notFilterFromDB = (NotFilterConfiguration) filtersFromDb.get(0);

			PropertyEqualFilterConfiguration propertyEqualOrginal = (PropertyEqualFilterConfiguration) notFilterOriginal
			        .getFilterConfiguration();
			PropertyEqualFilterConfiguration propertyEqualFromDB = (PropertyEqualFilterConfiguration) notFilterFromDB
			        .getFilterConfiguration();

			Assert.assertEquals(propertyEqualOrginal.getPropertyName(), propertyEqualFromDB.getPropertyName());
			Assert.assertEquals(propertyEqualOrginal.getValue(), propertyEqualFromDB.getValue());

			Assert.assertEquals(FilteredMessageHandler.class.getCanonicalName(),
			        filteredMessageHandlerFromDB.getClassName());
			Assert.assertEquals(1, filteredMessageHandlerFromDB.getFilters().size());
			Assert.assertEquals(1, filteredMessageHandlerFromDB.getFilterStrategyList().size());

			containerDao.delete(container);

			Assert.assertTrue(containerDao.getAll().isEmpty());

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testABMCNotompositeFilter() {

		final String errorQueue = "queue:ERROR_MDR";
		final String containerQueue = "queue:OSDE_ASIS_MDR";
		final String handlerQueue = "queue:MDR_CVRSPONSE";

		ContainerDAOImpl containerDao = (ContainerDAOImpl) this.applicationContext.getBean("container.dao");
		ContainerConfiguration container = DummyContainerFactory.createContainerWithNotAndCompositeFilter(errorQueue,
		        containerQueue, handlerQueue);

		containerDao.saveNew(container);

		Assert.assertEquals(1, containerDao.getAll().size());

		FilteredMessageHandlerConfiguration filteredMessageHandlerFromDB = (FilteredMessageHandlerConfiguration) containerDao
		        .getAll().get(0).getPosMessageHandler();

		Assert.assertEquals(FilteredMessageHandler.class.getCanonicalName(),
		        filteredMessageHandlerFromDB.getClassName());
		Assert.assertEquals(1, filteredMessageHandlerFromDB.getFilters().size());
		NotFilterConfiguration notFilter = (NotFilterConfiguration) filteredMessageHandlerFromDB.getFilters().get(0);
		CompositeFilterConfiguration compositeFilter = (CompositeFilterConfiguration) notFilter
		        .getFilterConfiguration();
		Assert.assertEquals(1, compositeFilter.getFilterConfigurationList().size());
		Assert.assertEquals(1, filteredMessageHandlerFromDB.getFilterStrategyList().size());

		containerDao.delete(container);

		Assert.assertTrue(containerDao.getAll().isEmpty());

	}

	@Test
	public void testCloneContainer() {
		ContainerConfiguration containerConfiguration = new ContainerConfiguration();
		containerConfiguration.setAcceptMessageWhileStoping(true);
		containerConfiguration.setConnectionFactoryName("pepe");
		ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
		errorHandler.setConnectionFactoryName("asdads");
		errorHandler.setId(2L);
		errorHandler.setQueueName("asdasd");
		errorHandler.setTimeOutQueueName("asdasdasd");
		containerConfiguration.setErrorHandler(errorHandler);
		containerConfiguration.setId(1L);
		containerConfiguration.setNumberOfConcurrentConsumers(1);
		FilteredMessageHandlerConfiguration filteredMessageHandlerConfiguration = new FilteredMessageHandlerConfiguration();
		filteredMessageHandlerConfiguration.setClassName("asdasdasd");
		List<FilterStrategyConfiguration> filterStrategyList = new ArrayList<FilterStrategyConfiguration>();
		filteredMessageHandlerConfiguration.setFilterStrategyList(filterStrategyList);
		List<FilterConfiguration> filters = new ArrayList<FilterConfiguration>();
		filteredMessageHandlerConfiguration.setFilters(filters);
		MigracionMessageHandlerConfiguration migracionMessageHandlerConfiguration = new MigracionMessageHandlerConfiguration();
		migracionMessageHandlerConfiguration.setClassName("asdas");
		migracionMessageHandlerConfiguration.setId(2L);
		Set<CodigoExceptuable> codigos = new HashSet<CodigoExceptuable>();
		migracionMessageHandlerConfiguration.setCodigosExceptuar(codigos);
		filteredMessageHandlerConfiguration.setPosMessageHandler(migracionMessageHandlerConfiguration);
		containerConfiguration.setPosMessageHandler(filteredMessageHandlerConfiguration);
		containerConfiguration.setQueueName("asdas");
		containerConfiguration.setStartOnLoad(true);

		try {
			Assert.assertEquals(containerConfiguration, containerConfiguration.clone());
			ContainerConfiguration clone = (ContainerConfiguration) containerConfiguration.clone();
			clone.cleanToSave();
			Assert.assertFalse(containerConfiguration.equals(clone));
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
