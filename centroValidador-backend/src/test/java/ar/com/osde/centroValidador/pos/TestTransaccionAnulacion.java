package ar.com.osde.centroValidador.pos;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.AnulacionTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionAnulacion extends AbstractTestTransactionFactory {


    private static final String anulacionPosMessage = "02827IT600000403069404450261053161184151601004A41593300000M20120928163659984115880000000000       0000000000       0000000000       0000000000         00000020120928            001010030694044502000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              000000000001637000 0000000000000000  00000000000                                                               000000000000000   TV O000000000000000000000000                                                                                                                                                             2012-09-28-16.36.23.723000";

    private static final String anulacionWDGPosMessage="     WG609000142010833619700000000000000000004A00280100000 20121109113836WG0027190000000000       0000000000       0000000000       0000000000         00000020121101            001010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000AA0000000000000000  00000000000                                                            000000000000000000      0000000000000000000000000                                                                                                                                                                 ";    
    
    private static final String parseoTransaccionReferencia="02827IT600000403069404450261053161184151601004A41593300000M20120928163659984115880000000000       0000000000       0000000000       0000000000         00000020120928            001010030694044502000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000163700  0000000000000000  00000000000                                                               000000000000000   TV O000000000000000000000000                                                                                                                                                             2012-09-28-16.36.23.723000";

    private static final String parseoTransaccionReferencia2="02827IT600000403069404450261053161184151601004A41593300000M20120928163659984115880000000000       0000000000       0000000000       0000000000         00000020120928            001010030694044502000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000163700000000000000000000  00000000000                                                               000000000000000   TV O000000000000000000000000                                                                                                                                                             2012-09-28-16.36.23.723000";

    @Test
    public void testSimpleAnulacionPosMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(anulacionPosMessage), anulacionPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(),posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulactionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion());
        Assert.assertEquals(415933l, anulactionTransaccionPos.getTransaccion().getNumeroTransaccion());
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getFechaSolicitudServicio());
        
        
    }
    
    
    @Test
    public void testWDGAnulacionPosMessage() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(anulacionWDGPosMessage), anulacionWDGPosMessage);
        Assert.assertTrue(posMessage.getClass().getName(),posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulactionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion());
        
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getFechaSolicitudServicio());
        
        
    }
    
    @Test
    public void testTransaccionReferencia() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(parseoTransaccionReferencia), parseoTransaccionReferencia);
        Assert.assertTrue(posMessage.getClass().getName(),posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulactionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion());
        
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals("IT",anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getTransaccionReferencia().getCodigoOperador());

        
        
    }
    
    @Test
    public void testTransaccionReferencia2() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(parseoTransaccionReferencia2), parseoTransaccionReferencia2);
        Assert.assertTrue(posMessage.getClass().getName(),posMessage instanceof TipoCuatroPosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof AnulacionTransaccionPos);
        AnulacionTransaccionPos anulactionTransaccionPos = (AnulacionTransaccionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion());
        
        Assert.assertNotNull(anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getFechaSolicitudServicio());
        Assert.assertEquals("IT",anulactionTransaccionPos.getTransaccionAnulacion().getSolicitudServicio().getTransaccionReferencia().getCodigoOperador());

        
        
    }

}
