package ar.com.osde.centroValidador.pos.services;

import java.util.List;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import ar.com.osde.centroValidador.services.pos.ContainerService;
import ar.com.osde.cv.util.DummyContainerFactory;
import ar.com.osde.entities.ContainerConfiguration;
import junit.framework.Assert;

@ContextConfiguration(locations = { "/spring/test/osde-framework-ws-client-spring.xml" })
public class TestContainerService extends AbstractJUnit4SpringContextTests {

    @Test
    public void testSaveContainer() {

        ContainerService containerService = (ContainerService) this.applicationContext.getBean("containerService");

        ContainerConfiguration containerConfiguration = DummyContainerFactory.createSimpleContainer("queue:ERROR_MDR",
                "queue:OSDE_ASIS_MDR", "queue:MDR_CVRSPONSE");

        List<ContainerConfiguration> containerList = null;
        Assert.assertNotNull(containerService);
//        try {
//            containerService.saveContainer(containerConfiguration);
//            containerList = containerService.getAllContainers();
//        } catch (ServiceException e) {
//            Assert.fail();
//        }
//
//        Assert.assertEquals(1, containerList.size());
    }
}
