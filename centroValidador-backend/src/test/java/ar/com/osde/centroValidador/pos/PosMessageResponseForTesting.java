package ar.com.osde.centroValidador.pos;

import com.ancientprogramming.fixedformat4j.annotation.Record;

/**
 * Clase utilizada para testing
 * @author MT27789605
 *
 */
@Record
public class PosMessageResponseForTesting extends PosMessageResponse {

    public PosMessageResponseForTesting(){
        
    }
}
