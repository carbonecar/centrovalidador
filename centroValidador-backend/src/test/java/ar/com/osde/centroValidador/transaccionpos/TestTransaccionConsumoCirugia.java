package ar.com.osde.centroValidador.transaccionpos;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.dozer.Mapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.AbstractTestTransactionFactory;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.commons.otorgamiento.DateUtils;
import ar.com.osde.cv.model.AbstractTransaccionPos;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.cv.model.TransaccionPrestacionPos;
import ar.com.osde.entities.aptitud.ContextoEvaluacion;
import ar.com.osde.entities.transaccion.ItemPrestador;

import com.ancientprogramming.fixedformat4j.format.FixedFormatManager;

@ContextConfiguration(locations = { "/spring/test/osde-framework-dozer-spring.xml" })
public class TestTransaccionConsumoCirugia extends AbstractTestTransactionFactory {

    private static final String transaccionUrgencias = "SAF  AA600010002013808579200000061026243101002Q94686600000M20130625173442980001111102111001             0000             0000             0000         00000020130624            001010020138085792                                                                                                                                             00000000000000000                                                                                              011227                                        110211                                                                                                                                                      ";


    @Test
    public void testSimpleTransaccionCirugia() {
        Mapper mapper = getMapper();

        FixedFormatManager manager = getMannager();

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(transaccionUrgencias),
                transaccionUrgencias);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessage);
        AbstractTransaccionPos message = TransaccionFactory.getInstance().createTransaccion(posMessage, mapper);
        Assert.assertTrue(message instanceof TransaccionPrestacionPos);
        TransaccionPrestacionPos transaccionCirugia = (TransaccionPrestacionPos) message;
        Assert.assertNotNull(message.getSecuencia());
        Assert.assertEquals(1, message.getSecuencia().getNumeroSecuenciaMensaje());
        Assert.assertEquals(1, message.getSecuencia().getNumeroTotalMensajes());
        Assert.assertEquals(ContextoEvaluacion.OFFLINE, transaccionCirugia.getTransaccionConsumo().getSolicitudServicio().getContextoEvaluacion());
        List<ItemPrestador> itemsPrestador = transaccionCirugia.getTransaccionConsumo().getItemsPrestador();

        Assert.assertEquals(1, itemsPrestador.size());
        ItemPrestador itemPrestador = itemsPrestador.get(0);
        Date fechaConsumo = null;
        try {
            fechaConsumo = DateUtils.createDateAAAAMMDD("20130624");
        } catch (ParseException e) {
            Assert.fail(e.getMessage());

        }
        Assert.assertEquals(fechaConsumo, DateUtils.getDateWithOutTime(itemPrestador.getFechaConsumo()));
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo());
        //
        // Assert.assertNotNull(transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getSolicitudServicio()
        // .getFechaSolicitudServicio());
        // Assert.assertEquals(2866,
        // transaccionPrestacionPosUrgencias.getTransaccionBloqueoDesbloqueo().getNumeroTransaccion());

    }
}
