package ar.com.osde.centroValidador.pos.filter;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.centroValidador.pos.exception.FilteredMessageException;

import java.util.ArrayList;
import java.util.List;

public class TestFilter {

    private final int ARANCEL_VALUE = 10;
    private final int NUMERO_TRANSACCION = 5;

    private PropertyEqualFilter propertyArancelFilter;
    private PropertyEqualFilter propertyNumeroTransaccionFilter;
    private OrFilter orFilter;
    private AndFilter andFilter;

    @Before
    public void setUp() {

        propertyArancelFilter = new PropertyEqualFilter();
        propertyArancelFilter.setPropertyName("getArancelPrestacion1");
        propertyArancelFilter.setValue(ARANCEL_VALUE);

        propertyNumeroTransaccionFilter = new PropertyEqualFilter();
        propertyNumeroTransaccionFilter.setPropertyName("getNumeroTransaccion");
        propertyNumeroTransaccionFilter.setValue(NUMERO_TRANSACCION);

        orFilter = new OrFilter();
        orFilter.addFilter(propertyArancelFilter);
        orFilter.addFilter(propertyNumeroTransaccionFilter);

        andFilter = new AndFilter();
        andFilter.addFilter(propertyArancelFilter);
        andFilter.addFilter(propertyNumeroTransaccionFilter);
    }

    @Test
    public void testOrFilter() {

        PosMessage posMessage = new PosMessage();
        posMessage.setNumeroTransaccion(NUMERO_TRANSACCION);
        posMessage.setArancelPrestacion1(ARANCEL_VALUE);

        propertyNumeroTransaccionFilter.validate(posMessage);
        propertyArancelFilter.validate(posMessage);

        orFilter.validate(posMessage);

        posMessage.setArancelPrestacion1(ARANCEL_VALUE + 1);

        orFilter.validate(posMessage);

        posMessage.setNumeroTransaccion(NUMERO_TRANSACCION + 1);

        boolean exceptionFired = false;

        try {
            orFilter.validate(posMessage);
        } catch (FilteredMessageException e) {
            exceptionFired = true;
        }

        Assert.assertTrue(exceptionFired);
    }

    @Test
    public void testAndFilter() {

        PosMessage posMessage = new PosMessage();
        posMessage.setNumeroTransaccion(NUMERO_TRANSACCION);
        posMessage.setArancelPrestacion1(ARANCEL_VALUE);

        andFilter.validate(posMessage);

        posMessage.setArancelPrestacion1(ARANCEL_VALUE + 1);

        boolean exceptionFired = false;

        try {
            andFilter.validate(posMessage);
        } catch (FilteredMessageException e) {
            exceptionFired = true;
        }

        Assert.assertTrue(exceptionFired);
    }

    @Test
    public void testNotFilter() {

        PosMessage posMessage = new PosMessage();
        posMessage.setNumeroTransaccion(NUMERO_TRANSACCION);
        posMessage.setArancelPrestacion1(ARANCEL_VALUE + 1);

        NotFilter notFilter = new NotFilter(andFilter);

        notFilter.validate(posMessage);

        posMessage.setArancelPrestacion1(ARANCEL_VALUE);

        boolean exceptionFired = false;

        try {
            notFilter.validate(posMessage);
        } catch (FilteredMessageException e) {
            exceptionFired = true;
        }

        Assert.assertTrue(exceptionFired);
    }

    @Test
    public void testNullFilter() {

        PosMessage posMessage = new PosMessage();
        posMessage.setNumeroTransaccion(NUMERO_TRANSACCION);
        posMessage.setArancelPrestacion1(ARANCEL_VALUE);

        NullFilter nullFilter = new NullFilter();

        nullFilter.validate(posMessage);
    }

    @Test(expected = FilteredMessageException.class)
    public void testNotYNullFilter() {

        PosMessage posMessage = new PosMessage();

        NotFilter notFilter = new NotFilter(new NullFilter());
        notFilter.validate(posMessage);
    }

    @Test
    public void testImplementaMetodoNotFilter() {
        PropertyEqualFilter propertyFilter = new PropertyEqualFilter();
        propertyFilter.setPropertyName("asda");
        propertyFilter.setValue(10);
        NotFilter notFilter = new NotFilter(propertyFilter);
        Assert.assertEquals(true, notFilter.evaluaPropiedad("asda"));
        Assert.assertEquals(false, notFilter.evaluaPropiedad("sasda"));
        Assert.assertEquals(false, notFilter.implementaPropiedadValor("asda", 10));
        Assert.assertEquals(false, notFilter.implementaPropiedadValor("asdas", 10));
        Assert.assertEquals(true, notFilter.implementaPropiedadValor("asda", 20));
    }

    @Test
    public void testImplementaMetodoAndFilter() {
        PropertyEqualFilter propertyFilter = new PropertyEqualFilter();
        propertyFilter.setPropertyName("asda");
        propertyFilter.setValue(10);
        PropertyEqualFilter propertyFilter2 = new PropertyEqualFilter();
        propertyFilter2.setPropertyName("sada");
        propertyFilter2.setValue(20);
        AndFilter andFilter = new AndFilter();
        List<PosMessageFilter> filtros = new ArrayList<PosMessageFilter>();
        filtros.add(propertyFilter);
        filtros.add(propertyFilter2);
        andFilter.setFiltros(filtros);
        Assert.assertEquals(true, andFilter.evaluaPropiedad("asda"));
        Assert.assertEquals(false, andFilter.evaluaPropiedad("sasda"));
        Assert.assertEquals(true, andFilter.implementaPropiedadValor("asda", 10));
        Assert.assertEquals(false, andFilter.implementaPropiedadValor("asdas", 10));
    }

    @Test
    public void testImplementaMetodoCompuesto() {
        PropertyEqualFilter propertyFilter = new PropertyEqualFilter();
        propertyFilter.setPropertyName("asda");
        propertyFilter.setValue(10);

        PropertyEqualFilter propertyFilter2 = new PropertyEqualFilter();
        propertyFilter2.setPropertyName("sada");
        propertyFilter2.setValue(20);
        NotFilter notFilter = new NotFilter(propertyFilter2);

        AndFilter andFilter = new AndFilter();
        List<PosMessageFilter> filtros = new ArrayList<PosMessageFilter>();
        filtros.add(propertyFilter);
        filtros.add(notFilter);
        andFilter.setFiltros(filtros);

        Assert.assertEquals(true, andFilter.evaluaPropiedad("asda"));
        Assert.assertEquals(true, andFilter.evaluaPropiedad("sada"));
        Assert.assertEquals(false, andFilter.evaluaPropiedad("sasda"));
        Assert.assertEquals(true, andFilter.implementaPropiedadValor("asda", 10));
        Assert.assertEquals(true, andFilter.implementaPropiedadValor("sada", 10));
        Assert.assertEquals(false, andFilter.implementaPropiedadValor("asdas", 10));
        Assert.assertEquals(false, andFilter.implementaPropiedadValor("sada", 20));
    }
}
