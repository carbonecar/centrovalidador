package ar.com.osde.centroValidador.pos;

import java.io.Serializable;

import org.junit.Assert;
import org.junit.Test;

public class TestPosMessageCredencialProvisoaria extends AbstractPosMessageTest {

    private static final String msj2 = "SAF  AA000069332008447205100000060728835201002A93836900000M20100330181316  0000000004751001       0004121001       0004811001       0060941001         00000000000000            001040220084472051                                                                                                                                             00000000000000000  0000000000000                                                                                                                                                                                         013                                                                                                      ";
    private static final String msj6 = "43421IM600007112010828432461053161030791502902A34317300000A20101115173537000000004201011001       0000000000       0000000000       0000000000         00000000000000            0000002000000000000000000000000000000000000000000000        000000000000000000000000000000000000000000000000000000000000000000000000000                              00000000000000000                                                                                                                                                                                                                                                                                                   ";

    private static final String mensaje2P = "IVR  AI600000102717031265700000061795707901 02P31747600000M20140815142942890890181103133001             0000       0000000000       0000000000       MC11752220140815            0010101                                                                                                                          8908901861795707901317476.TIF                                                                                                                058386                                                                                                    013                                                                                             ";
    private static final String mensaje2FversionMensajeria = "FRM  AF371301192322324621400000061554412504002F95179000000M20150219192325  000000                                                                    MC01511120150219            0010100                       5091151             01                               00                               00                                                            0000000000000000   0000000000                                                            1  050341 20150219  M    000000000000                                                                                2.0.0                                                                                   ";

    @Test
    public void testSerialize() {
        PosMessage msj = getMananger().load(PosMessage.class, msj6);
        Assert.assertTrue((msj instanceof Serializable));
    }

    @Test
    public void testVersionCredencial() {
        PosMessage msj = getMananger().load(PosMessage.class, msj6);
        Assert.assertEquals(2, msj.getVersionCredencial());
    }

    @Test
    public void testCodigoSeguridadExterno() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2);

        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());

        Assert.assertEquals("013", msj.getCodigoSeguridadExterno());

    }

    @Test
    public void testFormaIngresoAsociado() {
        PosMessage msj = getMananger().load(PosMessage.class, msj2);

        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());

        Assert.assertEquals("013", msj.getCodigoSeguridadExterno());

    }

    @Test
    public void testCredencialTransaccionProtocolo() {
        PosMessage msj = getMananger().load(PosMessage.class, mensaje2P);

        Assert.assertEquals("M", msj.getFormaIngresoDelAsociado());

        Assert.assertEquals("013", msj.getCodigoSeguridadExterno());
        Assert.assertEquals(1, msj.getVersionCredencial());
    }

    @Test
    public void testVersionMensajeria() {
        PosMessage msj = getMananger().load(PosMessage.class, mensaje2FversionMensajeria);

        Assert.assertEquals("     2.0.0", msj.getVersionMensajeria());

    }

}
