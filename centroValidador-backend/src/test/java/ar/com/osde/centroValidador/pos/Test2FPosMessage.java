package ar.com.osde.centroValidador.pos;

import org.junit.Assert;
import org.junit.Test;

public class Test2FPosMessage extends AbstractPosMessageTest {

    private String mensaje = "03240IF420100863070902926200000042203984202002F77261603240M20130305071834  000000                         00             0000             0000       MM 12006000000000            001010030709029262000000000000467630677953450108300100000000000229056477923710862660100000000000000000000000000000000000000000000                              00000000000000000  0000000000000000  00000000000                                                               000000020130305 MM    000000025416018746000000                                                                                                                                                            2013-03-05-19.17.21.665000";

    @Test
    public void testSimpleParse() {

        AbstractPosMessage posMessage = getAbstractPosMessage(mensaje);
        Assert.assertTrue(posMessage.getClass().getName(), posMessage instanceof PosMessageFarmacia);
        
        
    }
}
	