package ar.com.osde.centroValidador.pos;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;

import ar.com.osde.centroValidador.factory.FixedFormatMessageClassFactory;
import ar.com.osde.centroValidador.pos.handlers.FixedPosFormatManager;

public abstract class AbstractPosMessageTest {

    private FixedPosFormatManager manager = new FixedPosFormatManager();

    private Mapper mapper;

    @Before
    public void setUp() throws Exception {

        List<String> mappings = new ArrayList<String>();
        mappings.add("config/configuration.dzr.xml");
        mappings.add("config/pos.dzr.xml");
        mapper = new DozerBeanMapper(mappings);
    }

    protected Mapper getMapper() {
        return this.mapper;
    }

    protected AbstractPosMessage getAbstractPosMessage(String message) {

        AbstractPosMessage posMessage = manager.load(FixedFormatMessageClassFactory.getClassInstance(message), message);
        return posMessage;
    }
    
    protected FixedPosFormatManager getMananger(){
        return this.manager;
    }

}
