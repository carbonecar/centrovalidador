package ar.com.osde.cv.pos.utils;

import java.util.Enumeration;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

public class TextMessageAdapter implements TextMessage {

    private String messageId;
    private String correlationId;
    private String text;

    public void setStringProperty(String paramString1, String paramString2) throws JMSException {

    }

    public void setShortProperty(String paramString, short paramShort) throws JMSException {

    }

    public void setObjectProperty(String paramString, Object paramObject) throws JMSException {

    }

    public void setLongProperty(String paramString, long paramLong) throws JMSException {

    }

    public void setJMSType(String paramString) throws JMSException {

    }

    public void setJMSTimestamp(long paramLong) throws JMSException {

    }

    public void setJMSReplyTo(Destination paramDestination) throws JMSException {

    }

    public void setJMSRedelivered(boolean paramBoolean) throws JMSException {

    }

    public void setJMSPriority(int paramInt) throws JMSException {

    }

    public void setJMSMessageID(String paramString) throws JMSException {
        this.messageId = paramString;
    }

    public void setJMSExpiration(long paramLong) throws JMSException {
    }

    public void setJMSDestination(Destination paramDestination) throws JMSException {

    }

    public void setJMSDeliveryMode(int paramInt) throws JMSException {

    }

    public void setJMSCorrelationIDAsBytes(byte[] paramArrayOfByte) throws JMSException {

    }

    public void setJMSCorrelationID(String paramString) throws JMSException {
        this.correlationId = paramString;
    }

    public void setIntProperty(String paramString, int paramInt) throws JMSException {
    }

    public void setFloatProperty(String paramString, float paramFloat) throws JMSException {

    }

    public void setDoubleProperty(String paramString, double paramDouble) throws JMSException {

    }

    public void setByteProperty(String paramString, byte paramByte) throws JMSException {

    }

    public void setBooleanProperty(String paramString, boolean paramBoolean) throws JMSException {

    }

    public boolean propertyExists(String paramString) throws JMSException {
        return false;
    }

    public String getStringProperty(String paramString) throws JMSException {
        return null;
    }

    public short getShortProperty(String paramString) throws JMSException {
        return 0;
    }

    public Enumeration<String> getPropertyNames() throws JMSException {
        return null;
    }

    public Object getObjectProperty(String paramString) throws JMSException {
        return null;
    }

    public long getLongProperty(String paramString) throws JMSException {
        return 0;
    }

    public String getJMSType() throws JMSException {
        return null;
    }

    public long getJMSTimestamp() throws JMSException {
        return 0;
    }

    public Destination getJMSReplyTo() throws JMSException {
        return null;
    }

    public boolean getJMSRedelivered() throws JMSException {
        return false;
    }

    public int getJMSPriority() throws JMSException {
        return 0;
    }

    public String getJMSMessageID() throws JMSException {
        return this.messageId;
    }

    public long getJMSExpiration() throws JMSException {
        return 0;
    }

    public Destination getJMSDestination() throws JMSException {
        return null;
    }

    public int getJMSDeliveryMode() throws JMSException {
        return 0;
    }

    public byte[] getJMSCorrelationIDAsBytes() throws JMSException {
        return null;
    }

    public String getJMSCorrelationID() throws JMSException {
        return this.correlationId;
    }

    public int getIntProperty(String paramString) throws JMSException {
        return 0;
    }

    public float getFloatProperty(String paramString) throws JMSException {
        return 0;
    }

    public double getDoubleProperty(String paramString) throws JMSException {
        return 0;
    }

    public byte getByteProperty(String paramString) throws JMSException {
        return 0;
    }

    public boolean getBooleanProperty(String paramString) throws JMSException {
        return false;
    }

    public void clearProperties() throws JMSException {

    }

    public void clearBody() throws JMSException {

    }

    public void acknowledge() throws JMSException {

    }

    public void setText(String paramString) throws JMSException {
        this.text = paramString;
    }

    public String getText() throws JMSException {
        return this.text;
    }
}