package ar.com.osde.cv.pos.utils;

import ar.com.osde.framework.services.SearchResult;
import ar.com.osde.prestadores.exceptions.ObjectNotFoundException;
import ar.com.osde.prestadores.services.PrestadorService;
import ar.com.osde.rules.prestadores.common.entities.Prestador;

public class DummyPrestadorService implements PrestadorService {

	public String getCodigoRelacionPrestador(long cuit, String codigoOperador, int filialTerminal, int nroTerminal,
	        int delegacion) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return "101001010";
	}

	public Prestador getPrestadorConTerminal(long cuit, String codigoOperador, int filialTerminal, int nroTerminal,
	        int delegacion) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	public SearchResult<Prestador> searchPrestadores(long cuit) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	public SearchResult<Prestador> searchPrestadoresByCuitFilial(long cuit, int filialTerminal) {
		// TODO Auto-generated method stub
		return null;
	}

}
