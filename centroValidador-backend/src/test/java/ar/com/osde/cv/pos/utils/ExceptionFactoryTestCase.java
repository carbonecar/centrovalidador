package ar.com.osde.cv.pos.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;
import junit.framework.TestCase;
import ar.com.osde.centroValidador.pos.exception.TimeOutForzadoRulesException;
import ar.com.osde.entities.aptitud.AtributoInconsistencia;
import ar.com.osde.entities.aptitud.Inconsistencia;
import ar.com.osde.entities.inconsistencias.AtrDetallesLst;

public class ExceptionFactoryTestCase extends TestCase {

    public void testCreateTimeOutExceptionByErrorAptitudCode() {
        
        Inconsistencia inc=new Inconsistencia();
        inc.setCodigo(9999);
        AtrDetallesLst atrDetalleLst=new AtrDetallesLst();
        List<String>  detalles=new ArrayList<String>();
        detalles.add("detalle1");
        atrDetalleLst.setDet(detalles);
        
        Set<AtributoInconsistencia> atributosInconsistencias=new HashSet<AtributoInconsistencia>();
        atributosInconsistencias.add(atrDetalleLst);
        inc.setAtributos(atributosInconsistencias);
        
        
        
        
       TimeOutForzadoRulesException excepcion= (TimeOutForzadoRulesException) ExceptionFactory.createTimeOutExceptionByErrorAptitudCode(inc, null);
       
       Assert.assertEquals(1, excepcion.getServiciosFallidos().size());
       
        
       String servicioFallido=excepcion.getServiciosFallidos().get(0);
       Assert.assertEquals("detalle1",servicioFallido);
        
    }

}
