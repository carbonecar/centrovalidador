package ar.com.osde.cv.factory;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ar.com.osde.centroValidador.pos.handlers.FilteredMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.NullMessageHandler;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

/**
 * 
 * @author MT27789605
 *
 */
public class TestHandlerFactory {

    @Test
    public void testCreateNullHandler() {

        PosMessageHandlerConfiguration configuration = new PosMessageHandlerConfiguration();
        configuration.setClassName(NullMessageHandler.class.getName());

        try {
            NullMessageHandler nullHandler = (NullMessageHandler) new HandlerFactory().createHandler(configuration);
            Assert.assertNotNull(nullHandler);
        } catch (BusinessException e) {
            Assert.fail();
        }
    }

    @Test
    public void testCreateFilterMessageHandler() {

        FilteredMessageHandlerConfiguration configuration = new FilteredMessageHandlerConfiguration();
        configuration.setClassName(FilteredMessageHandler.class.getName());

        List<FilterStrategyConfiguration> strategyList = new LinkedList<FilterStrategyConfiguration>();

        PosMessageHandlerConfiguration nullConfig = new PosMessageHandlerConfiguration();
        nullConfig.setClassName(NullMessageHandler.class.getName());

        ProcessFilterStrategyConfiguration filterStrategyConfiguration = new ProcessFilterStrategyConfiguration();
        filterStrategyConfiguration.setPosMessageHandler(nullConfig);

        strategyList.add(filterStrategyConfiguration);
        strategyList.add(new FilterStrategyConfiguration());

        configuration.setFilterStrategyList(strategyList);

        try {
            FilteredMessageHandler filterHandler = (FilteredMessageHandler) new HandlerFactory().createHandler(configuration);
            Assert.assertNotNull(filterHandler);
            Assert.assertNotNull(filterHandler.getFormatManager());
            Assert.assertEquals(2, filterHandler.getFilterStrategyList().size());
            Assert.assertEquals(0, filterHandler.getFilters().size());

        } catch (BusinessException e) {
            Assert.fail();
        }
    }
    
	@Test
	public void testFixedFormadMessageHandler() throws BusinessException {
		PosMessageHandlerConfiguration configuration = new PosMessageHandlerConfiguration();
		configuration.setClassName(FixedFormatMessageHandler.class.getName());

		FixedFormatMessageHandler fixedFormat = (FixedFormatMessageHandler) new HandlerFactory().createHandler(
		        configuration);
		;

		Assert.assertNotNull(fixedFormat);
	}
    

    }
