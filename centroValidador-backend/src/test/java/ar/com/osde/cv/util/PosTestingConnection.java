package ar.com.osde.cv.util;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;

public class PosTestingConnection implements MessageListener {
    private static final Log LOG=LogFactory.getLog(PosTestingConnection.class);
	private JmsTemplate templateResponse;
	public PosTestingConnection(){
	    
	}
	public JmsTemplate getTemplateResponse() {
		return templateResponse;
	}

	public void setTemplateResponse(JmsTemplate templateResponse) {
		this.templateResponse = templateResponse;
	}

	public void onMessage(Message paramMessage) {
		try {

		    LOG.debug(((TextMessage) paramMessage).getText());
		} catch (JMSException e) {
		    LOG.debug(e);
		}
	}

}
