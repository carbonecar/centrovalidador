package ar.com.osde.cv.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.filefilter.AndFileFilter;

import ar.com.osde.centroValidador.pos.filter.AndFilter;
import ar.com.osde.centroValidador.pos.filter.NullFilter;
import ar.com.osde.centroValidador.pos.handlers.FilteredMessageHandler;
import ar.com.osde.centroValidador.pos.handlers.NullMessageHandler;
import ar.com.osde.entities.AbstractResponseMessageHandlerConfiguration;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.ErrorHandlerConfiguration;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;

public class DummyContainerFactory {

    public static ContainerConfiguration createSimpleContainer(String errorQueueName, String containerQueue,
                                                               String handlerQueue) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueueName);

        AbstractResponseMessageHandlerConfiguration handlerConfiguration = new AbstractResponseMessageHandlerConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.AlwaysRouteMessageHandler");
        handlerConfiguration.setQueueName(handlerQueue);

        PosMessageHandlerConfiguration nullMessageHandlerConfiguration = new PosMessageHandlerConfiguration();
        nullMessageHandlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.NullMessageHandler");

        handlerConfiguration.setPosMessageHandler(nullMessageHandlerConfiguration);

        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(handlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }

    public static ContainerConfiguration createContainerWithFilter(String errorQueueName, String containerQueue,
                                                                   String handlerQueue) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueueName);

        FilteredMessageHandlerConfiguration handlerConfiguration = new FilteredMessageHandlerConfiguration();
        handlerConfiguration.setClassName(FilteredMessageHandler.class.getName());

        PosMessageHandlerConfiguration nullHandler = new PosMessageHandlerConfiguration();
        nullHandler.setClassName(NullMessageHandler.class.getName());

        List<FilterStrategyConfiguration> filterStrategyList = new LinkedList<FilterStrategyConfiguration>();
        filterStrategyList.add(new FilterStrategyConfiguration());

        List<FilterConfiguration> filters = new LinkedList<FilterConfiguration>();
        FilterConfiguration nullFilter = new FilterConfiguration();
        nullFilter.setClassName(NullFilter.class.getName());
        filters.add(nullFilter);

        handlerConfiguration.setFilterStrategyList(filterStrategyList);
        handlerConfiguration.setFilters(filters);
        handlerConfiguration.setPosMessageHandler(nullHandler);

        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(handlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }

    public static ContainerConfiguration createContainerWithCompositeFilter(String errorQueueName, String containerQueue,
                                                                            String handlerQueue) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueueName);

        FilteredMessageHandlerConfiguration handlerConfiguration = new FilteredMessageHandlerConfiguration();
        handlerConfiguration.setClassName(FilteredMessageHandler.class.getName());

        PosMessageHandlerConfiguration nullHandler = new PosMessageHandlerConfiguration();
        nullHandler.setClassName(NullMessageHandler.class.getName());

        List<FilterStrategyConfiguration> filterStrategyList = new ArrayList<FilterStrategyConfiguration>();
        filterStrategyList.add(new FilterStrategyConfiguration());

        List<FilterConfiguration> filters = new ArrayList<FilterConfiguration>();
        CompositeFilterConfiguration compositeFilter = new CompositeFilterConfiguration();
        compositeFilter.setClassName(AndFileFilter.class.getName());
        filters.add(compositeFilter);

        handlerConfiguration.setFilterStrategyList(filterStrategyList);
        handlerConfiguration.setFilters(filters);
        handlerConfiguration.setPosMessageHandler(nullHandler);

        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(handlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }


    public static ContainerConfiguration createContainerWithNotAndCompositeFilter(String errorQueueName, String containerQueue,
                                                                                  String handlerQueue) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueueName);

        FilteredMessageHandlerConfiguration filteredHandlerConfiguration = new FilteredMessageHandlerConfiguration();
        filteredHandlerConfiguration.setClassName(FilteredMessageHandler.class.getName());

        NotFilterConfiguration notFilterConfig = new NotFilterConfiguration();
        notFilterConfig.setClassName(NotFilterConfiguration.class.getName());

        List<FilterStrategyConfiguration> filterStrategyList = new ArrayList<FilterStrategyConfiguration>();
        filterStrategyList.add(new FilterStrategyConfiguration());

        List<FilterConfiguration> filters = new ArrayList<FilterConfiguration>();
        CompositeFilterConfiguration andFilter = new CompositeFilterConfiguration();
        andFilter.setClassName(AndFilter.class.getName());
        List<FilterConfiguration> filterConfigurationForComposite = new ArrayList<FilterConfiguration>();
        PropertyEqualFilterConfiguration atributoCodigoTransaccionProperty = new PropertyEqualFilterConfiguration();
        atributoCodigoTransaccionProperty.setPropertyName("getCodigoTransaccion");
        atributoCodigoTransaccionProperty.setType("String");
        atributoCodigoTransaccionProperty.setValue("A");
        atributoCodigoTransaccionProperty.setClassName(PropertyEqualFilterConfiguration.class.getName());
        filterConfigurationForComposite.add(atributoCodigoTransaccionProperty);
        andFilter.setFilterConfigurationList(filterConfigurationForComposite);
        notFilterConfig.setFilterConfiguration(andFilter);
        filters.add(notFilterConfig);

        filteredHandlerConfiguration.setFilterStrategyList(filterStrategyList);
        filteredHandlerConfiguration.setFilters(filters);


        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(filteredHandlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }


    public static ContainerConfiguration createContainerNotFilter(String errorQueueName, String containerQueue,
                                                                  String handlerQueue) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueueName);

        FilteredMessageHandlerConfiguration filteredHandlerConfiguration = new FilteredMessageHandlerConfiguration();
        filteredHandlerConfiguration.setClassName(FilteredMessageHandler.class.getName());

        NotFilterConfiguration notFilterConfig = new NotFilterConfiguration();
        notFilterConfig.setClassName(NotFilterConfiguration.class.getName());

        List<FilterStrategyConfiguration> filterStrategyList = new ArrayList<FilterStrategyConfiguration>();
        filterStrategyList.add(new FilterStrategyConfiguration());

        List<FilterConfiguration> filters = new ArrayList<FilterConfiguration>();
        PropertyEqualFilterConfiguration atributoCodigoTransaccionProperty = new PropertyEqualFilterConfiguration();
        atributoCodigoTransaccionProperty.setClassName(PropertyEqualFilterConfiguration.class.getName());
        atributoCodigoTransaccionProperty.setPropertyName("getAtributoCodigoTransaccionProperty");
        atributoCodigoTransaccionProperty.setType("String");
        atributoCodigoTransaccionProperty.setValue("A");
        notFilterConfig.setFilterConfiguration(atributoCodigoTransaccionProperty);
        filters.add(notFilterConfig);

        filteredHandlerConfiguration.setFilterStrategyList(filterStrategyList);
        filteredHandlerConfiguration.setFilters(filters);

        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(filteredHandlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }

    public static ContainerConfiguration createContainerWithHandler(String errorQueue, String containerQueue, String handlerQueue, PosMessageHandlerConfiguration handlerConfiguration) {

        ErrorHandlerConfiguration errorHandler = new ErrorHandlerConfiguration();
        errorHandler.setQueueName(errorQueue);
        ContainerConfiguration containerConfiguration = new ContainerConfiguration();
        containerConfiguration.setContainerDescription("testing");
        containerConfiguration.setPosMessageHandler(handlerConfiguration);
        containerConfiguration.setErrorHandler(errorHandler);
        containerConfiguration.setQueueName(containerQueue);
        containerConfiguration.setNumberOfConcurrentConsumers(4);
        containerConfiguration.setGroupName("Sin Grupo");

        return containerConfiguration;
    }
}
