package ar.com.osde.cv.factory;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ar.com.osde.centroValidador.factory.FilterFactory;
import ar.com.osde.centroValidador.pos.filter.AndFilter;
import ar.com.osde.centroValidador.pos.filter.CompositeFilter;
import ar.com.osde.centroValidador.pos.filter.NotFilter;
import ar.com.osde.centroValidador.pos.filter.NullFilter;
import ar.com.osde.centroValidador.pos.filter.OrFilter;
import ar.com.osde.centroValidador.pos.filter.PropertyEqualFilter;
import ar.com.osde.entities.filter.CompositeFilterConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.NotFilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;

public class TestFilterFactory {

	@Test
    public void testCreateSimpleFilter() {

        FilterConfiguration filterConfiguration = new FilterConfiguration();
        filterConfiguration.setClassName(NullFilter.class.getName());

        try {
            NullFilter nullFilter = (NullFilter) FilterFactory.createFilter(filterConfiguration);
            Assert.assertNotNull(nullFilter);
        } catch (BusinessException e) {
            Assert.fail();
        }

        PropertyEqualFilterConfiguration propertyFilterConfiguration = new PropertyEqualFilterConfiguration();
        propertyFilterConfiguration.setClassName(PropertyEqualFilter.class.getName());
        propertyFilterConfiguration.setPropertyName("example");
        propertyFilterConfiguration.setType("Integer");
        propertyFilterConfiguration.setValue("5");

        try {
            PropertyEqualFilter filter = (PropertyEqualFilter) FilterFactory.createFilter(propertyFilterConfiguration);
            Assert.assertNotNull(filter);
            Assert.assertEquals(5, filter.getValue());
        } catch (BusinessException e) {
            Assert.fail();
        }
    }

    
    @Test
    public void testCreateCompositeFilter() {

        CompositeFilterConfiguration filterConfiguration = new CompositeFilterConfiguration();
        filterConfiguration.setClassName(AndFilter.class.getName());

        List<FilterConfiguration> filterList = new LinkedList<FilterConfiguration>();

        NotFilterConfiguration notFilter = new NotFilterConfiguration();
        notFilter.setClassName(NotFilter.class.getName());

        FilterConfiguration nullConfig = new FilterConfiguration();
        nullConfig.setClassName(NullFilter.class.getName());

        notFilter.setFilterConfiguration(nullConfig);

        filterList.add(notFilter);
        filterConfiguration.setFilterConfigurationList(filterList);
        
        try {
            CompositeFilter andFilter = (AndFilter) FilterFactory.createFilter(filterConfiguration);
            Assert.assertNotNull(andFilter);
            
            filterConfiguration.setClassName(OrFilter.class.getName());
            CompositeFilter orFilter = (OrFilter) FilterFactory.createFilter(filterConfiguration);
            
            Assert.assertNotNull(orFilter);
            Assert.assertEquals(1, orFilter.getFiltros().size());
            
        } catch (BusinessException e) {
            Assert.fail();
        }
    }

}
