package ar.com.osde.cv.pos.utils;

import java.io.Serializable;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;


public class SessionAdapter implements Session {

    public BytesMessage createBytesMessage() throws JMSException {
        return null;
    }

    public MapMessage createMapMessage() throws JMSException {
        return null;
    }

    public Message createMessage() throws JMSException {
        return null;
    }

    public ObjectMessage createObjectMessage() throws JMSException {
        return null;
    }

    public ObjectMessage createObjectMessage(Serializable paramSerializable) throws JMSException {
        return null;
    }

    public StreamMessage createStreamMessage() throws JMSException {
        return null;
    }

    public TextMessage createTextMessage() throws JMSException {
        return new TextMessageAdapter();
    }

    public TextMessage createTextMessage(String paramString) throws JMSException {
        return null;
    }

    public boolean getTransacted() throws JMSException {
        return false;
    }

    public int getAcknowledgeMode() throws JMSException {
        return 0;
    }

    public void commit() throws JMSException {
    }

    public void rollback() throws JMSException {

    }

    public void close() throws JMSException {

    }

    public void recover() throws JMSException {
    }

    public MessageListener getMessageListener() throws JMSException {
        return null;
    }

    public void setMessageListener(MessageListener paramMessageListener) throws JMSException {
    }

    public void run() {
    }

    public MessageProducer createProducer(Destination paramDestination) throws JMSException {
        return null;
    }

    public MessageConsumer createConsumer(Destination paramDestination) throws JMSException {
        return null;
    }

    public MessageConsumer createConsumer(Destination paramDestination, String paramString) throws JMSException {
        return null;
    }

    public MessageConsumer createConsumer(Destination paramDestination, String paramString, boolean paramBoolean)
            throws JMSException {
        return null;
    }

    public Queue createQueue(String paramString) throws JMSException {
        return null;
    }

    public Topic createTopic(String paramString) throws JMSException {
        return null;
    }

    public TopicSubscriber createDurableSubscriber(Topic paramTopic, String paramString) throws JMSException {
        return null;
    }

    public TopicSubscriber createDurableSubscriber(Topic paramTopic, String paramString1, String paramString2,
            boolean paramBoolean) throws JMSException {
        return null;
    }

    public QueueBrowser createBrowser(Queue paramQueue) throws JMSException {
        return null;
    }

    public QueueBrowser createBrowser(Queue paramQueue, String paramString) throws JMSException {
        return null;
    }

    public TemporaryQueue createTemporaryQueue() throws JMSException {
        return null;
    }

    public TemporaryTopic createTemporaryTopic() throws JMSException {
        return null;
    }

    public void unsubscribe(String paramString) throws JMSException {
    }

}
