package ar.com.osde.cv.factory;

import junit.framework.Assert;
import junit.framework.TestCase;
import ar.com.osde.cv.model.TransaccionFactory;
import ar.com.osde.entities.transaccion.TipoTransaccion;
import ar.com.osde.entities.transaccion.Transaccion;
import ar.com.osde.entities.transaccion.TransaccionConsumo;

public class TestTransaccionFactory extends TestCase {

    public void testGeneraAutorizacion() {
        Transaccion transaccion = new TransaccionConsumo();

        transaccion.setTipoTransaccion(TipoTransaccion.O2B);
        Assert.assertEquals(true, TransaccionFactory.getInstance().generaAutorizacion(transaccion));

        transaccion.setTipoTransaccion(TipoTransaccion.O2C);
        Assert.assertEquals(true, TransaccionFactory.getInstance().generaAutorizacion(transaccion));

        transaccion.setTipoTransaccion(TipoTransaccion.O2S);
        Assert.assertEquals(true, TransaccionFactory.getInstance().generaAutorizacion(transaccion));

        transaccion.setTipoTransaccion(TipoTransaccion.O2A);
        Assert.assertEquals(false, TransaccionFactory.getInstance().generaAutorizacion(transaccion));
    }

}
