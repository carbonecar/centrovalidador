package ar.com.osde.cv.pos.utils;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate102;
import org.springframework.jms.core.MessageCreator;


/**
 * Clase que en lugar de enviar la respuesta la colecta. Solamente para test.
 * 
 * @author VA27789605
 * 
 */
public class JmsCollectorTemplate extends JmsTemplate102 {

	private Message messageToSend;

	@Override
	public void send(MessageCreator messageCreator) throws JmsException {

		try {
			this.messageToSend = messageCreator
					.createMessage(new SessionAdapter() {
						@Override
						public TextMessage createTextMessage(String message)
								throws JMSException {

							return new TextMessageAdapter();
						}
					});
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public Message getMessageToSend() {
		return this.messageToSend;
	}
}
