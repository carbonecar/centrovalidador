package ar.com.osde.cv.pos.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.osde.centroValidador.pos.AbstractPosMessage;
import ar.com.osde.centroValidador.pos.PosMessage;
import ar.com.osde.framework.persistence.dao.crud.GenericCRUDDao;
import ar.com.osde.framework.persistence.dao.filter.Filter;
import ar.com.osde.framework.persistence.dao.filter.Page;
import ar.com.osde.framework.persistence.dao.filter.Result;

/**
 * Clase para poder hacer el testing unitario de mensajeria multiple.
 * 
 * @author VA27789605
 * 
 */
public class PosMessageHashMapCrudImpl implements GenericCRUDDao<AbstractPosMessage> /*extends GenericCRUDDaoINFImpl<PosMessage>*/ {

    private Map<Long, AbstractPosMessage> mensajes = new HashMap<Long, AbstractPosMessage>();

    public int count(Filter arg0) {
        return 0;
    }

    public void delete(AbstractPosMessage message) {
        mensajes.remove(message.getId());
    }

    public List<AbstractPosMessage> getAll() {
        List<AbstractPosMessage> list = new ArrayList<AbstractPosMessage>();
        list.addAll(mensajes.values());
        return list;
    }

    public Result<AbstractPosMessage> getAll(Page arg0) {
        return null;
    }

    public Result<AbstractPosMessage> getAll(Filter arg0, Page arg1) {
        return null;
    }

    public AbstractPosMessage getById(Serializable id) {
        return this.mensajes.get(id);
    }

    public PosMessage saveNew(AbstractPosMessage arg0) {
        return null;
    }

    public AbstractPosMessage saveOrUpdate(AbstractPosMessage message) {
        return this.mensajes.put(message.getId(), message);
    }

    public long getLifeValue() {
        return Long.MAX_VALUE;
    }
}
