package ar.com.osde.comunes.utils;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import ar.com.osde.entities.aptitud.Inconsistencia;
import ar.com.osde.entities.aptoServicio.ItemConsumoRespuesta;
import ar.com.osde.entities.aptoServicio.MedicamentoRespuesta;

public class ItemConsumoUtilsTestCase extends TestCase {

    public void testItemNOAprobado() {

        ItemConsumoRespuesta itemConsumo = new MedicamentoRespuesta();

        Inconsistencia inc1 = new Inconsistencia();
        inc1.setExceptuada(false);
        List<Inconsistencia> inconsistencias = new ArrayList<Inconsistencia>();
        inconsistencias.add(inc1);
        itemConsumo.setInconsistencias(inconsistencias);

        assertFalse(ItemConsumoUtils.isApproved(itemConsumo));

    }

    public void testItemAprobado() {

        ItemConsumoRespuesta itemConsumo = new MedicamentoRespuesta();

        Inconsistencia inc1 = new Inconsistencia();
        inc1.setExceptuada(true);
        List<Inconsistencia> inconsistencias = new ArrayList<Inconsistencia>();
        inconsistencias.add(inc1);
        itemConsumo.setInconsistencias(inconsistencias);

        assertTrue(ItemConsumoUtils.isApproved(itemConsumo));

    }

    public void testItemNOAprobadoMoreThanOneInc() {

        ItemConsumoRespuesta itemConsumo = new MedicamentoRespuesta();

        Inconsistencia inc1 = new Inconsistencia();
        inc1.setExceptuada(false);

        Inconsistencia inc2 = new Inconsistencia();
        inc2.setExceptuada(true);
        List<Inconsistencia> inconsistencias = new ArrayList<Inconsistencia>();
        inconsistencias.add(inc2);
        inconsistencias.add(inc1);
        itemConsumo.setInconsistencias(inconsistencias);

        assertFalse(ItemConsumoUtils.isApproved(itemConsumo));

    }

    public void testItemAprobadoMoreThanOneInc() {

        ItemConsumoRespuesta itemConsumo = new MedicamentoRespuesta();

        Inconsistencia inc1 = new Inconsistencia();
        inc1.setExceptuada(true);

        Inconsistencia inc2 = new Inconsistencia();
        inc2.setExceptuada(true);

        List<Inconsistencia> inconsistencias = new ArrayList<Inconsistencia>();
        inconsistencias.add(inc1);
        inconsistencias.add(inc2);
        itemConsumo.setInconsistencias(inconsistencias);

        assertTrue(ItemConsumoUtils.isApproved(itemConsumo));

    }
}
