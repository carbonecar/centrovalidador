<%@ include file="../template/includes/taglibs.jsp" %>

<div id="ajxDspId_Transacciones_${uid}">
    <script type="text/javascript">
    	$(document).ready(function() {

    	var objetoRetorno_${uid} = $("#ajxDspId_Transacciones_${uid}").gridSelection();
    	arraySelected_${uid} = objetoRetorno_${uid}.items;
    	checkAll_${uid} = objetoRetorno_${uid}.checkAll;

    	$("#resultadosItems_${uid}").html("");
    });
    </script>
    <table class="toolBar">
        <tr>          
             <td width="20" id="detalle_transac" class="botonGrilla" data="{multiple:'false'}" style="display: none;">
	                <a href="javascript:detalleTransaccion_${uid}();void(0)"><img id="imgDetalle_${uid}" src="${staticPath}images/iconos/iconos_on/detalle-on.gif"  border="0" title="Ver Items Temporales"  width="16" height="16"/></a>
	            </td>
	         <td width="20" id="disabled_detalle_transac">
	                <a href="#"><img src="${staticPath}images/iconos/iconos_off/detalle-off.gif"  border="0" title="Ver Items Temporales"  width="16" height="16"/></a>
	         </td>
        </tr>
    </table>
<!-- 
     -->
  <!--
 
    <display:table uid="transaccionDTO" name="${widgetContext.searchResult}" id="transaccionDTO" requestURI="${appCtx}/consultas/cabeceraTransacciones!search.do" 
            size="${widgetContext.totalResults}"    pagesize="${widgetContext.pageSize}" partialList="true" sort="external" defaultorder="ascending" class="datos" >
        
      <display:column headerClass="primero" style="width:1%">
            <input type="radio" id="seleccion" name="seleccion" value="${transaccionDTO.id}" >
        </display:column>
        <display:column title="Prestador" style="width:10%;text-align:center" sortable="true" property="prestador" sortName="NRO_RELACION" />
        <display:column title="Filial" style="width:10%" sortable="true" property="filial" sortName="FILIAL" />
        <display:column title="Nro de Transaccion" style="width:10%" sortable="true" property="nroTransaccion" sortName="NRO_TRANSACCION" />
        <display:column title="Fecha" style="width:10%" sortable="true" property="fecha" sortName="FECHA" />
        <display:column title="Nro Pos" style="width:10%" sortable="true" property="nroPos" sortName="NRO_POS" />
        <display:column title="Cap" style="width:10%" sortable="true" property="cap" sortName="CAP" />
        <display:column title="Operador" style="width:10%" sortable="true" property="operador" sortName="OPERADOR" />
        <display:column title="Estado Proceso" style="width:10%" sortable="true" property="estadoProceso" sortName="ESTADO_PROCESO" />

        <display:column headerClass="ultimo" style="width:1%"/>
    </display:table>
    -->
	<br/>
</div>
