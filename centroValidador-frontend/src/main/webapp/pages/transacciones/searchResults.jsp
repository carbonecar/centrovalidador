<%@ include file="../template/includes/taglibs.jsp" %>

<form id="resultsForm_${uid}">
	<s:hidden name="uid" value="%{uid}"/>
	<s:hidden name="selectedID" id="selectedID_%{uid}" value=""/>
</form>

<div class="bloque">
   <div class="top"><div>&nbsp;</div></div>
   <div class="cuerpo">
     <%@ include file="resultPage.jsp" %>
     <br/>
   </div>
   <div class="pie"><div></div></div>
</div>


