<%@ include file="../template/includes/taglibs.jsp" %>
<script type="text/javascript"><!--


    // Asigno funciones a los eventos ---
    $(function() {

    	$("#fechaSearch_${uid}").datepicker(datepickerDefaults);
        $("#fechaSearch_${uid}").mask("99/99/9999",{placeholder:" "});
        
        //Buscar ------------------------------------
                 
        $("#btnBuscar_${uid}").click( function( evnt ){
            evnt.preventDefault();
    
            // Submit del form por AJAX -
            searchFormOptions = {                   
                cache: false,
                target:        '#resultados_${uid}',
                beforeSubmit:   validaSearchForm_${uid},
                success:        function(){
                                    toggle_${uid}();
                                    updateDisplayTagLinks("ajxDspId_Transacciones_${uid}");           
                                }
            }           
            $('#searchForm_${uid}').ajaxForm(searchFormOptions);
            $('#searchForm_${uid}').submit();
        });


    });

    function validaSearchForm_${uid}(){
        	
			var prestador = $("#prestadorSearch_${uid}");
			var filial = $("#filialSearch_${uid}");
			var trx = $("#transaccionSearch_${uid}");
			var fecha = $("#fechaSearch_${uid}");

			prestador.removeClass('ui-state-error');
			filial.removeClass('ui-state-error');
			trx.removeClass('ui-state-error');
			fecha.removeClass('ui-state-error');
		
			
		
			
			if(jQuery.trim(trx.val()) && !checkNumero_${uid}(trx.val()) ){
				trx.addClass('ui-state-error');
				return false;
			}
			if( jQuery.trim(fecha.val())!= "" && !testValidDate(fecha.val()) ){
				fecha.addClass('ui-state-error');
				showAlertMsg('<s:text name="Fecha invalida"><s:param>' + "Fecha" + '</s:param></s:text>');
				return false;
			}

			return true;
	 }
	 
    function checkNumero_${uid}(codigo){
        var rta = true;
        if (!isInteger(jQuery.trim(codigo))) {
            showAlertMsg('<s:text name="Debe ingresar un numero"><s:param>' + "Prestador" + '</s:param></s:text>');
            rta = false;
        }
        return rta;
    }
	// Minimiza / Maximiza bloque del buscador
	function toggle_${uid}() {

	    if ($("#bloqueOcultable_${uid}").is(":hidden")) {
	        $("#bloqueOcultable_${uid}").slideDown();
	        $("#buttonHide_${uid}").attr("src","${staticPath}images/bot_minimizar.gif");
	        $("#tdBotonOculto_${uid}").hide();
	    } else {
	        $("#bloqueOcultable_${uid}").slideUp();
	        $("#buttonHide_${uid}").attr("src","${staticPath}images/bot_maximizar.gif");
	        $("#tdBotonOculto_${uid}").show();
	    }         
	}

--></script>

    <div class="bloque">
	   <h3><span>B�squeda de transacciones</span></h3>
	   <img src="${staticPath}images/bot_minimizar.gif" title="Minimizar / Restaurar" id="buttonHide_${uid}" class="iconos" onclick="toggle_${uid}()"/>
		
		
	   <form action="<s:url namespace="/consultas" action="searchTransacciones.do"/>" id="searchForm_${uid}">
		  <s:hidden name="uid" value="%{uid}"/>


        <div class="cuerpo" id="bloqueOcultable_${uid}" >

   		<table border="0" cellpadding="0" cellspacing="0" width="100%">
   		
   								<tr>
									<th width="22%"></th>
									<th width="29%"></th>
									<th width="16%"></th>
									<th width="20%"></th>
									<th width="13%"></th>
								</tr>
			<tr>
			
			<!--  Columna IZQUIERDA -->
				<td colspan="2">
				<table border="0" cellpadding="0" cellspacing="0" class="formulario" >
					<tr>
						<th width="50%"></th>
						<th width="50%"></th>

					</tr>

					<tr>
						<td class="label"><s:label value="Operador: " theme="simple" />
						</td>
						<td colspan="2"><s:textfield id="prestadorSearch_%{uid}"
							name="searchCriteria.transaccionBid.codigoOperadorTransaccion" theme="simple" cssClass="text ancho2" maxlength="2" />
						</td>
						
					</tr>					
				</table>
				</td>
				
			<!--  Columna DERECHA -->
				<td colspan="2">
				<table border="0" cellpadding="0" cellspacing="0" class="formulario" width="100%">
					<tr>
						<th width="50%"></th>
						<th width="50%"></th>

					</tr>
					<tr>
						<td class="label"><s:label value="Nro Transacci�n: "
							theme="simple" /></td>
						<td colspan="2"><s:textfield id="transaccionSearch_%{uid}"
							name="searchCriteria.transaccionBid.numeroTransaccion" theme="simple" cssClass="text ancho200" maxlength="25" />
						</td>
						
					</tr>
					<tr>
						<td class="label"><s:label value="Fecha: "
							theme="simple" /></td>
						<td colspan="2"><s:textfield id="fechaSearch_%{uid}"
							name="searchCriteria.transaccionBid.fechaSolicitud" theme="simple" cssClass="text fecha" maxlength="25" />
						</td>
						
					</tr>
					<tr>
						<td class="label"><s:label value="Estado: " theme="simple" />
						</td>
						<td colspan="2">
						<s:select id="estadoSelect_%{uid}" name="searchCriteria.estadoTransaccion" theme="simple" cssClass="ancho200" list="estadosTransaccion" listKey="name" listValue="descripcion" headerKey="-1" headerValue="Seleccione">
				        </s:select>
						</td>				
					</tr>
				</table>
				</td>
				<td>
				</td>
			</tr>
			<tr>		
				<td colspan="4" class="botones">
				<br/>
				<br/>	
				<s:submit type="button" id="btnBuscar_%{uid}" value="Buscar" theme="simple"/>
				</td>
				<td></td>				
			</tr>
		</table>
        </div>
    </form>

    <div class="pie"><div></div></div>
    
</div>
<div id="resultados_${uid}"></div>
<div id="resultadosItems_${uid}"></div>
