<%@ include file="/pages/template/includes/taglibs.jsp" %>
<ul class="base" >
	
	<li class="top"><a href="javascript:subMenu(1);void(0);"><s:text name="menu.opt2"/></a>
    	<ul id="sub_1">
 		<s:if test="%{#session.pageCode == 1}">
    		<li class="activo"><a href="${appCtx}/queue/list.do"><s:text name="menu.opt2.1"/></a></li>
    	</s:if>
    	<s:else>
    		<li> <a href="${appCtx}/queue/list.do"><s:text name="menu.opt2.1"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 2}">
    		<li class="activo"><a href="${appCtx}/container/list.do"><s:text name="menu.opt2.2"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/container/list.do"><s:text name="menu.opt2.2"/></a></li>
    	</s:else>
    	<%--  --%>
        </ul>
    </li>
	<li class="top"><a href="javascript:subMenu(2);void(0);"><s:text name="menu.opt3"/></a>
    	<ul id="sub_2">
 		<s:if test="%{#session.pageCode == 1}">
    		<li class="activo"><a href="${appCtx}/consultas/cabeceraTransacciones.do"><s:text name="menu.opt3.1"/></a></li>
    	</s:if>
    	<s:else>
    		<li> <a href="${appCtx}/consultas/cabeceraTransacciones.do"><s:text name="menu.opt3.1"/></a></li>
    	</s:else>
    	
        </ul>
    </li>
    <li class="ultimo"></li>
</ul>

