<%@ include file="../../template/includes/taglibs.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {

		// Datepicker
		$("#formDatepicker").datepicker(datepickerDefaults);

		$("#btnModificar").click(function(event) {
			void (0);
			event.preventDefault();
		});

	});
</script>

<form action="<s:url namespace="/container" action="stopContainer"/>"
	id="stop.container.form">
	<table class="formulario">
		<tr>
			<s:textfield name="idContainer" label="Id container a detener"  cssClass="text ancho200"/>
		</tr>
		<tr>
			<td colspan="4" class="botones"><s:submit type="button"
					id="stop.container.button" value="Detener container" />
			</td>
		</tr>
	</table>
</form>