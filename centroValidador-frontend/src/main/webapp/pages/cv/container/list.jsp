<%@ include file="../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Listado de Containers</title>
    <meta name="pageCode" content="7"/>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <script>
        $(document).ready(function () {

            // Submit del form por AJAX -
            formOptions = {
                target: '#formBlock',
                success: changeLinks
            };
            $('#myForm').ajaxForm(formOptions); // "Ajaxifico" el form

        });


        $(document).ready(function () {

            $('#ejemplo').toggle(function () {

                $(this).animate({left: '200px'}, 300)

            }, function () {

                $(this).animate({left: '0px'}, 300)

            });

        });

        //Como la pagina se recarga entera solamente 2 (uno para el link activar y otro desactivar)
        //es suficiente, pero para dejarlo listo para cuando la pagina no se recargue entera pongo los 4
        $(document).ready(function () {

            $('a[id^="link_desactivar_false"]').hide();
        });

        $(document).ready(function () {
            $('a[id^="link_desactivar_true"]').show();
        });

        $(document).ready(function () {
            $('a[id^="link_activar_false"]').show();
        });

        $(document).ready(function () {
            $('a[id^="link_activar_true"]').hide();
        });

        $(document).ready(function () {

            $('a[id^="link_eliminar#"]').click(function () {
                var url = "${appCtx}/container/removeContainer.do?idContainer=";
                var id = this.id;
                id = id.substring(id.indexOf('#') + 1, id.length);
                var fullUrl = url + id;
                showConfirmMsg('Seguro que desea ELIMINAR el container.<br>', function () {
                    $(location).attr('href', fullUrl);
                });
            });
        });

        //Para expansion de la informacion del container.
        $(document).ready(function () {

            $(".clickable").click(function () {
                toggleRaw($(this), "expandable", "flecha", "${appCtx}/images/iconos/");
            });

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
            });

            ultimaPalabraPorPuntos("className");

            $(".stopPropagation").click(function (event) {
                event.stopPropagation();
            });
        });

        //Fin Para expansion de la informacion del container
    </script>

    <style>
        .expandable {
            display: none;
        }

        .containerInfo {
            font-weight: bold;
        }
    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Containers en el Sistema:</span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
        <table class="displayTagColumn">
            <s:iterator value="containerConfigurationMapSet" var="key">
                <tr>
                    <td style="width: 100%">
                        <div style="float: left; margin-right: 10px;"><b>Nombre del Grupo: </b><s:property value="key"/></div>

                        <s:url id="startGroup" action="startGroup">
                            <s:param name="groupName" value="%{#key}"/>
                        </s:url>
                        <a class="stopPropagation" title="Start Group"
                           href="<s:property value="#startGroup" />"><img
                                src="<s:url value="/images/iconos/activar.gif"/>"/></a>

                        <s:url id="stopGroup" action="stopGroup">
                            <s:param name="groupName" value="%{#key}"/>
                        </s:url>
                        <a class="stopPropagation" title="Stop Group"
                           href="<s:property value="#stopGroup" />"><img
                                src="<s:url value="/images/iconos/action_stop.gif"/>"/></a>

                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <table class="displayTagColumn">
                            <tr>
                                <th style="width: 3%"></th>
                                <th style="width: 8%">id</th>
                                <th style="width: 15%">Descripcion</th>
                                <th style="width: 23%">Nombre de la cola inicial de lectura</th>
                                <th style="width: 23%">Nombre de la cola de error</th>
                                <th style="width: 9%">Concurrencia</th>
                                <th style="width: 10%">Incia al levantar</th>
                                <%-- <sec:authorize ifAllGranted="OSGWEBCEN-CON-ABM"> --%>
                                    <th style="width: 9%">Acciones
                                    </th>
                                <%-- </sec:authorize> --%>

                            </tr>
                            <s:iterator value="containerConfigurationMap.get(#key)">

                                <tr class="rawHover clickable">
                                    <td align="left"><img open="false" class="flecha"
                                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                                    </td>
                                    <td class="rawHoverTd" align="center"><s:property value="id"/></td>
                                    <td class="rawHoverTd" align="center"><s:property
                                            value="containerDescription"/></td>
                                    <td class="rawHoverTd" align="center"><s:property value="queueName"/></td>
                                    <td class="rawHoverTd" align="center">
                                        <s:property value="errorHandler.queueName"/>
                                        <%-- <sec:authorize ifAllGranted="OSGWEBCEN-CON-ABM"> --%>
                                            <s:url id="showMessagesErrorHandlerQueue"
                                                   action="showMessagesErrorHandlerQueue"
                                                   namespace="/queue">
                                                <s:param name="idContainer" value="%{id}"/>
                                            </s:url>
                                            <a class="stopPropagation" title="Navegar"
                                               href="<s:property value="#showMessagesErrorHandlerQueue" />"><img
                                                    src="<s:url value="/images/iconos/lupaOn.gif"/>"/></a>
                                        <%-- </sec:authorize> --%>
                                    </td>
                                    <td class="rawHoverTd" align="center"><s:property
                                            value="numberOfConcurrentConsumers"/></td>
                                    <td class="rawHoverTd" align="center" id="<s:property value="id"/>"><s:property
                                            value="startOnLoad"/></td>
                                    <%-- <sec:authorize ifAllGranted="OSGWEBCEN-CON-ABM"> --%>
                                        <td class="rawHoverTd" align="center" id="<s:property value="id"/>">
                                            <a title="Detener"
                                               id="link_desactivar_<s:property value="running"/>_id_<s:property value="id"/>"
                                               href="${appCtx}/container/stopContainer.do?idContainer=<s:property value="id"/>"
                                               class="off stopPropagation"><img src="${staticPath}images/iconos/activar.gif"/>
                                            </a>
                                            <a title="Arrancar"
                                               id="link_activar_<s:property value="running"/>_id_<s:property value="id"/>"
                                               href="${appCtx}/container/startContainer.do?idContainer=<s:property value="id"/>"
                                               class="off stopPropagation"><img src="${staticPath}images/iconos/action_stoped.gif"/>
                                            </a>
                                            
                                            <a title="Eliminar" 
                                               id="link_eliminar#<s:property value="id"/>" href="#" class="off stopPropagation"><img src="${staticPath}images/iconos/eliminar.gif"/></a>
                                           
                                           
                                            <s:url id="cloneUrl" action="cloneContainer"> <s:param name="idContainer" value="%{id}"/> </s:url>
                                            <a class="stopPropagation" title="Clonar"
                                               href="<s:property value="#cloneUrl" />"><img
                                                    src="<s:url value="/images/iconos/copy.gif"/>"/></a>
                                            <s:url id="editContainer" action="editContainer">
                                                <s:param name="idContainer" value="%{id}"/>
                                            </s:url>
                                            <a class="stopPropagation" title="Editar"
                                               href="<s:property value="#editContainer" />"><img
                                                    src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
                                        </td>
                                    <%-- </sec:authorize> --%>
                                </tr>
                                <tr class="expandable">
                                    <td></td>
                                    <td colspan="6">
                                        <s:include value="containerInfo.jsp"/>
                                    </td>
                                </tr>
                            </s:iterator>
                        </table>
                    </td>
                </tr>
            </s:iterator>
        </table>
    </div>
</div>
<%-- <sec:authorize ifAllGranted="OSGWEBCEN-CON-ABM"> --%>
    <div class="bloque">
        <div class="cuerpo" id="newContainer">
            <%@ include file="newContainerForm.jsp" %>
        </div>
    </div>

    <div class="bloque">

        <div class="cuerpo" id="reloadContainer">
            <%@ include file="reloadContainerForm.jsp" %>
        </div>
        <div class="pie">
            <div></div>
        </div>
    </div>
<%-- </sec:authorize> --%> 

</body>
</html>