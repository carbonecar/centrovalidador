<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Container</title>
    <style>
        input[readonly] {
            background-color: gray;
        }

        .containerInfo {
            font-weight: bold;
        }
    </style>
    <script>
        var queues;
        var connectionFactories;
        //TODO este json tendria que devolverse desde el action
        $(document).ready(function () {
            $('#withoutGroupName').click(function () {
                var object = $('#groupName');
                object.attr("readonly", !object.attr("readonly"));
            });
            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
     
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            inizialize();
        });


    </script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
</head>
<body>

<div class="bloque">
    <h3>
        <span>Agrega un contanier:</span>
    </h3>

    <div class="cuerpo" id="addContainer">
        <form action="<s:url namespace="/handler" action="updateHandlerTypes"/>" id="add.container.form">
            <table class="formulario">
                <tr>
                    <s:textfield name="containerConfiguration.containerDescription" label="Descripcion"
                                 required="true"/>
                </tr>
                <tr>
                    <td class="label">
                        <label class="containerInfo" for="groupName">Nombre del Grupo</label>
                    </td>
                    <td>
                        <input type="text" name="groupName" value="" id="groupName" readonly>
                        <input type="checkbox" name="withoutGroupName" value="true" checked="checked"
                               id="withoutGroupName">
                    </td>
                </tr>
                <tr>
                    <s:select cssClass="containerQueue" name="containerConfiguration.queueName"
                              label="Nombre cola lectura"
                              list="queueList"/>
                    <s:select nextSelect="containerQueue" cssClass="select"
                              name="containerConfiguration.connectionFactoryName" label="Nombre del connection factory"
                              list="connectionFactoyNames"/>
                </tr>
                <tr>
                    <s:select cssClass="errorQueue" name="containerConfiguration.errorHandler.queueName"
                              label="Nombre cola error"
                              list="queueList"/>
                    <s:select cssClass="errorQueue" name="containerConfiguration.errorHandler.timeOutQueueName"
                              label="Nombre cola TimeOut"
                              list="queueList"/>
                    <s:select nextSelect="errorQueue" cssClass="select"
                              name="containerConfiguration.errorHandler.connectionFactoryName"
                              label="Nombre del connection factory" list="connectionFactoyNames"/>
                </tr>
                <tr>
                    <s:textfield name="containerConfiguration.numberOfConcurrentConsumers"
                                 label="Cantidad de consumidores"
                                 value="1"/>
                </tr>
                <tr>
                    <s:checkbox name="containerConfiguration.startOnLoad" label="Iniciar al crear"/>
                </tr>

                <tr><s:submit type="button" id="add.container.button" value="Agregar container"/></tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>