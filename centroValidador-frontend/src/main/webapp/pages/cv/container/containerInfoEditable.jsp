<%@ include file="../../template/includes/taglibs.jsp" %>
 <script src="<cw:url value="/js/utils.js"/>"></script>

<table>
    <tr class="rawHover">
        <td></td>
        <td class="rawHover rawHoverTd">
            <span class="containerInfo">Id</span>
        </td>
        <td class="rawHover rawHoverTd">
            <span><s:property value="containerConfiguration.id"/></span>
            <input type="hidden" name="idContainer"
                   value="<s:property value="containerConfiguration.id"/>"/>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:textfield cssClass="rawHoverTd" label="Descripcion" name="descripcion"
                     value="%{containerConfiguration.containerDescription}" required="true"/>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:select cssClass="rawHoverTd queueName" name="queueName" label="Nombre cola lectura"
                  list="queueList" id="queueName"
                  value="%{containerConfiguration.queueName}"/>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:textfield cssClass="rawHoverTd" label="Cantidad de consumidores"
                     name="numberOfConcurrentConsumers"
                     value="%{containerConfiguration.numberOfConcurrentConsumers}" required="true"/>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:textfield cssClass="rawHoverTd" label="Nombre de Grupo"
                     name="groupName"
                     value="%{containerConfiguration.groupName}" required="true"/>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:checkbox value="%{containerConfiguration.startOnLoad}" name="startOnLoad"
                    label="Iniciar al crear"/>
    </tr>
    <tr class="rawHover">
        <td></td>
        <s:select cssClass="rawHoverTd select" name="connectionFactoryName" label="Connection Factory Name"
                  list="connectionFactoyNames" nextSelect = "queueName"  id="connectionFactoryName"
                  value="%{containerConfiguration.connectionFactoryName}"/>
    </tr>
    <tr class="rawHover clickable">
        <td align="left"><img open="false" class="flecha"
                              src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">Error Handler Id</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="containerConfiguration.errorHandler.id"/></span>
        </td>
    </tr>
    <tr class="expandable">
        <td></td>
        <td></td>
        <td>
            <s:include value="../handler/errorHandlerInfoEditable.jsp"/>
        </td>
    </tr>
    <tr class="rawHover clickable">
        <s:url id="editHandler" action="editHandler" namespace="/handler" escapeAmp="false">
            <s:param name="idHandler" value="%{containerConfiguration.posMessageHandler.id}"/>
            <s:param name="handlerClassName" value="%{containerConfiguration.posMessageHandler.className}"/>
        </s:url>
        <td align="left"><a class="actionClick" title="Editar" href="<s:property value="#editHandler" />"><img
                src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">PosMessage Handler</span>
        </td>
        <td class="rawHoverTd">
            <span class="className"><s:property value="containerConfiguration.posMessageHandler.className"/></span>
        </td>

    </tr>
</table>
