<%@ include file="../../template/includes/taglibs.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {

		// Datepicker
		$("#formDatepicker").datepicker(datepickerDefaults);

		$("#btnModificar").click(function(event) {
			void (0);
			event.preventDefault();
		});

	});
</script>

<form action="<s:url namespace="/container" action="removeContainer"/>"
	id="remove.container.form">
	<table class="formulario">
		<tr>
			<s:textfield name="idContainer" label="Id container a eliminar" cssClass="text ancho200"/>
		</tr>
		<tr>
			<td colspan="4" class="botones"><br /> <s:submit
					id="remove.container.button" value="Remover container" />
			</td>
		</tr>
	</table>
</form>