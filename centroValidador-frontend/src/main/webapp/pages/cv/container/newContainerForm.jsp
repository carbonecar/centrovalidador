<%@ include file="../../template/includes/taglibs.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {

		// Datepicker
		$("#formDatepicker").datepicker(datepickerDefaults);

		$("#btnModificar").click(function(event) {
			void (0);
			event.preventDefault();
		});

	});
</script>

<form action="<s:url namespace="/container" action="newContainer"/>"
	id="new.container.form">
	<table class="formulario">
		<tr>
			<td colspan="4" class="botones"><s:submit type="button"
					id="new.container.button" value="Crear nuevo container" /></td>
		</tr>
	</table>
</form>

