<%@ include file="../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Listado de Containers</title>
    <meta name="pageCode" content="7"/>
    <title>Formulario</title>
    <script src="<cw:url value="/js/ui/cv/expandir_container_info.js"/>"></script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <style>
        .containerInfo {
            font-weight: bold;
        }

        .label {
            font-weight: bold;
        }
    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Container n�mero : <s:property value="containerConfiguration.id"/></span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
        <form action="<s:url namespace="/container" action="updateContainer"/>" id="add.strategy.form">
            <s:include value="containerInfoEditable.jsp"/>
            <s:submit type="button" value="Save"/>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>