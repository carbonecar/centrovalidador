<%@ include file="../../template/includes/taglibs.jsp" %>
<table>
    <s:iterator value="posMessageHandler.codigosExceptuar">
        <tr class="rawHover">
            <td></td>
            <td class="rawHoverTd">
                <span class="containerInfo">Codigo</span>
            </td>
            <td class="rawHoverTd">
                <span><s:property value="codigo"/></span>
            </td>
        </tr>
        <tr class="rawHover">
            <td></td>
            <td class="rawHoverTd">
                <span class="containerInfo">Descripcion</span>
            </td>
            <td class="rawHoverTd">
                <span><s:property value="descripcion"/></span>
            </td>
        </tr>
    </s:iterator>
</table>