<%@ include file="../../template/includes/taglibs.jsp" %>
<table>
    <tr class="rawHover">
        <td></td>
        <td class="rawHover rawHoverTd">
            <span class="containerInfo">Id</span>
        </td>
        <td class="rawHover rawHoverTd">
            <span><s:property value="id"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Descripcion</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="containerDescription"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Cola Inicial de Lectura</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="queueName"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Concurrencia</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="numberOfConcurrentConsumers"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Incia al levantar</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="startOnLoad"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Connection Factory Name</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="connectionFactoryName"/></span>
        </td>
    </tr>
    <tr class="rawHover clickable">
        <td align="left"><img open="false" class="flecha"
                              src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">Error Handler Id</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="errorHandler.id"/></span>
        </td>
    </tr>
    <tr class="expandable">
        <td></td>
        <td></td>
        <td>
            <s:include value="../handler/errorHandlerInfo.jsp" />
        </td>
    </tr>
    <tr class="rawHover clickable">
        <td align="left"><img open="false" class="flecha"
                              src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">PosMessage Handler Id</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="posMessageHandler.id"/></span>
        </td>
    </tr>
    <tr class="expandable">
        <td></td>
        <td></td>
        <td>
            <s:include value="../handler/posMessageHandlerInfo.jsp" />
        </td>
    </tr>

</table>


