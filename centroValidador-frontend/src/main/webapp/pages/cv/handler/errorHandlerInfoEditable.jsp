<%@ include file="../../template/includes/taglibs.jsp" %>
<table>
    <tr class="rawHover">
        <s:select cssClass="rawHoverTd errorQueue" id="errorQueue" name="errorHandler.queueName" label="Nombre cola lectura"
                  list="queueList"
                  value="%{containerConfiguration.errorHandler.queueName}"/>
        <input type="hidden" name="errorHandler.id"
               value="<s:property value="containerConfiguration.errorHandler.id"/>"/>
    </tr>
    <tr class="rawHover">
        <s:select cssClass="rawHoverTd errorQueue" name="errorHandler.timeOutQueueName"
                  label="Nombre cola lectura de TimeOut" id="timeOutQueueName"
                  list="queueList"
                  value="%{containerConfiguration.errorHandler.timeOutQueueName}"/>
    </tr>
    <tr class="rawHover">
        <s:select cssClass="rawHoverTd select" name="errorHandler.connectionFactoryName"
                  label="Connection Factory Name" nextSelect="errorQueue"
                  list="connectionFactoyNames"    id="connectionFactoyNamesErrorQueue"
                  value="%{containerConfiguration.errorHandler.connectionFactoryName}"/>
    </tr>
</table>