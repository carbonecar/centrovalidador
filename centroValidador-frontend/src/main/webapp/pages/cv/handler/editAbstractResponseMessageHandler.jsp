<%@ include file="../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>PosMessageHandler</title>
    <meta name="pageCode" content="7"/>
    <script>
        //Para expansion de la informacion del container.
        $(document).ready(function () {

            $(".clickable").click(function () {
                toggleRaw($(this), "expandable", "flecha", "${appCtx}/images/iconos/");
            });

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
                $(this).children(".label").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
                $(this).children(".label").css("background", "transparent");
            });

            $(".flecha").attr("src", "${appCtx}/images/iconos/flechaAAbajo.png");
            $(".flecha").attr("open", "true");

            ultimaPalabraPorPuntos("className");
        });

        var queues;
        var connectionFactories;
        //TODO este json tendria que devolverse desde el action
        $(document).ready(function () {
            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            var connectionFactoryName = "<s:property value='abstractResponseMessageHandler.connectionFactoryName'/>";
            $("#connectionFactoryName ").val(connectionFactoryName);

            inizialize();

            var queueName = "<s:property value='abstractResponseMessageHandler.queueName'/>";
            $("#queueName").val(queueName);

        });


        //Fin Para expansion de la informacion del container
    </script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <style>
        .containerInfo {
            font-weight: bold;
        }

        .label {
            font-weight: bold;
        }
    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Handler: </span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
        <form action="<s:url namespace="/handler" action="updateAbstractResponseMessageHandler"/>"
              id="add.strategy.form">
            <table>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Id</span>
                    </td>
                    <td class="rawHoverTd">
                        <span>
                            <s:property value="abstractResponseMessageHandler.id"/>
                         </span>
                        <input type="hidden"
                               name="idHandler"
                               value="<s:property value="abstractResponseMessageHandler.id"/>"/>
                    </td>
                </tr>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Class Name</span>
                    </td>
                    <td class="rawHoverTd">
                        <span class="className">
                            <s:property value="abstractResponseMessageHandler.className"/>
                        </span>
                        <input type="hidden"
                               name="abstractResponseMessageHandler.className"
                               value="<s:property value="abstractResponseMessageHandler.className"/>"/>
                    </td>
                </tr>

                <s:if test="abstractResponseMessageHandler.queueName != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:if test="abstractResponseMessageHandler.escribeEnLaCola() == true">
                            <s:select cssClass="rawHoverTd queueName" name="queueName" label="Nombre cola Escritura"
                                      list="queueList" id="queueName"
                                      value="%{abstractResponseMessageHandler.queueName}"/>
                        </s:if>
                        <s:else>
                            <s:select cssClass="rawHoverTd queueName" name="queueName" label="Nombre cola Lectura"
                                      list="queueList" id="queueName"
                                      value="%{abstractResponseMessageHandler.queueName}"/>
                        </s:else>
                    </tr>
                </s:if>

                <s:if test="abstractResponseMessageHandler.connectionFactoryName != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:select cssClass="rawHoverTd select" name="connectionFactoryName"
                                  label="Connection Factory Name" nextSelect="queueName"
                                  list="connectionFactoyNames" id="connectionFactoryName"
                                  value="%{abstractResponseMessageHandler.connectionFactoryName}"/>
                    </tr>
                </s:if>

                <s:if test="abstractResponseMessageHandler.posMessageHandler != null">
                    <tr>
                        <s:url id="editHandler" action="editHandler" namespace="/handler" escapeAmp="false">
                            <s:param name="idHandler" value="%{abstractResponseMessageHandler.posMessageHandler.id}"/>
                            <s:param name="handlerClassName"
                                     value="%{abstractResponseMessageHandler.posMessageHandler.className}"/>
                        </s:url>
                        <td align="left"><a class="actionClick" title="Editar"
                                            href="<s:property value="#editHandler" />"><img
                                src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>

                        </td>
                        <td class="rawHoverTd">
                            <span class="containerInfo">PosMessage Handler</span>
                        </td>
                        <td class="rawHoverTd">
                            <span class="className"><s:property
                                    value="abstractResponseMessageHandler.posMessageHandler.className"/></span>
                        </td>
                    </tr>
                </s:if>
            </table>
            <s:submit type="button" value="Save"/>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>