<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Handlers</title>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <script>
        $(document).ready(function () {
            ultimaPalabraPorPuntos("className");
        });
    </script>
</head>
<body>

<div class="bloque">
    <h3>
		<span>Lista handlers creados para ser utilizados en el
			container :</span>
    </h3>

    <div class="cuerpo">
        <table border="1">
            <tr>
                <th>Posicion</th>
                <th>ClassName</th>
            </tr>
            <s:iterator value="handlerList" status="stat">
                <tr>
                    <td>
                        <s:property value="#stat.index"/>
                    </td>
                    <td>
                        <span class="className"><s:property value="className"/></span>
                        <s:url id="showHandler" action="findHanlderById" namespace="/handler">
                            <s:param name="idHandler" value="#stat.index"/>
                        </s:url>
                        <a title="Ver" href="<s:property value="#showHandler" />">
                            <img src="<s:url value="/images/iconos/lupaOn.gif"/>"/>
                        </a>
                        <s:url id="eliminarHandler" namespace="/handler" action="removeHandlerFromList">
                            <s:param name="idHandler" value="#stat.index"/>
                        </s:url>
                        <a title="Ver" href="<s:property value="#eliminarHandler" />">
                            <img src="<s:url value="/images/iconos/eliminar.gif"/>"/>
                        </a>
                    </td>
                </tr>
            </s:iterator>
        </table>

        <br>

        <p>Ingrese la lista de handlers por su numero, separados por
            coma. Ej: "0,1,2"</p>
        <br>

        <p>La secuencia de numeros indica cual se encadena con otra ("0"
            con el "1" con el "2" y asi ). Es responsabilidad del creador del
            container saber como deben encadenarse</p>

        <form name="handlers"
              action="<s:url namespace="/container" action="createContainer"/>"
              id="add.container.form">
            <table class="formulario">
                <tr>
                    <s:textfield name="handlersToAdd" label="Handlers"/>
                </tr>
                <tr>
                    <td colspan="4" class="botones"><s:submit type="button"
                                                              id="add.container.button" value="Crear container"/>
                    </td>
                </tr>
            </table>
        </form>

        <form action="<s:url namespace="/handler" action="updateHandlerTypes"/>"
              id="add.handler.form">
            <table class="formulario">
                <tr>
                    <td colspan="4" class="botones"><s:submit type="button"
                                                              id="back.button" value="Crear nuevo handler"/>
                    </td>
                </tr>
            </table>
        </form>

        <!-- usar un action en lugar de un form -->
        <form action="<s:url namespace="/handler" action="reiniciar"/>"
              id="reiniciar.form">
            <table class="formulario">
                <tr>
                    <td colspan="4" class="botones"><s:submit type="button"
                                                              id="reiniciar.button" value="Reiniciar"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>