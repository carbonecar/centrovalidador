<%@ include file="../../template/includes/taglibs.jsp" %>
<table>
    <tr class="rawHover">
        <td class="rawHoverTd">
            <span class="containerInfo">Nombre de la Cola</span>
        </td>

        <td class="rawHoverTd">
            <span><s:property value="errorHandler.queueName"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td class="rawHoverTd">
            <span class="containerInfo">Nombre de la Cola de TimeOut</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="errorHandler.timeOutQueueName"/></span>
        </td>
    </tr>
    <tr class="rawHover">
        <td class="rawHoverTd">
            <span class="containerInfo">Connection Factory Name</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="errorHandler.connectionFactoryName"/></span>
        </td>
    </tr>
</table>