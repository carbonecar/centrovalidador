<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Handler</title>
</head>
<body>

<div class="bloque">
    <h3>
        <span>Agregar handler:</span>
    </h3>

    <div class="cuerpo" id="addHandler">
        <form action="<s:url namespace="/handler" action="showForm"/>" id="add.handler.form">
            <table class="formulario">
                <tr>
                    <s:select name="selectedItem" list="handlerTypes"/>
                </tr>
                <tr>
                    <td colspan="4" class="botones"><s:submit type="button"
                                                              value="Siguiente paso"/></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>