<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Abstract Response Message Handler</title>
    <script>
        var queues;
        var connectionFactories;
        //TODO este json tendria que devolverse desde el action
        $(document).ready(function () {
            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            inizialize();
        });
    </script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
</head>

<body>
<div class="bloque">

    <s:if test="selectedItem.equals('AlwaysRouteMessageHandler')">
        <h3><span>Always Route Message Handler</span></h3>
    </s:if>
    <s:elseif test="selectedItem.equals('StreamResponseMessageHandler')">
        <h3><span>Stream Response Message Handler</span></h3>
    </s:elseif>
    <div class="cuerpo">

        <form action="<s:url namespace="/handler" action="createAbstractResponseMessageHandler"/>"
              id="add.route.handler.form">
            <table class="formulario">
                <tr><s:select cssClass="routeQueue" name="routeQueue" label="Nombre cola donde rutea" list="queueList"/></tr>
                <tr><s:select cssClass="select" nextSelect="routeQueue" name="connectionFactoryName" label="Connection Factory Name"
                              list="connectionFactoyNames"/></tr>
                <tr><s:submit type="button" id="add.route.handler.button" value="Agregar route handler"/></tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>