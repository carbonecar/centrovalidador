<%@ include file="../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>PosMessageHandler</title>
    <meta name="pageCode" content="7"/>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <script>
        //Para expansion de la informacion del container.
        $(document).ready(function () {

            $(".clickable").click(function () {
                toggleRaw($(this), "expandable", "flecha", "${appCtx}/images/iconos/");
            });

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
                $(this).children(".label").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
                $(this).children(".label").css("background", "transparent");
            });

            $(".flecha").attr("src", "${appCtx}/images/iconos/flechaAAbajo.png");
            $(".flecha").attr("open", "true");

            ultimaPalabraPorPuntos("className");
        });

        //Fin Para expansion de la informacion del container
    </script>

    <style>
        .containerInfo {
            font-weight: bold;
        }

        .label {
            font-weight: bold;
        }
    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Handler: </span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
    <form action="<s:url namespace="/handler" action="updateAbstractResponseMessageHandler"/>" id="add.strategy.form">
        <table>
            <tr class="rawHover">
                <td></td>
                <td class="rawHoverTd">
                    <span class="containerInfo">Id</span>
                </td>
                <td class="rawHoverTd">
                        <span>
                            <s:property value="posMessageHandler.id"/>
                         </span>
                         <input type="hidden"
                               name="idHandler"
                               value="<s:property value="posMessageHandler.id"/>"/>
                </td>
            </tr>
            <tr class="rawHover">
                <td></td>
                <td class="rawHoverTd">
                    <span class="containerInfo">Class Name</span>
                </td>
                <td class="rawHoverTd">
                        <span class="className">
                            <s:property value="posMessageHandler.className"/>
                        </span>                         
                </td>
            </tr>

            <s:if test="posMessageHandler.codigosExceptuar != null">           
                <tr class="rawHover clickable">
                    <td align="left"><img open="false" class="flecha"
                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Codigos a Exceptuar</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.codigosExceptuar.size()"/></span>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <s:include value="/pages/cv/container/codigosExceptuar.jsp"/>
                    </td>
                </tr>
            </s:if>

<!-- Filtered Handler -->
            <s:if test="posMessageHandler.filters != null">            
                <tr class="rawHover clickable">                
                    <td align="left"><img open="false" class="flecha" src="<s:url value="/images/iconos/flechaADerecha.png"/>"/></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Filtros</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.filters.size()"/></span>
                    </td>
                    <!-- para crear un nuevo filtro y cambiarlo por el existente -->
                    <s:url id="editFilteredHandler" action="showForm" namespace="/handler" escapeAmp="false">                    	 
                        <s:param name="selectedItem" value="%{posMessageHandler.className}"/>                        
                        <s:param name="replaceExistingFilter">replaceExistingFilter</s:param>
                        <s:param name="idHandler" value="%{posMessageHandler.id}"/>
                    </s:url>
                    <td align="left"><a class="actionClick" title="Editar" href="<s:property value="#editFilteredHandler" />"><img src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <table>
                            <s:iterator value="posMessageHandler.filters">
                                <s:include value="./filter/filterEditable.jsp"/>
                            </s:iterator>
                        </table>
                    </td>
                </tr>                                
            </s:if>

            <s:if test="posMessageHandler.filterStrategyList != null">
                <tr class="rawHover clickable">
                    <td align="left"><img open="false" class="flecha"
                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Filtros Strategy</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.filterStrategyList.size()"/></span>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <table>
                            <s:iterator value="posMessageHandler.filterStrategyList">
                                <s:include value="./filter/filterStrategyEditable.jsp"/>
                            </s:iterator>
                        </table>
                    </td>
                </tr>
            </s:if>

<!-- Handler anidado interno -->
            <s:if test="posMessageHandler.posMessageHandler != null">
                <tr>
                    <s:url id="editHandler" action="editHandler" namespace="/handler" escapeAmp="false">
                        <s:param name="idHandler" value="%{posMessageHandler.posMessageHandler.id}"/>
                        <s:param name="handlerClassName" value="%{posMessageHandler.posMessageHandler.className}"/>
                    </s:url>
                    <td align="left"><a class="actionClick" title="Editar"
                                        href="<s:property value="#editHandler" />"><img
                            src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">PosMessage Handler</span>
                    </td>
                    <td class="rawHoverTd">
                            <span class="className"><s:property
                                    value="posMessageHandler.posMessageHandler.className"/></span>
                    </td>
                </tr>
            </s:if>
        </table>
        <!-- Esto no va,  hay que hacer una navegabilidad bien, pero mientras tanto lo voy cerrando y puedo hacer cosas mas importantes -->
         <s:if test="posMessageHandler.className=='ar.com.osde.centroValidador.pos.handlers.FilteredMessageHandler'">
        	<s:submit type="button" value="Save"/>
        </s:if>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>