<%@ include file="../../../template/includes/taglibs.jsp" %>
<table>
    <s:iterator value="posMessageHandler.filterStrategyList">
        <tr class="rawHover">
            <td></td>
            <td class="rawHoverTd">
                <span class="containerInfo">Id</span>
            </td>
            <td class="rawHoverTd">
                <span class="className"><s:property value="id"/></span>
            </td>
        </tr>
        <tr class="rawHover">
            <td></td>
            <td class="rawHoverTd">
                <span class="containerInfo">Name</span>
            </td>
            <td class="rawHoverTd">
                <span class="className"><s:property value="getClass().getName()"/></span>
            </td>
        </tr>
        <s:if test="getClass().getName().equals('ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration')">
            <tr class="rawHover">
                <td></td>
                <td class="rawHoverTd">
                    <span class="containerInfo">Nombre de la cola Lectura</span>
                </td>
                <td class="rawHoverTd">
                    <span><s:property value="queueName"/></span>
                </td>
            </tr>

            <tr class="rawHover">
                <td></td>
                <td class="rawHoverTd">
                    <span class="containerInfo">Connection Factory Name</span>
                </td>
                <td class="rawHoverTd">
                    <span><s:property value="connectionFactoryName"/></span>
                </td>
            </tr>
        </s:if>
        <s:if test="getClass().getName().equals('ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration')">
            <tr class="rawHover clickable">
                <td align="left"><img open="false" class="flecha"
                                      src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                </td>
                <td class="rawHoverTd">
                    <span class="containerInfo">PosMessage Handler Id</span>
                </td>
                <td class="rawHoverTd">
                    <span><s:property value="posMessageHandler.id"/></span>
                </td>
            </tr>
            <tr class="expandable">
                <td></td>
                <td></td>
                <td>
                    <s:include value="../../handler/posMessageHandlerInfo.jsp"/>
                </td>
            </tr>
        </s:if>

    </s:iterator>
</table>