<%@ include file="../../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Route Filter Strategy:</title>
    <script>
        var queues;
        var connectionFactories;
        //TODO este json tendria que devolverse desde el action
        $(document).ready(function () {
            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            inizialize();
        });
    </script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
</head>

<body>
<div class="bloque">

    <h3><span>Route Filter Strategy:</span></h3>

    <div class="cuerpo">

        <form action="<s:url namespace="/filtro" action="createRouteStrategy"/>" id="add.route.strategy.form">
            <table class="formulario">
                <tr><s:select cssClass="routeQueue" name="routeQueue" label="Nombre cola de escritura" list="queueList"/></tr>
                <tr><s:select cssClass="select" nextSelect="routeQueue" name="connectionFactoryName" label="Connection Factory Name"
                              list="connectionFactoyNames"/></tr>
                <tr><s:submit type="button" id="add.route.handler.button" value="Agregar route strategy"/></tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>