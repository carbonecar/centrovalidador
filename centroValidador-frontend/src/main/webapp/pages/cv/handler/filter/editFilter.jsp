<%@ include file="../../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>FilterConfiguration</title>
    <meta name="pageCode" content="7"/>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <script>
        var propiedadesMap = "<s:property value="propertiesToFilter"/>";
        $(document).ready(function () {

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
                $(this).children(".label").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
                $(this).children(".label").css("background", "transparent");
            });

            ultimaPalabraPorPuntos("className");
        });

        $(document).ready(function () {
            //TODO este json tendria que devolverse desde el action
            propiedadesMap = JSON.parse(propiedadesMap.replace(/ /g, '').replace(/\{/g, '{"').replace(/\}/g, '"}').replace(/=/g, '":"').replace(/,/g, '","').replace(/"int"/g,'"Integer"'));
            $("select").change(function () {
                var value = propiedadesMap[this.value];
                $("#tipoMostrado").html(value);
                $("#tipoEnviado").attr("value", value);
            });
        });
    </script>
    <style>
        .containerInfo {
            font-weight: bold;
        }

        .label {
            font-weight: bold;
        }


    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Filter: </span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
        <form action="<s:url namespace="/filtro" action="updateFilter"/>"
              id="add.strategy.form">
            <table>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Id</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="filterConfiguration.id"/></span>
                        <input type="hidden"
                               name="propertyEqualFilterConfiguration.id"
                               value="<s:property value="filterConfiguration.id"/>"/>
                        <input type="hidden"
                               name="idHandler"
                               value="<s:property value="idHandler"/>"/>
                        <input type="hidden"
                               name="handlerClassName"
                               value="<s:property value="handlerClassName"/>"/>
                    </td>
                </tr>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Class Name</span>
                    </td>
                    <td class="rawHoverTd">
                        <span class="className"><s:property value="filterConfiguration.className"/></span>
                        <input type="hidden"
                               name="propertyEqualFilterConfiguration.className"
                               value="<s:property value="filterConfiguration.className"/>"/>
                    </td>
                </tr>
                <s:if test="filterConfiguration.type != null">
                    <tr class="rawHover">
                        <td></td>
                        <td class="rawHoverTd">
                            <span class="containerInfo">Tipo de la Propiedad</span>
                        </td>
                        <td class="rawHoverTd">
                            <span id="tipoMostrado" class="className"><s:property
                                    value="filterConfiguration.type"/></span>
                            <input type="hidden" id="tipoEnviado"
                                   name="propertyEqualFilterConfiguration.type"
                                   value="<s:property value="filterConfiguration.type"/>"/>
                        </td>
                    </tr>
                </s:if>
                <s:if test="filterConfiguration.propertyName != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:select cssClass="rawHoverTd" name="propertyEqualFilterConfiguration.propertyName"
                                  label="Nombre de la propiedad"  value="filterConfiguration.propertyName"
                                  list="keys"/>
                    </tr>
                </s:if>
                <s:if test="filterConfiguration.value != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:textfield cssClass="rawHoverTd" label="Valor"
                                     name="propertyEqualFilterConfiguration.value"
                                     value="%{filterConfiguration.value}" required="true"/>
                    </tr>
                </s:if>
            </table>
            <s:submit type="button" value="Save"/>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>