<%@ include file="../../../template/includes/taglibs.jsp" %>
<tr class="rawHover">
    <s:if test="getClass().getName().equals('ar.com.osde.entities.filter.PropertyEqualFilterConfiguration')">
        <s:url id="editFilter" action="editFilter" escapeAmp="false" namespace="/filtro">
            <s:param name="idFilter" value="%{id}"/>
            <s:param name="idHandler" value="%{posMessageHandler.id}"/>
            <s:param name="handlerClassName" value="%{posMessageHandler.className}"/>
        </s:url>
        <td align="left"><a class="actionClick" title="Editar"
                            href="<s:property value="#editFilter" />"><img
                src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
        </td>
    </s:if>
    <s:else>
        <td></td>
    </s:else>
    <td class="rawHoverTd">
        <span class="containerInfo">Id</span>
    </td>
    <td class="rawHoverTd">
        <span><s:property value="id"/></span>
    </td>
</tr>
<tr class="rawHover">
    <td></td>
    <td class="rawHoverTd">
        <span class="containerInfo">Class Name</span>
    </td>
    <td class="rawHoverTd">
        <span class="className"><s:property value="className"/></span>
    </td>
</tr>
<s:if test="type != null">
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Tipo de la Propiedad</span>
        </td>
        <td class="rawHoverTd">
            <span class="className"><s:property value="type"/></span>
        </td>
    </tr>
</s:if>
<s:if test="propertyName != null">
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Nombre de la Propiedad</span>
        </td>
        <td class="rawHoverTd">
            <span class="className"><s:property value="propertyName"/></span>
        </td>
    </tr>
</s:if>
<s:if test="value != null">
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Valor de la Propiedad</span>
        </td>
        <td class="rawHoverTd">
            <span class="className"><s:property value="value"/></span>
        </td>
    </tr>
</s:if>
<s:if test="getClass().getName().equals('ar.com.osde.entities.filter.CompositeFilterConfiguration')">
    <tr class="rawHover clickable">
        <td align="left"><img open="false" class="flecha"
                              src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">Filtros</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="filterConfigurationList.size()"/></span>
        </td>
    </tr>
    <tr class="rawHover expandable">
        <td></td>
        <td></td>
        <td>
            <table>
                <s:iterator value="filterConfigurationList" var="filter">
                    <s:push value="filter">
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <table>
                                    <s:include value="filterEditable.jsp"/>
                                </table>
                            </td>
                        </tr>
                    </s:push>
                </s:iterator>
            </table>
        </td>
    </tr>
</s:if>
<s:if test="getClass().getName().equals('ar.com.osde.entities.filter.NotFilterConfiguration')">
    <s:push value="filterConfiguration">
        <tr class="rawHover clickable">
            <td align="left"><img open="false" class="flecha"
                                  src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
            </td>
            <td class="rawHoverTd">
                <span class="containerInfo">Filter Class Name</span>
            </td>
            <td class="rawHoverTd">
                <span><s:property value="className"/></span>
            </td>
        </tr>
        <tr class="rawHover expandable">
            <td></td>
            <td></td>
            <td>
                <table>
                    <s:include value="filterEditable.jsp"/>
                </table>
            </td>
        </tr>
    </s:push>
</s:if>