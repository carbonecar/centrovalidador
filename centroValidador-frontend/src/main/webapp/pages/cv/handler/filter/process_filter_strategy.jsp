<%@ include file="../../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Process Filter Strategy:</title>
</head>

<body>
<div class="bloque">

    <h3>
        <span>Process Filter Strategy:</span>
    </h3>

    <div class="cuerpo">

        <table border="1">
            <tr>
                <th>Posicion</th>
                <th>ClassName</th>
            </tr>
            <s:iterator value="handlerList" status="stat">
                <tr>
                    <td>
                        <s:property value="#stat.index"/>
                    </td>
                    <td>
                        <s:property value="className"/>
                        <s:url id="showHandler" namespace="/handler" action="findHanlderById">
                            <s:param name="idHandler" value="#stat.index"/>
                        </s:url>
                        <a title="Ver" href="<s:property value="#showHandler" />">
                            <img src="<s:url value="/images/iconos/lupaOn.gif"/>"/>
                        </a>
                    </td>
                </tr>
            </s:iterator>
        </table>

        <p>Numero de handlers, separados por coma.</p>

        <form action="<s:url namespace="/filtro" action="addProcessHandler"/>" id="add.container.form">
            <table class="formulario">
                <s:textfield name="handlersToAdd" label="Handlers"/>
                <s:submit type="button" id="add.container.button" value="Crear process handler"/>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>