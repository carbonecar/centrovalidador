<%@ include file="../../../template/includes/taglibs.jsp" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>FilterConfiguration</title>
    <meta name="pageCode" content="7"/>
    <script>
        $(document).ready(function () {

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
                $(this).children(".label").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
                $(this).children(".label").css("background", "transparent");
            });

            ultimaPalabraPorPuntos("className");
        });

        var queues;
        var connectionFactories;
        //TODO este json tendria que devolverse desde el action
        $(document).ready(function () {
            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");

            var connectionFactoryName = "<s:property value='filterStrategyConfiguration.connectionFactoryName'/>";
            $("#connectionFactoryName").val(connectionFactoryName);

            inizialize();

            var queueName = "<s:property value='filterStrategyConfiguration.queueName'/>";
            $("#queueName ").val(queueName);

        });
    </script>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <style>
        .containerInfo {
            font-weight: bold;
        }

        .label {
            font-weight: bold;
        }


    </style>
</head>

<body>

<div class="bloque">
    <h3>
        <span>Filter: </span>
    </h3>

    <div id="ajxDspId" class="cuerpo">
        <form action="<s:url namespace="/filtro" action="updateFilterStrategy"/>"
              id="add.strategy.form">
            <table>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Id</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="filterStrategyConfiguration.id"/></span>
                        <input type="hidden"
                               name="routeFilterStrategyConfiguration.id"
                               value="<s:property value="filterStrategyConfiguration.id"/>"/>
                        <input type="hidden"
                               name="idHandler"
                               value="<s:property value="idHandler"/>"/>
                        <input type="hidden"
                               name="handlerClassName"
                               value="<s:property value="handlerClassName"/>"/>
                    </td>
                </tr>
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Name</span>
                    </td>
                    <td class="rawHoverTd">
                        <span class="className"><s:property
                                value="filterStrategyConfiguration.getClass().getName()"/></span>
                    </td>
                </tr>
                <s:if test="filterStrategyConfiguration.queueName != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:select cssClass="rawHoverTd queueName" name="routeFilterStrategyConfiguration.queueName"
                                  label="Nombre cola de escritura"
                                  list="queueList" id="queueName"
                                  value="%{filterStrategyConfiguration.queueName}"/>
                    </tr>
                </s:if>
                <s:if test="filterStrategyConfiguration.connectionFactoryName != null">
                    <tr class="rawHover">
                        <td></td>
                        <s:select cssClass="rawHoverTd select"
                                  name="routeFilterStrategyConfiguration.connectionFactoryName"
                                  label="Connection Factory Name" nextSelect="queueName"
                                  list="connectionFactoyNames" id="connectionFactoryNames"
                                  value="%{filterStrategyConfiguration.connectionFactoryName}"/>
                    </tr>
                    <s:submit type="button" value="Save"/>
                </s:if>
                <s:if test="filterStrategyConfiguration.posMessageHandler != null">
                    <tr class="rawHover clickable">
                        <s:url id="editHandler" action="editHandler" namespace="/handler" escapeAmp="false">
                            <s:param name="idHandler" value="%{filterStrategyConfiguration.posMessageHandler.id}"/>
                            <s:param name="handlerClassName"
                                     value="%{filterStrategyConfiguration.posMessageHandler.className}"/>
                        </s:url>
                        <td align="left"><a class="actionClick" title="Editar"
                                            href="<s:property value="#editHandler" />"><img
                                src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
                        </td>
                        <td class="rawHoverTd">
                            <span class="containerInfo">PosMessage Handler</span>
                        </td>
                        <td class="rawHoverTd">
                            <span class="className"><s:property
                                    value="filterStrategyConfiguration.posMessageHandler.className"/></span>
                        </td>

                    </tr>
                </s:if>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>