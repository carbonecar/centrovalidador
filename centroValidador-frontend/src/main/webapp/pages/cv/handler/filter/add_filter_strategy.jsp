<%@ include file="../../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Agregar Filter Strategy:</title>
</head>

<body>
<div class="bloque">

    <h3><span>Agregar filter strategy:</span></h3>

    <div class="cuerpo">
        <form action="<s:url namespace="/filtro" action="showFormStrategy"/>" id="add.strategy.form">
            <select name="selectedItem">
                <option value="ProcessFilterStrategy">ProcessFilterStrategy</option>
                <option value="RouteFilterStrategy">RouteFilterStrategy</option>
                <option value="NullFilterStrategy">NullFilterStrategy</option>
            </select>
            <s:submit type="button" value="Siguiente paso"/>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>