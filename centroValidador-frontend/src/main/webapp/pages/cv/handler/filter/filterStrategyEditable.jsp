<%@ include file="../../../template/includes/taglibs.jsp" %>
<tr class="rawHover">
    <s:url id="editFilterStrategy" action="editFilterStrategy" escapeAmp="false" namespace="/filtro">
        <s:param name="idFilter" value="%{id}"/>
        <s:param name="idHandler" value="%{posMessageHandler.id}"/>
        <s:param name="handlerClassName" value="%{posMessageHandler.className}"/>
    </s:url>
    <td align="left"><a class="actionClick" title="Editar"
                        href="<s:property value="#editFilterStrategy" />"><img
            src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
    </td>
    <td class="rawHoverTd">
        <span class="containerInfo">Id</span>
    </td>
    <td class="rawHoverTd">
        <span><s:property value="id"/></span>
    </td>
</tr>
<tr class="rawHover">
    <td></td>
    <td class="rawHoverTd">
        <span class="containerInfo">Name</span>
    </td>
    <td class="rawHoverTd">
        <span class="className"><s:property value="getClass().getName()"/></span>
    </td>
</tr>
<s:if test="queueName != null">
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Nombre de la cola Lectura</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="queueName"/></span>
        </td>
    </tr>
</s:if>
<s:if test="connectionFactoryName != null">
    <tr class="rawHover">
        <td></td>
        <td class="rawHoverTd">
            <span class="containerInfo">Connection Factory Name</span>
        </td>
        <td class="rawHoverTd">
            <span><s:property value="connectionFactoryName"/></span>
        </td>
    </tr>
</s:if>

<s:if test="posMessageHandler != null">
    <tr class="rawHover">
        <s:url id="editHandler" action="editHandler" namespace="/handler" escapeAmp="false">
            <s:param name="idHandler" value="%{posMessageHandler.id}"/>
            <s:param name="handlerClassName" value="%{posMessageHandler.className}"/>
        </s:url>
        <td align="left"><a class="actionClick" title="Editar"
                            href="<s:property value="#editHandler" />"><img
                src="<s:url value="/images/iconos/editar-over.gif"/>"/></a>
        </td>
        <td class="rawHoverTd">
            <span class="containerInfo">PosMessage Handler</span>
        </td>
        <td class="rawHoverTd">
                            <span class="className"><s:property
                                    value="posMessageHandler.className"/></span>
        </td>
    </tr>
</s:if>
