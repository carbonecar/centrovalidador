<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <s:if test="selectedItem.equals('FilteredMessageHandler')">
        <title>Filter Handler</title>
    </s:if>
    <s:elseif test="selectedItem.equals('TimeOutFilteredMessageHandler')">
        <title>TimeOut Filter Handler</title>
    </s:elseif>
    <meta name="pageCode" content="7"/>
</head>

<body>


<div class="bloque">
    <s:if test="selectedItem.equals('FilteredMessageHandler')">
        <h3>
            <span>Filter Message Handler(Estrategias de filtro)</span>
        </h3>
    </s:if>
    <s:elseif test="selectedItem.equals('TimeOutFilteredMessageHandler')">
        <h3>
            <span>TimeOut Filter Message Handler(Estrategias de filtro)</span>
        </h3>
    </s:elseif>
    <div class="cuerpo" id="id_filtreStrategy_list">

        <s:iterator value="filterStrategyList">
            <ul>
                <li>Class: <s:property value="class"/>
                </li>
            </ul>
        </s:iterator>
        <a
                href="/centroValidador-frontend/pages/cv/handler/filter/add_filter_strategy.jsp">�Queres
            agregar una nueva estrategia?</a>

        <h4>
            Ingrese la expresion en notacion infija y utilizando parentesis, ej: B == 1 AND ( ( B == 2 AND B == 3 ) OR B == 4 ) OR NOT B == 5
            IMPORTANTE: Ingresar bien los espacios
            para que si dicha condicion NO se cumple, se aplique la/las estrategias de ruteo.
            Aplicar la estrategia de ruteo implica que se corta la cadena de procesamiento y se aplica la estrategia
        </h4>

        <form action="<s:url namespace="/handler" action="createFilterHandler"/>"
              id="add.filter.handler.form">
            <table class="formulario" style="width: 100%">
                <tr>
                    <td style="width: 40%">
                        <table style="width: 100%">
                            <thead>
                            <tr>
                                <th align="left">Propiedad</th>
                                <th align="left">Tipo</th>
                            </tr>
                            </thead>
                        </table>
                        <div style="height: 370px; overflow-y: auto">
                            <table style="width: 100%">
                                <tbody>
                                <s:iterator value="keys" var="key">

                                    <tr>
                                        <td><s:property value="key"/></td>
                                        <td><s:property value="propertiesToFilter.get(#key)"/></td>
                                    </tr>

                                </s:iterator>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td style="width: 60%">
                        <label>
                            <textarea name="expresionFiltro"
                                      style="width: 100%;" rows="30">getAtributoCodigoTransaccion == A AND getCodigoTransaccion == 1</textarea>
                        </label>
                        <s:submit type="button" value="Siguiente paso"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>


</body>

</html>