<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Descripcion del handler</title>
    <script src="<cw:url value="/js/utils.js"/>"></script>
    <script>
        //Para expansion de la informacion del container.
        $(document).ready(function () {

            $(".clickable").click(function () {
                toggleRaw($(this), "expandable", "flecha", "${appCtx}/images/iconos/");
            });

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
            });

            ultimaPalabraPorPuntos("className");

            $(".actionClick").click(function (event) {
                event.stopPropagation();
            });
        });

        //Fin Para expansion de la informacion del container
    </script>

    <style>
        .expandable {
            display: none;
        }

        .containerInfo {
            font-weight: bold;
        }
    </style>
</head>

<body>
<div class="bloque">
    <h3>
        <span>Descripcion del handler</span>
    </h3>

    <div class="cuerpo">

        <table>
            <tr class="rawHover">
                <td></td>
                <td class="rawHoverTd">
                    <span class="containerInfo">Class Name</span>
                </td>
                <td class="rawHoverTd">
                    <span class="className"><s:property value="posMessageHandler.className"/></span>
                </td>
            </tr>

            <s:if test="posMessageHandler.queueName != null">
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <s:if test="posMessageHandler.escribeEnLaCola() == true">
                            <span class="containerInfo">Nombre de la Cola de Escritura</span>
                        </s:if>
                        <s:else>
                            <span class="containerInfo">Nombre de la Cola de Lectura</span>
                        </s:else>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.queueName"/></span>
                    </td>
                </tr>
            </s:if>

            <s:if test="posMessageHandler.connectionFactoryName != null">
                <tr class="rawHover">
                    <td></td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Connection Factory Name</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.connectionFactoryName"/></span>
                    </td>
                </tr>
            </s:if>

            <s:if test="posMessageHandler.codigosExceptuar != null">
                <tr class="rawHover clickable">
                    <td align="left"><img open="false" class="flecha"
                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Codigos a Exceptuar</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.codigosExceptuar.size()"/></span>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <s:include value="../container/codigosExceptuar.jsp"/>
                    </td>
                </tr>
            </s:if>

            <s:if test="posMessageHandler.filters != null">
                <tr class="rawHover clickable">
                    <td align="left"><img open="false" class="flecha"
                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Filtros</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.filters.size()"/></span>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <s:include value="filter/filters.jsp"/>
                    </td>
                </tr>
            </s:if>

            <s:if test="posMessageHandler.filterStrategyList != null">
                <tr class="rawHover clickable">
                    <td align="left"><img open="false" class="flecha"
                                          src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                    </td>
                    <td class="rawHoverTd">
                        <span class="containerInfo">Filtros Strategy</span>
                    </td>
                    <td class="rawHoverTd">
                        <span><s:property value="posMessageHandler.filterStrategyList.size()"/></span>
                    </td>
                </tr>
                <tr class="rawHover expandable">
                    <td></td>
                    <td></td>
                    <td>
                        <s:include value="filter/filterStrategyList.jsp"/>
                    </td>
                </tr>
            </s:if>

            <s:if test="posMessageHandler.posMessageHandler != null">
                <s:push value="posMessageHandler">
                    <tr class="rawHover clickable">
                        <td align="left"><img open="false" class="flecha"
                                              src="<s:url value="/images/iconos/flechaADerecha.png"/>"/>
                        </td>
                        <td class="rawHoverTd">
                            <span class="containerInfo">PosMessage Handler Id</span>
                        </td>
                        <td class="rawHoverTd">
                            <span><s:property value="posMessageHandler.id"/></span>
                        </td>
                    </tr>
                    <tr class="rawHover expandable">
                        <td></td>
                        <td></td>
                        <td>
                            <s:include value="posMessageHandlerInfo.jsp"/>
                        </td>
                    </tr>
                </s:push>
            </s:if>
        </table>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>
</body>
</html>