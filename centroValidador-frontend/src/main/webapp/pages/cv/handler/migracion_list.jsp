<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Migraci�n Handler</title>
</head>
<body>
<div class="bloque">
    <h3>
        <span>
           Selecci�n de los c�digos que desea exceptuar.
           Para selecci�n mas de uno presionar hacerlo presionando la tecla CTRL
        </span>
    </h3>

    <div class="cuerpo">
        <s:form action="createMigracionHandler" namespace="/handler">
            <s:select label="Codigos" name="codigosExceptuablesSeleccionados"
                      id="codigosExceptuablesSeleccionados"
                      list="codigosExceptuables"
                      listKey="id"
                      listValue="descripcion"
                      multiple="true" required="true"/>
            <s:submit type="button" value="Agregar"/>
        </s:form>
    </div>
    <div class="pie">
        <div></div>
    </div>
</div>

</body>
</html>