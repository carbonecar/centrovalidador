<%@ include file="../../template/includes/taglibs.jsp" %>

<html>
<head>
    <title>Mensajes de Cola</title>
</head>
<style>
    .redText {
        color: red !important;
    }
</style>
<body>
<h2>Nombre del Container:</h2> <s:property value="containerConfiguration.containerDescription"/>

<h2>Nombre de la Cola: </h2><s:property value="containerConfiguration.errorHandler.queueName"/>

<s:if test="queueMessage != null">
    <h2 class="redText">Mensaje Eliminado: </h2>
    <ul class="redText">
        <li>Id : <s:property value="queueMessage.id"/></li>
        <li>Mensaje : <s:property value="queueMessage.message"/></li>
    </ul>
</s:if>
<s:if test="messages != null && messages.size() > 0">
    <h2>Mensajes dentro de la cola:</h2>
    <ul>
        <s:iterator value="messages">
            <li>
                <s:property value="message"/>
                <s:url id="removeUrl" action="removeMessageErrorHandlerQueue" namespace="/queue" escapeAmp="false">
                    <s:param name="idContainer" value="%{containerConfiguration.id}"/>
                    <s:param name="idMessage" value="%{id}"/>
                </s:url>
                <a title="Eliminar" href="<s:property value="#removeUrl" />"><img
                        src="<s:url value="/images/iconos/eliminar.gif"/>"/></a>
            </li>
        </s:iterator>
    </ul>
</s:if>
<s:else>
    <h2 class="redText">No hay mensajes en la cola</h2>
</s:else>
</body>
</html>