<%@ include file="/pages/template/includes/taglibs.jsp" %>
<ul class="base" >
	<li class="primero"><a href="${appCtx}/context/index.do"><s:text name="menu.opt1"/></a></li>
	<li class="top"><a href="javascript:subMenu(1);void(0);"><s:text name="menu.opt2"/></a>
    	<ul id="sub_1">

 		<s:if test="%{#session.pageCode == 1}">
    		<li class="activo"><a href="${appCtx}/pages/template/ayuda.jsp"><s:text name="menu.opt2.1"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/ayuda.jsp"><s:text name="menu.opt2.1"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 2}">
    		<li class="activo"><a href="${appCtx}/pages/template/botones.jsp"><s:text name="menu.opt2.2"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/botones.jsp"><s:text name="menu.opt2.2"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 3}">
    		<li class="activo"><a href="${appCtx}/pages/template/bloque.jsp"><s:text name="menu.opt2.3"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/bloque.jsp"><s:text name="menu.opt2.3"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 4}">
    		<li class="activo"><a href="${appCtx}/pages/template/calendar.jsp"><s:text name="menu.opt2.4"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/calendar.jsp"><s:text name="menu.opt2.4"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 5}">
    		<li class="activo"><a href="${appCtx}/pages/template/descarga.jsp"><s:text name="menu.opt2.5"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/descarga.jsp"><s:text name="menu.opt2.5"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 6}">
    		<li class="activo"><a href="${appCtx}/pages/template/error_samples.jsp"><s:text name="menu.opt2.6"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/error_samples.jsp"><s:text name="menu.opt2.6"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 7}">
    		<li class="activo"><a href="${appCtx}/pages/template/formulario.jsp"><s:text name="menu.opt2.7"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/formulario.jsp"><s:text name="menu.opt2.7"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 8}">
    		<li class="activo"><a href="${appCtx}/pages/template/detalle.jsp?readonly=true"><s:text name="menu.opt2.8"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/detalle.jsp?readonly=true"><s:text name="menu.opt2.8"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 9}">
    		<li class="activo"><a href="${appCtx}/pages/template/tabs.jsp"><s:text name="menu.opt2.9"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/tabs.jsp"><s:text name="menu.opt2.9"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 10}">
    		<li class="activo"><a href="${appCtx}/pages/template/buscador.jsp"><s:text name="menu.opt2.10"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/buscador.jsp"><s:text name="menu.opt2.10"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 11}">
    		<li class="activo"><a href="${appCtx}/pages/template/tabla.jsp"><s:text name="menu.opt2.11"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/tabla.jsp"><s:text name="menu.opt2.11"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 12}">
    		<li class="activo"><a href="${appCtx}/pages/template/dialog_samples.jsp"><s:text name="menu.opt2.12"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/dialog_samples.jsp"><s:text name="menu.opt2.12"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 13}">
    		<li class="activo"><a href="${appCtx}/pages/template/popup_sample.jsp"><s:text name="menu.opt2.13"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/popup_sample.jsp"><s:text name="menu.opt2.13"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 14}">
    		<li class="activo"><a href="${appCtx}/pages/template/cargando.jsp"><s:text name="menu.opt2.14"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/cargando.jsp"><s:text name="menu.opt2.14"/></a></li>
    	</s:else>
    	<%--  --%>
    	<s:if test="%{#session.pageCode == 15}">
    		<li class="activo"><a href="${appCtx}/pages/template/print.jsp"><s:text name="menu.opt2.15"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/print.jsp"><s:text name="menu.opt2.15"/></a></li>
    	</s:else>
    	<s:if test="%{#session.pageCode == 16}">
    		<li class="activo"><a href="${appCtx}/pages/template/eventoFormulario.jsp"><s:text name="menu.opt2.16"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/eventoFormulario.jsp"><s:text name="menu.opt2.16"/></a></li>
    	</s:else>
    	<s:if test="%{#session.pageCode == 17}">
    		<li class="activo"><a href="${appCtx}/pages/template/arbol.jsp"><s:text name="menu.opt2.17"/></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/template/arbol.jsp"><s:text name="menu.opt2.17"/></a></li>
    	</s:else>
		<s:if test="%{#session.pageCode == 18}">
    		<li class="activo"><a href="${appCtx}/pages/random/list.jsp"/><s:text name="menu.opt2.18"/></a></a></li>
    	</s:if>
    	<s:else>
    		<li><a href="${appCtx}/pages/random/list.jsp"/><s:text name="menu.opt2.18"/></a></a></li>
    	</s:else>
 		            
        </ul>
    </li>
    <li class="top"><a href="javascript:subMenu(2);void(0);"><s:text name="menu.opt3"/></a>
    	<ul id="sub_2">
        	<li class="activo"><a href="javascript:void(0)"><s:text name="menu.opt3.1"/></a></li>
            <li><a href="<s:url action="changeLayout"/>?layout=layout2"><s:text name="menu.opt3.2"/></a></li>
            <!-- <li><a href="<s:url action="changeLayout"/>?layout=layout3"><s:text name="menu.opt3.3"/></a></li> -->
        </ul>
    </li>
    <li class="top"><a href="javascript:subMenu(3);void(0);"><s:text name="menu.opt4"/></a>
    	<ul id="sub_3">
        	<li><a href="javascript:subMenu(31);void(0);"><s:text name="menu.opt4.1"/></a>
            	<ul id="sub_31">
                    <li class="activo"><a href="${appCtx}/pages/template/abm.jsp"><s:text name="menu.opt4.1.1"/></a></li>
                </ul>
            </li>
        </ul>
    </li>
	<li class="top"><a href="javascript:subMenu(4);void(0);"><s:text name="menu.opt5"/></a>
    	<ul id="sub_4">
            <li><a href="#"><s:text name="menu.opt5.1"/></a></li>
        </ul>
    </li>
    <li class="ultimo"></li>
</ul>

