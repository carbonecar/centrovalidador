function ultimaPalabraPorPuntos(className) {
    $("." + className).each(function () {
        var value = $(this).html();
        var split = value.split(".");
        $(this).html(split[split.length - 1]);
    });
}
function toggleRaw(tr, expandableClass, flechaClass, pathUrl) {
    var next = tr.next();
    next.toggle();
    var flecha = tr.children("td").children("." + flechaClass);
    if (flecha.attr("open") == "false") {
        flecha.attr("open", "true");
        flecha.attr("src", pathUrl + "flechaAAbajo.png");
    } else {
        flecha.attr("open", "false");
        flecha.attr("src", pathUrl + "flechaADerecha.png");
    }
}

function inizialize(){
    var selects = $(".select");
    selects.change(filterSelect);
    $.each(selects, filterSelect);
}

function filterSelect() {
    var nextSelect = $("." + this.getAttribute("nextSelect"));
    var connectionName = this.value.split("_")[1];    
    
    
    nextSelect.empty();
    $.each(queues, function (key, value) {
        if (value.indexOf(connectionName) != -1)
            nextSelect.append($("<option></option>")
                .attr("value", value).text(value));
    });
}