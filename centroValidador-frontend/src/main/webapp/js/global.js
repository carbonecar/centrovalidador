// Preload de imagenes
jQuery.preloadImages = function()
{
  for(var i = 0; i<arguments.length; i++)
  {
    jQuery("<img>").attr("src", arguments[i]);
  }
};

// Aplica bloqueo a todo requerimiento AJAX usando config. default de blockUI
$().ajaxStart($.blockUI).ajaxStop($.unblockUI);


/**
 * Presenta un popup de dialogo con un mensaje informativo
 * @param txt Texto a mostrar
 * @param callback [opcional] Funcion a ejecutar luego de cerrar el dialogo
 */
function showInfoMsg( txt , callback)
{
	$("#infoDialog #infoTxt").html( txt );
	$("#infoDialog").dialog(dialogDefaults);
	// sobrescribo color del titulo
	$("#ui-dialog-title-infoDialog").css("color", "#0097D0") ;
	if( callback )
	{
		$('#infoDialog').bind('dialogclose',  callback);
	}
}

/**
 * Presenta un popup de dialogo con un mensaje de ayuda
 * @param txt Texto a mostrar
 * @param callback [opcional] Funcion a ejecutar luego de cerrar el dialogo
 */
function showAyudaMsg( txt , callback)
{
	$("#ayudaDialog #ayudaTxt").html( txt );
	$("#ayudaDialog").dialog(dialogDefaults);
	// sobrescribo color del titulo
	$("#ui-dialog-title-ayudaDialog").css("color", "#0097D0") ;
	if( callback )
	{
		$('#ayudaDialog').bind('dialogclose',  callback);
	}
}

/**
 * Presenta un popup de dialogo con un mensaje de confirmaci�n
 * @param txt Texto a mostrar
 * @param callback [opcional] Funcion a ejecutar luego de aceptar el dialogo
 */
function showConfirmMsg( txt , callback)
{
	$("#confirmDialog #confirmTxt").html( txt );
	$("#confirmDialog").dialog(dialogDefaults).dialog("open");
	// sobrescribo color del titulo
	$("#ui-dialog-title-confirmDialog").css("color", "#D0AC22") ;
	
	buttons = {
		CANCELAR: function() {$("#confirmDialog").dialog('destroy');},
		ACEPTAR: function() {$("#confirmDialog").dialog('destroy');}
	}

	if( callback ) // Ejecuta algo al confirmar?
	{
		buttons = {
			CANCELAR: function() {$("#confirmDialog").dialog('destroy');},
			ACEPTAR: function() {$("#confirmDialog").dialog('destroy');callback.call()}
		}
	}

	$("#confirmDialog").dialog('option', 'buttons', buttons)		
}


/**
 * Presenta un popup de dialogo con un mensaje de alerta
 * @param txt Texto a mostrar
 * @param callback [opcional] Funcion a ejecutar luego de cerrar el dialogo
 */
function showAlertMsg( txt, callback )
{
	$("#warningDialog #warningTxt").html( txt );
	$("#warningDialog").dialog(dialogDefaults).dialog("open");
	// sobrescribo color del titulo
	$("#ui-dialog-title-warningDialog").css("color", "#D0AC22") ;
	
	if( callback )
	{
		$('#warningDialog').bind('dialogclose',  callback);
	}
}

/**
 * Presenta un popup de dialogo con un mensaje de error
 * @param txt Texto a mostrar
 * @param callback [opcional] Funcion a ejecutar luego de cerrar el dialogo
 */
function showErrorMsg( txt, callback )
{
	$("#errorDialog #errorTxt").html( txt );
	$("#errorDialog").dialog(dialogDefaults).dialog("open");
	// sobrescribo color del titulo
	$("#ui-dialog-title-errorDialog").css("color", "#F00A10") ; 

	if( callback )
	{
		$('#errorDialog').bind('dialogclose',  callback);
	}
}

/**
 * Loggeo javascript
 */
jQuery.log = {
	error : function( msg ) {
		if(window.console) {
			console.error(msg);
		} 
		else {
			top.status = "error: " + msg;
		}
	},
	warn : function( msg ) {
		if(window.console) {
			console.warn(msg);
		} 
		else {
			top.status = "warn: " + msg;
		}
	},
	info : function( msg ) {
		if(window.console) {
			console.info(msg);
		} 
		else {
			top.status = "info: " + msg;
		}
	}
};


function testValidDate(strDate) {
	var dPart = strDate.substring(0, 2);
	var mPart = strDate.substring(3, 5);
	var yPart = strDate.substring(6, 10);
	date = new Date(yPart, mPart - 1, dPart);
	var mes1 = date.getMonth();
	var mes2 = ((mPart) - 1);
	return (mes1 == mes2);
}



function subMenu(elm){
	
	try
	{
		if(elm == this.menuActivo && document.getElementById('sub_'+elm).style.display == 'block') {
			document.getElementById('sub_'+elm).style.display = 'none';
		} else {
			if(String(this.menuActivo).length > 1 && this.menuActivo != null){ 
				elmPadreAnterior = String(menuActivo).substring( 0,1 );
				document.getElementById('sub_'+menuActivo).style.display = 'none';
			} 
			else {
				elmPadreAnterior = menuActivo;
			}
			if(this.menuActivo != null) document.getElementById('sub_'+elmPadreAnterior).style.display = 'none';
			
			if(String(elm).length > 1 && elm != null){
				elmPadreActivo = String(elm).substring( 0,1 );
				document.getElementById('sub_'+elmPadreActivo).style.display = 'block';
			}
			if(elm != null) document.getElementById('sub_'+elm).style.display = 'block';
		}
		this.menuActivo = elm;
	}
	catch(e)
	{
		//top.status = e.message;
	}
}



//TODO Esto hay que sacarlo a otro js
/**
 * Redondea los valores de un campo con dos d�gitos decimales
 */
function redondearDecimales ( componente ) {
    var value =  componente.val();
    if(jQuery.trim(value) != '' && !isNaN(parseFloat(jQuery.trim(value)))){
         value = value.replace(",", ".");
         var floatValue = parseFloat(value).toFixed(2); 
         componente.attr("value", floatValue);
     }
}

/**
 * Chequea si el valor es entero.
 * 
 * @param value
 *            Valor a chequear.
 * @return Boolean.
 */
function isInteger(value) {
	return /^-?\d+$/.test(value);
}

function isDecimal(value) {
    return  /^[0-9]*\.?[0-9]*$/.test(value); 
}

function isPorcentaje(value) {
    if ( /^[0-9]*\.?[0-9]*$/.test(value) ) {
    	 return ((value>=0) && (value<=100));
    }
    return false;    
}

function validarDecimalPositivoONegativo(value){
	return /^-?[0-9]+(\.[0-9]{1,2})?$/.test(value);
}

/**
 * Valida si una Fecha es v�lida.
 * 
 * @param fecha -
 *            La Fecha a validar en formato dd/MM/yyyy.
 */

function esFechaValida(fecha){
    if ( fecha != "" ){
        var dia  =  parseInt(fecha.substring(0,2),10); // D�a
        var mes  =  parseInt(fecha.substring(3,5),10); // Mes
        var anio =  parseInt(fecha.substring(6),10);   // A�o
        switch(mes){
          case 1: case 3: case 5: case 7: case 8: case 10: case 12:
              numDias=31;
              break;
          case 4: case 6: case 9: case 11:
              numDias=30;
              break;
          case 2: // Si es Febrero comprueba si el anio ingresado es bisiesto.
              numDias = (( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) ? 29 : 28;
              break;
          default:
              return false;
        }
        return !(dia>numDias || dia==0 || anio==0);
      }
    return false;
}


// Valida que los campos para socio sean con el formtato 2-7-2.
// y que sean del tipo correcto
// Recibe el conjunto de campos socio.
///////// Variables para mensaje que se pisan en la jsp.
var msgFormato = "<b>El campo Socio debe ser con el formato 2-7-2.</b></br>";
var msgEntero = "<b>Los campos para Socio deben ser enteros.</b></br>";
function validarCamposSocio(nroSocioPrefijo, nroSocioPlugin, nroBeneficiario){
	var returnMessage = "";
	var nspr = jQuery.trim(nroSocioPrefijo.val());
	var nspl = jQuery.trim(nroSocioPlugin.val());
	var nb = jQuery.trim(nroBeneficiario.val());
	var socio = nspr + nspl + nb;
	var tamanio = socio.length;    
	if(11 != tamanio){
		returnMessage = msgFormato;
	}else if((!isInteger(nspr))	||(!isInteger(nspl)) || (!isInteger(nb))){
		returnMessage = msgEntero;
	}
	
	if(returnMessage.length > 0){
		nroSocioPrefijo.addClass('ui-state-error');
		nroSocioPlugin.addClass('ui-state-error');
		nroBeneficiario.addClass('ui-state-error');
	}else{		
		nroSocioPrefijo.removeClass('ui-state-error');
	    nroSocioPlugin.removeClass('ui-state-error');
	    nroBeneficiario.removeClass('ui-state-error'); 
	}

	nroSocioPrefijo.val(nspr);	        	
	nroSocioPlugin.val(nspl);	        	
	nroBeneficiario.val(nb);
	
	return returnMessage;
}

function isAlfanumericoValido(valor) {
	var retorno = true;
	var noPermitidos = "!\"�$%&/()=?�^*�:;,.�`+�'|@#�[]{<�>|\\��~";
	for (i=0; i <= noPermitidos.length-1; i++) {
		caracter = noPermitidos.substr(i,1);
		if (valor.indexOf(caracter) >= 0) {
			retorno = false;
			break;
		}
	}
	return retorno;
}

// Esta validaci�n permite numeros separados por puntos
function isIntegerWithPointsValido(valor) {
	var valido = true
    if(valor != ''){
        var regex = /^[0-9].*$/.test(valor);
        if(regex == false){
        	valido = false;
        }
    }else{
        valido = false;
    }
    return valido;
}