/**
 * 
 */

        //Para expansion de la informacion del container.
        var queues;
        var connectionFactories;
        $(document).ready(function () {

            $(".clickable").click(function () {
                toggleRaw($(this), "expandable", "flecha", "${appCtx}/images/iconos/");
            });

            $(".rawHover").hover(function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "#dfdfdf");
                $(this).children(".label").css("background", "#dfdfdf");
            }, function (eventObject) {
                $(this).children(".rawHoverTd").css("background", "transparent");
                $(this).children(".label").css("background", "transparent");
            });

            $(".flecha").attr("src", "${appCtx}/images/iconos/flechaAAbajo.png");
            $(".flecha").attr("open", "true");

            ultimaPalabraPorPuntos("className");

            queues = "<s:property value='queueList'/>";
            connectionFactories = "<s:property value='connectionFactoyNames'/>";
            queues = queues.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");
            connectionFactories = connectionFactories.replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '').split(",");

            var connectionFactoryName = "<s:property value='containerConfiguration.connectionFactoryName'/>";
            $("#connectionFactoryName").val(connectionFactoryName);

            var connectionFactoyNamesErrorQueue = "<s:property value="containerConfiguration.errorHandler.connectionFactoryName"/>";
            $("#connectionFactoyNamesErrorQueue ").val(connectionFactoyNamesErrorQueue);

            inizialize();

            var errorQueue = "<s:property value="containerConfiguration.errorHandler.queueName"/>";
            $("#errorQueue").val(errorQueue);

            var timeOutQueueName = "<s:property value="containerConfiguration.errorHandler.timeOutQueueName"/>";
            $("#timeOutQueueName").val(timeOutQueueName);

            var queueName = "<s:property value='containerConfiguration.queueName'/>";
            $("#queueName ").val(queueName);

        });
        //Fin Para expansion de la informacion del container
