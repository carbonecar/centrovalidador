package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Factory para un handler de un simple texto
 * @author MT27789605
 *
 */
public class SimpleTestResponseHandlerFactory implements IHandlerFactory {
    /*
     * (non-Javadoc)
     * @see ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler()
     */
    public PosMessageHandlerConfiguration createHandler() {
        PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.SimpleTextResponseHandler");
        return handlerConfiguration;
    }
}
