package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;

/**
 * Hanlder que realiza el logueo
 * @author MT27789605
 *
 */
public class LoggerMessageRecordHandlerFactory implements IHandlerFactory {
    /*
     * (non-Javadoc)
     * @see ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler()
     */
    public PosMessageHandlerConfiguration createHandler() {
        PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerInterceptorConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.LoggerMessageRecordHandler");
        return handlerConfiguration;
    }
}
