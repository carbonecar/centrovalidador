package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;

/**
 * Handler para realizar el matcheo
 * 
 * @author MT27789605
 * 
 */
public class MatchingResponseHandlerFactory implements IHandlerFactory {
    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler
     * ()
     */
    public PosMessageHandlerConfiguration createHandler() {
        PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerInterceptorConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.MatchingResponseHandler");
        return handlerConfiguration;
    }
}
