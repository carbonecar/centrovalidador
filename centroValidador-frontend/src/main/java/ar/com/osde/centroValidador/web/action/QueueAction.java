package ar.com.osde.centroValidador.web.action;

import java.util.List;

import ar.com.osde.centroValidador.services.pos.ContainerService;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.QueueMessage;
import com.opensymphony.xwork2.ActionSupport;

import ar.com.osde.centroValidador.services.pos.QueueService;
import ar.com.osde.framework.services.ServiceException;

public class QueueAction extends ActionSupport {

    private static final long serialVersionUID = -8776387446921583284L;

    private QueueService service;
    private ContainerService containerService;
    private List<String> queueNames;
    private ContainerConfiguration containerConfiguration;
    private List<QueueMessage> messages;
    private Long idContainer;
    private String idMessage;
    private QueueMessage queueMessage;

    public String list() throws ServiceException {

        setQueueNames(service.getAllQueueNames());

        return SUCCESS;
    }

    public String showMessagesErrorHandlerQueue() throws ServiceException {
        this.containerConfiguration = this.containerService.getContainer(idContainer);
        this.messages = this.containerService.getMessagesErrorHandlerQueue(idContainer);
        return SUCCESS;
    }

    public String removeMessageErrorHandlerQueue() throws ServiceException {
        this.queueMessage = this.containerService.removeMessageFromErrorHandlerQueue(idContainer, idMessage);
        this.containerConfiguration = this.containerService.getContainer(idContainer);
        this.messages = this.containerService.getMessagesErrorHandlerQueue(idContainer);
        return SUCCESS;
    }

    public QueueService getService() {
        return service;
    }

    public void setService(QueueService service) {
        this.service = service;
    }

    public List<String> getQueueNames() {
        return queueNames;
    }

    public void setQueueNames(List<String> queueNames) {
        this.queueNames = queueNames;
    }

    public ContainerService getContainerService() {
        return containerService;
    }

    public void setContainerService(ContainerService containerService) {
        this.containerService = containerService;
    }

    public ContainerConfiguration getContainerConfiguration() {
        return containerConfiguration;
    }

    public void setContainerConfiguration(ContainerConfiguration containerConfiguration) {
        this.containerConfiguration = containerConfiguration;
    }

    public List<QueueMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<QueueMessage> messages) {
        this.messages = messages;
    }

    public Long getIdContainer() {
        return idContainer;
    }

    public void setIdContainer(Long idContainer) {
        this.idContainer = idContainer;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public QueueMessage getQueueMessage() {
        return queueMessage;
    }

    public void setQueueMessage(QueueMessage queueMessage) {
        this.queueMessage = queueMessage;
    }
}
