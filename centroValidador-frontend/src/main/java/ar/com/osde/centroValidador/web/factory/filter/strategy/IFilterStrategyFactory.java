package ar.com.osde.centroValidador.web.factory.filter.strategy;

import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;

/**
 * Interfaz para la creaci�n de los filtros
 * 
 * @author MT27789605
 * 
 */
public interface IFilterStrategyFactory {

    /**
     * creaci�n de la estrategia de configuracion.
     * 
     * @return
     */
    public FilterStrategyConfiguration createFilterStrategy();
}
