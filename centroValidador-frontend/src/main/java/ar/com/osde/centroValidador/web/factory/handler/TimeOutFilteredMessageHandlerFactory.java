package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Factory para los mensajes de timeou
 * @author MT27789605
 *
 */
public class TimeOutFilteredMessageHandlerFactory implements IHandlerFactory {
    /*
     * (non-Javadoc)
     * @see ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler()
     */
    public PosMessageHandlerConfiguration createHandler() {
        FilteredMessageHandlerConfiguration filterHandler = new FilteredMessageHandlerConfiguration();
        filterHandler.setClassName("ar.com.osde.centroValidador.pos.handlers.TimeOutFilteredMessageHandler");
        return filterHandler;
    }
}
