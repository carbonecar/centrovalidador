package ar.com.osde.centroValidador.web.action;

import ar.com.osde.centroValidador.services.pos.PosConsumerService;

import com.opensymphony.xwork2.ActionSupport;

public class PosConsumerAction extends ActionSupport {
	private static final long serialVersionUID = 20100526L;
	private PosConsumerService service;
	private boolean started;
	
	
	public String main() {
		started = service.isStarted();
		return SUCCESS;
	}
	
	public String switchConsumer() {
		service.switchConsumer();
		started = service.isStarted();
		return SUCCESS;
	}
	
	
	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public PosConsumerService getService() {
		return service;
	}

	public void setService(PosConsumerService service) {
		this.service = service;
	}
	
}
