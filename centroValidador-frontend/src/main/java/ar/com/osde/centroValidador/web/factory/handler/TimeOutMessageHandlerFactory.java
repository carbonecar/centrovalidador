package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Factory para los mensajes de timeout
 * @author MT27789605
 *
 */
public class TimeOutMessageHandlerFactory implements IHandlerFactory {
    
    /*
     * (non-Javadoc)
     * @see ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler()
     */
    public PosMessageHandlerConfiguration createHandler() {
        PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.TimeOutMessageHandler");
        return handlerConfiguration;
    }
}
