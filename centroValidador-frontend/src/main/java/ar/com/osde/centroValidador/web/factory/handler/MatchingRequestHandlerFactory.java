package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

public class MatchingRequestHandlerFactory implements IHandlerFactory {

	public PosMessageHandlerConfiguration createHandler() {
		 PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerConfiguration();
	        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.MatchingRequestHandler");
	        return handlerConfiguration;
	}

}
