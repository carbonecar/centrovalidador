package ar.com.osde.centroValidador.web.factory.filter.strategy;

import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration;

/**
 * Creacion del filtro RouteFilterStrategyConfiguration
 * 
 * @author MT27789605
 * 
 */
public class RouteFilterStrategyFactory implements IFilterStrategyFactory {

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.web.factory.filter.strategy.
     * IFilterStrategyFactory#createFilterStrategy()
     */
    public FilterStrategyConfiguration createFilterStrategy() {
        return new RouteFilterStrategyConfiguration();
    }
}
