package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.MigracionMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;


/**
 * Factory del handler para el manejo de la migración
 * @author MT27789605
 *
 */
public class MigracionMessageHandlerFactory implements IHandlerFactory {
    
    /*
     * (non-Javadoc)
     * @see ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler()
     */
    public PosMessageHandlerConfiguration createHandler() {
        MigracionMessageHandlerConfiguration handlerConfiguration = new MigracionMessageHandlerConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.MigracionMessageHandler");
        return handlerConfiguration;
    }
}
