package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

public class FixedFormatMessageHandlerFactory implements IHandlerFactory {
    public PosMessageHandlerConfiguration createHandler() {
        PosMessageHandlerConfiguration handlerConfiguration = new PosMessageHandlerConfiguration();
        handlerConfiguration.setClassName("ar.com.osde.centroValidador.pos.handlers.FixedFormatMessageHandler");
        return handlerConfiguration;
    }
}
