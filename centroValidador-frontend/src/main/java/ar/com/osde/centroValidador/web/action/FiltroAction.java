package ar.com.osde.centroValidador.web.action;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.interceptor.SessionAware;

import ar.com.osde.centroValidador.services.pos.FilterService;
import ar.com.osde.centroValidador.services.pos.QueueService;
import ar.com.osde.centroValidador.web.factory.filter.strategy.IFilterStrategyFactory;
import ar.com.osde.centroValidador.web.utils.Utils;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("unchecked")
public class FiltroAction extends ActionSupport implements SessionAware {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Factories
     */
    private Map<String, IFilterStrategyFactory> filterStrategyFactoryMap;

    /**
     * Session
     */
    private Map<String, Object> session;
    private static final String FILTER_STRATEGY_LIST = "FILTER_STRATEGY_LIST";
    private static final String HANDLER_LIST = "HANDLER_LIST";

    /**
     * Servicios
     */
    private QueueService queueService;
    private FilterService filterService;

    /**
     * Parametros
     */
    private LinkedList<PosMessageHandlerConfiguration> handlerList;
    private String selectedItem;
    private List<String> queueList;
    private LinkedList<FilterStrategyConfiguration> filterStrategyList;
    private String routeQueue;
    private String connectionFactoryName;
    private FilterConfiguration filterConfiguration;
    private Long idFilter;
    private PropertyEqualFilterConfiguration propertyEqualFilterConfiguration;
    private FilterStrategyConfiguration filterStrategyConfiguration;
    private RouteFilterStrategyConfiguration routeFilterStrategyConfiguration;
    private List<String> connectionFactoyNames;
    private String handlersToAdd;
    private Map<String, String> propertiesToFilter;
    private Set<String> keys;
    private Long firstParentFilter;
    private long idHandler;
    private String handlerClassName;

    /**
     * *************************************************************************
     * *******************************
     * *************************************Actions
     * ************************************************************
     * *************
     * *************************************************************
     * ******************************
     */

    public FiltroAction() {

    }

    /***/
    public String addProcessHandler() throws ServiceException, BusinessException {
        PosMessageHandlerConfiguration firstHandler = Utils.createHandler(handlersToAdd,
                (List<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST));
        ProcessFilterStrategyConfiguration processStrategy = (ProcessFilterStrategyConfiguration) getLastFilterStrategyAdded();
        processStrategy.setPosMessageHandler(firstHandler);
        addInfoToFilterHandler();
        return SUCCESS;
    }

    public String showFormStrategy() throws ServiceException {
        handlerList = (LinkedList<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST);
        LinkedList<PosMessageHandlerConfiguration> handlerListAux = new LinkedList<PosMessageHandlerConfiguration>(handlerList);
        handlerListAux.removeLast();
        handlerList = handlerListAux;
        if (selectedItem.equals("ProcessFilterStrategy") && handlerList.size() <= 0) {
            throw new ServiceException("Debe poseer un handler para poder agregar al ProcessFilterStrategy");
        }
        queueList = queueService.getAllQueueNames();
        createStrategyListIfNecessary();
        addConnectionFactoryNames();
        agregarFilterStrategy(this.filterStrategyFactoryMap.get(selectedItem).createFilterStrategy());
        String selectedItemAux = this.selectedItem;
        this.addInfoToFilterHandler();
        return selectedItemAux;
    }

    public String listStrategies() {
        createStrategyListIfNecessary();
        filterStrategyList = (LinkedList<FilterStrategyConfiguration>) session.get(FILTER_STRATEGY_LIST);
        return SUCCESS;
    }

    public String createRouteStrategy() throws ServiceException {
        RouteFilterStrategyConfiguration routeFilterStrategy = (RouteFilterStrategyConfiguration) this.getLastFilterStrategyAdded();
        routeFilterStrategy.setQueueName(routeQueue);
        routeFilterStrategy.setConnectionFactoryName(connectionFactoryName);
        addInfoToFilterHandler();
        return SUCCESS;
    }

    public String editFilter() throws ServiceException {
        this.filterConfiguration = this.filterService.getFilter(this.idFilter);
        this.propertiesToFilter = Utils.convertToMap(this.filterService.getPropertiesToFilter());
        this.keys = this.propertiesToFilter.keySet();
        return SUCCESS;
    }

    public String updateFilter() throws ServiceException {
        this.filterService.updateFilter(this.propertyEqualFilterConfiguration);
        return SUCCESS;
    }

    public String editFilterStrategy() throws ServiceException {
        this.filterStrategyConfiguration = this.filterService.getFilterStrategy(this.idFilter);
        addConnectionFactoryNames();
        this.queueList = this.queueService.getAllQueueNames();
        return SUCCESS;
    }

    public String updateFilterStrategy() throws ServiceException {
        this.filterService.updateFilterStrategy(this.routeFilterStrategyConfiguration);
        return SUCCESS;
    }

    /**
     * *************************************************************************
     * *** *******************************Imp
     * interna**********************************
     * ********************************
     * ********************************************
     */

    private void addConnectionFactoryNames() throws ServiceException {
        this.connectionFactoyNames = new ArrayList<String>();
        for (String connectionFactoryName : this.queueService.getAllConnectionFactories()) {
            this.connectionFactoyNames.add("java:" + connectionFactoryName);
        }
    }

    private void addInfoToFilterHandler() {
        this.propertiesToFilter = Utils.convertToMap(this.filterService.getPropertiesToFilter());
        this.keys = this.propertiesToFilter.keySet();
        PosMessageHandlerConfiguration posMessageHandlerConfiguration = ((LinkedList<PosMessageHandlerConfiguration>) this.session
                .get(HANDLER_LIST)).getLast();
        String className = posMessageHandlerConfiguration.getClassName();
        String[] classNameSplit = className.split("\\.");
        this.selectedItem = classNameSplit[classNameSplit.length - 1];
    }

    private void agregarFilterStrategy(FilterStrategyConfiguration filterConfiguration) {
        createStrategyListIfNecessary();
        filterStrategyList = ((LinkedList<FilterStrategyConfiguration>) session.get(FILTER_STRATEGY_LIST));
        filterStrategyList.add(filterConfiguration);
    }

    private FilterStrategyConfiguration getLastFilterStrategyAdded() {
        filterStrategyList = (LinkedList<FilterStrategyConfiguration>) session.get(FILTER_STRATEGY_LIST);
        return filterStrategyList.getLast();
    }

    private void createStrategyListIfNecessary() {
        if (!session.containsKey(FILTER_STRATEGY_LIST)) {
            session.put(FILTER_STRATEGY_LIST, new LinkedList<FilterStrategyConfiguration>());
        }
    }

    /**
     * *************************************************************************
     * **** ****************************Getter and
     * Setter********************************
     * ***********************************
     * ******************************************
     */

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, IFilterStrategyFactory> getFilterStrategyFactoryMap() {
        return filterStrategyFactoryMap;
    }

    public void setFilterStrategyFactoryMap(Map<String, IFilterStrategyFactory> filterStrategyFactoryMap) {
        this.filterStrategyFactoryMap = filterStrategyFactoryMap;
    }

    public QueueService getQueueService() {
        return queueService;
    }

    public void setQueueService(QueueService queueService) {
        this.queueService = queueService;
    }

    public FilterService getFilterService() {
        return filterService;
    }

    public void setFilterService(FilterService filterService) {
        this.filterService = filterService;
    }

    public LinkedList<PosMessageHandlerConfiguration> getHandlerList() {
        return handlerList;
    }

    public void setHandlerList(LinkedList<PosMessageHandlerConfiguration> handlerList) {
        this.handlerList = handlerList;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }

    public List<String> getQueueList() throws ServiceException {
        if (this.queueList == null) {
            this.queueList = this.getQueueService().getAllQueueNames();
        }
        return queueList;
    }

    public void setQueueList(List<String> queueList) {
        this.queueList = queueList;
    }

    public LinkedList<FilterStrategyConfiguration> getFilterStrategyList() {
        return filterStrategyList;
    }

    public void setFilterStrategyList(LinkedList<FilterStrategyConfiguration> filterStrategyList) {
        this.filterStrategyList = filterStrategyList;
    }

    public String getRouteQueue() {
        return routeQueue;
    }

    public void setRouteQueue(String routeQueue) {
        this.routeQueue = routeQueue;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    public FilterConfiguration getFilterConfiguration() {
        return filterConfiguration;
    }

    public void setFilterConfiguration(FilterConfiguration filterConfiguration) {
        this.filterConfiguration = filterConfiguration;
    }

    public Long getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(Long idFilter) {
        this.idFilter = idFilter;
    }

    public PropertyEqualFilterConfiguration getPropertyEqualFilterConfiguration() {
        return propertyEqualFilterConfiguration;
    }

    public void setPropertyEqualFilterConfiguration(PropertyEqualFilterConfiguration propertyEqualFilterConfiguration) {
        this.propertyEqualFilterConfiguration = propertyEqualFilterConfiguration;
    }

    public FilterStrategyConfiguration getFilterStrategyConfiguration() {
        return filterStrategyConfiguration;
    }

    public void setFilterStrategyConfiguration(FilterStrategyConfiguration filterStrategyConfiguration) {
        this.filterStrategyConfiguration = filterStrategyConfiguration;
    }

    public RouteFilterStrategyConfiguration getRouteFilterStrategyConfiguration() {
        return routeFilterStrategyConfiguration;
    }

    public void setRouteFilterStrategyConfiguration(RouteFilterStrategyConfiguration routeFilterStrategyConfiguration) {
        this.routeFilterStrategyConfiguration = routeFilterStrategyConfiguration;
    }

    public List<String> getConnectionFactoyNames() {
        return connectionFactoyNames;
    }

    public void setConnectionFactoyNames(List<String> connectionFactoyNames) {
        this.connectionFactoyNames = connectionFactoyNames;
    }

    public String getHandlersToAdd() {
        return handlersToAdd;
    }

    public void setHandlersToAdd(String handlersToAdd) {
        this.handlersToAdd = handlersToAdd;
    }

    public Map<String, String> getPropertiesToFilter() {
        return propertiesToFilter;
    }

    public void setPropertiesToFilter(Map<String, String> propertiesToFilter) {
        this.propertiesToFilter = propertiesToFilter;
    }

    public Set<String> getKeys() {
        return keys;
    }

    public void setKeys(Set<String> keys) {
        this.keys = keys;
    }

    public Long getFirstParentFilter() {
        return firstParentFilter;
    }

    public void setFirstParentFilter(Long firstParentFilter) {
        this.firstParentFilter = firstParentFilter;
    }

    public long getIdHandler() {
        return idHandler;
    }

    public void setIdHandler(long idHandler) {
        this.idHandler = idHandler;
    }

    public String getHandlerClassName() {
        return handlerClassName;
    }

    public void setHandlerClassName(String handlerClassName) {
        this.handlerClassName = handlerClassName;
    }
}
