package ar.com.osde.centroValidador.web.factory.filter.strategy;

import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;

/**
 * 
 * @author MT27789605
 * 
 */
public class NullFilterStrategyFactory implements IFilterStrategyFactory {

    /*
     * (non-Javadoc)
     * 
     * @see ar.com.osde.centroValidador.web.factory.filter.strategy.
     * IFilterStrategyFactory#createFilterStrategy()
     */
    public FilterStrategyConfiguration createFilterStrategy() {
        return new FilterStrategyConfiguration();
    }
}
