package ar.com.osde.centroValidador.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class TokenMementoFilter implements Filter {

    private String mementoKey;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        if(request.getParameter(this.mementoKey) != null) {
            HttpSession session = request.getSession();
            session.setAttribute(this.mementoKey, request.getParameter(this.mementoKey));
        }
        chain.doFilter(req, res);
    }

    public void init(FilterConfig arg0) throws ServletException {

    }

    public void destroy() {

    }

    public void setMementoKey(String mementoKey) {
        this.mementoKey = mementoKey;
    }

    public String getMementoKey() {
        return mementoKey;
    }

}
