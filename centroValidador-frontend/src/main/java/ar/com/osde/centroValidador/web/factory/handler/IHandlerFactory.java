package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Intefaz para la creaci�n de los handlers
 * @author MT27789605
 *
 */
public interface IHandlerFactory {

    /**
     * creacion del handler
     * @return
     */
    public PosMessageHandlerConfiguration createHandler();
}
