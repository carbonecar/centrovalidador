package ar.com.osde.centroValidador.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.interceptor.SessionAware;

import ar.com.osde.centroValidador.services.pos.CodigoExceptuableService;
import ar.com.osde.centroValidador.services.pos.ContainerService;
import ar.com.osde.centroValidador.services.pos.FilterService;
import ar.com.osde.centroValidador.services.pos.HandlerService;
import ar.com.osde.centroValidador.services.pos.QueueService;
import ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory;
import ar.com.osde.centroValidador.web.utils.Utils;
import ar.com.osde.entities.AbstractResponseMessageHandlerConfiguration;
import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.MigracionMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.framework.services.ServiceException;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("unchecked")
public class HandlerAction extends ActionSupport implements SessionAware {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Factories
	 */
	private transient Map<String, IHandlerFactory> handlerFactoryMap;

	/**
	 * Session
	 */
	private transient Map<String, Object> session;
	private static final String FILTER_STRATEGY_LIST = "FILTER_STRATEGY_LIST";
	private static final String HANDLER_LIST = "HANDLER_LIST";
	private static final String CONTAINER = "CONTAINER";
	private static final String REPLACE_EXISTING_FILTER = "replaceExistingFilter";
	private static final String BACK_ID_HANDLER = "backIdHandler";

	private static final String SIN_GRUPO = "Sin Grupo";

	/**
	 * Servicios
	 */
	private transient ContainerService containerService;
	private transient QueueService queueService;
	private transient HandlerService handlerService;
	private transient FilterService filterService;
	private transient CodigoExceptuableService codigoExceptuableService;

	/**
	 * Parametros
	 */
	private String routeQueue;
	private String connectionFactoryName;
	private List<Long> codigosExceptuablesSeleccionados;
	private PosMessageHandlerConfiguration posMessageHandler;
	private long idHandler;
	private LinkedList<PosMessageHandlerConfiguration> handlerList;
	private String handlerClassName;
	private List<String> queueList;
	private AbstractResponseMessageHandlerConfiguration abstractResponseMessageHandler;
	private String queueName;
	private List<String> handlerTypes;
	private ContainerConfiguration containerConfiguration;
	private List<String> connectionFactoyNames;
	private String expresionFiltro;
	private String selectedItem;
	private List<CodigoExceptuable> codigosExceptuables;
	private transient Map<String, String> propertiesToFilter;
	private Set<String> keys;
	private boolean withoutGroupName;
	private String groupName;
	private long idContainer;
	private String replaceExistingFilter;

	/**
	 * *************************************************************************
	 ******************************** ACTIONS ***********************************
	 * *************************************************************************
	 */

	/**
	 * Agrega el hanlder a la session y redirecciona a la p�gina correcta para
	 * cargar la informacion restante para configurar el handler seleccionado.
	 */
	public String showForm() throws ServiceException {
		session.put(REPLACE_EXISTING_FILTER, replaceExistingFilter);
		session.put(BACK_ID_HANDLER, this.idHandler);
		int lastIndex = selectedItem.lastIndexOf('.');
		String handlerType = selectedItem.substring(lastIndex + 1);
		addConnectionFactoryNames();
		IHandlerFactory handlerFactory = this.handlerFactoryMap.get(handlerType);
		if (handlerFactory != null) {
			agregarHandler(handlerFactory.createHandler());
		}
		this.setCodigosExceptuables(this.codigoExceptuableService.getAllCodigos());
		this.propertiesToFilter = Utils.convertToMap(this.filterService.getPropertiesToFilter());
		this.keys = this.propertiesToFilter.keySet();
		this.selectedItem = handlerType;
		return handlerType;
	}

	public String isReplaceExistingFilter() {
		return replaceExistingFilter;
	}

	public void setReplaceExistingFilter(String replaceExistingFilter) {
		this.replaceExistingFilter = replaceExistingFilter;
	}

	/***/
	public String clearSession() throws ServiceException {
		this.session.clear();
		return SUCCESS;
	}

	public String createAbstractResponseMessageHandler() {
		AbstractResponseMessageHandlerConfiguration routeHandler = (AbstractResponseMessageHandlerConfiguration) this
		        .getLastHandlerAdded();
		routeHandler.setQueueName(routeQueue);
		routeHandler.setConnectionFactoryName(connectionFactoryName);
		return SUCCESS;
	}

	public String createFilterHandler() throws ServiceException {
		this.agregarInformacionFilterHandler();
		if (REPLACE_EXISTING_FILTER.equals(this.session.get(REPLACE_EXISTING_FILTER))) {
			this.idHandler = (Long) this.session.get(BACK_ID_HANDLER);
			return "replaceExistingFilter";
		}
		return SUCCESS;
	}

	public String createMigracionHandler() throws Exception {
		MigracionMessageHandlerConfiguration handlerConfiguration = (MigracionMessageHandlerConfiguration) this
		        .getLastHandlerAdded();

		Set<CodigoExceptuable> codigos = new HashSet<CodigoExceptuable>();
		List<CodigoExceptuable> allCodigos = this.codigoExceptuableService.getAllCodigos();
		if (this.codigosExceptuablesSeleccionados != null) {
			for (Long id : this.codigosExceptuablesSeleccionados) {
				for (CodigoExceptuable codigoExceptuable : allCodigos) {
					if (codigoExceptuable.getId() == id) {
						codigos.add(codigoExceptuable);
					}
				}

			}
		}
		handlerConfiguration.setCodigosExceptuar(codigos);
		return SUCCESS;
	}

	public String findHanlderById() throws ServiceException {
		List<PosMessageHandlerConfiguration> handlerSessionList = (List<PosMessageHandlerConfiguration>) session
		        .get(HANDLER_LIST);
		this.posMessageHandler = handlerSessionList.get(Long.valueOf(this.idHandler).intValue());
		return SUCCESS;
	}

	public String removeHandlerFromList() {
		handlerList = (LinkedList<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST);
		handlerList.remove(Long.valueOf(this.idHandler).intValue());
		return SUCCESS;
	}

	public String editHandler() throws ServiceException {
		String className = this.handlerClassName;
		String[] split = className.split("\\.");
		return split[split.length - 1];
	}

	public String editPosMessageHandler() throws ServiceException {
		this.posMessageHandler = this.handlerService.getHandler(this.idHandler);
		queueList = queueService.getAllQueueNames();
		addConnectionFactoryNames();
		return SUCCESS;
	}

	public String editAbstractResponseMessageHandler() throws ServiceException {
		this.abstractResponseMessageHandler = (AbstractResponseMessageHandlerConfiguration) this.handlerService
		        .getHandler(this.idHandler);
		queueList = queueService.getAllQueueNames();
		addConnectionFactoryNames();
		return SUCCESS;
	}

	public String updateAbstractResponseMessageHandler() throws ServiceException {
		String response = "redirectHandler";
		PosMessageHandlerConfiguration handlerToUpdate = this.handlerService.getHandler(this.idHandler);
		PosMessageHandlerConfiguration parentHandler = this.handlerService.getParentHandler(this.idHandler);
		if (handlerToUpdate instanceof AbstractResponseMessageHandlerConfiguration) {
			this.abstractResponseMessageHandler = (AbstractResponseMessageHandlerConfiguration) handlerToUpdate;
			if (this.connectionFactoryName != null) {
				this.abstractResponseMessageHandler.setConnectionFactoryName(this.connectionFactoryName);
			}
			if (this.queueName != null) {
				this.abstractResponseMessageHandler.setQueueName(queueName);
			}
			this.handlerService.updateHandler(this.abstractResponseMessageHandler);
		} else {
			if (REPLACE_EXISTING_FILTER.equals(this.session.get(REPLACE_EXISTING_FILTER))) {
				FilteredMessageHandlerConfiguration oldFilteredHandler = (FilteredMessageHandlerConfiguration) ((PosMessageHandlerInterceptorConfiguration) parentHandler)
				        .getPosMessageHandler();
				FilteredMessageHandlerConfiguration filteredHandlerNew = (FilteredMessageHandlerConfiguration) this
				        .getLastHandlerAdded();

				this.handlerService.updateFilteredHandler(oldFilteredHandler, filteredHandlerNew);
			}
		}

		// si tiene un padre, configuro las propiedades para que el redirect sea
		// al padre. Si no vuelvo al container.
		if (parentHandler != null) {
			this.handlerClassName = parentHandler.getClassName();
			this.idHandler = parentHandler.getId();
			response = "redirectHandler";
		} else {
			ContainerConfiguration parentContainer = this.containerService.getParentContainer(this.idHandler);
			if (parentContainer != null) {
				this.idContainer = parentContainer.getId();
			}
			response = "redirectContainer";
		}

		return response;
	}

	public String updateHandlerTypes() {
		if (!session.containsKey(CONTAINER)) {
			if (withoutGroupName)
				this.groupName = SIN_GRUPO;
			this.containerConfiguration.setGroupName(this.groupName);
			session.put(CONTAINER, this.containerConfiguration);
		}
		handlerTypes = handlerService.getHandlerTypes();
		return SUCCESS;
	}

	/**
	 * *************************************************************************
	 * *** *******************************Imp
	 * interna**********************************
	 * ********************************
	 * ********************************************
	 */

	private void createStrategyListIfNecessary() {
		if (!session.containsKey(FILTER_STRATEGY_LIST)) {
			session.put(FILTER_STRATEGY_LIST, new LinkedList<FilterStrategyConfiguration>());
		}
	}

	private void addConnectionFactoryNames() throws ServiceException {
		this.connectionFactoyNames = new ArrayList<String>();
		for (String connectionFactoryName : this.queueService.getAllConnectionFactories()) {
			this.connectionFactoyNames.add("java:" + connectionFactoryName);
		}
	}

	private PosMessageHandlerConfiguration getLastHandlerAdded() {
		handlerList = (LinkedList<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST);
		return handlerList.getLast();
	}

	private void agregarInformacionFilterHandler() throws ServiceException {
		createStrategyListIfNecessary();

		FilteredMessageHandlerConfiguration filterHandler = (FilteredMessageHandlerConfiguration) this
		        .getLastHandlerAdded();
		filterHandler.setFilterStrategyList((List<FilterStrategyConfiguration>) session.get(FILTER_STRATEGY_LIST));

		List<FilterConfiguration> filters = filterService.createFilter(expresionFiltro);
		filterHandler.setFilters(filters);

		session.remove(FILTER_STRATEGY_LIST);
	}

	private void agregarHandler(PosMessageHandlerConfiguration handler) {
		LinkedList<PosMessageHandlerConfiguration> handlerSessionList;
		if (session.containsKey(HANDLER_LIST)
		        && !REPLACE_EXISTING_FILTER.equals(this.session.get(REPLACE_EXISTING_FILTER))) {
			handlerSessionList = (LinkedList<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST);
			handlerSessionList.add(handler);
		} else {
			handlerSessionList = new LinkedList<PosMessageHandlerConfiguration>();
			handlerSessionList.add(handler);
			session.put(HANDLER_LIST, handlerSessionList);
		}
		handlerList = handlerSessionList;
	}

	/**
	 * **********************************************************
	 * ******************Getter and Setter***********************
	 * *********************************************************
	 */

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, IHandlerFactory> getHandlerFactoryMap() {
		return handlerFactoryMap;
	}

	public void setHandlerFactoryMap(Map<String, IHandlerFactory> handlerFactoryMap) {
		this.handlerFactoryMap = handlerFactoryMap;
	}

	public ContainerService getContainerService() {
		return containerService;
	}

	public void setContainerService(ContainerService containerService) {
		this.containerService = containerService;
	}

	public QueueService getQueueService() {
		return queueService;
	}

	public void setQueueService(QueueService queueService) {
		this.queueService = queueService;
	}

	public HandlerService getHandlerService() {
		return handlerService;
	}

	public void setHandlerService(HandlerService handlerService) {
		this.handlerService = handlerService;
	}

	public FilterService getFilterService() {
		return filterService;
	}

	public void setFilterService(FilterService filterService) {
		this.filterService = filterService;
	}

	public CodigoExceptuableService getCodigoExceptuableService() {
		return codigoExceptuableService;
	}

	public void setCodigoExceptuableService(CodigoExceptuableService codigoExceptuableService) {
		this.codigoExceptuableService = codigoExceptuableService;
	}

	public String getRouteQueue() {
		return routeQueue;
	}

	public void setRouteQueue(String routeQueue) {
		this.routeQueue = routeQueue;
	}

	public String getConnectionFactoryName() {
		return connectionFactoryName;
	}

	public void setConnectionFactoryName(String connectionFactoryName) {
		this.connectionFactoryName = connectionFactoryName;
	}

	public List<Long> getCodigosExceptuablesSeleccionados() {
		return codigosExceptuablesSeleccionados;
	}

	public void setCodigosExceptuablesSeleccionados(List<Long> codigosExceptuablesSeleccionados) {
		this.codigosExceptuablesSeleccionados = codigosExceptuablesSeleccionados;
	}

	public PosMessageHandlerConfiguration getPosMessageHandler() {
		return posMessageHandler;
	}

	public void setPosMessageHandler(PosMessageHandlerConfiguration posMessageHandler) {
		this.posMessageHandler = posMessageHandler;
	}

	public long getIdHandler() {
		return idHandler;
	}

	public void setIdHandler(long idHandler) {
		this.idHandler = idHandler;
	}

	public LinkedList<PosMessageHandlerConfiguration> getHandlerList() {
		return handlerList;
	}

	public void setHandlerList(LinkedList<PosMessageHandlerConfiguration> handlerList) {
		this.handlerList = handlerList;
	}

	public String getHandlerClassName() {
		return handlerClassName;
	}

	public void setHandlerClassName(String handlerClassName) {
		this.handlerClassName = handlerClassName;
	}

	public List<String> getQueueList() throws ServiceException {
		if (this.queueList == null) {
			this.queueList = this.getQueueService().getAllQueueNames();
		}
		return queueList;
	}

	public void setQueueList(List<String> queueList) {
		this.queueList = queueList;
	}

	public AbstractResponseMessageHandlerConfiguration getAbstractResponseMessageHandler() {
		return abstractResponseMessageHandler;
	}

	public void setAbstractResponseMessageHandler(
	        AbstractResponseMessageHandlerConfiguration abstractResponseMessageHandler) {
		this.abstractResponseMessageHandler = abstractResponseMessageHandler;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public List<String> getHandlerTypes() {
		return handlerTypes;
	}

	public void setHandlerTypes(List<String> handlerTypes) {
		this.handlerTypes = handlerTypes;
	}

	public ContainerConfiguration getContainerConfiguration() {
		return containerConfiguration;
	}

	public void setContainerConfiguration(ContainerConfiguration containerConfiguration) {
		this.containerConfiguration = containerConfiguration;
	}

	public List<String> getConnectionFactoyNames() {
		return connectionFactoyNames;
	}

	public void setConnectionFactoyNames(List<String> connectionFactoyNames) {
		this.connectionFactoyNames = connectionFactoyNames;
	}

	public String getExpresionFiltro() {
		return expresionFiltro;
	}

	public void setExpresionFiltro(String expresionFiltro) {
		this.expresionFiltro = expresionFiltro;
	}

	public String getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<CodigoExceptuable> getCodigosExceptuables() {
		return codigosExceptuables;
	}

	public void setCodigosExceptuables(List<CodigoExceptuable> codigosExceptuables) {
		this.codigosExceptuables = codigosExceptuables;
	}

	public Map<String, String> getPropertiesToFilter() {
		return propertiesToFilter;
	}

	public void setPropertiesToFilter(Map<String, String> propertiesToFilter) {
		this.propertiesToFilter = propertiesToFilter;
	}

	public Set<String> getKeys() {
		return keys;
	}

	public void setKeys(Set<String> keys) {
		this.keys = keys;
	}

	public boolean isWithoutGroupName() {
		return withoutGroupName;
	}

	public void setWithoutGroupName(boolean withoutGroupName) {
		this.withoutGroupName = withoutGroupName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public long getIdContainer() {
		return idContainer;
	}

	public void setIdContainer(long idContainer) {
		this.idContainer = idContainer;
	}
}
