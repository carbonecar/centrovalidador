package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.AbstractResponseMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;

public class StreamResponseMessageHandlerFactory implements IHandlerFactory {
    public PosMessageHandlerConfiguration createHandler() {
        AbstractResponseMessageHandlerConfiguration streamResponseHandler = new AbstractResponseMessageHandlerConfiguration();
        streamResponseHandler.setClassName("ar.com.osde.centroValidador.pos.handlers.StreamResponseMessageHandler");
        return streamResponseHandler;
    }
}
