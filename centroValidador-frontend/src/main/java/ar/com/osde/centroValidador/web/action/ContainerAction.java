package ar.com.osde.centroValidador.web.action;

import java.util.*;

import ar.com.osde.centroValidador.services.pos.*;
import ar.com.osde.centroValidador.web.factory.filter.strategy.IFilterStrategyFactory;
import ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory;
import ar.com.osde.centroValidador.web.utils.Utils;
import ar.com.osde.entities.*;
import ar.com.osde.entities.filter.PropertyEqualFilterConfiguration;
import org.apache.struts2.interceptor.SessionAware;

import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.ProcessFilterStrategyConfiguration;
import ar.com.osde.entities.filter.strategy.RouteFilterStrategyConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("unchecked")
public class ContainerAction extends ActionSupport implements SessionAware {

    private static final long serialVersionUID = 9205434389199405559L;

    /**
     * Session
     */
    private Map<String, Object> session;
    private static final String HANDLER_LIST = "HANDLER_LIST";
    private static final String CONTAINER = "CONTAINER";

    /**
     * Servicios
     */
    private ContainerService containerService;
    private QueueService queueService;
    private FilterService filterService;

    /**
     * Parametros
     */
    private ContainerConfiguration containerConfiguration;
    private List<ContainerConfiguration> containerList;
    private List<String> connectionFactoyNames;
    private List<String> queueList;
    private Long idContainer;
    private String descripcion;
    private String connectionFactoryName;
    private String queueName;
    private ErrorHandlerConfiguration errorHandler;
    private Integer numberOfConcurrentConsumers;
    private Boolean startOnLoad;
    private String selectedItem;
    private String handlersToAdd;
    private HashMap<String, List<ContainerConfiguration>> containerConfigurationMap;
    private Set<String> containerConfigurationMapSet;
    private String groupName;

    /**
     * *************************************************************************
     * *******************************
     * *************************************Actions
     * ************************************************************
     * *************
     * *************************************************************
     * ******************************
     */

    /***/
    public String clearSession() throws ServiceException {
        this.session.clear();
        return list();
    }

    /**
     * Crea el container especificado con los handlers que se indicaron
     * 
     * @throws ServiceException
     * @throws BusinessException
     */
    public String createContainer() throws ServiceException, BusinessException {
        PosMessageHandlerConfiguration firstHandler = Utils.createHandler(handlersToAdd,
                (List<PosMessageHandlerConfiguration>) session.get(HANDLER_LIST));
        ContainerConfiguration container = (ContainerConfiguration) session.get(CONTAINER);
        container.setPosMessageHandler(firstHandler);
        getContainerService().saveContainer(container);
        session.remove(HANDLER_LIST);
        session.remove(CONTAINER);
        return list();
    }

    public String newContainer() throws ServiceException {
        this.queueList = queueService.getAllQueueNames();
        addConnectionFactoryNames();
        return SUCCESS;
    }

    public String cloneContainer() throws ServiceException {
        this.containerService.cloneContainer(this.idContainer);
        return list();
    }

    public String editContainer() throws ServiceException {
        this.containerConfiguration = this.containerService.getContainer(this.idContainer);
        queueList = queueService.getAllQueueNames();
        addConnectionFactoryNames();
        return SUCCESS;
    }

    public String updateContainer() throws ServiceException {
        this.containerConfiguration = this.containerService.getContainer(this.idContainer);
        if (this.descripcion != null)
            this.containerConfiguration.setContainerDescription(this.descripcion);
        if (this.connectionFactoryName != null)
            this.containerConfiguration.setConnectionFactoryName(this.connectionFactoryName);
        if (this.queueName != null)
            this.containerConfiguration.setQueueName(queueName);
        if (this.errorHandler != null)
            this.containerConfiguration.setErrorHandler(this.errorHandler);
        if (this.numberOfConcurrentConsumers != null)
            this.containerConfiguration.setNumberOfConcurrentConsumers(this.numberOfConcurrentConsumers);
        if (this.startOnLoad != null)
            this.containerConfiguration.setStartOnLoad(this.startOnLoad);
        if (this.groupName != null)
            this.containerConfiguration.setGroupName(groupName);
        this.containerService.updateContainer(this.containerConfiguration);
        return this.list();
    }

    public String removeContainer() throws ServiceException {
        containerService.deleteContainer(idContainer);
        return list();
    }

    public String reloadAllContainers() throws ServiceException {
        this.containerService.reloadAllContainers();
        return list();
    }

    public String list() throws ServiceException {
        List<ContainerConfiguration> list = getContainerService().getAllContainers();
        this.containerConfigurationMap = new HashMap<String, List<ContainerConfiguration>>();
        if (list != null) {
            for (ContainerConfiguration container : list) {
                addToContainerMap(container.getGroupName(), container);
            }
        }
        this.containerConfigurationMapSet = this.containerConfigurationMap.keySet();
        return SUCCESS;
    }

    public String startContainer() throws ServiceException {
        containerService.startContainer(idContainer);
        return list();
    }

    public String stopContainer() throws ServiceException {
        containerService.stopContainer(idContainer);
        return list();
    }

    public String startGroup() throws Exception {
        if (groupName != null)
            this.containerService.startContainersWithGroupName(groupName);
        return list();
    }

    public String stopGroup() throws Exception {
        if (groupName != null)
            this.containerService.stopContainersWithGroupName(groupName);
        return list();
    }

    /**
     * *************************************************************************
     * *** *******************************Imp
     * interna**********************************
     * ********************************
     * ********************************************
     */

    private void addToContainerMap(String groupName, ContainerConfiguration container) {
        List<ContainerConfiguration> containers = this.containerConfigurationMap.get(groupName);
        if (containers == null) {
            containers = new ArrayList<ContainerConfiguration>();
            this.containerConfigurationMap.put(groupName, containers);
        }
        containers.add(container);
    }

    private void addConnectionFactoryNames() throws ServiceException {
        this.connectionFactoyNames = new ArrayList<String>();
        for (String connectionFactoryName : this.queueService.getAllConnectionFactories()) {
            this.connectionFactoyNames.add("java:" + connectionFactoryName);
        }
    }

    public HashMap<String, List<ContainerConfiguration>> getContainerConfigurationMap() {
        return containerConfigurationMap;
    }

    public void setContainerConfigurationMap(HashMap<String, List<ContainerConfiguration>> containerConfigurationMap) {
        this.containerConfigurationMap = containerConfigurationMap;
    }

    public Set<String> getContainerConfigurationMapSet() {
        return containerConfigurationMapSet;
    }

    public void setContainerConfigurationMapSet(Set<String> containerConfigurationMapSet) {
        this.containerConfigurationMapSet = containerConfigurationMapSet;
    }

    /**
     * *************************************************************************
     * **** ****************************Getter and
     * Setter********************************
     * ***********************************
     * ******************************************
     */

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public ContainerService getContainerService() {
        return containerService;
    }

    public void setContainerService(ContainerService containerService) {
        this.containerService = containerService;
    }

    public QueueService getQueueService() {
        return queueService;
    }

    public void setQueueService(QueueService queueService) {
        this.queueService = queueService;
    }

    public FilterService getFilterService() {
        return filterService;
    }

    public void setFilterService(FilterService filterService) {
        this.filterService = filterService;
    }

    public ContainerConfiguration getContainerConfiguration() {
        return containerConfiguration;
    }

    public void setContainerConfiguration(ContainerConfiguration containerConfiguration) {
        this.containerConfiguration = containerConfiguration;
    }

    public List<String> getConnectionFactoyNames() {
        return connectionFactoyNames;
    }

    public void setConnectionFactoyNames(List<String> connectionFactoyNames) {
        this.connectionFactoyNames = connectionFactoyNames;
    }

    public List<String> getQueueList() throws ServiceException {
        if (this.queueList == null) {
            this.queueList = this.getQueueService().getAllQueueNames();
        }
        return queueList;
    }

    public void setQueueList(List<String> queueList) {
        this.queueList = queueList;
    }

    public Long getIdContainer() {
        return idContainer;
    }

    public void setIdContainer(Long idContainer) {
        this.idContainer = idContainer;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getConnectionFactoryName() {
        return connectionFactoryName;
    }

    public void setConnectionFactoryName(String connectionFactoryName) {
        this.connectionFactoryName = connectionFactoryName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public ErrorHandlerConfiguration getErrorHandler() {
        return errorHandler;
    }

    public void setErrorHandler(ErrorHandlerConfiguration errorHandler) {
        this.errorHandler = errorHandler;
    }

    public Integer getNumberOfConcurrentConsumers() {
        return numberOfConcurrentConsumers;
    }

    public void setNumberOfConcurrentConsumers(Integer numberOfConcurrentConsumers) {
        this.numberOfConcurrentConsumers = numberOfConcurrentConsumers;
    }

    public Boolean getStartOnLoad() {
        return startOnLoad;
    }

    public void setStartOnLoad(Boolean startOnLoad) {
        this.startOnLoad = startOnLoad;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }

    public String getHandlersToAdd() {
        return handlersToAdd;
    }

    public void setHandlersToAdd(String handlersToAdd) {
        this.handlersToAdd = handlersToAdd;
    }

    public void setContainerList(List<ContainerConfiguration> containerList) {
        this.containerList = containerList;
    }

    public List<ContainerConfiguration> getContainerList() {
        return containerList;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
