package ar.com.osde.centroValidador.web.utils;

import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerInterceptorConfiguration;
import ar.com.osde.framework.business.exception.BusinessException;
import ar.com.osde.framework.services.ServiceException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Utils {

    private Utils() {

    }

    public static PosMessageHandlerConfiguration createHandler(String handlersToAdd, List<PosMessageHandlerConfiguration> handlerList)
            throws ServiceException, BusinessException {
        StringTokenizer tokenizer = new StringTokenizer(handlersToAdd, ",");
        if (handlerList.isEmpty() || !tokenizer.hasMoreElements()) {
            throw new ServiceException("La lista de handlers a agregarle al container no debe ser nula");
        }
        int handlerSelected = Integer.valueOf(tokenizer.nextToken().trim());
        PosMessageHandlerConfiguration firstHandler = handlerList.get(handlerSelected);
        PosMessageHandlerConfiguration lastHandler = firstHandler;
        while (tokenizer.hasMoreTokens()) {
            handlerSelected = Integer.valueOf(tokenizer.nextToken().trim());

            if (lastHandler instanceof PosMessageHandlerInterceptorConfiguration) {
                PosMessageHandlerConfiguration actualHandler = handlerList.get(handlerSelected);

                ((PosMessageHandlerInterceptorConfiguration) lastHandler).setPosMessageHandler(actualHandler);
                lastHandler = actualHandler;
            } else {
                throw new BusinessException("Error en la creacion de los handlers: " + lastHandler.getClassName()
                        + " se configuro como handler y no puede ser ser encandenado. ");
            }
        }
        return firstHandler;
    }

    public static Map<String, String> convertToMap(List<String> listaConDosPuntos) {
        Map<String, String> map = new HashMap<String, String>();
        for (String valor : listaConDosPuntos) {
            String[] valorSeparado = valor.split(":");
            if (valorSeparado.length > 1) {
                map.put(valorSeparado[0], valorSeparado[1]);
            }
        }
        return map;
    }
}
