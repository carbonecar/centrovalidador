package ar.com.osde.centroValidador.web.factory.handler;

import ar.com.osde.entities.AbstractResponseMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;

/**
 * Factory para el handler que rutea siempre
 * @author MT27789605
 * 
 */
public class AlwaysRouteMessageHandlerFactory implements IHandlerFactory {
    /*
     * (non-Javadoc)
     * 
     * @see
     * ar.com.osde.centroValidador.web.factory.handler.IHandlerFactory#createHandler
     * ()
     */
    public PosMessageHandlerConfiguration createHandler() {
        AbstractResponseMessageHandlerConfiguration routeHandler = new AbstractResponseMessageHandlerConfiguration();
        routeHandler.setClassName("ar.com.osde.centroValidador.pos.handlers.AlwaysRouteMessageHandler");
        return routeHandler;
    }
}
