package ar.com.osde.centroValidador.services.pos;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ar.com.osde.entities.FilteredMessageHandlerConfiguration;
import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

@WebService(name = "HandlerService", targetNamespace = "http://osde.com.ar/services/handler/HandlerService")
public interface HandlerService extends Service {

    @WebMethod
    List<String> getHandlerTypes();

    @WebMethod
    PosMessageHandlerConfiguration save(@WebParam(name = "handler") PosMessageHandlerConfiguration messageHandlerConfiguration)
            throws ServiceException;

    @WebMethod
    PosMessageHandlerConfiguration getHandler(@WebParam(name = "idHandler") long idHandler) throws ServiceException;

    @WebMethod
    PosMessageHandlerConfiguration getParentHandler(@WebParam(name = "idHandler") long idHandler) throws ServiceException;

    @WebMethod
    void updateHandler(@WebParam(name = "posMessageHandler") PosMessageHandlerConfiguration posMessageHandler)
            throws ServiceException;

    @WebMethod
    void updateHandlerOnMemory(@WebParam(name = "handler") PosMessageHandlerConfiguration handler) throws ServiceException;

    @WebMethod
    void updateFilteredHandler(@WebParam(name = "oldFilter") FilteredMessageHandlerConfiguration oldFilter,
            @WebParam(name = "newFilter") FilteredMessageHandlerConfiguration newFilter) throws ServiceException;

    @WebMethod
    PosMessageHandlerConfiguration getParentHandlerForFilter(@WebParam(name = "id") Long idFilter) throws ServiceException;

    @WebMethod
    PosMessageHandlerConfiguration getParentHandlerForFilterStrategy(@WebParam(name = "id") Long idFilter) throws ServiceException;

    @WebMethod
    void deleteHandler(@WebParam(name = "posMessageHandler") PosMessageHandlerConfiguration posMessageHandler)
            throws ServiceException;
}
