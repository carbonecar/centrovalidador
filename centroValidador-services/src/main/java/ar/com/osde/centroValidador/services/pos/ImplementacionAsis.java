package ar.com.osde.centroValidador.services.pos;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

@WebService(name = "ImplementacionAsis", targetNamespace = "http://osde.com.ar/services/filter/ImplementacionAsis")
public interface ImplementacionAsis extends Service {

    @WebMethod
    public boolean isImplementada(@WebParam(name = "filial") Integer filial) throws ServiceException;

    @WebMethod
    public void iniciarRollback(@WebParam(name = "codigoFilial", header = false) String codigoFilial) throws ServiceException;

    @WebMethod
    public int crearMensajesMigracion(@WebParam(name = "codigoFilial", header = false) String codigoFilial) throws ServiceException;
    
    @WebMethod
    public int elminarMensajesMigracion(@WebParam(name="codigoFilial",header=false) String codigoFilial)throws ServiceException;
}
