package ar.com.osde.centroValidador.services.pos;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ar.com.osde.framework.services.Service;

@WebService
public interface PosConsumerService extends Service {
	
	@WebMethod
	public boolean isStarted();
	
	@WebMethod
	public boolean switchConsumer();
	
}
