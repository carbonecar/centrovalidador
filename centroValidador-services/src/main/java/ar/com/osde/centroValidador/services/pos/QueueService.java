package ar.com.osde.centroValidador.services.pos;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

@WebService(name = "QueueService", targetNamespace = "http://osde.com.ar/services/queue/QueueService")
public interface QueueService extends Service {

    @WebMethod
    public List<String> getAllQueueNames() throws ServiceException;
   
    @WebMethod
    public List<String> getAllConnectionFactories() throws ServiceException;
}
