package ar.com.osde.centroValidador.services.pos;

import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ar.com.osde.entities.PosMessageHandlerConfiguration;
import ar.com.osde.entities.filter.FilterConfiguration;
import ar.com.osde.entities.filter.strategy.FilterStrategyConfiguration;
import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

@WebService(name = "FilterService", targetNamespace = "http://osde.com.ar/services/filter/FilterService")
public interface FilterService extends Service {

    @WebMethod
    public List<FilterConfiguration> createFilter(@WebParam(name = "expression") String expression) throws ServiceException;

    @WebMethod
    public void updateFilter(@WebParam(name = "filterConfiguration") FilterConfiguration filterConfiguration) throws ServiceException;

    @WebMethod
    public void updateFilterOnMemory(@WebParam(name = "filterConfiguration") FilterConfiguration filterConfiguration) throws ServiceException;

    public void updateFilterStrategy(@WebParam(name = "filterStrategyConfiguration") FilterStrategyConfiguration filterStrategyConfiguration) throws ServiceException;

    public FilterConfiguration getParentFilterForFilter(@WebParam(name = "filterId") Long filterId);

    @WebMethod
    public FilterConfiguration getFilter(@WebParam(name = "id") Long id) throws ServiceException;

    @WebMethod
    FilterStrategyConfiguration getFilterStrategy(@WebParam(name = "id") Long idFilter) throws ServiceException;

    /**
     * Devuevle todas las propiedades por las cuales se puede filtrar
     *
     * @return
     */
    @WebMethod
    public List<String> getPropertiesToFilter();


    @WebMethod
    public List<String> getAvailableOperators();

}
