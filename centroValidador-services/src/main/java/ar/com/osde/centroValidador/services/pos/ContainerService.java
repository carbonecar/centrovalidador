package ar.com.osde.centroValidador.services.pos;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ar.com.osde.entities.ContainerConfiguration;
import ar.com.osde.entities.QueueMessage;
import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

@WebService(name = "ContainerService", targetNamespace = "http://osde.com.ar/services/container/ContainerService")
public interface ContainerService extends Service {

    @WebMethod
    ContainerConfiguration getContainer(@WebParam(name = "id") Long id) throws ServiceException;

    @WebMethod
    void saveContainer(@WebParam(name = "container") ContainerConfiguration container) throws ServiceException;

    @WebMethod
    void updateContainer(@WebParam(name = "container") ContainerConfiguration container) throws ServiceException;

    @WebMethod
    void updateContainerOnMemory(@WebParam(name = "idContainer") ContainerConfiguration container) throws ServiceException;

    @WebMethod
    List<ContainerConfiguration> getAllContainers() throws ServiceException;

    @WebMethod
    void deleteContainer(@WebParam(name = "id") Long id) throws ServiceException;

    @WebMethod
    void startContainer(@WebParam(name = "id") Long id) throws ServiceException;

    @WebMethod
    void startContainersWithGroupName(@WebParam(name = "groupName") String groupName) throws ServiceException;

    @WebMethod
    void stopContainersWithGroupName(@WebParam(name = "groupName") String groupName) throws ServiceException;

    @WebMethod
    void stopContainer(@WebParam(name = "id") Long id) throws ServiceException;

    /**
     * Recarga la configuracion de todos los containers. Ya sea que esten en memoria ejecutandose o en la base de datos
     */
    @WebMethod
    void reloadAllContainers() throws ServiceException;

    /**
     * Recarga el container indicado. Si no se encuentra en la base de datos lo quita de ejecucion.
     */
    @WebMethod
    void reloadContainer(@WebParam(name = "containersId") List<Long> containersId) throws ServiceException;

    /**
     * Realiza un deep clone del container
     */
    @WebMethod
    void cloneContainer(@WebParam(name = "idToClone") Long idToClone) throws ServiceException;

    @WebMethod
    List<QueueMessage> getMessagesErrorHandlerQueue(@WebParam(name = "idContainer") Long idContainer) throws ServiceException;

    @WebMethod
    QueueMessage removeMessageFromErrorHandlerQueue(@WebParam(name = "idContainer") Long idContainer, @WebParam(name = "idMessage") String idMessage) throws ServiceException;

    @WebMethod
    ContainerConfiguration getParentContainer(@WebParam(name = "idHandler") long idHandler);
}
