package ar.com.osde.centroValidador.services.pos;

import ar.com.osde.entities.CodigoExceptuable;
import ar.com.osde.framework.services.Service;
import ar.com.osde.framework.services.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * Maneja los codigos exceputables. (ABM)
 * @author MT27789605
 *
 */
@WebService(name = "CodigoExceptuableService", targetNamespace = "http://osde.com.ar/services/queue/CodigoExceptuableService")
public interface CodigoExceptuableService extends Service {

    /**
     * Lista todos los codigos que se pueden exceputar
     * @return
     * @throws ServiceException
     */
    @WebMethod
    List<CodigoExceptuable> getAllCodigos() throws ServiceException;

    /**
     * Guarda un nuevo codigo
     * @param codigoExceptuable
     * @throws ServiceException
     */
    @WebMethod
    void saveNew(@WebParam(name = "codigoExceptuable") CodigoExceptuable codigoExceptuable) throws ServiceException;

    /**
     * Actualiza el existente
     * @param codigoExceptuable
     * @throws ServiceException
     */
    @WebMethod
    void update(@WebParam(name = "codigoExceptuable") CodigoExceptuable codigoExceptuable) throws ServiceException;

    /**
     * Elimina el existente
     * @param codigoExceptuable
     * @throws ServiceException
     */
    @WebMethod
    void delete(@WebParam(name = "codigoExceptuable") CodigoExceptuable codigoExceptuable) throws ServiceException;

}
